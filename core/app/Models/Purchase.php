<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mail;
/**
 * Purchase	 Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class Purchase extends Model{
	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_purchase';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	
	protected $fillable = ['inoice_no','status','amount','currency_code','country_id','city','province','postcode','street_addresss_1','street_addresss_2','first_name','last_name','phone','email','shipping_method','shipping_amount','coupon_code','coupon_value','created_by'];

	public function getItems()
	{
		return $this->hasMany('App\Models\PurchaseItem', 'purchase_id', 'id');
	}
	public function getPaypal()
	{
		return $this->hasOne('App\Models\PaypalTransaction', 'INVNUM', 'inoice_no');
	}
	public function getUser()
	{
		return $this->belongsTo(\UserManage\Models\User::class, 'created_by');
	}

	public function sendRecieptEmails()
	{
			$thisUser = $this->getUser;
			$oId = $this->inoice_no;
			if($this->getItems->where('type', 1)->count() > 0){ // products
				Mail::send('emails.reciepts.product-receipt',['order' => $this], function($message) use($thisUser, $oId) {
					$message->to($thisUser->email, '')->subject('Products Purchasing Confirmation Email | sweetdelightscakery.com | #'.$oId);
				});
			}
			if ($this->getItems->where('type', 2)->count() > 0) {// tutorials
				Mail::send('emails.reciepts.tutorial-receipt',['order' => $this], function($message) use($thisUser, $oId) {
					$message->to($thisUser->email, '')->subject('Tutorials Purchasing Confirmation Email | sweetdelightscakery.com | #'.$oId);
				});
			}
		
	}
	


	

}
