<?php

namespace App\Http\Controllers;


use App\Http\Requests\ReCaptchataTestFormRequest;
use CakeTypeManager\Models\CakeType;
use Illuminate\Http\Request;

use File;
use NewsManage\Models\News;
use PrivateClassManage\Models\PrivateClassCategory;
use PrivateClassManage\Models\PrivateReg;
use GalleryManage\Models\Gallery;
use GalleryManage\Models\GalleryCategory;
use GalleryManage\Models\GalleryImage;
use RecipesManager\Models\Recipes;
use PublicationManager\Models\Publication;
use TastingManage\Models\Tasting;
use TastingManage\Models\TastingPeopleType;
use SliderManager\Models\Slider;
use App\Models\ShoppingCart;
use App\Models\BillingDetail;
use CountryManager\Models\Country;
use App\Models\LastShipping;
use App\Models\ShippingMethod;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use App\Models\PaypalTransaction;
use App\Models\SortType;
use App\Models\Rating;


use Response;
use Input;
use DB;
use Session;
use Mail;
use Cart;
use PayPal;
use Usps;

use TestimonialsManager\Models\Testimonials;
use UserManage\Models\User;
use UserRoles\Models\UserRole;
use App\Http\Requests\RegisterRequest;
use QuoteManager\Models\Quote;
use BlogManage\Models\Blog;
use BlogManage\Models\BlogImage;
use BlogManage\Models\Comment;
use ProductCreationManage\Models\ProductCreationCategory;
use ProductCreationManage\Models\ProductCreation;
use ProductCreationManage\Models\ProductCreationImage;
use ProductCreationManage\Models\ProductHasCategory;

use Sentinel;
use PDF;
use View;
use Permissions\Models\Permission;

use Hash;

class PrivetClassController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Web Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		//$this->middleware('guest');
	}

	/**
	* Show the application welcome screen to the user.
	*
	* @return Response
	*/
	public function beginner(Request $request){
		$type = $_GET['skill'];
		$data = DB::table('sa_privateclass_category');
		 // var_dump($data);die;
		if($type == 'beginner'){
			$data = $data -> where('skill_level', '=', 1) ->where('status', '=',1);
		}

		$data = $data->get();
		return $data;
	}
public function intermediate(Request $request){
	$type = $_GET['skill'];
	$data = DB::table('sa_privateclass_category');
	//  var_dump($data);die;
	if($type == 'intermediate'){
		$data = $data -> where('skill_level', '=', 2) ->where('status', '=',1);
	}

	$data = $data->get();
	return $data;

}

public function advance(Request $request){
	$type = $_GET['skill'];
	$data = DB::table('sa_privateclass_category');
	//  var_dump($data);die;
	if($type == 'advance'){
		$data = $data -> where('skill_level', '=', 3) ->where('status', '=',1);
	}

	$data = $data->get();
	return $data;

}


public function kids(Request $request){
	$type = $_GET['skill'];
	$data = DB::table('sa_privateclass_category');
	//  var_dump($data);die;
	if($type == 'kids'){
		$data = $data -> where('skill_level', '=', 4) ->where('status', '=',1);
	}

	$data = $data->get();
	return $data;

}
public function filterall(Request $request){
$type = $_GET['skill'];
$data = DB::table('sa_privateclass_category');
//  var_dump($data);die;

	$data = $data ->where('status', '=',1);
$data = $data->get();
return $data;

}
}
