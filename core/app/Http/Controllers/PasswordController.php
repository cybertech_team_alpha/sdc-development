<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Mail;
use Sentinel;
use Carbon\Carbon;
use App\Http\Requests;
use UserManage\Models\User;
use App\Models\PasswordReset;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Http\Requests\NewPasswordRequest;

class PasswordController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function putForgetPassword(Request $request)
    {

        if($request->has('email')){

            $dbUser = User::where('email', $request->input('email'))->first();
            if($dbUser){
                $PasswordReset = PasswordReset::create(['email' => $request->input('email'), 'token' => md5(uniqid(mt_rand(), true)), 'created_at' => Carbon::now()]);

                $user = $request->input('email');
                $url = route('user.reset.link', $PasswordReset->token);

                Mail::send('emails.password-reset-email', ['url' => $url, 'user' => $dbUser], function($message) use ($user) {
                    $message->to($user, '')->subject('Password Reset Request for Sweet Delights Cakery Account');
                });

                return redirect()->back()->with([ 'success' => true,
                    'success.message'=> 'Password Reset Link Emailed!',
                    'success.title' => 'Success!']);
            }
            else{
                return redirect()->back()->with([ 'error' => true,
                    'error.message'=> 'Invalid Email Address!',
                    'error.title' => 'Try Again!']);
            }
        }
        else{
            echo 'Invalid Form Submit!';
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getResetPassword($token)
    {
        $PasswordReset = PasswordReset::where('token', $token)->first();

        if(!$PasswordReset){
            return redirect()->route('user.login')->with([ 'error' => true,
                'error.message'=> 'Invalid Request!',
                'error.title' => 'Try Again!']);
        }
        else{
            if($PasswordReset['created_at'] <= Carbon::now()->subHour(1)){
                return redirect()->route('user.login')->with([ 'error' => true,
                    'error.message'=> 'Password Reset time exprired!',
                    'error.title' => 'Try Again!']);
            }
            else{
                return view('layouts.front.password-new')
                ->with(['email' => $PasswordReset['email']])
                ->with(['token' => $token]);
            }
        }
    }

    public function passwordReset($code, Request $request)
    {
        $PasswordReset = PasswordReset::where('token', $code)->first();
        if($PasswordReset){
            if($PasswordReset['created_at'] <= Carbon::now()->subHour(1)){
                return redirect()->route('user.login')->with([ 'error' => true,
                    'error.message'=> 'Password Reset time exprired!',
                    'error.title' => 'Try Again!']);
            }
            $user = User::where('email', $PasswordReset->email)->first();

            if(!$user){
                return redirect()->route('user.login')->with([ 'error' => true,
                    'error.message'=> 'Invalid Email Address',
                    'error.title' => 'Try Again!']);
            }
            else{
                Sentinel::update($user, array('password' => $request->input('password')));
                PasswordReset::where('email', $PasswordReset->email)->delete();
                return redirect()->route('user.login')->with([ 'success' => true,
                    'success.message'=> 'Password Reset Success!',
                    'success.title' => 'Success']);
            }
        }
        else{
            echo 'Invalid Request!';
        }
        
    }	
}