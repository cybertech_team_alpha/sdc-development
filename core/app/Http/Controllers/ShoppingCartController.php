<?php

namespace App\Http\Controllers;


use App\Http\Requests\ReCaptchataTestFormRequest;
use Permissions\Models\Permission;
use Illuminate\Http\Request;

use File;
use Response;
use Input;
use DB;
use Session;
use Mail;
use Cart;
use PayPal;
use Usps;
use Carbon\Carbon;

use RecipesManager\Models\Recipes;
use App\Models\ShoppingCart;
use App\Models\BillingDetail;
use CountryManager\Models\Country;
use App\Models\LastShipping;
use App\Models\ShippingMethod;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use App\Models\PaypalTransaction;
use UserManage\Models\User;
use UserRoles\Models\UserRole;
use ProductCreationManage\Models\ProductCreationCategory;
use ProductCreationManage\Models\ProductCreation;
use ProductCreationManage\Models\ProductCreationImage;
use ProductCreationManage\Models\ProductHasCategory;
use CakeTypeManager\Models\CakeType;
use CouponManager\Models\CouponItem;
use ConfigManage\Models\Config;

use Sentinel;
use PDF;
use View;
use Hash;

class ShoppingCartController extends Controller {

	/*
		|--------------------------------------------------------------------------
		| ShoppingCart Controller
		|--------------------------------------------------------------------------
		|
		| This controller renders the "marketing page" for the application and
		| is configured to only allow guests. Like most of the other sample
		| controllers, you are free to modify or remove it as you desire.
		|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
    public function test()
    {
        Date::setLocale('hr');
        return Date::now()->format('F j, Y');
        // return $rate = Usps::domesticRate();
       //  return $rate = Usps::domesticRate('FIRST CLASS','LETTER',99501,72201,0,3.5,'REGULAR');
       //  return json_decode($rate);
       // return response()->json(
       //      Usps::domesticRate()
       //  );
    }




   public function checkout1()
    {
        $country=Country::orderBy('name','asc')->get();
        $user=Sentinel::getUser();
        $billing_details=BillingDetail::where('created_by',$user->id)->with(['getCountry'])->get();
        //var_dump($billing_details);die;

        return view('front.checkout1')->with(['country'=>$country,'billing_details'=>$billing_details]);
    }
   public function checkout1_submit()
    {
         $user=Sentinel::getUser();

         $billing_detail=0;
        if (Input::get('submit_type')=='exsist_address') {
            $billing_detail=BillingDetail::find(Input::get('billing_id'));
        }else if (Input::get('submit_type')=='new_address') {
            $country=Input::get('country');
            $city=Input::get('city');
            $province=Input::get('province');
            $postcode=Input::get('postcode');
            $address1=Input::get('address1');
            $address2=Input::get('address2');
            $first_name=Input::get('first_name');
            $last_name=Input::get('last_name');
            $phone=Input::get('phone');
            $email=Input::get('email');

            $billing_detail=BillingDetail::Create([
                'country_id'=>$country,
                'city'=>$city,
                'province'=>$province,
                'postcode'=>$postcode,
                'street_addresss_1'=>$address1,
                'street_addresss_2'=>$address2,
                'first_name'=>$first_name,
                'last_name'=>$last_name,
                'phone'=>$phone,
                'email'=>$email,
                'is_primary'=>1,
                'created_by'=>$user->id
            ]);
        }

        $country=$billing_detail->country_id;
        $city=$billing_detail->city;
        $province=$billing_detail->province;
        $postcode=$billing_detail->postcode;
        $address1=$billing_detail->street_addresss_1;
        $address2=$billing_detail->street_addresss_2;
        $first_name=$billing_detail->first_name;
        $last_name=$billing_detail->last_name;
        $phone=$billing_detail->phone;
        $email=$billing_detail->email;

        $lastShipping=LastShipping::where('created_by',$user->id)->first();
        if ($lastShipping) {
            $lastShipping->country_id=$country;
            $lastShipping->city=$city;
            $lastShipping->province=$province;
            $lastShipping->postcode=$postcode;
            $lastShipping->street_addresss_1=$address1;
            $lastShipping->street_addresss_2=$address2;
            $lastShipping->first_name=$first_name;
            $lastShipping->last_name=$last_name;
            $lastShipping->phone=$phone;
            $lastShipping->email=$email;

            $lastShipping->save();
        }else{
            LastShipping::Create([
                'country_id'=>$country,
                'city'=>$city,
                'province'=>$province,
                'postcode'=>$postcode,
                'street_addresss_1'=>$address1,
                'street_addresss_2'=>$address2,
                'first_name'=>$first_name,
                'last_name'=>$last_name,
                'phone'=>$phone,
                'email'=>$email,
                'created_by'=>$user->id
            ]);

        }

        return redirect('checkout2');

    }

   public function checkout2()
    {

        $user=Sentinel::getUser();
        $lastShipping=LastShipping::where('created_by',$user->id)->first();
        $shippingMethod=ShippingMethod::get();
        return view('front.checkout2')->with(['shippingMethod'=>$shippingMethod,'lastShipping'=>$lastShipping]);
    }
    public function checkout2_submit()
    {
       $shipping_method=Input::get('shipping_method');

       $user=Sentinel::getUser();
        $lastShipping=LastShipping::where('created_by',$user->id)->first();
        if ($lastShipping) {
            $lastShipping->shipping_method=$shipping_method;

            $lastShipping->save();
            return redirect('checkout3');
        }

    }

     public function checkout3()
    {
        return view('front.checkout3');
    }

    public function checkout3_submit()
    {
      $payment_method=Input::get('payment_method');
      if ($payment_method=='paypal') {
          return redirect('checkout4');
      }else{
        return 'SORRY THIS PAYMENT TYPE IS NOT VALIED , PLEASE SELECT PAYPAL';
      }

    }

     public function checkout4()
     {
        $user=Sentinel::getUser();
        $lastShipping=LastShipping::where('created_by',$user->id)->with(['getShippingMethod','getCountry'])->first();
        $country=Country::find($lastShipping->country_id);
        $shipping_charges=0;
        if ($country->usps_domestic_rate) {
            // return $this->shipping_charge_calculate($lastShipping->postcode);
          $shipping_charges=$this->shipping_charge_calculate($lastShipping->postcode);
        }else{
            $shipping_charges=$country->price;
        }

        $shipping_condition = new \Darryldecode\Cart\CartCondition(array(
            'name' => 'shipping',
            'type' => 'shipping',
            'target' => 'subtotal',
            'value' => '+'.$shipping_charges,
        ));

        Cart::condition($shipping_condition);
        $coupon_condition = Cart::getCondition('coupon');
        $cartCollection = Cart::getContent();
        $cartTotalQuantity = Cart::getTotalQuantity();
        $subTotal = Cart::getSubTotal();
        $total = Cart::getTotal();
        $cartArray=$cartCollection->toArray();
        $shoppingCartArray_product=array();
        $shoppingCartArray_tutorial=array();
        foreach ($cartArray as $key => $value) {
        $cart_id=explode('-', $value['id']);
        $type=$cart_id[0];
        $id=$cart_id[1];

            if ($type=='P') {
                 /*THIS IS DEFAULT PRODUCT */
                $selected_product=ProductCreation::find($id);
                $path = url(). '/core/storage/'. $selected_product->cover_path . '/' . $selected_product->cover_file;
                if ($selected_product->on_sale=='on' & date('Y-m-d')<=$selected_product->end_date & date('Y-m-d')>=$selected_product->start_date) {
                      array_push($shoppingCartArray_product,
                         array(
                            'id'=>$id,
                            'type'=>1,
                            'cart_id'=>$value['id'],
                            'name'=>$value['name'],
                            'price'=>$selected_product->sale_price,
                            'quantity'=>$value['quantity'],
                            'details'=>$selected_product,
                            'image'=>url($path)
                            )
                    );
                } else {
                      array_push($shoppingCartArray_product,
                         array(
                            'id'=>$id,
                            'type'=>1,
                            'cart_id'=>$value['id'],
                            'name'=>$value['name'],
                            'price'=>$selected_product->price,
                            'quantity'=>$value['quantity'],
                            'details'=>$selected_product,
                            'image'=>url($path)
                            )
                    );
                }



            }else if ($type=='T') {
                  /*THIS IS TOUTORIALS*/
                $selected_product=Recipes::find($id);
                $path = url(). '/core/storage/'. $selected_product->cover_path . '/' . $selected_product->cover_file;

                array_push($shoppingCartArray_tutorial,
                     array(
                        'id'=>$id,
                        'type'=>2,
                        'cart_id'=>$value['id'],
                        'name'=>$value['name'],
                        'price'=>$selected_product->price,
                        'quantity'=>$value['quantity'],
                        'image'=>url($path),
                        'details'=>$selected_product,
                        )
                );
            }
         }
         return view('front.checkout4')
         ->with([
            'user'=>$user,
            'lastShipping'=>$lastShipping,
            'cartCollection'=>$cartCollection,
            'shoppingCartArray_product'=>$shoppingCartArray_product,
            'shoppingCartArray_tutorial'=>$shoppingCartArray_tutorial,
            'cartTotalQuantity'=>$cartTotalQuantity,
            'subTotal'=>$subTotal,
            'total'=>$total,
            'shipping_charges'=>$shipping_charges,
            'coupon_condition'=>$coupon_condition,
        ]);
     }
     public function checkout4_submit()
     {
        $user=Sentinel::getUser();
        $invoice_id=date('ymdHis').$user->id;
        $total = Cart::getTotal();
        $subTotal = Cart::getSubTotal();
        $coupon_condition = Cart::getCondition('coupon');
        $shipping_condition = Cart::getCondition('shipping');
        $lastShipping=LastShipping::where('created_by',$user->id)->with(['getShippingMethod','getCountry'])->first();
        $coupon_amount=0;
        $coupon_code='';
        $shipping_amount=0;
        if (!empty($coupon_condition)) {
            $coupon_code=$coupon_condition->getAttributes()['coupon_code'];
            $coupon_amount=abs($coupon_condition->getValue())  ;
        }
        if (!empty($shipping_condition)) {
            $shipping_amount=$shipping_condition->getValue();
        }

        if ($total<=0) {
             if (!empty($coupon_condition)) {
                $result= $this->purchase($invoice_id,$total,'USD',$lastShipping,$user,$shipping_amount,$coupon_code,$coupon_amount);
                if ($result) {
                    return $this->purchase_success($invoice_id);
                }
                    // return $coupon_condition->getValue() ;
             }else{
                return 'TRY AGAIN !';
             }
        }else{
            $provider = PayPal::setProvider('express_checkout');
            $shoppingCartArray=$this->shopping_cart_creation_for_checkout($invoice_id);
            $response = $provider->setExpressCheckout($shoppingCartArray, true);
            return redirect($response['paypal_link']);
             // print_r($response);
        }


     }
      public function paypal_success()
    {

        $user=Sentinel::getUser();
        $provider = PayPal::setProvider('express_checkout');
        $token=Input::get('token');
        $PayerID=Input::get('PayerID');
        $response = $provider->getExpressCheckoutDetails($token);
        $invoice_id=$response['INVNUM'];
        $invoice_count=Purchase::where('inoice_no',$invoice_id)->get();
        $coupon_code='';
        $coupon_amount=0;
        if (!empty($invoice_count)) {
                if($response['BILLINGAGREEMENTACCEPTEDSTATUS']==1 & $response['ACK']=='Success'){
                    $amount=$response['AMT'];
                    $shipping_amount=$response['SHIPPINGAMT'];
                    $currency_code=$response['CURRENCYCODE'];
                    $timestamp=$response['TIMESTAMP'];
                    $lastShipping=LastShipping::where('created_by',$user->id)->first();
                    $coupon_amount=0;
                    $coupon_code='';
                    $coupon_condition = Cart::getCondition('coupon');
                    if (!empty($coupon_condition)) {
                        $coupon_code=$coupon_condition->getAttributes()['coupon_code'];
                        $coupon_amount=abs($coupon_condition->getValue())  ;
                    }
                    if ( $lastShipping) {
                         $result=$this->purchase($invoice_id,$amount,'USD',$lastShipping,$user,$shipping_amount,$coupon_code,$coupon_amount);
                         if ($result) {
                            $paypalTransaction=PaypalTransaction::Create([
                                'TOKEN'=>$token,
                                'TIMESTAMP'=>$response['TIMESTAMP'],
                                'EMAIL'=>$response['EMAIL'],
                                'PAYERID'=>$response['PAYERID'],
                                'FIRSTNAME'=>$response['FIRSTNAME'],
                                'LASTNAME'=>$response['LASTNAME'],
                                'COUNTRYCODE'=>$response['COUNTRYCODE'],
                                'CURRENCYCODE'=>$response['CURRENCYCODE'],
                                'AMT'=>$response['AMT'],
                                'DESCRIPTION'=>$response['DESC'],
                                'INVNUM'=>$response['INVNUM'],
                                'PAYMENTREQUESTINFO_0_ERRORCODE'=>$response['PAYMENTREQUESTINFO_0_ERRORCODE'],
                                'SHIPPINGAMT'=>$response['SHIPPINGAMT'],
                                'TAXAMT'=>$response['TAXAMT'],
                                'CORRELATIONID'=>$response['CORRELATIONID'],
                                'ACK'=>$response['ACK'],
                                'VERSION'=>$response['VERSION'],


                            ]);

                            if ($paypalTransaction) {
                               $shoppingCartArray=$this->shopping_cart_creation_for_checkout($invoice_id);
                               $response = $provider->doExpressCheckoutPayment($shoppingCartArray, $token, $PayerID);
                               $paypalTransaction->PAYMENTINFO_0_SECUREMERCHANTACCOUNTID=$response['PAYMENTINFO_0_SECUREMERCHANTACCOUNTID'];
                               $paypalTransaction->PAYMENTINFO_0_PAYMENTSTATUS=$response['PAYMENTINFO_0_SECUREMERCHANTACCOUNTID'];
                               $paypalTransaction->PAYMENTINFO_0_TRANSACTIONID=$response['PAYMENTINFO_0_TRANSACTIONID'];
                               $paypalTransaction->save();
                               return $this->purchase_success($invoice_id);


                            }
                         }

                    }


            }else{

            }
        }else{
            return 'DUPLICATE REFRESH';
        }



    }
    public function purchase($invoice_id,$amount,$currency_code,$lastShipping,$user,$shipping_amount,$coupon_code,$coupon_amount)
    {


         $shoppingCartArray=$this->shopping_cart_creation_for_checkout($invoice_id);
         $purchase=Purchase::Create([
                            'inoice_no'=>$invoice_id,
                            'amount'=>$amount,
                            'currency_code'=>$currency_code,
                            'country_id'=>$lastShipping->country_id,
                            'city'=>$lastShipping->city,
                            'province'=>$lastShipping->province,
                            'postcode'=>$lastShipping->postcode,
                            'street_addresss_1'=>$lastShipping->street_addresss_1,
                            'street_addresss_2'=>$lastShipping->street_addresss_2,
                            'first_name'=>$lastShipping->first_name,
                            'last_name'=>$lastShipping->last_name,
                            'phone'=>$lastShipping->phone,
                            'email'=>$lastShipping->email,
                            'shipping_method'=>$lastShipping->shipping_method,
                            'shipping_amount'=>$shipping_amount,
                            'coupon_code'=>$coupon_code,
                            'coupon_value'=>$coupon_amount,
                            'created_by'=>$user->id,
                            'status'=>1
                        ]);

         if ($purchase) {
                $shoppingCartArray=$this->shopping_cart_creation_for_checkout($invoice_id);
                foreach ($shoppingCartArray['items_Id'] as $key => $item) {
                    PurchaseItem::Create([
                        'purchase_id'=>$purchase->id,
                        'item_id'=>$item['item_id'],
                        'name'=>$item['name'],
                        'unit_price'=>$item['price'],
                        'qty'=>$item['qty'],
                        'type'=>$item['type']
                    ]);

                }


        }
        return 1;

    }
    public function purchase_success($invoice_id)
    {
        $user=Sentinel::getUser();
        $coupon_condition = Cart::getCondition('coupon');
        $shipping_condition = Cart::getCondition('shipping');
        if (!empty($coupon_condition)) {
            $coupon_code=$coupon_condition->getAttributes()['coupon_code'];
            CouponItem::where('code', $coupon_code)
            ->update(['status' => 1]);
        }
        $shopping_carts=ShoppingCart::where('created_by',$user->id)->get();
        foreach ($shopping_carts as $key => $shopping_cart) {
           $now = Carbon::now()->toDateTimeString();
           $shopping_cart->deleted_at = $now;
           $shopping_cart->save();
        }
        Cart::clear();
        Cart::clearCartConditions();
         $invoice = Purchase::where('inoice_no',$invoice_id)->first();
         if($invoice){
             try {
                 $invoice->sendRecieptEmails();
             } catch (\Exception  $e) {
                 //throw $th;
             }
         }
        return redirect('paypal/payment/recipt/'.$invoice_id);
    }
    public function shopping_cart_creation_for_checkout($invoice_id)
    {

        $cartCollection = Cart::getContent();
        $subTotal = Cart::getSubTotal();
        $total = Cart::getTotal();
        $shipping_condition = Cart::getCondition('shipping');;
        $coupon_condition = Cart::getCondition('coupon');;
        $coupon_amount=0;
        if (!empty($coupon_condition)) {
            $coupon_amount=abs($coupon_condition->getValue())  ;
        }
        $shipping_cost = $shipping_condition->getValue()  ;
        $cartArray=$cartCollection->toArray();
        $shoppingCartArray=[
            'items'=>array(),
            'items_Id'=>array(),
            'invoice_id'=>$invoice_id,
            'invoice_description'=>'Order # '.$invoice_id.' Invoice',
            'return_url'=>url('/paypal/payment/success'),
            'cancel_url'=>url('/paypal/payment/reject'),
            'subtotal'=>$subTotal-$coupon_amount,
            'total'=>$total,
            'shipping'=>(float)$shipping_cost

        ];

       foreach ($cartArray as $key => $value) {

            $cart_id=explode('-',$value['id']);
            $type=$cart_id[0];
            $id=$cart_id[1];
            if ($cart_id[0]=='P') {
                $type=1;
            }else if ($cart_id[0]=='T') {
               $type=2;
            }


           // $selected_product=ProductCreation::with(['getImages'])->find($value['id']);
            array_push($shoppingCartArray['items'],
                 array(

                    'name'=>$value['name'],
                    'price'=>$value['price'],
                    'qty'=>$value['quantity']

                    )
            );
            array_push($shoppingCartArray['items_Id'],
                 array(
                    'item_id'=>$id,
                    'name'=>$value['name'],
                    'price'=>$value['price'],
                    'qty'=>$value['quantity'],
                    'type'=>$type

                    )
            );

       }
       if (!empty($coupon_condition)) {
          array_push($shoppingCartArray['items'],
                 array(
                    'name'=>'coupon',
                    'price'=>(float)$coupon_condition->getValue(),
                    'qty'=>1

                    )
            );
       }



       return $shoppingCartArray;

    } 
    public function paypal_recipt($invoice)
    {

        $user=Sentinel::getUser();
       $purchase=Purchase::with(['getItems','getPaypal'])
       ->where('inoice_no',$invoice)
       ->where('created_by',$user->id)
       ->first();

       if($purchase){
           return view('front.paypal_recipt')->with(['purchase'=>$purchase]);
       }

       return redirect()->back();
    }
    public function paypal_recipt_print($invoice)
    {

        $user=Sentinel::getUser();
       $purchase=Purchase::with(['getItems','getPaypal'])
       ->where('inoice_no',$invoice)
       ->where('created_by',$user->id)
       ->first();

       if($purchase){
           return view('front.paypal_recipt_print')
           ->with(['purchase'=>$purchase]);
       }

       return redirect()->back();
    }
    public function paypal_reject()
    {
        // $provider = PayPal::setProvider('express_checkout');
        // $token=Input::get('token');
        // return $response = $provider->getExpressCheckoutDetails($token);
        // print_r($response);
        return redirect('my-cart');
    }

    public function cart_loading_from_db()
    {
        if ($user = Sentinel::check()) {
            Cart::clear();
            $exsistProductDB=ShoppingCart::where('created_by',$user->id)->get();
               foreach ($exsistProductDB as $key => $singleProduct) {
                    $product_array=array();
                   if ($singleProduct->type==1) {
                       /*THIS IS DEFAULT PRODUCT */
                        $selected_product=ProductCreation::find($singleProduct->product_id);
                        $product_array=array(
                                                'product_id'=>$selected_product->id,
                                                'product_name'=>$selected_product->product_name,
                                                'price'=>$selected_product->price,
                                                'qty'=>$singleProduct->qty,
                                                'type'=>1,
                                                'cart_id'=>'P-'.$selected_product->id

                                            );

                   }else if ($singleProduct->type==2) {
                       /*THIS IS TOUTORIALS*/
                        $selected_product=Recipes::find($singleProduct->product_id);
                        $product_array=array(
                                                'product_id'=>$selected_product->recipe_id,
                                                'product_name'=>$selected_product->name,
                                                'price'=>$selected_product->price,
                                                'qty'=>$singleProduct->qty,
                                                'type'=>2,
                                                'cart_id'=>'T-'.$selected_product->recipe_id

                                            );

                   }
                   if (!empty($product_array)) {
                       Cart::add(array(
                            'id' => $product_array['cart_id'],
                            'name' => $product_array['product_name'],
                            'price' => $product_array['price'],
                            'quantity' =>$product_array['qty'],
                            'attributes' => array(
                                'cart_status' => 'DB'
                            )
                        ));
                   }



               }

       }
    }

	   public function myCart()
    {

        $this->cart_loading_from_db();
        $cartCollection = Cart::getContent();
        $cartTotalQuantity = Cart::getTotalQuantity();
        $subTotal = Cart::getSubTotal();
        $total = Cart::getTotal();
        $coupon_condition = Cart::getCondition('coupon');
        $shipping_condition = Cart::getCondition('shipping');
        $cartArray=$cartCollection->toArray();
        $shoppingCartArray_product=array();
        $shoppingCartArray_tutorial=array();
        foreach ($cartArray as $key => $value) {
        $cart_id=explode('-', $value['id']);
        $type=$cart_id[0];
        $id=$cart_id[1];

        if ($type=='P') {
             /*THIS IS DEFAULT PRODUCT */
            $selected_product=ProductCreation::with(['getImages'])->find($id);
            $path='/core/storage/'.$selected_product->getImages[0]->path.'/'.$selected_product->getImages[0]->filename;

            array_push($shoppingCartArray_product,
                 array(
                    'id'=>$id,
                    'type'=>1,
                    'cart_id'=>$value['id'],
                    'name'=>$value['name'],
                    'price'=>$selected_product->price,
                    'quantity'=>$value['quantity'],
                    'details'=>$selected_product,
                    'image'=>url($path)
                    )
            );

        }else if ($type=='T') {
              /*THIS IS TOUTORIALS*/
            $selected_product=Recipes::with(['getImages'])->find($id);
            $path='/core/storage/'.$selected_product->getImages[0]->cover_path.'/'.$selected_product->getImages[0]->cover_file;

            array_push($shoppingCartArray_tutorial,
                 array(
                    'id'=>$id,
                    'type'=>2,
                    'cart_id'=>$value['id'],
                    'name'=>$value['name'],
                    'price'=>$selected_product->price,
                    'quantity'=>$value['quantity'],
                    'image'=>url($path)
                    )
            );
        }


       }

     return view('front.myCart')
     ->with([
        'cartCollection'=>$cartCollection,
        'shoppingCartArray_product'=>$shoppingCartArray_product,
        'shoppingCartArray_tutorial'=>$shoppingCartArray_tutorial,
        'cartTotalQuantity'=>$cartTotalQuantity,
        'coupon_condition'=>$coupon_condition,
        'shipping_condition'=>$shipping_condition,
        'subTotal'=>$subTotal,
        'total'=>$total
    ]);

    }

    public function cartADD(){
       $product_id= Input::get('product_id');
       $product_type= Input::get('product_type');
       $qty= Input::get('qty');
       $product_array=array();
       if ($product_type==1) {
           /*THIS IS DEFAULT PRODUCT */
            $selected_product=ProductCreation::find($product_id);
            $product_array=array(
                                            'product_id'=>$selected_product->id,
                                            'product_name'=>$selected_product->product_name,
                                            'price'=>$selected_product->price,
                                            'qty'=>$qty,
                                            'type'=>1,
                                            'cart_id'=>'P-'.$selected_product->id

                                        );

       }else if ($product_type==2) {
           /*THIS IS TOUTORIALS*/
            $selected_product=Recipes::find($product_id);
            $product_array=array(
                                            'product_id'=>$selected_product->recipe_id,
                                            'product_name'=>$selected_product->name,
                                            'price'=>$selected_product->price,
                                            'qty'=>$qty,
                                            'type'=>2,
                                            'cart_id'=>'T-'.$selected_product->recipe_id

                                        );

       }


      if (!empty($product_array)) {
           if ($user = Sentinel::check()) {
                 $exsistProduct=ShoppingCart::where('product_id',$product_array['product_id'])
                                            ->where('type',$product_array['type'])
                                            ->where('created_by',$user->id)
                                            ->get();
                if(count($exsistProduct)>0){
                    $newQTY=($exsistProduct[0]->qty)+$product_array['qty'];
                   $result=ShoppingCart::where('product_id', $product_array['product_id'])
                                        ->where('type',$product_array['type'])
                                        ->where('created_by',$user->id)
                                        ->update(['qty' => $newQTY]);
                        return $result;
                }else{
                    ShoppingCart::Create([
                    'product_id'=>$product_array['product_id'],
                    'product_name'=>$product_array['product_name'],
                    'price'=>$product_array['price'],
                    'qty'=>$product_array['qty'],
                    'type'=>$product_array['type'],
                    'created_by'=>$user->id,
                    ]);
                    return 1;
                }

            }else{

                if (count(Cart::get($product_array['cart_id']))>0) {
                    Cart::update($product_array['cart_id'], array(
                          'quantity' => $product_array['qty']
                        ));
                    return 1;
                 }else{

                   Cart::add(array(
                        'id' => $product_array['cart_id'],
                        'name' => $product_array['product_name'],
                        'price' => $product_array['price'],
                        'quantity' =>$product_array['qty'],
                        'attributes' => array(
                            'cart_status' => 'OFFLINE'
                        )
                    ));
                   return 1;


                 }
            }


      }else{
        return 0;
      }



    }
    public function cartREMOVE($id,$type){
        $cart_id=0;
        if ($type==1) {
            $cart_id='P-'.$id;
        }else if ($type==2) {
            $cart_id='T-'.$id;
        }

       Cart::remove($cart_id);
       if ($user = Sentinel::check()) {
             $result=ShoppingCart::where('product_id', $id)
                        ->where('type',$type)
                        ->where('created_by',$user->id)
                        ->Delete();
        }
       return 1;

    }
    public function cartUpdate($id,$qty,$type){
        if ($qty<=0) {
           $this->cartREMOVE($id,$type);
        }else{
            $cart_id=0;
            if ($type==1) {
                $cart_id='P-'.$id;
            }else if ($type==2) {
                $cart_id='T-'.$id;
            }
            Cart::update($cart_id, array(
                  'quantity' => array(
                      'relative' => false,
                      'value' => $qty
                  ))
              );
            if ($user = Sentinel::check()) {
                 $result=ShoppingCart::where('product_id', $id)
                            ->where('created_by',$user->id)
                            ->where('type',$type)
                            ->update(['qty' => $qty]);
            }
        }

        return redirect('my-cart');

    }
    public function shipping_charge_calculate($zip_destination)
    {
        $config=Config::first();
        $cartCollection = Cart::getContent();
        $cartArray=$cartCollection->toArray();
        $shipping_charge=0;
        foreach ($cartArray as $key => $value) {
            $cart_id=explode('-', $value['id']);
            $type=$cart_id[0];
            $id=$cart_id[1];
                if ($type=='P') {
                     /*THIS IS DEFAULT PRODUCT */
                    $product=ProductCreation::with(['getUspsMailType','getUspsServiceType','getUspsSize'])->find($id);
                    $shipping_charge+=$this->get_usps_domestric_rate($product->getUspsServiceType->name,$product->getUspsMailType->name,$config->zip,$zip_destination,$product->pounds,$product->ounces,$product->getUspsSize->name);


                }else if ($type=='T') {
                      /*THIS IS TOUTORIALS*/
                    $selected_product=Recipes::find($id);

                }
        }
        return $shipping_charge;
    }

    public function get_usps_domestric_rate($sevice_type,$mail_type,$zip_origination,$zip_destination,$pounds,$ounces,$size)
    {
         $result = Usps::domesticRate($sevice_type,$mail_type,$zip_origination,$zip_destination,$pounds,$ounces,$size);
          return $result['RateV4Response']['Package']['Postage']['Rate'];
    }


}
