<?php
namespace DeviceManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Device Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Yasith Samarawickrama
 * @version    v1.0.0
 */
class Device extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_device';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'status'];

	public function getSeries()
	{
		return $this->hasMany('SeriesManage\Models\Series', 'device_id', 'id');
	}

	

}
