<?php
/**
 * DEVICE MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'device', 'namespace' => 'DeviceManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'device.add', 'uses' => 'DeviceController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'device.edit', 'uses' => 'DeviceController@editView'
      ]);

      Route::get('list', [
        'as' => 'device.list', 'uses' => 'DeviceController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'device.list', 'uses' => 'DeviceController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'device.add', 'uses' => 'DeviceController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'device.edit', 'uses' => 'DeviceController@edit'
      ]);

      Route::post('status', [
        'as' => 'device.status', 'uses' => 'DeviceController@status'
      ]);

      Route::post('delete', [
        'as' => 'device.delete', 'uses' => 'DeviceController@delete'
      ]);
    });
});