<?php
namespace DeviceManage\Http\Controllers;

use App\Http\Controllers\Controller;
use DeviceManage\Http\Requests\DeviceRequest;
use Illuminate\Http\Request;
use DeviceManage\Models\Device;
use Permissions\Models\Permission;
use Sentinel;
use Response;



class DeviceController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Device Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the Branch add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
	
		return view( 'deviceManage::device.add');
	}

	/**
	 * Add new Branch data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add(DeviceRequest $request)
	{
	
		$count= Device::where('name', '=',$request->get('name' ))->count();
		if ($count==0) {
			Device::Create(array(
				'name' => $request->input('name')
				
				)
			);		

			return redirect('device/add')->with([ 'success' => true,
				'success.message'=> 'Device Created successfully!',
				'success.title' => 'Well Done!']);
		}else{
			
				return redirect('device/add')->with([ 'error' => true,
				'error.message'=> 'Device Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
	}

	/**
	 * View Branch List View
	 *
	 * @return Response
	 */
	public function listView()
	{		
		return view( 'deviceManage::device.list' );
	}

	/**
	 * Device list
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$data= Device::where('status','=',1)->get();			
			$jsonList = array();
			$i=1;
			foreach ($data as $key => $device) {
				
				$dd = array();
				array_push($dd, $i);
				
				if($device->name != ""){
					array_push($dd, $device->name);
				}else{
					array_push($dd, "-");
				}
				
				$permissions = Permission::whereIn('name',['device.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('device/edit/'.$device->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit device"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['device.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="device-delete" data-id="'.$device->id.'" data-toggle="tooltip" data-placement="top" title="Delete device"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate user
	 * @param  Request $request id with status to change
	 * @return json object with status of success or failure
	 */
	public function status(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$branch = Branch::find($id);
			if($branch){
				$branch->status = $status;
				$branch->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete a device
	 * @param  Request $request branch id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$device = Device::find($id);
			if($device){
				$device->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the devcie edit screen to the devcie.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		$curdevice=Device::find($id);
		
		if($curdevice){
			return view( 'deviceManage::device.edit' )->with([ 
				'curDevice' => $curdevice						

				 ]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new device data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit(DeviceRequest $request, $id)
	{
		
		
		$count= Device::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();
		
			if($count==0){
				$deviceOld =  Device::where('id',$id)->take(1)->get();
				$device=$deviceOld[0];			
				$device->name = $request->get('name');				
				$device->save();
				
				return redirect( 'device/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Device updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('device/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Device Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
   
		
	}
}
