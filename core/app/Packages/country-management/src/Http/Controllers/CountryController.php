<?php

namespace CountryManager\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use CountryManager\Models\Country;

class CountryController extends Controller {
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\View\View
   */
  public function index(Request $request)
  {
      $country = Country::get();

      return view('countryManager::country.index', compact('country'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\View\View
   */
  public function create()
  {
      return view('countryManager::country.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
  public function store(Request $request)
  {
      $this->validate($request, ['name' => 'required|max:255','zip' => 'required|max:255','price' => ['required','regex:/^(?!0*(\.0+)?$)[+]?\d*\.?\d+$/']]);
      $requestData = $request->all();

      Country::create($requestData);

      return redirect()->route('country.list');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   *
   * @return \Illuminate\View\View
   */
  public function show($id)
  {
      return redirect('country/'.$id.'/edit');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   *
   * @return \Illuminate\View\View
   */
  public function edit($id)
  {
      $country = Country::findOrFail($id);

      return view('countryManager::country.edit', compact('country'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
  public function update($id, Request $request)
  {
      $this->validate($request, ['name' => 'required|max:255','zip' => 'required|max:255','price' => ['required','regex:/^(?!0*(\.0+)?$)[+]?\d*\.?\d+$/']]);

      $requestData = $request->all();

      $country = Country::findOrFail($id);
      $country->update($requestData);

      return redirect()->route('country.list');;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
  public function destroy($id)
  {
      Country::destroy($id);

      return response(200);
  }
}
