<?php
namespace CountryManager\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model{
	protected $fillable = ['name','zip','price','usps_domestic_rate'];
}
