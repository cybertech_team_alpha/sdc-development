@extends('layouts.back.master')

@section('current_title','New Country')

@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{route('country.list')}}">Country Management</a></li>

        <li class="active">
            <span>New Country</span>
        </li>
    </ol>
</div>
@stop

@section('content')
<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">

                {!! Form::open(['route' => 'country.add', 'class' => 'form-horizontal', 'files' => true]) !!}

                @include ('countryManager::country.form')

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection
