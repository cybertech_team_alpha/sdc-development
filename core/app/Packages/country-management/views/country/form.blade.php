<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'NAME *', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('zip') ? 'has-error' : ''}}">
    {!! Form::label('zip', 'ZIP *', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('zip', null, ['class' => 'form-control']) !!}
        {!! $errors->first('zip', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'PRICE *', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('price', null, ['class' => 'form-control']) !!}
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Done', ['class' => 'btn btn-primary']) !!}
        
    </div>
</div>
