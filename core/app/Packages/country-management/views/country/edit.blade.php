@extends('layouts.back.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                {!! Form::model($country, [
                    'method' => 'POST',
                    'route' => ['country.edit', $country->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}

                @include ('countryManager::country.form', ['submitButtonText' => 'Update'])

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>

@endsection
