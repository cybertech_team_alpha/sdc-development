@extends('layouts.back.master')

@section('current_title','All Countries')

@section('css')
<style type="text/css">
    #floating-button{
      width: 55px;
      height: 55px;
      border-radius: 50%;
      background: #db4437;
      position: fixed;
      bottom: 50px;
      right: 30px;
      cursor: pointer;
      box-shadow: 0px 2px 5px #666;
      z-index:2
    }

    .plus{
      color: white;
      position: absolute;
      top: 0;
      display: block;
      bottom: 0;
      left: 0;
      right: 0;
      text-align: center;
      padding: 0;
      margin: 0;
      line-height: 55px;
      font-size: 38px;
      font-family: 'Roboto';
      font-weight: 300;
      animation: plus-out 0.3s;
      transition: all 0.3s;
    }
</style>

@stop

@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('country/list')}}">Country Management</a></li>

        <li class="active">
            <span>Country List</span>
        </li>
    </ol>
</div>
@stop


@section('content')
  <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create" onclick="location.href = '{{route('country.add')}}';">
      <p class="plus">+</p>
  </div>
  <div class="row">
      <div class="col-md-12">
          <div class="panel panel-default">
              <div class="panel-body">

                  <div class="table-responsive">
                      <table class="table table-striped table-bordered table-hover">
                          <thead>
                              <tr>
                                  <th>#</th><th>Name</th><th>Zip</th><th>Price</th><th width="1%">Edit</th><th width="1%">Delete</th>
                              </tr>
                          </thead>
                          <tbody>
                          @foreach($country as $item)
                              <tr>
                                  <td>{{ $item->id }}</td>
                                  <td>{{ $item->name }}</td><td>{{ $item->zip }}</td><td>{{ $item->price }}</td>
                                  <td>

                                      <a href="{{ route('country.edit',$item->id) }}" class="disabled" title="Edit Country"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                                  </td>
                                  <td>
                                    <a href="#" class="disabled btn-delete" data-id={{ $item->id }}>
                                      <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                  </td>
                              </tr>
                          @endforeach
                          </tbody>
                      </table>
                  </div>

              </div>
          </div>
      </div>
  </div>

@endsection
@section('js')
<script type="text/javascript">
  $('.table').dataTable()

  $('.btn-delete').click(function(event) {
    event.preventDefault()

    confirmAlert($(this).data('id'));


  });

  function confirmAction(id){
    $.ajax({
      url: '{{ url('country/delete') }}/'+id,
      type: 'POST',
      dataType: 'json',
      data: {_token: '{{ csrf_token() }}'}
    })
    .done(function() {
      location.reload();
      console.log("success");
    })
  }


</script>
@endsection
