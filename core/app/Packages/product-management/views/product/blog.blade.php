@extends('layouts.back.master') @section('current_title','New Product')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('product/blog')}}">BLOG</a></li>
       
        
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form  class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                	{!!Form::token()!!}
                    <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                         <div class="col-sm-10"><textarea name="description" class="form-control">{{$curBlog->description}}</textarea></div>
                        
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">IMAGES</label>
                        <div class="col-sm-10">
                            <input id="project-file" name="project-file[]" type="file" multiple class="file-loading">
                        </div>
                    </div>
                    
                	
                	
                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Done</button>
	                    </div>
	                </div>
                	
                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
        
    $("#project-file").fileinput({
            uploadUrl: "", // server upload action
            // uploadAsync: true,
            showUpload:false,
     
      validateInitialCount : true,
      overwriteInitial: false,

            allowedFileExtensions: ["jpg", "gif", "png"],
      initialPreview: <?php echo json_encode($images ); ?>,
      initialPreviewConfig: <?php echo json_encode($image_config) ?>,
        });




		$("#form").validate({
            rules: {
                name: {
                    required: true
                  
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
	});
        
       
	
	
</script>
@stop