<?php
/**
 * Product MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'product', 'namespace' => 'ProductManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'product.add', 'uses' => 'ProductController@addView'
      ]); 
      Route::get('blog', [
        'as' => 'blog', 'uses' => 'ProductController@blog'
      ]);

      Route::get('edit/{id}', [
        'as' => 'product.edit', 'uses' => 'ProductController@editView'
      ]);

      Route::get('list', [
        'as' => 'product.list', 'uses' => 'ProductController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'product.list', 'uses' => 'ProductController@jsonList'
      ]);
      Route::get('json/getSeries', [
        'as' => 'product.list', 'uses' => 'ProductController@getSeries'
      ]);
       Route::get('json/getFeature', [
        'as' => 'product.list', 'uses' => 'ProductController@getFeature'
      ]);
      Route::delete('image/deleteFile', [
      'as' => 'product.edit', 'uses' => 'ProductController@jsonImageFileDelete',
    ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'product.add', 'uses' => 'ProductController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'product.edit', 'uses' => 'ProductController@edit'
      ]);

      Route::post('status', [
        'as' => 'product.status', 'uses' => 'ProductController@status'
      ]);

      Route::post('delete', [
        'as' => 'product.delete', 'uses' => 'ProductController@delete'
      ]);
      Route::post('blog', [
        'as' => 'blog', 'uses' => 'ProductController@blogEdit'
      ]);

    });
});