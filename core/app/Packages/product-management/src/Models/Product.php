<?php
namespace ProductManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Product Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class Product extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_product';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','model_no','launch_date','series_id','device_id','created_by', 'status','description','special_feature','price','old_price','instock','rate','colour'];

	public function getDevice()
	{
		return $this->belongsTo('DeviceManage\Models\Device', 'device_id', 'id');
	}
	public function getSeries()
	{
		return $this->belongsTo('SeriesManage\Models\Series', 'series_id', 'id');
	}
	public function getImages()
	{
		return $this->hasMany('ProductManage\Models\ProductImage', 'product_id', 'id');
	}
	public function getFeature()
	{
		return $this->hasMany('ProductManage\Models\ProductFeature', 'product_id', 'id')->where('status','=',1);
	}

	

}
