<?php
namespace ProductManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * ProductFeature Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class ProductFeature extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_product_feature';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['product_id','feature_id','feature_value', 'status'];

	public function getFeature()
	{
		return $this->belongsTo('FeatureManage\Models\Feature', 'feature_id', 'id');
	}
	

	

}
