<?php

namespace PagesManage\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use PagesManage\Http\Requests\PagesRequest;
use Illuminate\Http\Request;
use PagesManage\Models\pages;
use Permissions\Models\Permission;
use Sentinel;
use Response;
use File;

class PagesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addView() {

        return view('pagesManage::pages.add');
    }

    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request) {

        $count= Pages::where('allies_name', '=',$request->get('allies' ))->count();
        if ($count==0) {
            if (!file_exists(storage_path('uploads/images/pages'))) {

            File::makeDirectory(storage_path('uploads/images/pages'));
            }
            $path='uploads/images/pages';

            $banner_image = $request->file('banner_image');
            $fileName='';
            if ($request->hasFile('banner_image')) {
                    $file = $request->file('banner_image');
                    $extn =$file->getClientOriginalExtension();
                    $destinationPath = storage_path($path);
                    $fileName = 'banner-image-' .date('YmdHis') .  '.' . $extn;
                    $file->move($destinationPath, $fileName);

             }

            $name = $request->get('name');
            $allies = $request->get('allies');
            $description = $request->get('desc');
            $tittle = $request->get('tittle');
            $tag_line = $request->get('tag_line');
            $value=$request->get('status');  
            $statuse=0;
             if($value=='Enable'){
                 $statuse=1;
             }else{
                 $statuse=0;
             }
            
            
            pages::Create([
                'page_name' => $name,
                'allies_name' => $allies,
                'content' => $description,
                'tittle' => $tittle,
                'tag_line' => $tag_line,
                'path'=>$path,
                'filename'=>$fileName,
                'status'=>$statuse
                    ]
            );
            return redirect('pages/add')->with(['success' => true,
                        'success.message' => 'Blog Created successfully!',
                        'success.title' => 'Well Done!']);
        }else{
                return redirect('pages/add')->with([ 'error' => true,
                'error.message'=> 'Pages  Already Exsist!',
                'error.title' => 'Duplicate!']);
            }
        
    }

    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function listView() {
        return view('pagesManage::pages.list');
    }

    public function jsonList(Request $request) {


        if ($request->ajax()) {
            $data = pages::get();
            $user = Sentinel::getUser();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $page) {
             
                    $dd = array();
                array_push($dd, $i);

                if ($page->page_name != "") {
                    array_push($dd, $page->page_name);
                } else {
                    array_push($dd, "-");
                }

                if ($page->allies_name != "") {
                    array_push($dd, $page->allies_name);
                } else {
                    array_push($dd, "-");
                }
                // if ($page->content != "") {
                //     array_push($dd, $page->content);
                // } else {
                //     array_push($dd, "-");
                // }
                 if ($page->status != "") {
                    array_push($dd, $page->status);
                } else {
                    array_push($dd, "-");
                }
                

                $permissions = Permission::whereIn('name', ['menu.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('pages/edit/' . $page->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit Menu"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['menu.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a  class="page-delete" data-id="' . $page->id . '" data-toggle="tooltip" data-placement="top" title="Delete Menu"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<center><a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a></center>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

        public function editView($id) {
        $data=pages::find($id);
        if($data){
            return view('pagesManage::pages.edit',['data'=>$data]);
        }else{
            return view('errors.404');
        }
            
    }
    
       public function edit(Request $request, $id) {
        $count= Pages::where('id', '!=', $id)->where('allies_name', '=',$request->get('allies' ))->count();
        if ($count==0) {
             if (!file_exists(storage_path('uploads/images/pages'))) {
                    File::makeDirectory(storage_path('uploads/images/pages'));
                }
                $path='uploads/images/pages';

                $name = $request->get('name');
                $allies = $request->get('allies');
                $description = $request->get('desc');
                $tittle = $request->get('tittle');
                $tag_line = $request->get('tag_line');
                $value=$request->get('status');  
                $statuse=0;
                 if($value=='Enable'){
                     $statuse=1;
                 }else{
                     $statuse=0;
                 }
                
                $page = pages::find($id);

                $page->page_name = $name;
                $page->allies_name = $allies;
                $page->content = $description;
                $page->tittle = $tittle;
                $page->tag_line = $tag_line;
                $page->status = $statuse;
                
                $image_hidden=$request->get('image_hidden');
                if($image_hidden){
                    File::delete(storage_path($page->path . '/' . $page->filename));
                }
                $fileName='';
                if ($request->hasFile('banner_image')) {
                        $file = $request->file('banner_image');
                        $extn =$file->getClientOriginalExtension();
                        $destinationPath = storage_path($path);
                        $fileName = 'banner-image-' .date('YmdHis') .  '.' . $extn;
                        $file->move($destinationPath, $fileName);
                        $page->filename=$fileName;
                 }
               $page->save();

                
                return redirect('pages/edit/' . $id)->with(['success' => true,
                            'success.message' => 'Pages updated successfully!',
                            'success.title' => 'Good Job!']);
        }else{
            return redirect('pages/edit/'.$id)->with([ 'error' => true,
                'error.message'=> 'Pages  Already Exsist!',
                'error.title' => 'Duplicate!']);
        }

       
    } 
      public function delete(Request $request) {
        if ($request->ajax()) {
            $id = $request->input('id');

            $pages = pages::find($id);
            if ($pages) {
                $pages->delete();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }
    public function preview($slug)
    {
        $page=Pages::where('allies_name',$slug)->where('status',1)->get();
        return view('pagesManage::pages.preview')->with(['page' => $page]);;
    }
}
