<?php

namespace pagesManage\Models;

use Illuminate\Database\Eloquent\Model;

class pages extends Model
{
protected $table = 'sa_pages';

protected $fillable = ['page_name','allies_name','content','tittle','tag_line','status','path','filename'];
}
