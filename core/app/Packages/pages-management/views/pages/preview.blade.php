@extends('layouts.front.master') @section('title','About | www.theprinceofgalle.com')
@section('css')
<style type="text/css">
.page-bg-img {
    /*background-color: #aa856a;*/
    padding-top: 80px;
    padding-bottom: 60px;
    /*background-image: url(../../assets/front/images/about_header.jpg)*/
    
}
</style>
@stop


@section('content')

<section class="breadcrumb men parallax margbot30">

</section><!-- //BREADCRUMBS -->
@if(count($page) > 0)
<!-- PAGE HEADER -->
<section class="page_header">


    <!-- CONTAINER -->
    <div class="container">

        <hr class="banner-top">
        <div class="page-bg-img center"><img src="{{ url('') . '/core/storage/' .$page[0]['path'].'/'.$page[0]['filename']}}">
            <h3>{{$page[0]['tittle']}}</h3>
            <p>{{$page[0]['tag_line']}}</p>
        </div>
        <hr class="banner-bottom">

    </div><!-- //CONTAINER -->

</section><!-- //PAGE HEADER -->

<!-- ABOUT US INFO -->
<section class="about_us_info" >

    <!-- CONTAINER -->
    <div class="container">                
       <?php echo htmlspecialchars_decode($page[0]['content']) ?>
   </div><!-- //CONTAINER -->
</section><!-- //ABOUT US INFO -->
@else

@endif
<p class="text-center">No Data</p>
@stop

@section('js')

@stop