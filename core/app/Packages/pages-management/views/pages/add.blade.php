@extends('layouts.back.master') @section('current_title','New Pages')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('pages/list')}}">Pages Management</a></li>
       
        <li class="active">
            <span>New Page</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form method="POST" class="form-horizontal" id="form"  enctype="multipart/form-data">
                	{!!Form::token()!!}

					
                	<div class="form-group"><label class="col-sm-2 control-label">Page Name</label>
                    	<div class="col-sm-10"><input type="text" class="form-control" name="name"></div>
                	</div>
                    <div class="form-group"><label class="col-sm-2 control-label">Allies Name</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="allies"></div>
                    </div>
                     <div class="form-group"><label class="col-sm-2 control-label">Header</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="tittle"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Tag Line</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="tag_line"></div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label required">IMAGES</label>
                        <div class="col-sm-10">
                            <input id="banner_image" name="banner_image" type="file" multiple class="file-loading">
                        </div>
                    </div>
                    
                     <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                        <div class="col-sm-10"><textarea name="desc" id="desc" class="form-control"></textarea></div>
                      

                    </div>
			         <div class=" col-sm-8 col-sm-offset-2">
                        <input class="form-check-input" type="radio" name="status" id="gridRadios2" value="Enable" >
                        <label class="control-label">Activate this page</label><br/>
                        <input class="form-check-input" type="radio" name="status" id="gridRadios2" value="Disable">
                        <label class=" control-label">Deactivate this page</label><br/><br/><br/>
                         </div>		
                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Done</button>
	                    </div>
	                </div>
                	
                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
  <script src="{{asset('assets/back/vendor/niceedit/nicEdit.js')}}"></script>
  <script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
          new nicEditor({fullPanel : true,iconsPath :'{{url('assets/back/vendor/niceedit/nicEditorIcons.gif')}}' }).panelInstance('desc');


        $("#banner_image").fileinput({
        allowedFileExtensions: ['jpg', 'png', 'gif'],
         previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
        'showUpload': false,
            overwriteInitial: true,
            removeIcon: '<i class="glyphicon glyphicon-trash"></i>',



        });

		$(".js-source-states").select2();

		$("#form").validate({
            rules: {
                label: {
                    required: true
                  
                },
                menu_url:{
                	required: true
                },
                parent_menu:{
                	required: true
                },               
                tel:{
                	digits:true
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
	});
	
	
</script>
 <script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>

@stop
