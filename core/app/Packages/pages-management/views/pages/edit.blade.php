@extends('layouts.back.master') @section('current_title','Update Pages')
@section('css')
<link rel="stylesheet" href="{{asset('assets/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('pages/list')}}">Pages Management</a></li>

        <li class="active">
            <span>Pages Feature</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form method="POST" class="form-horizontal" id="form" enctype="multipart/form-data">
                    {!!Form::token()!!}



                    <div class="form-group"><label class="col-sm-2 control-label">Page Name</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="name" value="{{$data->page_name}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Allies Name</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="allies" value="{{$data->allies_name}}"></div>
                    </div>
                     <div class="form-group"><label class="col-sm-2 control-label">Header</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="tittle" value="{{$data->tittle}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Tag Line</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="tag_line" value="{{$data->tag_line}}"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">FEATURED IMAGE</label>
                        <div class="col-sm-10">
                        <input id="image_hidden" name="image_hidden" type="hidden" value="0" >
                            <input id="banner_image" name="banner_image" type="file" class="file-loading">
                        </div>
                    </div>

                    <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                        <div class="col-sm-10"><textarea name="desc" id="desc" class="form-control">{{$data->content}}</textarea></div>
                      

                    </div>
                  
                     
                    <div class=" col-sm-8 col-sm-offset-2">
                        <input class="form-check-input" type="radio" name="status" id="gridRadios2" value="Enable" @if(old('status',$data->status)==1) checked @endif>
                        <label class="control-label">Activate this page</label><br/>
                        <input class="form-check-input" type="radio" name="status" id="gridRadios2" value="Disable" @if(old('status',$data->status)==0) checked @endif>
                        <label class=" control-label">Deactivate this page</label><br/><br/><br/>
                    </div>
                    
                    
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-2">
                            <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                            <button class="btn btn-primary" type="submit">Save Changes</button>
                        </div>
                    </div>

                </form>
                </div>
            </div>
        </div>
        @stop
        @section('js')
        <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
        <script src="{{asset('assets/back/vendor/niceedit/nicEdit.js')}}"></script>
        <script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
        <script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
        <script>
            new nicEditor({fullPanel : true,iconsPath :'{{url('assets/back/vendor/niceedit/nicEditorIcons.gif')}}' }).panelInstance('desc');

        $("#banner_image").fileinput({
            uploadUrl: "", // server upload action
            uploadAsync: true,
            maxFileCount: 1,
            showUpload:false,
            initialPreview: [
            "<img style='height:160px' src='{{ url('') . '/core/storage/' .$data->path.'/'.$data->filename}}'>"

            ],
            allowedFileExtensions: ["jpg", "gif", "png"]
        });
        $('#banner_image').on('filecleared', function(event) {
          $('#image_hidden').val(1);
            console.log("filecleared");
        });
                  
      
        </script>
        @stop
