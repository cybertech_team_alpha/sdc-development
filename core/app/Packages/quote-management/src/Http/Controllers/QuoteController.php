<?php

namespace QuoteManager\Http\Controllers;

use App\Http\Controllers\Controller;
use CakeTypeManager\Models\CakeType;
use Illuminate\Support\Facades\DB;
use QuoteManager\Models\Quote;
use Response;
use Sentinel;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use SliderManager\Models\Slider;
use TestimonialsManager\Models\Testimonials;
use Validator;
use Image;
use SliderManager\Models\AnimationType;


class QuoteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function index()
    {
        return view('QuoteManager::quote.list');
    }


    public function jsonList(Request $request)
    {
        if ($request->ajax()) {
            $data = Quote::where('status', '>=', 1)->get();
//            return $data;
            $jsonList = array();
            $i = 1;

            foreach ($data as $key => $q) {
                $dd = array();
                array_push($dd, $i);
                array_push($dd, $q->first_name . ' ' . $q->last_name);
                array_push($dd, $q->email);
                array_push($dd, $q->event_date);
                array_push($dd, $q->requested_date);
                array_push($dd, ($q->completed_date)?$q->completed_date:'-');
//                array_push($dd, $q->ip);
//                array_push($dd, $q->ip_location);
                // if ($q->status == 1) {
                //     array_push($dd, '<center><a href="#" class="state" data-id="' . $q->quote_id . '"  data-toggle="tooltip" data-placement="top" title="Edit Cake Type"><i class="fa fa-minus-circle"</i></a></center>');
                // } else {
                //     array_push($dd, '<center><a href="#" data-id="' . $q->quote_id . '"  data-toggle="tooltip" data-placement="top" title="Edit Cake Type"><i class="fa fa-check-circle"</i></a></center>');
                // }


                $permissions = Permission::whereIn('name', ['caketype.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('quote/edit/' . $q->quote_id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit Cake Type"><i class="fa fa-eye"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

               // $permissions = Permission::whereIn('name', ['quote.delete', 'admin'])->where('status', '=', 1)->lists('name');
               // if (Sentinel::hasAnyAccess($permissions)) {
               //     array_push($dd, '<center><a href="#" class="caketype-delete" data-id="' . $q->quote_id . '" data-toggle="tooltip" data-placement="top" title="Delete Cake Type"><i class="fa fa-trash-o"></i></a></center>');
               // } else {
               //     array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
               // }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    public function complete(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');
            $quote = Quote::where('quote_id', '=', $id)->first();
            if ($quote) {
                $quote->status = 2;
                $quote->save();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    public function editView($id)
    {
      $quote = Quote::with('cakeType')->findOrFail($id);

      return view('QuoteManager::quote.edit',compact('quote'));
    }

}
