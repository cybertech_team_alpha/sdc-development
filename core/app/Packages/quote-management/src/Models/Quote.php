<?php

namespace QuoteManager\Models;

use Illuminate\Database\Eloquent\Model;
use CakeTypeManager\Models\CakeType;
use Mail;

class Quote extends Model{
	protected $table = 'quote';

    protected $primaryKey = 'quote_id';

		public function cakeType()
		{
		  return $this->belongsTo(CakeType::class, 'cake_type','cake_type_id');
		}

		function sendAdminQuotationInquiryEmail(){
			$adminEmail = env("ADMIN_EMAIL");
			
			$firstname = $this->first_name;
			$lastName = $this->last_name;

			Mail::send('emails.quotes.quotation-inquiry',['details' => $this], function($message) use($adminEmail, $firstname, $lastName) {
				$message->to($adminEmail, $adminEmail)->subject('New Quotation Inquiry | sweetdelightscakery.com | '.$firstname.' '.$lastName);
			});
		}
		
}
