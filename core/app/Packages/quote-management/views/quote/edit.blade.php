@extends('layouts.back.master') @section('current_title',' UPDATE REQUESTED QUOTE')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('caketype/list')}}">Quotes Management</a></li>

        <li class="active">
            <span>Quote</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                    {!!Form::token()!!}
                    <div class="form-group"><label class="col-sm-2 control-label">EVENT DATE</label>
                        <div class="col-sm-3"><div class="input-group date" id="datetimepicker1">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            <input type="text" name="datetime" value="{{date('Y-m-d',strtotime($quote->event_date))}}" class="form-control"/>
                        </div></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">REQUESTED DATE</label>
                        <div class="col-sm-3"><div class="input-group date" id="datetimepicker1">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            <input type="text" name="datetime" value="{{date('Y-m-d',strtotime($quote->requested_date))}}" class="form-control"/>
                        </div></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">CAKE TYPE</label>
                        <div class="col-sm-10"><label class="control-label">{{$quote->cakeType->type}}</label></div>
                    </div>
                     <div class="form-group"><label class="col-sm-2 control-label">NAME</label>
                        <div class="col-sm-10"><label class="control-label">{{$quote->first_name.' '.$quote->last_name}}</label></div>
                    </div>
                     <div class="form-group"><label class="col-sm-2 control-label">CONTACT NO.</label>
                        <div class="col-sm-10"><label class="control-label">{{$quote->phone}}</label></div>
                    </div>
                     <div class="form-group"><label class="col-sm-2 control-label">EMAIL</label>
                        <div class="col-sm-10"><label class="control-label">{{$quote->email}}</label></div>
                    </div>
               {{--       <div class="form-group"><label class="col-sm-2 control-label">PROVINCE</label>
                        <div class="col-sm-10"><label class="control-label">{{$quote->province}}</label></div>
                    </div>

                     <div class="form-group"><label class="col-sm-2 control-label">POST CODE</label>
                        <div class="col-sm-10"><label class="control-label">{{$quote->post_code}}</label></div>
                    </div> --}}
                     <div class="form-group"><label class="col-sm-2 control-label">VENUE</label>
                        <div class="col-sm-10"><label class="control-label">{{$quote->venue}}</label></div>
                    </div>
                     <div class="form-group"><label class="col-sm-2 control-label">DETAILS</label>
                        <div class="col-sm-10"><label class="control-label">{{$quote->details}}</label></div>
                    </div>



                    <div class="hr-line-dashed"></div>



                </form>
        </div>
    </div>
</div>
@stop
