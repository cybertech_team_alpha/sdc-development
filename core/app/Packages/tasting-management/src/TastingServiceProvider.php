<?php

namespace TastingManage;

use Illuminate\Support\ServiceProvider;

class TastingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'TastingManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('TastingManage', function($app){
            return new TastingManage;
        });
    }
}
