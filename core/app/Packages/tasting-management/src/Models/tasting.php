<?php
namespace TastingManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mail;
/**
 * Tasting Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class Tasting extends Model{
	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tasting';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['people_type_id','event_date','details','first_name','last_name','province','post_code','venue','phone','email','status','accepted_date','ip_address','ip_location','created_by','status', 'servings'];

	public function getPeopleType()
	{
		return $this->belongsTo('TastingManage\Models\TastingPeopleType', 'people_type_id', 'id');
	}
	public function getUser()
	{
		return $this->belongsTo(\UserManage\Models\User::class, 'created_by');
	}

	public function sendRecieptEmail()
	{
			$userEmail = $this->email;
			$oId = $this->id;

				Mail::send('emails.reciepts.tasting-receipt',['order' => $this], function($message) use($userEmail, $oId) {
					$message->to($userEmail, '')->subject('Scheduled Tasting Confirmation Email | sweetdelightscakery.com | #'.$oId);
				});
			
		
	}
	public function sendAdminRecieptEmail()
	{
			$oId = $this->id;
			$adminEmail = env("ADMIN_EMAIL");
			
			Mail::send('emails.reciepts.tasting-receipt',['order' => $this], function($message) use($adminEmail, $oId) {
				$message->to($adminEmail, $adminEmail)->subject('Scheduled Tasting Confirmation Email Administrator Copy | sweetdelightscakery.com | #'.$oId);
			});
			
		
	}
	


	

}
