@extends('layouts.back.master') @section('current_title','Update Blog')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-tagsinput-latest/bootstrap-tagsinput-latest/dist/bootstrap-tagsinput.css')}}" />

<style>
       
        .imageBox
    {
      position: relative;
      width: 300px;
      height: 400px;
      border:1px solid #aaa;
      background: #fff;
      overflow: hidden;
      background-repeat: no-repeat;
      cursor:move;
    }

    .imageBox .thumbBox
    {
      position: absolute;
      top: 50%;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      width: 270px;
      height: 370px;
      margin-top: auto;
      margin-bottom: auto;
      margin-left: auto;
      margin-right: auto;
      box-sizing: border-box;
      border: 1px solid rgb(102, 102, 102);
      box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
      background: none repeat scroll 0% 0% transparent;
    }

    .imageBox .spinner
    {
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      text-align: center;
      line-height: 400px;
      background: rgba(0,0,0,0.7);
    }
    </style>

 <style>
        
 .imageBox-cover-image
    {
        position: relative;
      width: 800px;
      height: 450px;
      border:1px solid #aaa;
      background: #fff;
      overflow: hidden;
      background-repeat: no-repeat;
      cursor:move;
    }

    .imageBox-cover-image .thumbBox-cover-image
    {
     position: absolute;
      top: 50%;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      width: 770px;
      height: 400px;
      margin-top: auto;
      margin-bottom: auto;
      margin-left: auto;
      margin-right: auto;
      box-sizing: border-box;
      border: 1px solid rgb(102, 102, 102);
      box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
      background: none repeat scroll 0% 0% transparent;
    }

    .imageBox-cover-image .spinner-cover-image
    {
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      text-align: center;
      line-height: 400px;
      background: rgba(0,0,0,0.7);
    }
 .bootstrap-tagsinput{
     width: 100%;
 }

 .bootstrap-tagsinput input{
     width: 100% !important;
 }
 .file-preview {
     border-radius: 5px;
     border: 1px solid #ddd;
     padding: 5px;
     width: 100%;
     margin-bottom: 5px;
 }

 .fileUpload {
     position: relative;
     overflow: hidden;
     margin: 10px;
 }
 .fileUpload input.upload {
     position: absolute;
     top: 0;
     right: 0;
     margin: 0;
     padding: 0;
     font-size: 20px;
     cursor: pointer;
     opacity: 0;
     filter: alpha(opacity=0);
 }
    </style>
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('blog/list')}}">Blog Management</a></li>

        <li class="active">
            <span>Update Blog</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul class="">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
              @endif
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                	{!!Form::token()!!}

                    <input type="hidden" name="front_image" id="front_img" form="form">
                    <input type="hidden" name="cover_image" id="cvr_img" form="form">

                    <div class="form-group"><label class="col-sm-2 control-label">NAME</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : $curblog->name}}"></div>
                    </div>

                   <div class="form-group"><label class="col-sm-2 control-label">START DATE</label>
                        <div class="col-sm-2">
                            <div class="input-group date" >
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                <input type="text" id="startdate"  name="start_date" class="form-control" value="{{old('start_date') ? old('start_date') :$curblog->start_date}}"/>
                            </div>
                       </div>
                    </div>
                        <div class="form-group"><label class="col-sm-2 control-label">END DATE</label>
                        <div class="col-sm-2">
                            <div class="input-group date" >
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                                <input type="text" id="enddate"  name="end_date" class="form-control" value="{{ old('end_date') ? old('end_date') :$curblog->end_date}}"/>
                            </div>
                        </div>
                    </div>
                         <div class="form-group"><label class="col-sm-2 control-label">VIDEO LINK</label>
                             <div class="col-sm-10"><input type="text" class="form-control" name="video" value="{{ old('video_url') ? old('video_url') :$curblog->video_url}}"></div>
                    </div>
                        <div class="form-group">
                        <label class="col-sm-2 control-label">HOME PAGE</label>
                        <div class="col-sm-10">
                        <div class="checkbox">
                            <input type="checkbox"  name="home" value="home" <?php echo $curblog->home? 'checked':''?>>
                        </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">FEATURED POSTS</label>
                        <div class="col-sm-10">
                            <div class="checkbox">
                                <input type="checkbox" value="featuredpost" name="featuredpost" value="{{ old('featuredpost') }}" <?php echo $curblog->featuredpost ? 'checked':''?>>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label required">FRONT IMAGE</label>
                        <div class="col-sm-10">
                            <div class="col-sm-10">
                                <div class="col-md-6">
                                  <button type="button" class="btn bg-grey waves-effect" data-toggle="modal" data-target="#front-image-crop-modal" >Select Front Image</button>
                                </div>
                                <div class="col-md-6">
                                  <div class="front-image-cropped">
                                    <img style='height:160px' src='{{ url('') . '/core/storage/uploads/images/blog/'.$curblog->front_image}}' >
                                  </div>
                                </div>
                              </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label required">COVER IMAGE</label>
                        <div class="col-sm-10">
                            <div class="col-sm-10">
                                <div class="col-md-6">
                                  <button type="button" class="btn bg-grey waves-effect" data-toggle="modal" data-target="#cover-image-crop-modal" >Select Cover Image</button>
                                </div>
                                <div class="col-md-6">
                                  <div class="cover-image-cropped">
                                    <img style="height:160px" src='{{ url('') . '/core/storage/uploads/images/blog/'.$curblog->cover_image}}'>
                                  </div>
                                </div>
                              </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label required">IMAGES</label>
                        {{--<div class="col-sm-10">--}}
                            {{--<input id="image_hidden_sub" name="image_hidden_sub" type="hidden" value="0" >--}}
                            {{--<input id="project-file" name="project-file[]" type="file" multiple class="file-loading" hidden="hidden">--}}
                        {{--</div>--}}
                        <div class="col-sm-10">
                        <div class="file-preview">
                            <div class="fileUpload btn bg-grey waves-effect">
                                <button class="btn bg-grey waves-effect"><span>Upload</span></button>
                                <input id="project-file1" name="project-file1[]" class="upload" type="file" multiple onchange="handleFileSelect()">
                            </div>
                            <div id="image-preview">
                                @if($images)

                                    @foreach($images as $img)
                                        <div id="imgExtdiv{{$img['key']}}" class="container" style="max-width:100%; border:1px solid #ddd; border-radius:5px;">
                                            <img id="imgExt{{$img['key']}}" style='height:160px;padding : 10px;' src='{{$img['url']}}'>
                                            <button type="button" class="btn btn-danger glyphicon glyphicon-trash" onclick="removeExtimage({{$img['key']}});" id="btnExt{{$img['key']}}" style="position: sticky; cursor: pointer; margin-bottom: 15px; left: 90%;"></button>
                                        </div>
                                    @endforeach
                                            @endif
                            </div>
                        </div>
                        </div>
                    </div>



                    <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                         <div class="col-sm-10"><textarea name="description" class="form-control">{{  old('description') ? old('description') :$curblog->description}}</textarea></div>

                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">TAGS</label>
                        <div class="col-sm-10">
                            <input type="text" id="tags" name="tags" style="width: 100%;" value="{{ old('tags') ? old('tags') :$curblog->tags}}" data-role="tagsinput" class="form-control" placeholder="Enter the tags seperated by commas"/>
                        </div>

                    </div>


                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Save Changes</button>
	                    </div>
	                </div>

                </form>
        </div>
    </div>
</div>


 <!-- crop modal -->
        <div class="modal fade" id="front-image-crop-modal" tabindex="-1" role="dialog">
          <div class="modal-dialog " role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" >Crop Front Image</h4>
              </div>
              <div class="modal-body">
                <div class="container">
                  <div class="imageBox">
                    <div class="thumbBox"></div>
                    {{-- <div class="spinner" style="display: none">Loading...</div> --}}
                  </div>
                  <div class="action">
                    <input type="file" id="cropFile" style="float:left; width: 250px">

                    <input type="button" id="btnZoomIn" value="+" class="btn bg-grey waves-effect">
                    <input type="button" id="btnZoomOut" value="-" class="btn bg-grey waves-effect">

                  </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn bg-success waves-effect" id="btnCrop">Crop</button>
                <button type="button" class="btn bg-amber waves-effect" data-dismiss="modal">CLOSE</button>
              </div>
            </div>
          </div>
        </div>
        <!-- crop modal -->
 <!-- crop modal -->
        <div class="modal fade" id="cover-image-crop-modal" tabindex="-1" role="dialog">
          <div class="modal-dialog " role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" >Crop Cover Image</h4>
              </div>
              <div class="modal-body">
                <div class="container">
                  <div class="imageBox-cover-image">
                    <div class=" thumbBox-cover-image"></div>
                    {{-- <div class="spinner" style="display: none">Loading...</div> --}}
                  </div>
                  <div class="action">
                    <input type="file" id="cropFileCoverImage" style="float:left; width: 250px">

                    <input type="button" id="btnZoomInCoverImage" value="+" class="btn bg-grey waves-effect">
                    <input type="button" id="btnZoomOutCoverImage" value="-" class="btn bg-grey waves-effect">

                  </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn bg-success waves-effect" id="btnCropCoverImage">Crop</button>
                <button type="button" class="btn bg-amber waves-effect" data-dismiss="modal">CLOSE</button>
              </div>
            </div>
          </div>
        </div>
        <!-- crop modal -->
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-tagsinput-latest/bootstrap-tagsinput-latest/dist/bootstrap-tagsinput.min.js')}}"></script>


<script type="text/javascript">
    var storedFiles = [];
    var imgid = 0;
	$(document).ready(function(){
        $("#input-1").rating();
		$(".js-source-states").select2();
           $('.date').datepicker(
            {
                 format: 'yyyy-mm-dd',
            });

        var isAfterStartDate = function(startDateStr, endDateStr) {

            if(new Date(startDateStr).getTime() > new Date(endDateStr).getTime()) {

                return false;
            }
            else
                return true;

        };
        jQuery.validator.addMethod("isAfterStartDate", function(value, element) {

                return isAfterStartDate($('#startdate').val(), value);
            }, "End date should be after start date");


		$("#form").validate({
            rules: {
                name: {
                    required: true

                },
                launch_date:{
                    required : true,
                    date: true
                },
                end_date:{
                    required : true,
                    date: true,
                      isAfterStartDate: true
                },
                description:{
                    required : true
                },


            },
            submitHandler: function(form) {
                //form.submit();
                handleForm();
            }
        });


	});
    //  $("#front_img").fileinput({
    //     uploadUrl: "", // server upload action
    //     uploadAsync: true,
    //     maxFileCount: 1,
    //     showUpload:false,
    //     initialPreview: [
    //     "<img style='height:160px' src='{{ url('') . '/core/storage/uploads/images/blog/'.$curblog->front_image}}'>"

    //     ],
    //     allowedFileExtensions: ["jpg", "gif", "png"]
    // });
    //  $("#cover_image").fileinput({
    //     uploadUrl: "", // server upload action
    //     uploadAsync: true,
    //     maxFileCount: 1,
    //     showUpload:false,
    //     initialPreview: [
    //     "<img style='height:160px' src='{{ url('') . '/core/storage/uploads/images/blog/'.$curblog->cover_image}}'>"],
    //     allowedFileExtensions: ["jpg", "gif", "png"]
    // });
    /* $("#project-file").fileinput({
            uploadUrl: "", // server upload action
          //  uploadAsync: true,
            uploadAsync: false,
            maxFileCount: 5,
            showUpload:false,
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            initialPreview: <?php echo json_encode($images ); ?>,
            initialPreviewConfig: <?php echo json_encode($image_config) ?>,
         showRemove: true,
        });
*/
    // $("#project-file").on('click', function() {
    //     $("#project-file").fileinput('refresh');
    // });
    $("#tags").tagsinput('items');
    //    $('#front_img').on('filecleared', function(event) {
    //       $('#image_hidden').val(1);
    //         console.log("filecleared");
    //     });
    //     $('#project-file').on('filecleared', function(event) {
    //           $('#image_hidden_sub').val(1);
    //             console.log("filecleared");
    //         });



    var editor_config={
        selector: 'textarea',  // change this value according to your HTML
        plugins: [
            'advlist autolink lists link image charmap preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking table contextmenu directionality emoticons template paste textcolor colorpicker textpattern imagetools codesample'
        ],
        toolbar: 'formatselect | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | undo redo | image code | forecolor backcolor emoticons | bold italic strikethrough forecolor backcolor | removeformat',
        image_advtab: true,
        fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
        menubar: true,
        paste_data_images: true,
        height: 400,
        path_absolute:"{{ URL::to('/') }}/",
        relative_urls:false,
        file_picker_callback: function (callback, value, meta) {
            let x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            let y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
    
            let type = 'image' === meta.filetype ? 'Images' : 'Files',
                url  = editor_config.path_absolute + 'laravel-filemanager?editor=tinymce5&type=' + type;
    
            tinymce.activeEditor.windowManager.openUrl({
                url : url,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable:"yes",
                close_previous:"no",
                onMessage: (api, message) => {
                    callback(message.content);
                }
            });
        }   
        
    };
       
    tinymce.init(editor_config);

    function handleFileSelect() {

        // var preview = document.querySelector('#img_preview');
        // var files   = document.querySelector('input[type=file]').files;
        var seldiv = document.querySelector("#image-preview");
        var files   = document.getElementById('project-file1').files;

        function readAndPreview(file) {
            if ( /\.(jpe?g|png|gif|jpg)$/i.test(file.name) ) {

                var filesArr = Array.prototype.slice.call(files);
                filesArr.forEach(function(f) {
                    storedFiles.push(f);
                });

                // document.getElementById(labelid).style.display = "none";
                var reader = new FileReader();

                reader.addEventListener("load", function () {
                    var div = document.createElement('div');
                    div.setAttribute('style', 'max-width:100%; border:1px solid #ddd; border-radius:5px;');
                    div.setAttribute('class', 'container');
                    div.setAttribute('id', 'imgdiv' + imgid);
                    var image = new Image();
                    //image.height = 50;
                    //  image.class = 'uploaded_image';
                    image.id = 'img'+imgid;
                    image.style = "height:160px; padding : 10px; ";
                    image.title = file.name;
                    image.src = this.result;
                    //image.click(removeimage(image.id)");
                    // image.on("click", function() {
                    //     image.remove();
                    //     removeimage(imgid);
                    // });
                    var btn = document.createElement('button');
                    btn.setAttribute('type', 'button');
                    btn.setAttribute('class', 'btn btn-danger glyphicon glyphicon-trash');
                    btn.setAttribute('onclick', 'removeimage('+imgid+');');
                    btn.setAttribute('id', 'button' + imgid);
                    btn.setAttribute('style', 'position: sticky; cursor: pointer; margin-bottom: 15px; left: 90%;');

                    div.appendChild(image);
                    div.appendChild(btn);
                    seldiv.appendChild(div);
                    // seldiv.appendChild( image );
                    // seldiv.appendChild(btn);
                    // seldiv.appendChild( '<div id= "imgprev"'+imgid+'>'+image+'<button class="fa-file-video-o" onclick="removeimage('+imgid+')"></div>' );
                }, false);

                reader.readAsDataURL(file);
                imgid++;
            }else{
                swal("Uploaded file is not an image");
            }


        }

        if (files) {
            [].forEach.call(files, readAndPreview);
        }
    }

    function removeExtimage(key){
        $.ajax({
            url:'{{url('blog/image/deleteFile')}}'+ '?' + $.param({"key": key}),
            type: 'DELETE',
            data: key,
            success: function(result) {
                swal('Image removed successfully');
                $("#imgExt"+key).remove();
                $("#imgExtdiv"+key).remove();
                $("#btnExt"+key).remove();
            }
        });
    }
</script>
<script src="{{url('assets/back/cropbox/cropbox.js')}}"></script>
<script type="text/javascript">


  var options =
  {
      thumbBox: '.thumbBox',
      spinner: '.spinner',
      imgSrc: 'avatar.png'
  }
  var cropper = $('.imageBox').cropbox(options);
  $('#cropFile').on('change', function(){
      var reader = new FileReader();
      reader.onload = function(e) {
          options.imgSrc = e.target.result;
          cropper = $('.imageBox').cropbox(options);
      }
      reader.readAsDataURL(this.files[0]);
      this.files = [];
  })
  $('#btnCrop').on('click', function(){
    var img = cropper.getDataURL()
    $('#front_img').val(img);
    document.querySelector('.front-image-cropped').innerHTML = '<img style="max-width:100%;" src="'+img+'">';
    $('#front-image-crop-modal').modal('hide');
  })
  $('#btnZoomIn').on('click', function(){
      cropper.zoomIn();
  })
  $('#btnZoomOut').on('click', function(){
      cropper.zoomOut();
  })

</script>
<script type="text/javascript">


  var optionsCI =
  {
      thumbBox: '.thumbBox-cover-image',
      spinner: '.spinner-cover-image',
      imgSrc: 'avatar.png'
  }
  var cropperCI = $('.imageBox-cover-image').cropbox(optionsCI);
  $('#cropFileCoverImage').on('change', function(){
      var reader = new FileReader();
      reader.onload = function(e) {
          optionsCI.imgSrc = e.target.result;
          cropperCI = $('.imageBox-cover-image').cropbox(optionsCI);
      }
      reader.readAsDataURL(this.files[0]);
      this.files = [];
  })
  $('#btnCropCoverImage').on('click', function(){
    var img = cropperCI.getDataURL()
    $('#cvr_img').val(img);
    document.querySelector('.cover-image-cropped').innerHTML = '<img style="max-width:100%;" src="'+img+'">';
    $('#cover-image-crop-modal').modal('hide');
  })
  $('#btnZoomInCoverImage').on('click', function(){
      cropperCI.zoomIn();
  })
  $('#btnZoomOutCoverImage').on('click', function(){
      cropperCI.zoomOut();
  })

  function handleForm() {
       e.preventDefault();
      var newpost = document.getElementById('form');
      var formData = new FormData(newpost);
      var len=(storedFiles.length - 1);
      for(var i=0;  i<len; i++) {
          formData.append('project-file[]', storedFiles[i]);
      }

      $.ajax({
          type: "POST",
          url: "{{url('blog/edit/'.$curblog->id)}}",
          data: formData,
          processData: false,
          contentType: false,
          dataType: "json",
          success: function (data) {
              window.location.reload();

          },
          error: function (data, textStatus, jqXHR) {
              //process error msg
          },
      });
  }
</script>
@stop
