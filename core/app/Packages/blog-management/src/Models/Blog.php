<?php
namespace BlogManage\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;


class Blog extends Model{
	// use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_blog';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','video_url','front_image','cover_image','start_date','end_date','description','home','status','tags'];

	public function getImages()
	{
		return $this->hasMany('BlogManage\Models\BlogImage', 'blog_id', 'id');
	}

	public function comments()
	{
	  return $this->hasMany(Comment::class);
	}



}
