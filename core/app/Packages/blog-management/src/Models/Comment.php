<?php
namespace BlogManage\Models;

use Illuminate\Database\Eloquent\Model;


class Comment extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_comment';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['email','name','content','blog_id'];

	public function blog()
  {
    return $this->belongsTo(Blog::class);
  }



}
