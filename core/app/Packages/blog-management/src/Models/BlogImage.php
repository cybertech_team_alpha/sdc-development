<?php
namespace BlogManage\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;


class BlogImage extends Model{
	// use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_blog_image';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['blog_id','path','filename'];

	public function blog()
	{
	  return $this->belongsTo(Blog::class);
	}

}
