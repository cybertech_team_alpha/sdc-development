<?php

namespace BlogManage\Http\Controllers;

use App\Http\Controllers\Controller;
use BlogManage\Http\Requests\BlogRequest;
use Illuminate\Http\Request;
use BlogManage\Models\Blog;
use BlogManage\Models\BlogImage;
use Permissions\Models\Permission;
use Sentinel;
use Response;
use File;
use Image;

class BlogController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Blog Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest');
    }

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {

        return view('blogManage::blog.add');
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

        $this->validate($request, [
          'front_image' => 'required',
          'cover_image' => 'required',
          'video_url' => ['nullable','regex:/^(?:https?:\/\/)?(?:www[.])?(?:youtube[.]com\/watch[?]v=|youtu[.]be\/)([^&]{11})$/'],
        ]);
            //ISSUE #116
        // if (Image::make($request->front_image)->width() != 270) {
        //   return back()->withInput()->withErrors(['front_image' => 'front image is not in required width 270px']);
        // }

        // if (Image::make($request->cover_image)->width() != 870) {
        //     return back()->withInput()->withErrors(['cover_image' => 'cover image is not in required width 950px']);
        // }

        $name = $request->get('name');

        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $description = $request->get('description');
        $file = $request->file('file');
        $video = $request->get('video');
        $tags = $request -> get('tags');
        $featuredpost =0;

        if ($request->get('home') != NULL) {
            $home = 1;
        } else {
            $home = 0;
        }
        if ($request->get('featuredpost') != NULL) {
            $featuredpost = 1;
        } else {
            $featuredpost = 0;
        }
        $imgname = '';

         if (!file_exists(storage_path('uploads/images/blog'))) {

            File::makeDirectory(storage_path('uploads/images/blog'));
        }
        $blog = Blog::Create([
          'name' => $name,
          'video_url' => $video,
          'start_date' => $start_date,
          'end_date' => $end_date,
          'description' => $description,
          'home' => $home,
          'featuredpost'=>$featuredpost,
          'front_image' => $this->base64_img_upload($request->front_image),
          'cover_image' => $this->base64_img_upload($request->cover_image),
          'created_by' => Sentinel::getUser()->id,
            'tags' => $request -> get('tags')
        ]);


        //echo $tags;exit;
        $path = 'uploads/images/blog';


        if ($request->hasFile('project-file')) {
            $project_files = $request->file('project-file');
            $i = 0;
            foreach ($project_files as $key => $project_file) {
                if (File::exists($project_file)) {
                    $file = $project_file;
                    $extn = $file->getClientOriginalExtension();
                    $destinationPath = storage_path($path);
                    $project_fileName = 'blog-' . date('YmdHis') . '-' . $i . '.' . $extn;
                    $file->move($destinationPath, $project_fileName);

                    BlogImage::create([
                        'blog_id' => $blog->id,
                        'path' => 'core/storage/'.$path,
                        'filename' => $project_fileName
                    ]);
                    $i++;
                }
            }


        }

        // $imgname = str_slug($blog->name)."-".time().'.png';

        // Image::make($request->front_image)->save('core/storage/'.$path.'/'.$imgname);
        // Image::make($request->cover_image)->save('core/storage/'.$path.'/cover_'.$imgname);

        // $blog->update([
        //   'front_image' => $imgname,
        //   'cover_image' => 'cover_'.$imgname,
        // ]);

        return redirect('blog/add')->with(['success' => true,
          'success.message' => 'Blog Created successfully!',
          'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('blogManage::blog.list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Blog::with(['getImages'])->where('status', '=', 1)->get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $blog) {

                $dd = array();
                array_push($dd, $i);

                if ($blog->name != "") {
                    array_push($dd, $blog->name);
                } else {
                    array_push($dd, "-");
                }

                if ($blog->date != "") {
                    array_push($dd, $blog->date);
                } else {
                    array_push($dd, "-");
                }
                if ($blog->description != "") {
                    array_push($dd, $blog->description);
                } else {
                    array_push($dd, "-");
                }


                $permissions = Permission::whereIn('name', ['product.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('blog/edit/' . $blog->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['product.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $blog->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    /**
     * Activate or Deactivate user
     * @param  Request $request id with status to change
     * @return json object with status of success or failure
     */
    public function status(Request $request) {
        if ($request->ajax()) {
            $id = $request->input('id');
            $status = $request->input('status');

            $branch = Branch::find($id);
            if ($branch) {
                $branch->status = $status;
                $branch->save();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
        if ($request->ajax()) {
            $id = $request->input('id');

            $blog = Blog::find($id);
            if ($blog) {
                $blog->delete();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
        $curblog = Blog::with(['getImages'])->find($id);
       // print_r($curblog->home);die();
       
        $image_url_array = [];
        $image_config_array = [];
        $font_img = ["<img style='height:160px' src='" . url("core/storage/uploads/images/blog/".$curblog->front_image) . "'>" , ['caption' => $curblog->name, 'caption' => $curblog->front_image, 'key' => $curblog->id, 'url' => '']];
        foreach ($curblog->getImages as $key => $value) {
            //array_push($image_url_array, "<img style='height:160px;padding : 10px; ' src='" . url('') .'/'. $value->path . '/' . $value->filename . "'>");
            //array_push($image_url_array, url('') .'/'. $value->path . '/' . $value->filename );
            array_push($image_url_array, array('url' => url('') .'/'. $value->path . '/' . $value->filename ,  'caption' => $value->filename, 'key' => $value->id, 'deleteurl' => url('blog/image/deleteFile')));
        }

        foreach ($curblog->getImages as $key => $value) {
            array_push($image_config_array, array('caption' => $value->filename, 'caption' => $value->filename, 'key' => $value->id, 'url' => url('blog/image/deleteFile')));
        }

        if ($curblog) {

            return view('blogManage::blog.edit')->with([
                        'curblog' => $curblog,
                        'images' => $image_url_array, 'image_config' => $image_config_array, 'front_img' => $font_img
            ]);
        } else {
            return view('errors.404');
        }
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request, [
        'video_url' => 'nullable|regex:/^(?:https?:\/\/)?(?:www[.])?(?:youtube[.]com\/watch[?]v=|youtu[.]be\/)([^&]{11})$/',
      ]);

        $name = $request->get('name');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $description = $request->get('description');
        $file = $request->file('file');
        $video = $request->get('video');
        // $image_hidden=$request->get('image_hidden');
        //$image_hidden_sub=$request->get('image_hidden_sub');
        $tags = $request -> get('tags');
        if ($request->get('home') != NULL) {
            $home = 1;
        } else {
            $home = 0;
        }
        $featuredpost = 0;
        if ($request->get('featuredpost') != NULL) {
            $featuredpost = 1;
        } else {
            $featuredpost = 0;
        }

        $blog = Blog::find($id);

        // if($image_hidden){
        //   File::delete(storage_path('uploads/images/blog' . '/' . $blog->front_image));
        // }
//        if($image_hidden_sub){
//         $blog_images=BlogImage::where('blog_id','=',$id)->get();
//          foreach ($blog_images as $key => $value) {
//            BlogImage::find($value->id)->delete();
//            File::delete(storage_path('uploads/images/blog' . '/' . $value->filename));
//          }
//
//        }

        if (!file_exists(storage_path('uploads/images/blog'))) {

            File::makeDirectory(storage_path('uploads/images/blog'));
        }
        
        $path = 'uploads/images/blog';
        if ($request->front_image != "") {
						$blog->front_image= $this->base64_img_upload($request->front_image);
				 }
        if ($request->cover_image != "") {
						$blog->cover_image= $this->base64_img_upload($request->cover_image);
                 }

        // if ($request->hasFile('front_image')) {

        // //   if (Image::make($request->front_image)->width() != 270) {
        // //     return back()->withInput()->withErrors(['front_image' => 'front image is not in required width 270px']);
        // //   }

        //   $imgname = str_slug($blog->name)."-".time().'.png';

        //   Image::make($request->front_image)->save('core/storage/'.$path.'/'.$imgname);

        //   $blog->update([
        //     'front_image' => $imgname
        //   ]);
        // }

        // if ($request->hasFile('cover_image')) {

        // //   if (Image::make($request->cover_image)->width() != 870) {
        // //       return back()->withInput()->withErrors(['cover_image' => 'cover image is not in required width 950px']);
        // //   }

        //   $imgname = str_slug($blog->name)."-".time().'.png';

        //   Image::make($request->cover_image)->save('core/storage/'.$path.'/cover_'.$imgname);

        //   $blog->update([
        //     'cover_image' => 'cover_'.$imgname
        //   ]);

        // }

        $blog->name = $name;
        $blog->video_url = $video;
        $blog->start_date = $start_date;
        $blog->end_date = $end_date;
        $blog->description = $description;
        $blog->home = $home;
        $blog->featuredpost = $featuredpost;
        $blog->tags = $tags;
        $blog->save();



        if ($request->hasFile('project-file')) {
          $project_files = $request->file('project-file');
          $i = 0;
          foreach ($project_files as $key => $project_file) {
              if (File::exists($project_file)) {
                  $file = $project_file;
                  $extn = $file->getClientOriginalExtension();
                  $destinationPath = storage_path($path);
                  $project_fileName = 'blog-' . date('YmdHis') . '-' . $i . '.' . $extn;
                  $file->move($destinationPath, $project_fileName);

                  BlogImage::create([
                      'blog_id' => $blog->id,
                      'path' => 'core/storage/'.$path,
                      'filename' => $project_fileName
                  ]);
                  $i++;
              }
          }
        }



        return redirect('blog/edit/' . $id)->with(['success' => true,
                    'success.message' => 'Blog updated successfully!',
                    'success.title' => 'Good Job!']);
    }

    public function jsonImageFileDelete(Request $request) {
        $image_id = $request->get('key');
        $image = BlogImage::with('blog')->find($image_id);
        $image->delete();
        return 1;
    }

    public function base64_img_upload($img_data){//save base64 encoded image
	try {
		$newname = str_random(10).".png";
		$data = $img_data;
		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);


		$save_target = "core/storage/uploads/images/blog/".$newname;

		$target = $save_target;


		if(file_put_contents($target, $data)){
			$img = imagecreatefrompng($target);
			imagealphablending($img, false);


			imagesavealpha($img, true);

			imagepng($img, $target, 8);
			return $newname;
		}else{
			return "error";
		}
	} catch (Exception $e) {
		return "error";
	}



}

}
