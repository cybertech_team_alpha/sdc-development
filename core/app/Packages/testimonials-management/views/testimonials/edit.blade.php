@extends('layouts.back.master') @section('current_title','Update Homepage Testimonials')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all"/>
@stop
@section('current_path')
    <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
            <li><a href="{{url('testimonials/list')}}">Testimonials Management</a></li>

            <li class="active">
                <span>Update Testimonial</span>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">
                    <form class="form-horizontal" id="form" method="post" files="true" enctype="multipart/form-data">
                        {!!Form::token()!!}

                        <div class="form-group"><label class="col-sm-2 control-label">NAME</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="txtName" value="{{$testi->name}}"></div>
                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label">TESTIMONIAL TEXT</label>
                            <div class="col-sm-10"><textarea name="txtText" class="form-control" required >{{$testi->testimonial}}</textarea></div>
                            <div class="col-sm-10 col-sm-offset-2">
                                <p style="color: #ff0000;">
                                    You Can add maximum 500 characters. Don't copy & paste texts from word files, pdf or other documents directly.<br/>First paste those texts in to a wordpad & then copy it again and paste it in to here.
                                </p>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label">BUTTON URL</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="txtUrl" placeholder="Enter button url (http://www.sweetdelightscakery.com/)" value="{{$testi->url}}"></div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button class="btn btn-default" type="button" onclick="location.reload();">Cancel
                                </button>
                                <button class="btn btn-primary" type="submit">Done</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $(".js-source-states").select2({placeholder: "Select Animation type"});

            $("#project-file").fileinput({
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
                'showUpload': false,
                overwriteInitial: true,
                removeIcon: '<i class="glyphicon glyphicon-trash"></i>',


            });


            $("#form").validate({
                rules: {
                    txtName: {
                        required: true
                    },
                    txtText: {
                        required: true
                    },
                    txtUrl: {
                        required: false,
                        url :true
                    },
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });

        tinymce.init({
            selector: 'textarea',  // change this value according to your HTML
            plugins: [
                'advlist autolink lists link image charmap preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code',
                'insertdatetime media nonbreaking table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
            ],
            toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
            '| bullist numlist outdent indent | link | undo redo | forecolor backcolor emoticons ',
            fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
            menubar: false
        });


    </script>
@stop