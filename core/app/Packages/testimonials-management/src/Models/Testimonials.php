<?php

namespace TestimonialsManager\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model{
	protected $table = 'testimonials';

    protected $primaryKey = 'testimonials_id';

}
