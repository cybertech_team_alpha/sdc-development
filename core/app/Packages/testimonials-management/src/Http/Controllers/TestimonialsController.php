<?php

namespace TestimonialsManager\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Response;
use Sentinel;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use SliderManager\Models\Slider;
use TestimonialsManager\Models\Testimonials;
use Validator;
use Image;
use SliderManager\Models\AnimationType;


class TestimonialsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function index()
    {
        return view('TestimonialsManager::testimonials.list');
    }

    public function add()
    {
        return view('TestimonialsManager::testimonials.add');
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'txtText' => 'required|max:500'
        ]);

        $testi=New Testimonials();
        $testi->name=$request->txtName;
        $testi->testimonial=$request->txtText;
        $testi->url=$request->txtUrl;
        $testi->save();

        return redirect('testimonials/list')->with(['success' => true,
            'success.message' => 'Testimonials Created successfully!',
            'success.title' => 'Well Done!']);

    }

    // public function getRules($id = -1){
    //     $rules = [
    //         'txtText' => 'required|max:500'
    //     ];
    //     return $rules;
    // }

    public function jsonList(Request $request)
    {
        if ($request->ajax()) {
            $data=Testimonials::where('status','=',1)->get();
            $jsonList = array();
            $i = 1;

            foreach ($data as $key => $testi) {
                $dd = array();
                array_push($dd, $i);
                array_push($dd, $testi->name);
                array_push($dd, $testi->testimonial);
                 array_push($dd, $testi->url);

                $permissions = Permission::whereIn('name', ['testimonials.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('testimonials/edit/' . $testi->testimonials_id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit Testimonial"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['testimonials.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="testimonials-delete" data-id="' . $testi->testimonials_id . '" data-toggle="tooltip" data-placement="top" title="Delete Testimonial"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    public function delete(Request $request){
        if($request->ajax()){
            $id = $request->input('id');
            $testi = Testimonials::where('testimonials_id','=',$id)->first();
            if($testi){
                $testi->status=0;
                $testi->save();
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    public function editView($id){
        $testi = Testimonials::where('testimonials_id','=',$id)->first();
        return view('TestimonialsManager::testimonials.edit')->with(['testi' => $testi]);
    }

    public function update(Request $request ,$id){

        $this->validate($request, [
            'txtText' => 'required|max:500'
        ]);

        $testi =Testimonials::find($id)->first();
        $testi->name=$request->txtName;
        $testi->testimonial=$request->txtText;
        $testi->url=$request->txtUrl;
        $testi->save();


        return redirect('testimonials/list')->with(['success' => true,
            'success.message' => 'Testimonials Edited successfully!',
            'success.title' => 'Well Done!']);
    }

    public function viewFront(Request $request)
    {
        if($request->has('id')){
            return Testimonials::select("name", "testimonial", "url")
            ->whereStatus(1)
            ->find($request->id);
        }
    }
}
