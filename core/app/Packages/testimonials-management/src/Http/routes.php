<?php

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'testimonials', 'namespace' => 'TestimonialsManager\Http\Controllers'], function () {

        Route::get('list', [
            'as' => 'testimonials.list', 'uses' => 'TestimonialsController@index'
        ]);

        Route::get('json/list', [
            'as' => 'testimonials.list', 'uses' => 'TestimonialsController@jsonList'
        ]);
        Route::get('add', [
            'as' => 'testimonials.add', 'uses' => 'TestimonialsController@add'
        ]);

        Route::get('edit/{id}', [
            'as' => 'testimonials.edit', 'uses' => 'TestimonialsController@editView'
        ]);


        Route::post('add', [
            'as' => 'testimonials.add', 'uses' => 'TestimonialsController@save'
        ]);

        Route::post('delete', [
            'as' => 'testimonials.delete', 'uses' => 'TestimonialsController@delete'
        ]);

        Route::post('edit/{id}', [
            'as' => 'testimonials.edit', 'uses' => 'TestimonialsController@update'
        ]);

    });
});