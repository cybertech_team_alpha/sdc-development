@extends('layouts.back.master') @section('current_title','New Series')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('series/list')}}">Series Management</a></li>
       
        <li class="active">
            <span>New Series</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                	{!!Form::token()!!}
                    <div class="form-group"><label class="col-sm-2 control-label">DEVICE <sup>*</sup></label>
                        <div class="col-sm-10">
                             <select class="js-source-states" style="width: 100%" name="device">
                                <?php foreach ($devices as $key => $value): ?>
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                <?php endforeach ?>
                                
                            </select>
                        </div>
                    </div>
                	<div class="form-group"><label class="col-sm-2 control-label">NAME</label>
                    	<div class="col-sm-10"><input type="text" class="form-control" name="name"></div>
                	</div>
                	<div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                         <div class="col-sm-10"><textarea name="desc" class="form-control"></textarea></div>
                    	
                	</div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">Images</label>
                        <div class="col-sm-10">
                            <input id="main-file" name="main-file" type="file" class="file-loading">
                        </div>
                    </div>
                	<div class="form-group"><label class="col-sm-2 control-label">LAUNCHED DATE</label>
                    	<div class="col-sm-2">
                            <div class="input-group date" >
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                <input type="text" id="date"  name="date" class="form-control" value="{{date('Y-m-d')}}"/>
                            </div>
                       </div>
                	</div>
                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Done</button>
	                    </div>
	                </div>
                	
                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".js-source-states").select2();
         $('.date').datepicker(
            {
                 format: 'yyyy-mm-dd',
            });
                $("#main-file").fileinput({
            uploadUrl: "", // server upload action
            uploadAsync: true,
            maxFileCount: 1,
            showUpload:false,
            allowedFileExtensions: ["jpg", "gif", "png"]
        });

		$("#form").validate({
            rules: {
                name: {
                    required: true
                  
                },
                code:{
                	required: true
                },
                city:{
                	required: true
                },               
                tel:{
                	digits:true
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
	});
	
	
</script>
@stop
