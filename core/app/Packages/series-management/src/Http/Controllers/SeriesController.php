<?php
namespace SeriesManage\Http\Controllers;

use App\Http\Controllers\Controller;
use SeriesManage\Http\Requests\SeriesRequest;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use SeriesManage\Models\Series;
use DeviceManage\Models\Device;
use Sentinel;
use Response;
use File;


class SeriesController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Series Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the Branch add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		$devices=Device::where('status','=',1)->get();	
		return view( 'SeriesManage::series.add')->with(['devices'=>$devices]);
	}

	/**
	 * Add new Branch data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add(SeriesRequest $request)
	{
		
        $count= Series::where('name', '=',$request->get('name' ))->count();
        if (!file_exists(storage_path('uploads/images/series'))) {
			 File::makeDirectory(storage_path('uploads/images/series'));
		}
		$path='uploads/images/series';
		$main_file = $request->file('main-file');

		
        if ($count==0) {
        	$fileName='';
			if ($request->hasFile('main-file')) {
					$file = $request->file('main-file');
					$extn =$file->getClientOriginalExtension();
					$destinationPath = storage_path($path);
					$fileName = 'series-' .date('YmdHis') .  '.' . $extn;
					$file->move($destinationPath, $fileName);
					
			 }
            $series = Series::Create(array(
                    'device_id' => $request->input('device'),
                    'path'=>$path,
				 	'filename'=>$fileName,
                    'name' => $request->input('name'),
                    'description' => $request->input('desc'),
                    'launch_date' => $request->input('date'),
                    'created_by'=>Sentinel::getUser()->id,
                )
            );

            if ($series) {
                return redirect('series/add')->with(['success' => true,
                    'success.message' => 'Series Created successfully!',
                    'success.title' => 'Well Done!']);
            }
        }
        else{
            return redirect('series/add')->with([ 'error' => true,
                'error.message'=> 'Series Already Exsist!',
                'error.title' => 'Duplicate!']);
        }
	}

	/**
	 * View Branch List View
	 *
	 * @return Response
	 */
	public function listView()
	{		
		return view( 'SeriesManage::series.list' );
	}

	/**
	 * Branch list
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$s= Series::with(['getDevice'])->where('status','=',1)->get();
			$jsonList = array();
			$i=1;
			foreach ($s as $key => $series) {
				
				$dd = array();
				array_push($dd, $i);
                array_push($dd,$series->name);
                array_push($dd,$series->getDevice->name);
                array_push($dd,$series->launch_date);

				$permissions = Permission::whereIn('name',['series.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('series/edit/'.$series->id).'\'" data-toggle="tooltip" data-placement="top" title="Series Branch"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['series.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="series-delete" data-id="'.$series->id.'" data-toggle="tooltip" data-placement="top" title="Delete Series"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate user
	 * @param  Request $request series id with status to change
	 * @return json object with status of success or failure
	 */
	public function status(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$branch = Branch::find($id);
			if($branch){
				$branch->status = $status;
				$branch->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete a series
	 * @param  Request $request series id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$series = Series::find($id);
			if($series){
                $series->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the series edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		$series=Series::find($id);
		$devices=Device::where('status','=',1)->get();	
		if($series){
			return view( 'SeriesManage::series.edit' )->with(['series'=>$series,'devices'=>$devices]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit(SeriesRequest $request, $id)
	{
		$count= Series::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();
		
			if($count==0){
                $series =  Series::find($id);
                $series->device_id = $request->get('device');
                $series->name = $request->get('name');
                $series->description = $request->get('desc');
                $series->launch_date = $request->get('date' );
                $image_hidden=$request->get('image_hidden');
				if($image_hidden){
					File::delete(storage_path($series->path . '/' . $series->filename));
				}
				$path='uploads/images/series';
				$fileName='';
				if ($request->hasFile('main-file')) {
						$file = $request->file('main-file');
						$extn =$file->getClientOriginalExtension();
						$destinationPath = storage_path($path);
						$fileName = 'series-' .date('YmdHis') .  '.' . $extn;
						$file->move($destinationPath, $fileName);
						$series->filename=$fileName;

						
				 }
				 
                $series->save();
				
				return redirect( 'series/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Series updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('series/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Series Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
   
		
	}
}
