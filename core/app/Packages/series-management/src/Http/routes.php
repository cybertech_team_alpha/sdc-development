<?php
/**
 * BRANCH MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'series', 'namespace' => 'SeriesManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'series.add', 'uses' => 'SeriesController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'series.edit', 'uses' => 'SeriesController@editView'
      ]);

      Route::get('list', [
        'as' => 'series.list', 'uses' => 'SeriesController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'series.list', 'uses' => 'SeriesController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'series.add', 'uses' => 'SeriesController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'series.edit', 'uses' => 'SeriesController@edit'
      ]);

      Route::post('status', [
        'as' => 'series.status', 'uses' => 'SeriesController@status'
      ]);

      Route::post('delete', [
        'as' => 'series.delete', 'uses' => 'SeriesController@delete'
      ]);
    });
});
