<?php

namespace SeriesManage;

use Illuminate\Support\ServiceProvider;

class SeriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'SeriesManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SeriesManage', function($app){
            return new SeriesManage;
        });
    }
}
