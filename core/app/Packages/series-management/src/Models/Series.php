<?php
namespace SeriesManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Series Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Chinthaka Dilan F <ccdilan@gmail.com>
 * @copyright  Copyright (c) 2015, Chinthaka Dilan F
 * @version    v1.0.0
 */
class Series extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_series';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['device_id','name', 'description', 'status','launch_date','created_by','path','filename'];

	public function getDevice()
	{
		return $this->belongsTo('DeviceManage\Models\Device', 'device_id', 'id');
	}


}
