@extends('layouts.back.master') @section('current_title','Update Gallery')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('gallery/list')}}">Gallery Management</a></li>
       
        <li class="active">
            <span>Gallery Series</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                    {!!Form::token()!!}
                    
                    <div class="form-group"><label class="col-sm-2 control-label">ALBUM NAME</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="album_name" value="{{$gallery->album_name}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                         <div class="col-sm-10"><textarea name="description" class="form-control">{{$gallery->description}}</textarea></div>
                        
                    </div>
                     <div class="form-group"><label class="col-sm-2 control-label">CATEGORY</label>
                         <div class="col-sm-10">
                         <select class="js-source-states" multiple="multiple" name="gallery_category[]" style="width: 100%">
                         <?php foreach ($galleryCategory as $key => $value): ?>
                          <?php if (count($cur_category)>0): ?>
                              <?php if (in_array($value->id, $cur_category)): ?>
                                <option selected="true" value="{{$value->id}}">{{$value->name}}</option>
                            <?php else: ?>
                                <option  value="{{$value->id}}">{{$value->name}}</option>
                            <?php endif ?>
                          <?php else: ?>
                            <option  value="{{$value->id}}">{{$value->name}}</option>
                              
                          <?php endif ?>
                            
                             
                         <?php endforeach ?>
                            
                            
                        </select>
                    </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">FEATURED IMAGE</label>
                        <div class="col-sm-10">
                        <input id="image_hidden" name="image_hidden" type="hidden" value="0" >
                            <input id="featured_image" name="featured_image" type="file" class="file-loading">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">SUB IMAGES</label>
                        <div class="col-sm-10">
                            <input id="sub_image" name="sub_image[]" type="file" multiple class="file-loading">
                        </div>
                    </div>
                    
                    
                    
                    <div class="hr-line-dashed"></div>
                	
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Save Changes</button>
	                    </div>
	                </div>
                	
                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".js-source-states").select2();
         
        $("#featured_image").fileinput({
            uploadUrl: "", // server upload action
            uploadAsync: true,
            maxFileCount: 1,
            showUpload:false,
            initialPreview: [
            "<img style='height:160px' src='{{ url('') . '/core/storage/' .$gallery->path.'/'.$gallery->filename}}'>"

            ],
            allowedFileExtensions: ["jpg", "gif", "png"]
        });
        $("#sub_image").fileinput({
            uploadUrl: "", // server upload action
            uploadAsync: true,
            maxFileCount: 5,
            showUpload:false,
            allowedFileExtensions: ["jpg", "gif", "png"],
            initialPreview: <?php echo json_encode($images ); ?>,
            initialPreviewConfig: <?php echo json_encode($image_config) ?>,

        });

        $("#form").validate({
            rules: {
                album_name: {
                    required: true

                }

            },
            submitHandler: function(form) {
                form.submit();
            }
        });
	});
      $('#featured_image').on('filecleared', function(event) {
      $('#image_hidden').val(1);
        console.log("filecleared");
    });


</script>
@stop
