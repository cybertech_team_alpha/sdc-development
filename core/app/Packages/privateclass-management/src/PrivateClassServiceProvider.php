<?php

namespace PrivateClassManage;

use Illuminate\Support\ServiceProvider;

class PrivateClassServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'PrivateClassManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PrivateClassManage', function($app){
            return new PrivateClassManage;
        });
    }
}
