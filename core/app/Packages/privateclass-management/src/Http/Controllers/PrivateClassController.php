<?php
namespace PrivateClassManage\Http\Controllers;

use App\Http\Controllers\Controller;
use PrivateClassManage\Http\Requests\PrivateClassRequest;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use PrivateClassManage\Models\PrivateClassCategory;
use PrivateClassManage\Models\PrivateClass;
use PrivateClassManage\Models\PrivateClassType;
use PrivateClassManage\Models\PrivateClassSkillLevel;
use PrivateClassManage\Models\PrivateClassRegistration;
// use GalleryManage\Models\Gallery;
use PrivateClassManage\Models\PrivateClassImage;
use PrivateClassManage\Models\PrivetclassDateTime;
// use DeviceManage\Models\Device;
use Sentinel;
use Response;
use File;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PrivateClassController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| PrivateClass Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the GALLERY CATEGORY add screen to the user.
	 *
	 * @return Response
	 */
	public function addView_category()
	{
		$privateClassType=PrivateClassType::get();
		$privateClassSkillLevel = PrivateClassSkillLevel::get();
		return view( 'PrivateClassManage::category.add')->with([
			'privateClassType'=>$privateClassType,
			'privateClassSkillLevel'=>$privateClassSkillLevel
		]);
	}

	/**
	 * Add new GALLERY CATEGORY data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add_category(PrivateClassRequest $request){
		$this_skill_level = $request->skill_level;
        $cat = $request->get('gallery_category');

        $count= PrivateClassCategory::where('name', '=',$request->get('name' ))->count();
        $id=0;
        $maxid = DB::table('sa_privateclass_category')->max('id');
        
        if($maxid != " "){            
            $maxid = $maxid+1;
        }else{
            $maxid = 1;
		}		
      
        if (!file_exists(storage_path('uploads/images/privateclass'))) {
			 File::makeDirectory(storage_path('uploads/images/privateclass'));
		}
		
		$path='uploads/images/privateclass';
		$featured_image = $request->file('featured_image');
                
        if ($count==0) {
        	$fileName = $this->base64_img_upload($request->featuredCrop);    

            $privateclassCategory = PrivateClassCategory::Create([
				'id'  => $maxid,
				'name' => $request->input('name'),
				'description' => $request->input('description'),				
				'skill_level' => $this_skill_level,				
				'noofseat' => $request->input('noofseat'),
				'price' => $request->input('price'),
				'path' => $path,
				'filename' => $fileName,
				'created_by' => Sentinel::getUser()->id
            ]);		  		  
		  
            if ($privateclassCategory) {
				/* this part is unnecessary*/

                /*$project_files = $request->file('sub_image');
                $i = 0;
                if (count($project_files) > 0) {
                    foreach ($project_files as $key => $project_file) {
                        if (File::exists($project_file)) {
                            $file = $project_file;
                            $extn = $file->getClientOriginalExtension();
                            $destinationPath = storage_path($path);
                            $project_fileName = 'privateclass-sub-' . date('YmdHis') . '-' . $i . '.' . $extn;
                            $file->move($destinationPath, $project_fileName);

                            PrivateClassImage::create([
                                'privateclass_id' => $privateclassCategory->id,
                                'path' => $path,
                                'filename' => $project_fileName
                            ]);
                            $i++;
                        }
                    }
				}*/				
				
                
                $gettime = $request->input('datetime');
                $fromtime = $request->input('fromtime');
                $totime = $request->input('totime');
               
                if (count($gettime) > 0) {
                    foreach ($gettime as $key => $gettimes) {                        
                        $table = new PrivetclassDateTime();                        
                            
                        PrivetclassDateTime::create([
                            'privateclass_cat_id' => $maxid,
                            'date' =>$gettimes,
                            'start' => $fromtime[$key],
                            'end' => $totime[$key]
                        ]);                           
                    }
                }

                return redirect('privateclass/category/add')->with(['success' => true,
                    'success.message' => 'Category  Created successfully!',
                    'success.title' => 'Well Done!']);
            }

        }else{
            return redirect('privateclass/category/add')->with([ 'error' => true,
                'error.message'=> 'Category Already Exsist!',
                'error.title' => 'Duplicate!']);
        }
	}

	/**
	 * View GALLERY CATEGORY List View
	 *
	 * @return Response
	 */
	public function listView_category()
	{
		return view( 'PrivateClassManage::category.list' );
	}

	/**
	 * GALLERY CATEGORY list
	 *
	 * @return Response
	 */
	public function jsonList_category(Request $request)
	{
		if($request->ajax()){
			$privateClassCategories= PrivateClassCategory::where('status','=',1)->get();
			$jsonList = array();
			$i=1;
			foreach ($privateClassCategories as $key => $privateClassCategory) {

				$dd = array();
				array_push($dd, $i);
                array_push($dd,$privateClassCategory->name);
                array_push($dd,$privateClassCategory->datetime);
                array_push($dd,$privateClassCategory->noofseat);
                array_push($dd,$privateClassCategory->price);

				$permissions = Permission::whereIn('name',['privateclass.category.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('privateclass/category/edit/'.$privateClassCategory->id).'\'" data-toggle="tooltip" data-placement="top" title="privateclass Category Branch"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['privateclass.category.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="privateclass-category-delete" data-id="'.$privateClassCategory->id.'" data-toggle="tooltip" data-placement="top" title="Delete Series" onClick="deleteMe('.$privateClassCategory->id.')"><i class="fa fa-trash-o"></i></a></center>'); 
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate user
	 * @param  Request $request series id with status to change
	 * @return json object with status of success or failure
	 */
	public function status_category(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$branch = Branch::find($id);
			if($branch){
				$branch->status = $status;
				$branch->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete a GALLERY CATEGORY
	 * @param  Request $request series id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete_category(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$privateClassCategory = PrivateClassCategory::find($id);
			if($privateClassCategory){
                $privateClassCategory->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the PRIVATE CLASS CATEGORY edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView_category($id)
	{
		$privateClassType=PrivateClassType::get();
		$privateClassCategory=PrivateClassCategory::find($id);
		$PrivetclassDateTime=PrivetclassDateTime::where('privateclass_cat_id', '=', $id)->get();
		// dd($PrivetclassDateTime);
		if($privateClassCategory){
			$cur_category=json_decode($privateClassCategory->category);
			$image_url_array=[];
			 $image_config_array=[];
			  foreach ($privateClassCategory->getImages as $key => $value) {
					array_push($image_url_array,"<img style='height:160px' src='". url('') . '/core/storage/' .$value->path.'/'.$value->filename."'>");

			  }

			  foreach ($privateClassCategory->getImages as $key => $value) {
					array_push($image_config_array,array('caption'=>$value->filename,'caption'=>$value->filename,'key'=>$value->id,'url'=>url('privateclass/category/image/deleteFile')));

			  }
			  $privateClassSkillLevel = PrivateClassSkillLevel::get();
			return view( 'PrivateClassManage::category.edit' )->with(['privateClassCategory'=>$privateClassCategory,'privateClassType'=>$privateClassType,'privateClassSkillLevel'=>$privateClassSkillLevel,'PrivetclassDateTime'=>$PrivetclassDateTime,'cur_category'=>$cur_category,'images'=>$image_url_array,'image_config'=>$image_config_array]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new PRIVATE CLASS CATEGORY data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit_category(PrivateClassRequest $request, $id)
	{
//	    dd($request);
		if (!file_exists(storage_path('uploads/images/privateclass'))) {

			File::makeDirectory(storage_path('uploads/images/privateclass'));
		 }

        DB::table('sa_privateclass_category')
            ->where('id', $id)
            ->update(['name' => $request->get('name'),
                      'description' => $request->get('description'),
                      'noofseat' => $request->get('noofseat'),
                      'skill_level' => $request->get('skill_level'),
                      'price' => $request->get('price')]);
		$count= PrivateClassCategory::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();
//dd($request);
			if($count==0){
//				$cat= $request->get('class_category');
                $privateClassCategory =  PrivateClassCategory::find($id);
				if($request->featuredCrop != ""){
//				    dd($request);
					File::delete(storage_path($privateClassCategory->path . '/' . $privateClassCategory->filename));
					$fileName= $this->base64_img_upload($request->featuredCrop);
					 DB::table('sa_privateclass_category')
                        ->where('id', $id)
                        ->update(['filename' => $fileName]);
				}
				$path='uploads/images/privateclass';

				// $fileName='';
				// if ($request->hasFile('featured_image')) {
				// 		$file = $request->file('featured_image');
				// 		$extn =$file->getClientOriginalExtension();
				// 		$destinationPath = storage_path($path);
				// 		$fileName = 'privateclass-cover-' .date('YmdHis') .  '.' . $extn;
				// 		$file->move($destinationPath, $fileName);
                //     DB::table('sa_privateclass_category')
                //         ->where('id', $id)
                //         ->update(['filename' => $fileName]);
				//  }

				/*
                $project_files=[];
                if ($request->hasFile('sub_image')) {
                    $project_files = $request->file('sub_image');

                    DB::table('sa_privateclass_image')->where('privateclass_id', '=', $id)->delete();
                }
				$i=0;
				if(count($project_files)>0){
					foreach ($project_files as $key => $project_file) {
						if (File::exists($project_file)) {
								$file = $project_file;
								$extn =$file->getClientOriginalExtension();
								$destinationPath = storage_path($path);
								$project_fileName = 'privateclass-sub-' .date('YmdHis') .'-'.$i. '.' . $extn;
								$file->move($destinationPath, $project_fileName);
								PrivateClassImage::create([
									'privateclass_id'=>$id,
									'path'=>$path,
									'filename'=>$project_fileName
								]);
								$i++;
						 }
					}
				}
				*/

                DB::table('sa_privetclass_date_time')->where('privateclass_cat_id', '=', $id)->delete();

                $gettime = $request->input('datetime');
                $fromtime = $request->input('fromtime');
                $totime = $request->input('totime');
                //    var_dump($request->input('fromtime'));die;

                if (count($gettime) > 0) {

                    foreach ($gettime as $key => $gettimes) {

                        $table = new PrivetclassDateTime();

                        //  var_dump($request->input('fromtime'));die;
                        //    var_dump($gettime);die;
                        PrivetclassDateTime::create([
                            'privateclass_cat_id' => $id,
                            'date' =>$gettimes,
                            'start' => $fromtime[$key],
                            'end' => $totime[$key]
                        ]);


                    }
                };

				return redirect( 'privateclass/category/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Private Class Category updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('privateclass/category/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Private Class Category Already Exsist!',
				'error.title' => 'Duplicate!']);
			}




	}
	public function jsonImageFileDelete_category(Request $request)
	{
		$image_id=$request->get('key');
		$image=PrivateClassImage::find($image_id);
		$image->delete();
		return 1;
	}

	/****************************************PRIVATE CLASS ******/

	/**
	 * View Private Class  List View
	 *
	 * @return Response
	 */
	public function listView()
	{
		return view( 'PrivateClassManage::privateclass.list' );
	}

	/**
	 * PRIVATE CLASS list
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$privateClassAll= PrivateClassRegistration::get();
			$jsonList = array();
			$i=1;
			foreach ($privateClassAll as $key => $privateClass) {

				$dd = array();
				array_push($dd, $i);
                array_push($dd,$privateClass->first_name . ' ' . $privateClass->last_name);
                array_push($dd,$privateClass->email);
                array_push($dd,$privateClass->phone);
                array_push($dd,$privateClass->parent_full_name);
                $datetime = PrivetclassDateTime::find($privateClass->class_datetime_id)->date;
                $start = PrivetclassDateTime::find($privateClass->class_datetime_id)->start;
                $end = PrivetclassDateTime::find($privateClass->class_datetime_id)->end;
                array_push($dd, $datetime . ' ' . $start . ' to ' . $end);
                if ($privateClass->status=='0') {
                	array_push($dd,'Pending');
                	array_push($dd, '<button class="btn btn-success btn-xs blue class-approve" data-id='.$privateClass->id.' data-toggle="tooltip" data-placement="top" title="Approve "><i class="fa fa-check" style="color: green;"></i> Approve</button><button class="btn btn-danger btn-xs blue class-reject" data-id='.$privateClass->id.' data-toggle="tooltip" data-placement="top" title="Reject"><i class="fa fa-times" style="color: red;"></i> Reject</button>');
                }
                if ($privateClass->status=='1') {
                	array_push($dd,'Approved');
                	array_push($dd, '<center><button class="btn btn-danger btn-xs blue class-reject" data-id='.$privateClass->id.' data-toggle="tooltip" data-placement="top" title="Reject"><i class="fa fa-times" style="color: red;"></i> Reject</button></center>');
                }
                if ($privateClass->status=='2') {
                	array_push($dd,'Rejected');
                	array_push($dd, '<center><button class="btn btn-success btn-xs blue class-approve" data-id='.$privateClass->id.' data-toggle="tooltip" data-placement="top" title="Approve "><i class="fa fa-check" style="color: green;"></i> Approve</button></center>');
                }

				$permissions = Permission::whereIn('name',['privateclass.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="class-delete" data-id="'.$privateClass->id.'" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data' => $jsonList));
		}else{
			return Response::json(array('data' => []));
		}
	}

	public function delete(Request $request){
		if($request->ajax()){
			$id = $request->input('id');

			$privateClass = PrivateClassRegistration::find($id);
			if($privateClass){
                $privateClass->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * PRIVATE CLASS Registration approval for admin
	 *
	 * @return Response
	 */
	public function registrationListView()
	{
		if (Sentinel::getUser()->id == 1) {
			return view( 'PrivateClassManage::registration.list' );
		} else {
			return view( 'errors.404' );
		}
	}

	public function registrationJsonList(Request $request)
	{
		if($request->ajax()){
			$privateClassAll= PrivateClassRegistration::get();
			$jsonList = array();
			$i=1;
			foreach ($privateClassAll as $key => $privateClass) {

				$dd = array();
				array_push($dd, $i);
				if ($privateClass->class != "") {
					array_push($dd,$privateClass->class);
				} else {
					array_push($dd,"-");
				}
				if ($privateClass->parent_is_reg != 1) {
					array_push($dd,"Yes");
				} else {
					array_push($dd,"No");
				}
                if ($privateClass->parent_full_name != "") {
                	array_push($dd,$privateClass->parent_full_name);
                } else {
                	array_push($dd,"-");
                }
                if ($privateClass->first_name != "") {
                	array_push($dd,$privateClass->first_name);
                } else {
                	array_push($dd,"-");
                }
                if ($privateClass->last_name != "") {
                	array_push($dd,$privateClass->last_name);
                } else {
                	array_push($dd,"-");
                }
                if ($privateClass->email != "") {
                	array_push($dd,$privateClass->email);
                } else {
                	array_push($dd,"-");
                }
                if ($privateClass->phone != "") {
                	array_push($dd,$privateClass->phone);
                } else {
                	array_push($dd,"-");
                }
                if ($privateClass->status == 1) {
                	array_push($dd, '<center>
                	<button type="button" class="btn btn-primary status" onclick="approveStatus('.$privateClass->id.')">Approve</button>
                	<button type="button" class="btn btn-warning status" onclick="rejectStatus('.$privateClass->id.')"> Reject </button></center>');
                } else if ($privateClass->status == 2) {
                	array_push($dd, '<center>
                	<button type="button" class="btn btn-success status">Approved</button>
                	</center>');
                } else {
                	array_push($dd, '<center>
                	<button type="button" class="btn btn-danger status">Rejected</button>
                	</center>');
                }
                

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}

	public function changeStatus(Request $request)
	{
		if ($request->ajax()) {
			$id = $request->id;
			$status = $request->status;

			PrivateClassRegistration::where('id', $id)->update(['status'=>$status]);
			return;
		} else {
			return;
		}
	}

	public function base64_img_upload($img_data){//save base64 encoded image
	try {
		$newname = str_random(10).".png";
		$data = $img_data;
		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);


		$save_target = "core/storage/uploads/images/privateclass/".$newname;

		$target = $save_target;


		if(file_put_contents($target, $data)){
			$img = imagecreatefrompng($target);
			imagealphablending($img, false);


			imagesavealpha($img, true);

			imagepng($img, $target, 8);
			return $newname;
		}else{
			return "error";
		}
	} catch (Exception $e) {
		return "error";
	}



}

}
