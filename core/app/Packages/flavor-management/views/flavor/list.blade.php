@extends('layouts.back.master') @section('current_title','All Flavors')
@section('css')
<style type="text/css">
    #floating-button{
      width: 55px;
      height: 55px;
      border-radius: 50%;
      background: #db4437;
      position: fixed;
      bottom: 50px;
      right: 30px;
      cursor: pointer;
      box-shadow: 0px 2px 5px #666;
      z-index:2
    }

    .plus{
      color: white;
      position: absolute;
      top: 0;
      display: block;
      bottom: 0;
      left: 0;
      right: 0;
      text-align: center;
      padding: 0;
      margin: 0;
      line-height: 55px;
      font-size: 38px;
      font-family: 'Roboto';
      font-weight: 300;
      animation: plus-out 0.3s;
      transition: all 0.3s;
    }
</style>

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('flavor/list')}}">Flavor Management</a></li>
       
        <li class="active">
            <span>Flavor List</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
             	<table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                      <tr>
                          <th rowspan="2" width="4%">#</th>
                          <th rowspan="2"> Name</th>                        
                          <th rowspan="2"> Description</th>                        
                          
                          <th colspan="2" class="text-center" width="4%" style="font-weight:normal;">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                         @foreach ($flavors as $el)
                          <tr>
                            <td>{{$el->id}}</td>
                            <td>{{$el->flavor_name}}</td>
                            <td><?php echo $el->description; ?></td>
                            <td>
                                <a href="{{url('flavor/edit/'.$el->id)}}" class="blue"
                             data-toggle="tooltip" data-placement="top" title="Edit Flavor">
                                <i class="fa fa-pencil"></i></a>
                            </td>
                            
                          </tr>
                      @endforeach
                    </tbody>
                </table>
        	</div>
    	</div>
	</div>
</div>
@stop
@section('js')



@stop
