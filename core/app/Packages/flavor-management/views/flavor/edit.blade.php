@extends('layouts.back.master') @section('current_title','Update Flavor')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" media="all" />
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('flavor/list')}}">Flavor Management</a></li>

        <li class="active">
            <span>Update Flavor</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul class="">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
              @endif
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                	{!!Form::token()!!}


                 @if ($flavor->id != 1)
                    <div class="form-group"><label class="col-sm-2 control-label">NAME</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="name" value="{{$flavor->flavor_name}}"></div>
                    </div>

                  
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">IMAGE</label>
                        <div class="col-sm-10">
                            <input id="image_hidden" name="image_hidden" type="hidden" value="0" >
                            <input id="front_img" name="front_image" type="file" class="images-file file-loading">
                        </div>
                    </div>
                  @endif
                    
                    
                    <div class="form-group"><label class="col-sm-2 control-label">{{ $flavor->id == 1 ? 'Notification Message' : 'DESCRIPTION' }}</label>
                         <div class="col-sm-10"><textarea name="description" class="form-control">{{$flavor->description}}</textarea></div>

                    </div>

                  @if ($flavor->id != 1)
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">BASIC FLAVORS</label>
                        <div class="col-sm-10">
                            <table class="table table-bordered" id="bf_field">  
                                <tr>  
                                    <td><input type="text" name="bf_name[]" placeholder="Enter flavor name" class="form-control name_list" /></td>  
                                    <td><button type="button" name="bf-add" id="bf-add" class="btn btn-success">Add More</button></td>  
                                </tr> 
                                
                                @if ($flavor->basicTypes->count() > 0)
                                    @foreach ($flavor->basicTypes as $el)
                                        <tr id="row" class="dynamic-added">
                                            <td><input type="text" name="bf_name[]" value="{{$el->name}}" placeholder="Enter flavor name" class="form-control name_list" /></td>
                                            <td><button type="button" name="remove" onclick="delete_row(this)" class="btn btn-danger">X</button></td>
                                        </tr>
                                    @endforeach
                                    
                                @endif 
                            </table>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label required">PREMIUM FLAVORS</label>
                        <div class="col-sm-10">
                            <table class="table table-bordered" id="pf_field">  
                                <tr>  
                                    <td><input type="text" name="pf_name[]" placeholder="Enter flavor name" class="form-control name_list" /></td>  
                                    <td><button type="button" name="pf-add" id="pf-add" class="btn btn-success">Add More</button></td>  
                                </tr>
                                @if ($flavor->premiumTypes->count() > 0)
                                    @foreach ($flavor->premiumTypes as $el)
                                        <tr id="row" class="dynamic-added">
                                            <td><input type="text" name="pf_name[]" value="{{$el->name}}" placeholder="Enter flavor name" class="form-control name_list" /></td>
                                            <td><button type="button" name="remove" onclick="delete_row(this)" class="btn btn-danger">X</button></td>
                                        </tr>
                                    @endforeach
                                    
                                @endif 
                            </table>
                        </div>
                    </div>
                    @endif 

                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Save Changes</button>
	                    </div>
	                </div>

                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
                <script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>


{{--  add more fields  --}}

<script type="text/javascript">
    function delete_row(e)
    {
        e.parentNode.parentNode.parentNode.removeChild(e.parentNode.parentNode);
    }
    //basic flavors
    var b=1;  
     $('#bf-add').click(function(){  
           b++;  
           $('#bf_field').append('<tr id="row'+b+'" class="dynamic-added"><td><input type="text" name="bf_name[]" placeholder="Enter flavor name" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+b+'" class="btn btn-danger bf_btn_remove">X</button></td></tr>');  
      });
      $(document).on('click', '.bf_btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  

      //premium flavors
      var p=1;  
     $('#pf-add').click(function(){  
           p++;  
           $('#pf_field').append('<tr id="row'+p+'" class="dynamic-added"><td><input type="text" name="pf_name[]" placeholder="Enter flavor name" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+p+'" class="btn btn-danger pf_btn_remove">X</button></td></tr>');  
      });
      $(document).on('click', '.pf_btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
</script>

<script type="text/javascript">
	$(document).ready(function(){
        $("#input-1").rating();
		$(".js-source-states").select2();
           $('.date').datepicker(
            {
                 format: 'yyyy-mm-dd',
            });

        var isAfterStartDate = function(startDateStr, endDateStr) {

            if(new Date(startDateStr).getTime() > new Date(endDateStr).getTime()) {

                return false;
            }
            else
                return true;

        };
        jQuery.validator.addMethod("isAfterStartDate", function(value, element) {

                return isAfterStartDate($('#startdate').val(), value);
            }, "End date should be after start date");


		$("#form").validate({
            rules: {
                name: {
                    required: true

                },
                
                description:{
                    required : true
                },


            },
            submitHandler: function(form) {
                form.submit();
            }
        });


	});
     $("#front_img").fileinput({
        uploadUrl: "", // server upload action
        uploadAsync: true,
        maxFileCount: 1,
        showUpload:false,
        initialPreview: [
        "<img style='height:160px' src='{{ url('') . '/core/storage/uploads/images/flavors/'.$flavor->img}}'>"


        ],
        allowedFileExtensions: ["jpg", "gif", "png"]
    });
    
     
       $('#front_img').on('filecleared', function(event) {
          $('#image_hidden').val(1);
            console.log("filecleared");
        });
       



      tinymce.init({
        selector: 'textarea',  // change this value according to your HTML
        plugins: [
            'advlist autolink lists link image charmap preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code',
            'insertdatetime media nonbreaking table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
        ],
        toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
        '| bullist numlist outdent indent | link | undo redo | forecolor backcolor emoticons ',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        menubar: false
    });


</script>
@stop
