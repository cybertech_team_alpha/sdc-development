<?php

namespace FlavorManage;

use Illuminate\Support\ServiceProvider;

class FlavorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'FlavorManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('FlavorManage', function($app){
            return new FlavorManage;
        });
    }
}
