<?php

namespace FlavorManage\Models;

use Illuminate\Database\Eloquent\Model;

class FlavorType extends Model
{
    protected $table = 'flavor_types';

    
    public function flavors()
    {
        return $this->belongsTo(Flavor::class);
    }
    

}
