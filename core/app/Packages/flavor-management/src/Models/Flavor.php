<?php
namespace FlavorManage\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;


class Flavor extends Model{
	// use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'flavors';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['flavor_name', 'description', 'img'];

	
	public function basicTypes(){
		return $this->hasMany( FlavorType::class , 'flavor_id', 'id')->where('type', 'basic');
	}
	public function premiumTypes(){
		return $this->hasMany( FlavorType::class , 'flavor_id', 'id')->where('type', 'premium');
	}


}
