<?php

namespace FlavorManage\Http\Controllers;

use App\Http\Controllers\Controller;
use FlavorManage\Http\Requests\FlavorRequest;
use Illuminate\Http\Request;
use FlavorManage\Models\Flavor;
// use BlogManage\Models\BlogImage;
use Permissions\Models\Permission;
use Sentinel;
use Response;
use File;
use Image;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
class FlavorController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Flavor Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest');
    }

     /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function addView() {
     
      return view('FlavorManage::flavor.add' );
      
      
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      try {
        $flavor=Flavor::with('basicTypes', 'premiumTypes')->where('id', $id)->first();
         return view('FlavorManage::flavor.edit' )->with(['flavor'=>$flavor]);
      } catch (ModelNotFoundException $e) {
         return view('errors.404');
      }
     
      
      
    }
      /**
     * View Flavor  List View
     *
     * @return Response
     */
    public function listView()
    {
      $flavors = Flavor::all();
      return view( 'FlavorManage::flavor.list', compact('flavors'));
    }
    /**
   * Honor  list
   *
   * @return Response
   */
  public function jsonList(Request $request)
  {
    if($request->ajax()){
      $allHonor= Honor::where('status','=',1)->get();
      $jsonList = array();
      $i=1;
      foreach ($allHonor as $key => $honor) {

        $dd = array();
        array_push($dd, $i);
        array_push($dd,$honor->name);
        array_push($dd,$honor->description);

        $permissions = Permission::whereIn('name',['honor.edit','admin'])->where('status','=',1)->lists('name');
        if(Sentinel::hasAnyAccess($permissions)){
          array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('honor/edit/'.$honor->id).'\'" data-toggle="tooltip" data-placement="top" title="Honor"><i class="fa fa-pencil"></i></a></center>');
        }else{
          array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
        }

        $permissions = Permission::whereIn('name',['honor.delete','admin'])->where('status','=',1)->lists('name');
        if(Sentinel::hasAnyAccess($permissions)){
          array_push($dd, '<center><a href="#" class="honor-delete" data-id="'.$honor->id.'" data-toggle="tooltip" data-placement="top" title="Delete Honor"><i class="fa fa-trash-o"></i></a></center>');
        }else{
          array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
        }

        array_push($jsonList, $dd);
        $i++;
      }
      return Response::json(array('data'=>$jsonList));
    }else{
      return Response::json(array('data'=>[]));
    }
  }

    public function add(Request $request)
    {
      // return $request->all();
       if (!file_exists(storage_path('uploads/images/honor'))) {

        File::makeDirectory(storage_path('uploads/images/honor'));
      }
      $path='uploads/images/honor';
      $fileName='';

       if ($request->hasFile('front_image')) {
          $file = $request->file('front_image');
          $extn =$file->getClientOriginalExtension();
          $destinationPath = storage_path($path);
          $fileName = 'honor-' .date('YmdHis') .  '.' . $extn;
          $file->move($destinationPath, $fileName); 
       }
      $honor=Honor::create([
          'name'=>$request->get('name'),
          'description'=>$request->get('description'),
          'filename'=>$fileName,
          'path'=>$path,
          'url'=>$request->get('url'),
          'more_link'=>$request->get('more_link'),
          'type'=>$request->get('type')
        ]);
       return redirect( 'honor/list')->with([ 'success' => true,
          'success.message'=> 'Honor Created successfully!',
          'success.title' => 'Good Job!' ]);
     
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request,$id) {

      // dd($request->all());

      if (!file_exists(storage_path('uploads/images/flavors'))) {

        File::makeDirectory(storage_path('uploads/images/flavors'));
     }
      $this->validate($request, [
        'video_url' => 'nullable|regex:/^(?:https?:\/\/)?(?:www[.])?(?:youtube[.]com\/watch[?]v=|youtu[.]be\/)([^&]{11})$/',
      ]);

      $path='uploads/images/flavors';
      $fileName='';

      $flavor=Flavor::find($id);
      $flavor->flavor_name=$request->get('name');
      $flavor->description=$request->get('description');
      
      if ($request->hasFile('front_image')) {
          $file = $request->file('front_image');
          $extn =$file->getClientOriginalExtension();
          $destinationPath = storage_path($path);
          
          $fileName = 'flavor-' .date('YmdHis') .  '.' . $extn;
          $file->move($destinationPath, $fileName); 
          $flavor->img = $fileName;

       }
       $flavor->save();
       
      DB::table('flavor_types')->where('flavor_id', $flavor->id)->delete();

       if(isset($request->bf_name)){
          foreach($request->bf_name as $el){
            if ($el != null) {
              DB::table('flavor_types')->insert([
              'flavor_id' => $flavor->id,
              'name' => $el,
              'type' => 'basic'
            ]);
            }
          }
       }

       if(isset($request->pf_name)){
          foreach($request->pf_name as $el){
            if ($el != null) {
              DB::table('flavor_types')->insert([
              'flavor_id' => $flavor->id,
              'name' => $el,
              'type' => 'premium'
            ]);
            }
          }
       }
       
      return redirect( 'flavor/edit/'.$id )->with([ 'success' => true,
          'success.message'=> 'flavor  updated successfully!',
          'success.title' => 'Good Job!' ]);
    }
    public function delete(Request $request)
      {
        if($request->ajax()){
          $id = $request->input('id');

          $honor = Honor::find($id);
          if($honor){
                    $honor->delete();
            return response()->json(['status' => 'success']);
          }else{
            return response()->json(['status' => 'invalid_id']);
          }
        }else{
          return response()->json(['status' => 'not_ajax']);
        }
      }

}
