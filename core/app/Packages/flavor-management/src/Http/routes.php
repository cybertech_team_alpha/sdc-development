<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2018 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'flavor', 'namespace' => 'FlavorManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      

      // Route::get('add', [
      //   'as' => 'flavor.add', 'uses' => 'FlavorController@addView'
      // ]);
      Route::get('edit/{id}', [
        'as' => 'flavor.edit', 'uses' => 'FlavorController@editView'
      ]);   
       Route::get('list', [
        'as' => 'flavor.list', 'uses' => 'FlavorController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'flavor.list', 'uses' => 'FlavorController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'flavor.add', 'uses' => 'FlavorController@add'
      ]);
      Route::post('edit/{id}', [
        'as' => 'flavor.edit', 'uses' => 'FlavorController@edit'
      ]);

      Route::post('delete', [
        'as' => 'flavor.delete', 'uses' => 'FlavorController@delete'
      ]);

     

    });
});