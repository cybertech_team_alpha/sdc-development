<?php

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'slider', 'namespace' => 'SliderManager\Http\Controllers'], function () {

        Route::get('list', [
            'as' => 'slider.list', 'uses' => 'SliderController@SliderList'
        ]);

        Route::get('json/list', [
            'as' => 'slider.list', 'uses' => 'SliderController@jsonList'
        ]);
        Route::get('add', [
            'as' => 'slider.add', 'uses' => 'SliderController@add'
        ]);

        Route::get('edit/{id}', [
            'as' => 'slider.edit', 'uses' => 'SliderController@editView'
        ]);

        Route::get('image/{id}', [
            'as' => 'slider.edit', 'uses' => 'SliderController@getImage'
        ]);


        Route::post('add', [
            'as' => 'slider.add', 'uses' => 'SliderController@save'
        ]);

        Route::post('delete', [
            'as' => 'slider.delete', 'uses' => 'SliderController@delete'
        ]);

        Route::post('edit/{id}', [
            'as' => 'slider.edit', 'uses' => 'SliderController@update'
        ]);

    });
});