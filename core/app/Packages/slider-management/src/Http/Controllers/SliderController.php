<?php

namespace SliderManager\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Input;
use Illuminate\Support\Facades\Storage;
use Response;
use Sentinel;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use SliderManager\Models\Slider;
use Validator;
use Image;
use SliderManager\Models\AnimationType;


class SliderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $imagePath = 'uploads/images/main_slider';
    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function SliderList()
    {
        return view('SliderManager::slider.list');
    }

    public function add()
    {
        $animation_type = AnimationType::where('status', '=', 1)->get();
        return view('SliderManager::slider.add')->with(['aType' => $animation_type]);
    }

    public function save(Request $request)
    {
        $mainImage = $request->file('sliderImg');
        $mainImageName = $this->uploadImage($mainImage);
        
        $slide = new Slider();
        $slide->animation_type = $request->aType;
        $slide->image = $mainImageName;
        $slide->animation_txt = $request->aText;
        $slide->message_type = $request->message_type;
        
        if($request->message_type == "image"){
            $slide->img_position = $request->imgposition;
            $firstLineImage = $request->file('firstLineImage');
            $slide->message_first_line_img =  $this->uploadImage($firstLineImage);
            $slide->top_first = $request->firstLineTop;
            $slide->maxwidth_first = $request->firstLineWidth;

            
            $secondLineImage = $request->file('secondLineImage');
            $slide->message_second_line_img = $this->uploadImage($secondLineImage);
            $slide->top_second = $request->secondLineTop;
            $slide->maxwidth_second = $request->secondLineWidth;
            
            if($request->hasFile('thirdLineImage')){
                $thirdLineImage = $request->file('thirdLineImage');
                $slide->message_third_line_img =  $this->uploadImage($thirdLineImage);
                $slide->top_third = $request->thirdLineTop;
                $slide->maxwidth_third = $request->thirdLineWidth;
            }
            if($request->hasFile('fourthLineImage')){
                $fourthLineImage = $request->file('fourthLineImage');
               $slide->message_fourth_line_img = $this->uploadImage($fourthLineImage);
                $slide->top_fourth = $request->fourthLineTop;
                $slide->maxwidth_fourth = $request->fourthLineWidth;
            }
        }

        $buttonImage = $request->file('buttonImage');
        $buttonImageName = $this->uploadImage($buttonImage);
        
        $slide->btn_animation_type = $request->btnAniType;
        $slide->btn_image = $buttonImageName;
        $slide->btn_url = $request->btnUrl;
        $slide->top_button = $request->btnTop;
        $slide->maxwidth_button = $request->btnWidth;
        if($slide->btn_animation_type == 'center'){
            $slide->center_btn = $request->btnCenter;
        }
        $slide->save();

        return redirect('slider/list')->with(['success' => true,
            'success.message' => 'Slide Created successfully!',
            'success.title' => 'Well Done!']);

    }

    function uploadImage($file){
         $path = $this->imagePath;
        $destinationPath = storage_path($path);

        $extn =$file->getClientOriginalExtension();
        $imageName = 'main-slider-' .str_random(12) .'.' . $extn;

        $file->move($destinationPath, $imageName);

        return $imageName;
    }

    public function jsonList(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('slides')
                ->join('animation_type','animation_type','=','animation_type_id')
                ->where('slides.status','=',1)
                ->get();
//            $data = Slider::with(['animationType'])->where('status', '=', 1)->get();
            $jsonList = array();
            $i = 1;
            $path = '/SDCadmin/core/storage/'.$this->imagePath;

            foreach ($data as $key => $slide) {
                $dd = array();
                array_push($dd, $i);
//                array_push($dd, '<img height="100px" src="' . $path . $slide->image . '"/>');
//                array_push($dd, ' <button class="btn btn-default" style="background-color:'.$slide->btn_clr.';color: '.$slide->btn_txt_color.'">'. $slide->btn_txt.'</button>');

                array_push($dd, '<a class="btn btn-xs btn-success sliderImage" href="#" role="button" data-id=" ' .$slide->slides_id .'">Show Image</a>');
                array_push($dd, $slide->type);
                array_push($dd, $slide->message_type);
                array_push($dd, $slide->btn_url);


                $permissions = Permission::whereIn('name', ['slider.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('slider/edit/' . $slide->slides_id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['slider.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="slider-delete" data-id="' . $slide->slides_id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    public function delete(Request $request){
        if($request->ajax()){
            $id = $request->input('id');
            $slide = Slider::where('slides_id','=',$id)->first();
            if($slide){
                $slide->status=0;
                $slide->save();
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    public function editView($id){
       // $slide = Slider::with(['animationType'])->where('slides_id','=',$id)->first();
        $slide = DB::table('slides')
            ->join('animation_type','animation_type','=','animation_type_id')
            ->where('slides_id','=',$id)
            ->first();
        $animation_type = AnimationType::where('status', '=', 1)->get();

        $mainImage=[];
        $mainImage['preview'] = "<img style='height:160px' src='". url('') . '/core/storage/' .$this->imagePath.'/'.$slide->image."'>";
        $mainImage['previewConfig'] = array('caption'=>$slide->image,'caption'=>$slide->image,'key'=>$slide->image);

        $btnImage=[];
        $btnImage['preview'] = "<img style='height:160px' src='". url('') . '/core/storage/' .$this->imagePath.'/'.$slide->btn_image."'>";
        $btnImage['previewConfig'] = array('caption'=>$slide->btn_image,'caption'=>$slide->btn_image,'key'=>$slide->btn_image);
    
        $firstLineImage=[];
        $secondLineImage=[];
        $thirdLineImage=[];
        $fourthLineImage=[];
        if($slide->message_type == "image"){
            $firstLineImage['preview'] = "<img style='height:160px' src='". url('') . '/core/storage/' .$this->imagePath.'/'.$slide->message_first_line_img."'>";
            $firstLineImage['previewConfig'] = array('caption'=>$slide->message_first_line_img,'caption'=>$slide->message_first_line_img,'key'=>$slide->message_first_line_img);

            $secondLineImage['preview'] = "<img style='height:160px' src='". url('') . '/core/storage/' .$this->imagePath.'/'.$slide->message_second_line_img."'>";
            $secondLineImage['previewConfig'] = array('caption'=>$slide->message_second_line_img,'caption'=>$slide->message_second_line_img,'key'=>$slide->message_second_line_img);

            if($slide->message_third_line_img != ""){
                $thirdLineImage['preview'] = "<img style='height:160px' src='". url('') . '/core/storage/' .$this->imagePath.'/'.$slide->message_third_line_img."'>";
                $thirdLineImage['previewConfig'] = array('caption'=>$slide->message_third_line_img,'caption'=>$slide->message_third_line_img,'key'=>$slide->message_third_line_img);
            }
            if($slide->message_fourth_line_img != ""){
                $fourthLineImage['preview'] = "<img style='height:160px' src='". url('') . '/core/storage/' .$this->imagePath.'/'.$slide->message_fourth_line_img."'>";
                $fourthLineImage['previewConfig'] = array('caption'=>$slide->message_fourth_line_img,'caption'=>$slide->message_fourth_line_img,'key'=>$slide->message_fourth_line_img);
            }
        }
        
              
              
        return view('SliderManager::slider.edit')->with([
            'slide' => $slide ,
            'mainImage' => $mainImage,
            'btnImage' => $btnImage,
            'firstLineImage' => $firstLineImage,
            'secondLineImage' => $secondLineImage,
            'thirdLineImage' => $thirdLineImage,
            'fourthLineImage' => $fourthLineImage,
            'aType'=>$animation_type
            ]);
    }

    public function getImage($id){
        $slide = DB::table('slides')
            ->select('image')
            ->where('slides_id','=',$id)
            ->first();
        return json_encode($slide);
    }

    public function update(Request $request ,$id){

        $input = $request->file('sliderImg');
        $slide = Slider::where('slides_id','=',$id)->first();
        $slide->animation_type = $request->aType;

        $slide->message_type = $request->message_type;

        if(Input::hasfile('sliderImg'))
        {
            $mainImage = $request->file('sliderImg');
            $slide->image = $this->uploadImage($mainImage);
        }

          if($request->message_type == "image"){
              $slide->img_position = $request->imgposition;
              if(Input::hasfile('firstLineImage'))
            {
                $firstLineImage = $request->file('firstLineImage');
                $slide->message_first_line_img =  $this->uploadImage($firstLineImage);

            }
              $slide->top_first = $request->firstLineTop;
              $slide->maxwidth_first = $request->firstLineWidth;
             if(Input::hasfile('secondLineImage'))
            {
                $secondLineImage = $request->file('secondLineImage');
                $slide->message_second_line_img = $this->uploadImage($secondLineImage);

            }
              $slide->top_second = $request->secondLineTop;
              $slide->maxwidth_second = $request->secondLineWidth;
            if($request->hasFile('thirdLineImage')){
                $thirdLineImage = $request->file('thirdLineImage');
                $slide->message_third_line_img =  $this->uploadImage($thirdLineImage);

              }
              $slide->top_third = $request->thirdLineTop;
              $slide->maxwidth_third = $request->thirdLineWidth;
            if($request->hasFile('fourthLineImage')){
                $fourthLineImage = $request->file('fourthLineImage');
               $slide->message_fourth_line_img = $this->uploadImage($fourthLineImage);

              }
              $slide->top_fourth = $request->fourthLineTop;
              $slide->maxwidth_fourth = $request->fourthLineWidth;
        }

        if($request->hasFile('buttonImage')){
            $buttonImage = $request->file('buttonImage');
            $buttonImageName = $this->uploadImage($buttonImage);
            $slide->btn_image = $buttonImageName;
        }    

        $slide->btn_animation_type = $request->btnAniType;
        $slide->top_button = $request->btnTop;
        $slide->maxwidth_button = $request->btnWidth;
        $slide->animation_txt = $request->aText;

        if($slide->btn_animation_type == 'center'){
            $slide->center_btn = $request->btnCenter;
        }


        
        $slide->btn_url = $request->btnUrl;

       // dd($slide);exit;
        $slide->save();



        return redirect('slider/list')->with(['success' => true,
            'success.message' => 'Slide Edited successfully!',
            'success.title' => 'Well Done!']);
    }
}
