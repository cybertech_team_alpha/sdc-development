<?php

namespace SliderManager\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model{
	protected $table = 'slides';

    protected $primaryKey = 'slides_id';

    public function animationType()
    {
        return $this->belongsTo('SliderManager\Models\AnimationType','animation_type');
    }

    public function btnAnimationType()
    {
        return $this->belongsTo('SliderManager\Models\AnimationType','btn_animation_type');
    }


}
