@extends('layouts.back.master') @section('current_title','Add new homepage slider')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all"/>
    <style>
        .rating-container .rating-stars:before {
            text-shadow: none;
        }
    </style>

@stop
@section('current_path')
    <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
            <li><a href="{{url('blog/list')}}">Slider Management</a></li>

            <li class="active">
                <span>New Slide</span>
            </li>
        </ol>
    </div>
@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">
                    <form class="form-horizontal" id="form" method="post" files="true" enctype="multipart/form-data">
                        {!!Form::token()!!}

                        <div class="form-group"><label class="col-sm-2 control-label">ANIMATION TYPE <sup>*</sup></label>
                            <div class="col-sm-10">
                                <select class="js-source-states" style="width: 100%" name="aType" required>
                                    @foreach($aType as $type)
                                        <option value="{{$type->animation_type_id}}">{{$type->type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label required">IMAGE <sup>*</sup></label>
                            <div class="col-sm-10">
                                <input id="project-file" name="sliderImg" type="file" class="file-loading" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">MESSAGE TYPE<sup>*</sup></label>
                            <div class="col-sm-10">
                                <div class="radio">
                                    <label><input type="radio" onchange="setMessageType()" name="message_type" checked value="text">Text </label>
                                    <label><input type="radio" onchange="setMessageType()" name="message_type" value="image">Image</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group typeText"><label class="col-sm-2 control-label">MESSAGE TEXT <sup>*</sup></label>
                            <div class="col-sm-10">
                                <textarea  id="aText" name="aText" class="form-control" required></textarea>
                                <div id="messageTextError" ></div>
                            </div>
                            
                        </div>

                        <div class="form-group typeImages"><label class="col-sm-2 control-label">IMAGE POSITION<sup>*</sup></label>
                            <div class="col-sm-10">
                                <select class="js-source-states" style="width: 100%" name="imgposition" required>
                                    {{--@foreach($aType as $type)--}}
                                    {{--<option value="{{$type->animation_type_id}}">{{$type->type}}</option>--}}
                                    {{--@endforeach--}}
                                    <option value="left">Left</option>
                                    <option value="right">Right</option>
                                    <option value="center">Center</option>
                                </select>
                            </div>
                        </div>
                        <hr class="typeImages">
                        <div class="form-group typeImages"><label class="col-sm-2 control-label">MESSAGE FIRST LINE <sup>*</sup></label>
                            <div class="col-sm-10">
                                    <input id="firstLineImage" name="firstLineImage" type="file" class="file-loading" >
                               
                            </div>
                        </div>
                        <div class="form-group typeImages">
                            <label class="col-sm-2 control-label">TOP (%)<sup>*</sup></label>
                            <div class="col-sm-3">
                                <input id="firstLineTop" name="firstLineTop" type="number" class="form-control" >

                            </div>
                            <label class="col-sm-2 control-label">MAX-WIDTH (%)<sup>*</sup></label>

                            <div class="col-sm-3">
                                <input id="firstLineWidth" name="firstLineWidth" type="number" class="form-control" >

                            </div>
                        </div>

                        <hr class="typeImages">
                        <div class="form-group typeImages"><label class="col-sm-2 control-label">MESSAGE SECOND LINE <sup>*</sup></label>
                            <div class="col-sm-10">
                                    <input id="secondLineImage" name="secondLineImage" type="file" class="file-loading" >
                               
                            </div>
                        </div>
                        <div class="form-group typeImages">
                            <label class="col-sm-2 control-label">TOP (%)<sup>*</sup></label>
                            <div class="col-sm-3">
                                <input id="secondLineTop" name="secondLineTop" type="number" class="form-control" >

                            </div>
                            <label class="col-sm-2 control-label">MAX-WIDTH (%)<sup>*</sup></label>
                            <div class="col-sm-3">
                                <input id="secondLineWidth" name="secondLineWidth" type="number" class="form-control" >

                            </div>
                        </div>
                        <hr class="typeImages">
                        <div class="form-group typeImages"><label class="col-sm-2 control-label">MESSAGE THIRD LINE </label>
                            <div class="col-sm-10">
                                    <input id="thirdLineImage" name="thirdLineImage" type="file" class="file-loading" >
                               
                            </div>
                        </div>

                        <div class="form-group typeImages">
                            <label class="col-sm-2 control-label">TOP (%)<sup>*</sup></label>
                            <div class="col-sm-3">
                                <input id="thirdLineTop" name="thirdLineTop" type="number" class="form-control" >

                            </div>
                            <label class="col-sm-2 control-label">MAX-WIDTH (%)<sup>*</sup></label>
                            <div class="col-sm-3">
                                <input id="thirdLineWidth" name="thirdLineWidth" type="number" class="form-control" >

                            </div>
                        </div>
                        <hr class="typeImages">
                        <div class="form-group typeImages"><label class="col-sm-2 control-label">MESSAGE FOURTH LINE </label>
                            <div class="col-sm-10">
                                    <input id="fourthLineImage" name="fourthLineImage" type="file" class="file-loading" >
                               
                            </div>
                        </div>
                        <div class="form-group typeImages">
                            <label class="col-sm-2 control-label">TOP (%)<sup>*</sup></label>
                            <div class="col-sm-3">
                                <input id="fourthLineTop" name="fourthLineTop" type="number" class="form-control" >

                            </div>
                            <label class="col-sm-2 control-label">MAX-WIDTH (%)<sup>*</sup></label>
                            <div class="col-sm-3">
                                <input id="fourthLineWidth" name="fourthLineWidth" type="number" class="form-control" >

                            </div>
                        </div>
                        <hr class="typeImages">
                        <div class="form-group"><label class="col-sm-2 control-label">BUTTON IMAGE <sup>*</sup></label>
                            <div class="col-sm-10">
                                <input id="buttonImage" name="buttonImage" type="file" class="file-loading" >
                        </div>
                        </div>
                        {{--<div class="form-group"><label class="col-sm-2 control-label">BUTTON ANIMATION TYPE <sup>*</sup></label>--}}
                        <div class="form-group"><label class="col-sm-2 control-label">BUTTON POSITION<sup>*</sup></label>
                            <div class="col-sm-10">
                                <select class="js-source-states" style="width: 100%"  id="btnAniType" name="btnAniType" required>
                                    {{--@foreach($aType as $type)--}}
                                        {{--<option value="{{$type->animation_type_id}}">{{$type->type}}</option>--}}
                                    {{--@endforeach--}}
                                    <option value="left">Left</option>
                                    <option value="right">Right</option>
                                    <option value="center">Center</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">TOP (%)<sup>*</sup></label>
                            <div class="col-sm-2">
                                <input id="btnTop" name="btnTop" type="number" class="form-control" >

                            </div>
                            <label class="col-sm-2 control-label">MAX-WIDTH (%)<sup>*</sup></label>

                            <div class="col-sm-2">
                                <input id="btnWidth" name="btnWidth" type="number" class="form-control" >

                            </div>

                            <label class="col-sm-2 control-label">LEFT (%)<sup>*</sup></label>

                            <div class="col-sm-2">
                                <input id="btnCenter" name="btnCenter" type="number" class="form-control"  readonly>

                            </div>
                        </div>

                        

                        <div class="form-group"><label class="col-sm-2 control-label">BUTTON URL <sup>*</sup></label>
                            <div class="col-sm-6"><input type="text" class="form-control" name="btnUrl" placeholder="Enter button url"></div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button class="btn btn-default" type="button" onclick="location.reload();">Cancel
                                </button>
                                <button class="btn btn-primary" type="submit">Done</button>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
        @stop
        @section('js')
            <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
            <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
            <script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}"
                    type="text/javascript"></script>
            <script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
            <script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
            <script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>

            <script type="text/javascript">
                $(document).ready(function () {
                    setMessageType();
                    $("#input-1").rating();
                    $(".js-source-states").select2({placeholder: "Select Animation type"});
                    $('.date').datepicker(
                        {
                            format: 'yyyy-mm-dd',
                        });

                    $("#project-file").fileinput({
                        allowedFileExtensions: ['jpg', 'png', 'gif'],
                        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
                        'showUpload': false,
                        overwriteInitial: true,
                        removeIcon: '<i class="glyphicon glyphicon-trash"></i>',


                    });
                    $("#firstLineImage").fileinput({
                        allowedFileExtensions: ['jpg', 'png', 'gif'],
                        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
                        'showUpload': false,
                        overwriteInitial: true,
                        removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
                    });
                    $("#secondLineImage").fileinput({
                        allowedFileExtensions: ['jpg', 'png', 'gif'],
                        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
                        'showUpload': false,
                        overwriteInitial: true,
                        removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
                    });
                    $("#thirdLineImage").fileinput({
                        allowedFileExtensions: ['jpg', 'png', 'gif'],
                        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
                        'showUpload': false,
                        overwriteInitial: true,
                        removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
                    });
                    $("#fourthLineImage").fileinput({
                        allowedFileExtensions: ['jpg', 'png', 'gif'],
                        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
                        'showUpload': false,
                        overwriteInitial: true,
                        removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
                    });
                    $("#buttonImage").fileinput({
                        allowedFileExtensions: ['jpg', 'png', 'gif'],
                        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
                        'showUpload': false,
                        overwriteInitial: true,
                        removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
                    });

                    $("#btnAniType").change( function() {
                        if ($(this).val() == 'center') {
                            $('#btnCenter').prop('readonly', false);
                        } else {
                            $('#btnCenter').prop("readonly", true);
                            $('#btnCenter').val("");

                        }

                    });

                    $("#form").validate({
                        errorPlacement: function(error, element) {
                            if (element.attr("name") == "aText")
                                {
                                    error.appendTo("#messageTextError");
                                }
                                else
                                {
                                    error.insertAfter(element);
                                }
                            },
                        rules: {
                            aType: {
                                required: true
                            },
                            sliderImg: {
                                required: true
                            },
                            firstLineImage: {
                                required : function(element){
                                    return $("input[name='message_type']:checked").val() == "image";
                                }
                            },
                            secondLineImage: {
                                required : function(element){
                                    return $("input[name='message_type']:checked").val() == "image";
                                }
                            },
                            aText: {
                                required : function(element){
                                    return $("input[name='message_type']:checked").val() == "text";
                                }
                            },
                            buttonImage : {
                                required : true
                            },
                            btnUrl: {
                                required: true
                            }
                        },
                        submitHandler: function (form) {
                            form.submit();
                        }
                    });
                });

                tinymce.init({
                    selector: 'textarea#aText',  // change this value according to your HTML
                    plugins: [
                        'advlist autolink lists link image charmap preview hr anchor pagebreak',
                        'searchreplace wordcount visualblocks visualchars code',
                        'insertdatetime media nonbreaking table contextmenu directionality',
                        'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
                    ],
                    toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
                    '| bullist numlist outdent indent | link | undo redo | forecolor backcolor emoticons ',
                    fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
                    menubar: false
                });

                
                function setMessageType(){
                    let selType = $("input[name='message_type']:checked").val()
                    if(selType == "image"){
                        $(".typeText").hide();
                        $(".typeImages").show();
                        tinymce.activeEditor.setContent('');
                    }else if(selType == "text"){
                        $(".typeImages").hide();
                        $(".typeText").show();
                    }
                }
            </script>
@stop