<?php
namespace NewsManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * News Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Praveen Chameera <praveenchameera@gmail.com>
 * @copyright  Copyright (c) 2015, Praveen Chameera
 * @version    v1.0.0
 */
class News extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_news';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title', 'date', 'cover_image','front_image','volume_no','description','video_link','status'];

	public function getImages()
	{
		return $this->hasMany('NewsManage\Models\NewsImage', 'news_id', 'id');
	}





}
