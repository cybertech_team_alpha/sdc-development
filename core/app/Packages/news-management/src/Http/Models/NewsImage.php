<?php
namespace NewsManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * News Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Praveen Chameera <praveenchameera@gmail.com>
 * @copyright  Copyright (c) 2015, Praveen Chameera
 * @version    v1.0.0
 */
class NewsImage extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_news_image';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['news_id','path','file_name','type'];

	

}
