<?php

namespace NewsManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use Response;
use File;
use \NewsManage\Models\News;
use \NewsManage\Models\NewsImage;
use Image;
use Session;
use Illuminate\Support\Facades\Input as Input;

class NewsController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | News Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest');
    }

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {

        return view('newsManage::news.add');
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {  
        
      $this->validate($request, [
        'front_image' => 'required',
        'video_url' => ['nullable','regex:/^(?:https?:\/\/)?(?:www[.])?(?:youtube[.]com\/watch[?]v=|youtu[.]be\/)([^&]{11})$/'],
      ]);
        //ISSUE #116 
    //   if (Image::make($request->front_image)->width() != 270) {
    //     return back()->withInput()->withErrors(['cover_image' => 'front image is not in required width 270px']);
    //   }

    //   if (Image::make($request->cover_image)->width() != 870) {
    //       return back()->withInput()->withErrors(['cover_image' => 'cover image is not in required width 950px']);
    //   }

    
        $title = $request->get('title');
        $valume_no = $request->get('valume_no');
        $video_link = $request->get('video_link');
        $description = $request->get('description');
        // $front_image = $request->file('front-file');
        $featured_image = $request->file('featured_file');
        $imgname = str_slug($title)."-".time().'.png';

        $news = News::create([
          'title' => $title,
          'volume_no' => $valume_no,
          //'date' => $request->date,
          'status' => $request->has('home') ? 1 : 0 ,
          'description' => $description,
          'created_by' => Sentinel::getUser()->id,
          'video_link' => $video_link,
          'cover_image' => $imgname,
          'front_image' => $imgname,
        ]);
        
        if (!file_exists(storage_path('uploads/images/news'))) {
            File::makeDirectory(storage_path('uploads/images/news'));
        }
        $path = 'uploads/images/news';

        
        if ($request->cover_image) {
          $cvrFileName= $this->base64_img_upload($request->cover_image);
        } else {
          $cvrFileName= '';
        }
        $frontFileName= $this->base64_img_upload($request->front_image);

        $news->update([
          'cover_image' => $cvrFileName,
          'front_image' => $frontFileName,
        ]);        

        
        if ($request->hasFile('featured_file')) { 

          $featured_files = $request->file('featured_file');
          $i = 0;
          foreach ($featured_files as $key => $project_file) {
               if (File::exists($project_file)) {
                   $file = $project_file;
                   $extn = $file->getClientOriginalExtension();
                   $destinationPath = storage_path($path);
                   $project_fileName = 'news-featured-' . date('YmdHis') . '-' . $i . '.' . $extn;
                   $file->move($destinationPath, $project_fileName);

                   NewsImage::create([
                       'news_id' => $news->id,
                       'path' => 'core/storage/'.$path,
                       'file_name' => $project_fileName,
                       'type' => 'featured'
                   ]);
                   $i++;
               }            
          }
        }

        

        return redirect('news/add')->with(['success' => true,
          'success.message' => 'News Created successfully!',
          'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('newsManage::news.list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {

            $data = News::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $news) {

                $dd = array();
                array_push($dd, $i);

                if ($news->title != "") {
                    array_push($dd, $news->title);
                } else {
                    array_push($dd, "-");
                }

                if ($news->volume_no != "") {
                    array_push($dd, $news->volume_no);
                } else {
                    array_push($dd, "-");
                }
                if ($news->description != "") {
                    array_push($dd, $news->description);
                } else {
                    array_push($dd, "-");
                }

                $permissions = Permission::whereIn('name', ['news.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('news/edit/' . $news->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['news.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $news->id . '" data-toggle="tooltip" data-placement="top" title="Delete News"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    /**
     * Activate or Deactivate user
     * @param  Request $request id with status to change
     * @return json object with status of success or failure
     */
    public function status(Request $request) {
        if ($request->ajax()) {
            $id = $request->input('id');
            $status = $request->input('status');

            $branch = Branch::find($id);
            if ($branch) {
                $branch->status = $status;
                $branch->save();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
        if ($request->ajax()) {
            $id = $request->input('id');

            $news = News::find($id);
            if ($news) {
                $news->delete();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
        $news = News::with(['getImages'])->find($id);
        $featured_image_url_array = [];
        $featured_image_config_array = [];

        $featured_images = NewsImage::where('news_id', $id)->where('type', 'featured')->get();

        foreach ($featured_images as $key => $value) {

            array_push($featured_image_url_array, "<img style='height:160px' src='" . url($value->path . '/' . $value->file_name)."'>");
            array_push($featured_image_config_array, array('caption' => $value->file_name, 'caption' => $value->file_name, 'key' => $value->id, 'url' => url('news/image/deleteFile')));
        }

        // $front_image_url_array = [];
        // $front_image_config_array = [];
        // $front_images = NewsImage::where('news_id', $id)->where('type', 'front')->get();
        // foreach ($front_images as $key => $value) {
        //     array_push($front_image_url_array, "<img style='height:160px' src='" . url('') . '/core/storage/' . $value->path . '/' . $value->file_name . "'>");
        //     array_push($front_image_config_array, array('caption' => $value->file_name, 'caption' => $value->file_name, 'key' => $value->id, 'url' => url('news/image/deleteFile')));
        // }

        if ($news) {
            return view('newsManage::news.edit')->with([
                        'news' => $news,
                        'featured_images' => $featured_image_url_array, 'featured_img_config' => $featured_image_config_array,
                        // 'front_images' => $front_image_url_array, 'front_img_config' => $front_image_config_array
            ]);
        } else {
            return view('errors.404');
        }
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

        $title = $request->get('title');
        $valume_no = $request->get('valume_no');
        $video_link = $request->get('video_link');
        $description = $request->get('description');
        // $front_image = $request->file('front-file');
        $featured_image = $request->file('featured-file');



        $news = News::find($id);

        $news->title = $title;
        $news->volume_no = $valume_no;
        //$news->date = $request->date;
        $news->description = $description;
        $news->status = $request->has('home') ? 1 : 0 ;
        //   $news->created_by = Sentinel::getUser()->id;
        $news->video_link = $video_link;
  


        if (!file_exists(storage_path('uploads/images/news'))) {

            File::makeDirectory(storage_path('uploads/images/news'));
        }
        $path = 'uploads/images/news';

        if ($request->front_image != "") {
						$news->front_image= $this->base64_img_upload($request->front_image);
				 }
        if ($request->cover_image != "") {
						$news->cover_image= $this->base64_img_upload($request->cover_image);
                 }
                 
        $news->save();

        // if ($request->hasFile('front_image')) {

        // //   if (Image::make($request->front_image)->width() != 270) {
        // //     return back()->withInput()->withErrors(['front_image' => 'front image is not in required width 270px']);
        // //   }

        //   $imgname = str_slug($news->name)."-".time().'.png';

        //   Image::make($request->front_image)->save('core/storage/'.$path.'/'.$imgname);

        //   $news->update([
        //     'front_image' => $imgname
        //   ]);
        // }

        // if ($request->hasFile('cover_image')) {

        // //   if (Image::make($request->cover_image)->width() != 870) {
        // //       return back()->withInput()->withErrors(['cover_image' => 'cover image is not in required width 950px']);
        // //   }

        //   $imgname = str_slug($news->name)."-".time().'.png';

        //   Image::make($request->cover_image)->save('core/storage/'.$path.'/cover_'.$imgname);

        //   $news->update([
        //     'cover_image' => 'cover_'.$imgname
        //   ]);

        // }

        if ($request->hasFile('featured-file')) {



            $featured_files = $request->file('featured-file');
            $i = 0;
            foreach ($featured_files as $key => $project_file) {
                if (File::exists($project_file)) {
                    $file = $project_file;
                    $extn = $file->getClientOriginalExtension();
                    $destinationPath = storage_path($path);
                    $project_fileName = 'news-featured-' . date('YmdHis') . '-' . $i . '.' . $extn;
                    $file->move($destinationPath, $project_fileName);

                    NewsImage::create([
                        'news_id' => $news->id,
                        'path' => 'core/storage/'.$path,
                        'file_name' => $project_fileName,
                        'type' => 'featured'
                    ]);
                    $i++;
                }
            }



        }

        return redirect('news/edit/' . $id)->with(['success' => true,
                    'success.message' => 'News updated successfully!',
                    'success.title' => 'Good Job!']);
    }

    public function jsonImageFileDelete(Request $request) {
        $image_id = $request->get('key');
        $image = NewsImage::find($image_id);
        $image->delete();
        return 1;
    }

    public function base64_img_upload($img_data){//save base64 encoded image
        try {
            $newname = str_random(10).".png";
            $data = $img_data;
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);


            $save_target = "core/storage/uploads/images/news/".$newname;

            $target = $save_target;


            if(file_put_contents($target, $data)){
                $img = imagecreatefrompng($target);
                imagealphablending($img, false);


                imagesavealpha($img, true);

                imagepng($img, $target, 8);
                return $newname;
            }else{
                return "error";
            }
        } catch (Exception $e) {
            return "error";
        }



    }

    
}
