@extends('layouts.back.master') @section('current_title','New News')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<!--<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />--> 
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/fontawesome/css/font-awesome.css')}}">

<!--Dropzone JS--> 
<link rel="stylesheet" href="{{ asset('assets/back/dropzone/css/dropzone.css') }}">

<style>
    .rating-container .rating-stars:before {
        text-shadow: none;
    }

    .fileinput-upload{
        display: none;
    }

    .file-thumb-progress.kv-hidden {
        visibility: hidden;
    }

</style>

 <style>
       
        .imageBox
    {
      position: relative;
      width: 300px;
      height: 400px;
      border:1px solid #aaa;
      background: #fff;
      overflow: hidden;
      background-repeat: no-repeat;
      cursor:move;
    }

    .imageBox .thumbBox
    {
      position: absolute;
      top: 50%;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      width: 270px;
      height: 370px;
      margin-top: auto;
      margin-bottom: auto;
      margin-left: auto;
      margin-right: auto;
      box-sizing: border-box;
      border: 1px solid rgb(102, 102, 102);
      box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
      background: none repeat scroll 0% 0% transparent;
    }

    .imageBox .spinner
    {
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      text-align: center;
      line-height: 400px;
      background: rgba(0,0,0,0.7);
    }
    </style>

 <style>
        
 .imageBox-cover-image
    {
        position: relative;
      width: 800px;
      height: 450px;
      border:1px solid #aaa;
      background: #fff;
      overflow: hidden;
      background-repeat: no-repeat;
      cursor:move;
    }

    .imageBox-cover-image .thumbBox-cover-image
    {
     position: absolute;
      top: 50%;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      width: 770px;
      height: 400px;
      margin-top: auto;
      margin-bottom: auto;
      margin-left: auto;
      margin-right: auto;
      box-sizing: border-box;
      border: 1px solid rgb(102, 102, 102);
      box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
      background: none repeat scroll 0% 0% transparent;
    }

    .imageBox-cover-image .spinner-cover-image
    {
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      text-align: center;
      line-height: 400px;
      background: rgba(0,0,0,0.7);
    }

    </style>

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('news/list')}}">News Management</a></li>

        <li class="active">
            <span>New News</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul class="">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
              @endif
                <form class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                	{!!Form::token()!!}
                    <input type="hidden" name="front_image" id="front_img" form="form">
                    <input type="hidden" name="cover_image" id="cvr_img" form="form">
                    <div class="form-group"><label class="col-sm-2 control-label">TITLE <sup>*</sup></label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="title" value="{{ old('title') }}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">ISSUE DATE / VOLUME NO</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="valume_no" value={{ old('valume_no') }}></div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label">VIDEO LINK</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="video_link" value="{{old('video_link')  }}"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">HOME PAGE</label>
                        <div class="col-sm-10">
                        <div class="checkbox">
                            <input type="checkbox" value="1" name="home">
                        </div>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION <sup>*</sup></label>
                         <div class="col-sm-10"><textarea name="description" class="form-control">{{ old('description') }}</textarea></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">FRONT IMAGE *</label>
                        <div class="col-sm-10">
                            <div class="col-sm-10">
                                <div class="col-md-6">
                                  <button type="button" class="btn bg-grey waves-effect" data-toggle="modal" data-target="#front-image-crop-modal" >Select Front Image</button>
                                </div>
                                <div class="col-md-6">
                                  <div class="front-image-cropped">

                                  </div>
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">COVER IMAGE </label>
                        <div class="col-sm-10">
                            <div class="col-sm-10">
                                <div class="col-md-6">
                                  <button type="button" class="btn bg-grey waves-effect" data-toggle="modal" data-target="#cover-image-crop-modal" >Select Cover Image</button>
                                </div>
                                <div class="col-md-6">
                                  <div class="cover-image-cropped">

                                  </div>
                                </div>
                              </div>
                        </div>
                    </div> 

                    <div class="form-group">
                        <label class="col-sm-2 control-label required">FEATURED IMAGES</label>
                        <div class="col-sm-10">
                            <input id="featured_file" name="featured_file[]" type="file" multiple class="file-loading">
                            <p class="notice">Please use to upload 550px width x 670px height images for better view</p>
                        </div>
                    </div>                  

                    <!--<div class="dropzone" id="my-dropzone">
                        <div class="dz-message">
                            <div class="col-xs-8">
                                <div class="message">
                                    <p>Drop files here or Click to Upload</p>
                                </div>
                            </div>                    
                        </div>
                        <div class="fallback">
                            <input name="featured_file" type="file[]" multiple />
                        </div>
                    </div>-->


                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Done</button>
	                    </div>
	                </div>

                </form>
        </div>
    </div>
</div>

{{--Dropzone Preview Template--}}
<div id="preview" style="display: none;">

    <div class="dz-preview dz-file-preview">
        <div class="dz-image"><img data-dz-thumbnail /></div>

        <div class="dz-details">
            <div class="dz-size"><span data-dz-size></span></div>
            <div class="dz-filename"><span data-dz-name></span></div>
        </div>
        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
        <div class="dz-error-message"><span data-dz-errormessage></span></div>



        <div class="dz-success-mark">

            <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                <title>Check</title>
                <desc>Created with Sketch.</desc>
                <defs></defs>
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                    <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                </g>
            </svg>

        </div>
        <div class="dz-error-mark">

            <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                <title>error</title>
                <desc>Created with Sketch.</desc>
                <defs></defs>
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                    <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                        <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                    </g>
                </g>
            </svg>
        </div>
    </div>
</div>
{{--End of Dropzone Preview Template--}}

 <!-- crop modal -->
        <div class="modal fade" id="front-image-crop-modal" tabindex="-1" role="dialog">
          <div class="modal-dialog " role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" >Crop Front Image</h4>
              </div>
              <div class="modal-body">
                <div class="container">
                  <div class="imageBox">
                    <div class="thumbBox"></div>
                    {{-- <div class="spinner" style="display: none">Loading...</div> --}}
                  </div>
                  <div class="action">
                    <input type="file" id="cropFile" style="float:left; width: 250px">

                    <input type="button" id="btnZoomIn" value="+" class="btn bg-grey waves-effect">
                    <input type="button" id="btnZoomOut" value="-" class="btn bg-grey waves-effect">

                  </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn bg-success waves-effect" id="btnCrop">Crop</button>
                <button type="button" class="btn bg-amber waves-effect" data-dismiss="modal">CLOSE</button>
              </div>
            </div>
          </div>
        </div>
        <!-- crop modal -->
 <!-- crop modal -->
        <div class="modal fade" id="cover-image-crop-modal" tabindex="-1" role="dialog">
          <div class="modal-dialog " role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" >Crop Cover Image</h4>
              </div>
              <div class="modal-body">
                <div class="container">
                  <div class="imageBox-cover-image">
                    <div class=" thumbBox-cover-image"></div>
                    {{-- <div class="spinner" style="display: none">Loading...</div> --}}
                  </div>
                  <div class="action">
                    <input type="file" id="cropFileCoverImage" style="float:left; width: 250px">

                    <input type="button" id="btnZoomInCoverImage" value="+" class="btn bg-grey waves-effect">
                    <input type="button" id="btnZoomOutCoverImage" value="-" class="btn bg-grey waves-effect">

                  </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn bg-success waves-effect" id="btnCropCoverImage">Crop</button>
                <button type="button" class="btn bg-amber waves-effect" data-dismiss="modal">CLOSE</button>
              </div>
            </div>
          </div>
        </div>
        <!-- crop modal -->
@stop
@section('js')

<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<!--<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/tinymce/js/tinymce/spellchecker/spellchecker.php')}}"></script>

<!--Dropzone Js--> 
<script src="{{ asset('assets/back/dropzone/js/dropzone.js') }}"></script>

<script type="text/javascript">

    /*Dropzone.autoDiscover = false;
    $(document).ready(function () {
        $("#my-dropzone").dropzone({ 
            url: '{{ url("/news/uploadimgsaddmode") }}',
            headers: {
                    'x-csrf-token': "{{ csrf_token() }}"
                    },
            method: 'post',
            paramName: "file",
            maxFiles: 100,
            maxFileSize: 100,
            acceptedFiles: ".jpeg,.jpg,.png",
            uploadMultiple: true,
            parallelUploads: 100,
            previewTemplate: document.querySelector('#preview').innerHTML,
            addRemoveLinks: true,
            dictRemoveFile: 'Remove file',
            dictFileTooBig: 'Image is larger than 16MB',
            timeout: 10000, 
            paramName: "file",                       
            success: function (file, response) {
                alert(JSON.stringify(response));
            },
            error: function(file, response)
            {
               alert(response);
            },            
            addedfile:function(file,response){ 
                var file=file;
                var formData=new FormData();
                var _token='{{ csrf_token() }}';                
                formData.append("_token", _token); 
                formData.append('file',file);
                formData.append('_method', 'PATCH');

                $.ajaxSetup({
                    headers: {
                    'X-CSRF-Token': _token,
                    'Content-type': 'application/x-www-form-urlencoded'
                    }
                }); 
                
                $.ajax({
                    type: 'POST',                    
                    url: '{{ url("/news/uploadimgsaddmode") }}',
                    data: {'file': formData,'id':'Hi'},                    
                    processData:false,
                    dataType: 'json',                     
                });
            }            
        });        
    });*/



	$(document).ready(function(){
        $("#input-1").rating();

        $(".js-source-states").select2({ placeholder: "Select a State"});
        
        $('.date').datepicker({
            format: 'yyyy-mm-dd',
        });

        $("#front-file,#cover_image").fileinput({
            allowedFileExtensions: ['jpg', 'png', 'gif'],
            previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
            'showUpload': false,
            overwriteInitial: true,
            removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
        });
         
            



		$("#form").validate({
            rules: {
                title: {
                    required: true

                },
                  description: {
                    required: true

                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
    
    
    $(document).on("ready", function() {
        $("#featured_file").fileinput({  
            theme: 'fa',
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],            
            uploadAsync:true,           
            overwriteInitial: false,
            maxFileSize:1000,
            maxFileCount: 10,  
            showRemove: true,                      
         }).on("filebatchselected", function(event, files) {                
             //alert($(":featured-file").files);        
             $("#featured_file").fileinput("upload"); //upload method
         }).on('fileuploaded', function(event, previewId, index, fileId) {
             console.log('File Uploaded', 'ID: ' + fileId + ', Thumb ID: ' + previewId);            
         }).on('fileuploaderror', function(event, data, msg) {
             console.log('File Upload Error', 'ID: ' + data.fileId + ', Thumb ID: ' + data.previewId);
        });
    });
    

    function loadSeries() {
        var selected_device = $('#device').val();
        console.log(selected_device);
    }
     function loadSeries(){
        $('#series').empty();
        $('#series').select2("val", "0");
        // $('#series').append(
        //                 '<option value="0">SELECT A SERIES</option>'
        //                 );
        var selected_device=$('#device').val();
        if(selected_device!=0){
             $.ajax({
              method: "GET",
              url: '{{url('product/json/getSeries')}}',
              data:{ 'id' : selected_device  }
            })
              .done(function( msg ) {
                $vehicleDetailArray=jQuery.parseJSON(JSON.stringify(msg));
                console.log($vehicleDetailArray['data']);
                $.each($vehicleDetailArray['data'], function( index, value ) {
                    $('#series').append(
                        '<option value="'+value['id']+'">'+value['name']+'</option>'
                        );

                });


              });
        }
    }

    tinymce.init({
        selector: 'textarea',  // change this value according to your HTML
        plugins: [
            'advlist autolink lists link image charmap preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code',
            'insertdatetime media nonbreaking table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample'            
        ],
        toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
        '| bullist numlist outdent indent link | undo redo | forecolor backcolor emoticons',
        spellchecker_rpc_url:'spellchecker.php',
        fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
        menubar: false,
        browser_spellcheck : true,
        contextmenu: false,
        spellchecker_callback: function (method, text, success, failure) {
            var words = text.match(this.getWordCharPattern());
            if (method === "spellcheck") {
            var suggestions = {};
            for (var i = 0; i < words.length; i++) {
                suggestions[words[i]] = ["First", "Second"];
            }
            success({ words: suggestions, dictionary: [ ] });
            } else if (method === "addToDictionary") {
            // Add word to dictionary here
            success();
            }
        }
    });


</script>
<script src="{{url('assets/back/cropbox/cropbox.js')}}"></script>
<script type="text/javascript">


  var options =
  {
      thumbBox: '.thumbBox',
      spinner: '.spinner',
      imgSrc: 'avatar.png'
  }
  var cropper = $('.imageBox').cropbox(options);
  $('#cropFile').on('change', function(){
      var reader = new FileReader();
      reader.onload = function(e) {
          options.imgSrc = e.target.result;
          cropper = $('.imageBox').cropbox(options);
      }
      reader.readAsDataURL(this.files[0]);
      this.files = [];
  })
  $('#btnCrop').on('click', function(){
    var img = cropper.getDataURL()
    $('#front_img').val(img);
    document.querySelector('.front-image-cropped').innerHTML = '<img style="max-width:100%;" src="'+img+'">';
    $('#front-image-crop-modal').modal('hide');
  })
  $('#btnZoomIn').on('click', function(){
      cropper.zoomIn();
  })
  $('#btnZoomOut').on('click', function(){
      cropper.zoomOut();
  })

</script>
<script type="text/javascript">


  var optionsCI =
  {
      thumbBox: '.thumbBox-cover-image',
      spinner: '.spinner-cover-image',
      imgSrc: 'avatar.png'
  }
  var cropperCI = $('.imageBox-cover-image').cropbox(optionsCI);
  $('#cropFileCoverImage').on('change', function(){
      var reader = new FileReader();
      reader.onload = function(e) {
          optionsCI.imgSrc = e.target.result;
          cropperCI = $('.imageBox-cover-image').cropbox(optionsCI);
      }
      reader.readAsDataURL(this.files[0]);
      this.files = [];
  })
  $('#btnCropCoverImage').on('click', function(){
    var img = cropperCI.getDataURL()
    $('#cvr_img').val(img);
    document.querySelector('.cover-image-cropped').innerHTML = '<img style="max-width:100%;" src="'+img+'">';
    $('#cover-image-crop-modal').modal('hide');
  })
  $('#btnZoomInCoverImage').on('click', function(){
      cropperCI.zoomIn();
  })
  $('#btnZoomOutCoverImage').on('click', function(){
      cropperCI.zoomOut();
  })

</script>
@stop
