<?php
namespace BranchManage\Http\Controllers;

use App\Http\Controllers\Controller;
use BranchManage\Http\Requests\BranchRequest;
use Illuminate\Http\Request;
use BranchManage\Models\Branch;
use BranchManage\Models\City;
use Permissions\Models\Permission;
use Sentinel;
use Response;



class BranchController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Branch Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the Branch add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		$city=City::get();		
		return view( 'branchManage::branch.add')->with(['city' => $city]);
	}

	/**
	 * Add new Branch data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add(BranchRequest $request)
	{
	
		$count= Branch::where('name', '=',$request->get('name' ))->count();
		if ($count==0) {
			Branch::Create(array(
				'name' => $request->input('name'),
				'code' => $request->input('code'),
				'address' => $request->input('address'),
				'tel' => $request->input('tel'),
				'city_id' => $request->input('city'),
				)
			);		

			return redirect('branch/add')->with([ 'success' => true,
				'success.message'=> 'Branch Created successfully!',
				'success.title' => 'Well Done!']);
		}else{
			
				return redirect('branch/add')->with([ 'error' => true,
				'error.message'=> 'Branch Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
	}

	/**
	 * View Branch List View
	 *
	 * @return Response
	 */
	public function listView()
	{		
		return view( 'branchManage::branch.list' );
	}

	/**
	 * Branch list
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$data= Branch::with(['getCity'])->where('status','=',1)->get();			
			$jsonList = array();
			$i=1;
			foreach ($data as $key => $branch) {
				
				$dd = array();
				array_push($dd, $i);
				
				if($branch->name != ""){
					array_push($dd, $branch->name);
				}else{
					array_push($dd, "-");
				}
				if($branch->code != ""){
					array_push($dd, $branch->code);
				}else{
					array_push($dd, "-");
				}
				if($branch->tel != ""){
					array_push($dd, $branch->tel);
				}else{
					array_push($dd, "-");
				}
				if($branch->address != ""){
					array_push($dd, $branch->address);
				}else{
					array_push($dd, "-");
				}						
				if($branch->getCity->name !=""){
					array_push($dd, $branch->getCity->name);
				}else{
					array_push($dd, "-");
				}	
				$permissions = Permission::whereIn('name',['branch.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('branch/edit/'.$branch->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit Branch"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['branch.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="branch-delete" data-id="'.$branch->id.'" data-toggle="tooltip" data-placement="top" title="Delete Branch"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate user
	 * @param  Request $request branch id with status to change
	 * @return json object with status of success or failure
	 */
	public function status(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$branch = Branch::find($id);
			if($branch){
				$branch->status = $status;
				$branch->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete a branch
	 * @param  Request $request branch id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$branch = Branch::find($id);
			if($branch){
				$branch->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the branch edit screen to the branch.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		$curbranch=Branch::find($id);
		$city=City::get();
		if($curbranch){
			return view( 'branchManage::branch.edit' )->with([ 
				'curBranch' => $curbranch,
				'city'=>$city			

				 ]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit(BranchRequest $request, $id)
	{
		
		
		$count= Branch::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();
		
			if($count==0){
				$branchOld =  Branch::where('id',$id)->take(1)->get();
				$branch=$branchOld[0];			
				$branch->name = $request->get('name');
				$branch->code = $request->get('code');
				$branch->address = $request->get('address' );
				$branch->tel	=$request->get('tel' );	
				$branch->city_id	=$request->get('city' );	
				$branch->save();
				
				return redirect( 'branch/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Branch updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('branch/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Branch Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
   
		
	}
}
