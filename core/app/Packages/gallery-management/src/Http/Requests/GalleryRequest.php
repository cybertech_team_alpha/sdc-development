<?php
namespace GalleryManage\Http\Requests;

use App\Http\Requests\Request;
use Input;

class GalleryRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){    

		$rules = [];

		if(!isset($this->id)){
			$rules['featuredCrop'] = 'required';
		}
	
		
		return $rules;
	}

	 public function messages()
    {

        return [
			'featuredCrop.required' => 'Please select and crop an image for featured image'
		];
    }

}
