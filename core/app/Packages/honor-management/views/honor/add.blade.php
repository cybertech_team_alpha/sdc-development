@extends('layouts.back.master') @section('current_title','Update Honor')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" media="all" />
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('honor/list')}}">Honor Management</a></li>

        <li class="active">
            <span>Add Honor</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
              @if ($errors->any())
              <div class="alert alert-danger">
                  <ul class="">
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
              @endif
              <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
               {!!Form::token()!!}

               <div class="form-group"><label class="col-sm-2 control-label">NAME</label>
                <div class="col-sm-10"><input type="text" class="form-control" name="name" value=""></div>
            </div>
                  <div class="form-group">
                      <label class="col-sm-2 control-label required">WHICH ONE DO YOU WANT TO USE ?</label>
                      <div class="col-sm-6">
                          <div class="radio radio-info radio-inline">
                              <input type="radio" id="inlineRadio1" value="0" name="type" checked>
                              <label for="inlineRadio1"> IMAGE </label>
                          </div>
                          <div class="radio radio-inline">
                              <input type="radio" id="inlineRadio2" value="1" name="type">
                              <label for="inlineRadio2"> VIDEO </label>
                          </div>
                      </div>
                  </div>
            <div class="form-group"><label class="col-sm-2 control-label">VIDEO URL</label>
               <div class="col-sm-10"><input type="text" id="videoInput" class="form-control" name="url" value="" disabled></div>
           </div>
           <div class="form-group">
            <label class="col-sm-2 control-label required">IMAGE</label>
            <div class="col-sm-10">
                <input id="image_hidden" name="image_hidden" type="hidden" value="0" >
                <input id="front_img" name="front_image" id="imageInput" type="file" class="images-file file-loading">
            </div>
        </div>

    <div class="form-group"><label class="col-sm-2 control-label">MORE URL</label>
       <div class="col-sm-10"><input type="text" class="form-control" name="more_link" value=""></div>
   </div>

   <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
       <div class="col-sm-10"><textarea name="description" class="form-control"></textarea></div>

   </div>

   <div class="hr-line-dashed"></div>
   <div class="form-group">
       <div class="col-sm-8 col-sm-offset-2">
           <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
           <button class="btn btn-primary" type="submit">Save Changes</button>
       </div>
   </div>

</form>
</div>
</div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>


<script type="text/javascript">
	$(document).ready(function(){
        $("#input-1").rating();
        $(".js-source-states").select2();
        $('.date').datepicker(
        {
           format: 'yyyy-mm-dd',
       });

        var isAfterStartDate = function(startDateStr, endDateStr) {

            if(new Date(startDateStr).getTime() > new Date(endDateStr).getTime()) {

                return false;
            }
            else
                return true;

        };
        jQuery.validator.addMethod("isAfterStartDate", function(value, element) {

            return isAfterStartDate($('#startdate').val(), value);
        }, "End date should be after start date");


        $("#form").validate({
            rules: {
                name: {
                    required: true

                },
                launch_date:{
                    required : true,
                    date: true
                },
                end_date:{
                    required : true,
                    date: true,
                    isAfterStartDate: true
                },
                description:{
                    required : true
                },


            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        //image, video input disable toggle
        $('input[type=radio][name=type]').change(function() {
            if (this.value == '0') {
                $('#front_img').fileinput('enable');
                $('#videoInput').prop('disabled',true);
            }
            else if (this.value == '1') {
                $('#front_img').fileinput('disable');
                $('#videoInput').prop('disabled',false);
            }
        });
    });

   $("#front_img").fileinput({
        uploadUrl: "", // server upload action
        uploadAsync: true,
        maxFileCount: 1,
        showUpload:false,
        initialPreview: [
        


        ],
        allowedFileExtensions: ["jpg", "gif", "png"]
    });


   $('#front_img').on('filecleared', function(event) {
      $('#image_hidden').val(1);
      console.log("filecleared");
  });




   // tinymce.init({
   //      selector: 'textarea',  // change this value according to your HTML
   //      plugins: [
   //      'advlist autolink lists link image charmap preview hr anchor pagebreak',
   //      'searchreplace wordcount visualblocks visualchars code',
   //      'insertdatetime media nonbreaking table contextmenu directionality',
   //      'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
   //      ],
   //      toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
   //      '| bullist numlist outdent indent | link | undo redo | forecolor backcolor emoticons ',
   //      fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
   //      menubar: false
   //  });


</script>
@stop
