<?php

namespace HonorManage;

use Illuminate\Support\ServiceProvider;

class HonorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'HonorManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('HonorManage', function($app){
            return new HonorManage;
        });
    }
}
