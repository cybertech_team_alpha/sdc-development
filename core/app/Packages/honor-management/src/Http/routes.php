<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2018 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'honor', 'namespace' => 'HonorManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      

      Route::get('add', [
        'as' => 'honor.add', 'uses' => 'HonorController@addView'
      ]);
      Route::get('edit/{id}', [
        'as' => 'honor.edit', 'uses' => 'HonorController@editView'
      ]);   
       Route::get('list', [
        'as' => 'honor.list', 'uses' => 'HonorController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'honor.list', 'uses' => 'HonorController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'honor.add', 'uses' => 'HonorController@add'
      ]);
      Route::post('edit/{id}', [
        'as' => 'honor.edit', 'uses' => 'HonorController@edit'
      ]);

      Route::post('delete', [
        'as' => 'honor.delete', 'uses' => 'HonorController@delete'
      ]);

     

    });
});