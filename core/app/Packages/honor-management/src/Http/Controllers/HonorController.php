<?php

namespace HonorManage\Http\Controllers;

use App\Http\Controllers\Controller;
use HonorManage\Http\Requests\HonorRequest;
use Illuminate\Http\Request;
use HonorManage\Models\Honor;
// use BlogManage\Models\BlogImage;
use Permissions\Models\Permission;
use Sentinel;
use Response;
use File;
use Image;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
class HonorController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Honor Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest');
    }

     /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
     public function addView() {

      return view('HonorManage::honor.add' );
      
      
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {

      try {
        $honor=Honor::findOrFail($id);;
        return view('HonorManage::honor.edit' )->with(['honor'=>$honor]);
      } catch (ModelNotFoundException $e) {
       return view('errors.404');
     }
     


   }
      /**
     * View Honor  List View
     *
     * @return Response
     */
      public function listView()
      {
        return view( 'HonorManage::honor.list' );
      }
    /**
   * Honor  list
   *
   * @return Response
   */
    public function jsonList(Request $request)
    {
      if($request->ajax()){
        $allHonor= Honor::where('status','=',1)->get();
        $jsonList = array();
        $i=1;
        foreach ($allHonor as $key => $honor) {

          $dd = array();
          array_push($dd, $i);
          array_push($dd,$honor->name);
          array_push($dd,$honor->description);

          $permissions = Permission::whereIn('name',['honor.edit','admin'])->where('status','=',1)->lists('name');
          if(Sentinel::hasAnyAccess($permissions)){
            array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('honor/edit/'.$honor->id).'\'" data-toggle="tooltip" data-placement="top" title="Honor"><i class="fa fa-pencil"></i></a></center>');
          }else{
            array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
          }

          $permissions = Permission::whereIn('name',['honor.delete','admin'])->where('status','=',1)->lists('name');
          if(Sentinel::hasAnyAccess($permissions)){
            array_push($dd, '<center><a href="#" class="honor-delete" data-id="'.$honor->id.'" data-toggle="tooltip" data-placement="top" title="Delete Honor"><i class="fa fa-trash-o"></i></a></center>');
          }else{
            array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
          }

          array_push($jsonList, $dd);
          $i++;
        }
        return Response::json(array('data'=>$jsonList));
      }else{
        return Response::json(array('data'=>[]));
      }
    }

    public function add(Request $request)
    {
      // return $request->all();
      if($request->get('type') == 0){
        if (!file_exists(storage_path('uploads/images/honor'))) {

          File::makeDirectory(storage_path('uploads/images/honor'));
        }

        $path='uploads/images/honor';
        $fileName='';

        if ($request->hasFile('front_image')) {
          $file = $request->file('front_image');
          $extn =$file->getClientOriginalExtension();
          $destinationPath = storage_path($path);
          $fileName = 'honor-' .date('YmdHis') .  '.' . $extn;
          $file->move($destinationPath, $fileName); 
        }
      }


      $honor = new Honor();
      $honor->name = $request->get('name');
      $honor->description = $request->get('description');

      if($request->get('type') == 0 && $request->hasFile('front_image')){
        $honor->filename = $fileName;
        $honor->path = $path;
      }

      if($request->get('type') == 1){
        $honor->url = $request->get('url');
      }
      $honor->more_link = $request->get('more_link');
      $honor->type = $request->get('type');
      $honor->save();

    //  $honor=Honor::create([
    //   'name'=>$request->get('name'),
    //   'description'=>$request->get('description'),
    //   'filename'=>$fileName,
    //   'path'=>$path,
    //   'url'=>$request->get('url'),
    //   'more_link'=>$request->get('more_link'),
    //   'type'=>$request->get('type')
    // ]);
      return redirect( 'honor/list')->with([ 'success' => true,
        'success.message'=> 'Honor Created successfully!',
        'success.title' => 'Good Job!' ]);

    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request,$id) {
      // dd('ss');
      if($request->get('type') == 0 && $request->hasFile('front_image')){
        if (!file_exists(storage_path('uploads/images/honor'))) {

          File::makeDirectory(storage_path('uploads/images/honor'));
        }
      // $this->validate($request, $this->getRules());

        $path='uploads/images/honor';
        $fileName='';

        $file = $request->file('front_image');
        $extn =$file->getClientOriginalExtension();
        $destinationPath = storage_path($path);
        $fileName = 'honor-' .date('YmdHis') .  '.' . $extn;
        $file->move($destinationPath, $fileName);   
      }

      $honor=Honor::find($id);
      $honor->name=$request->get('name');
      if($request->get('type') == 1){
        $honor->url=$request->get('url');
      }
      $honor->description=$request->get('description');
      $honor->type=$request->get('type');
      $honor->more_link=$request->get('more_link');
      if($request->get('type') == 0 && $request->hasFile('front_image')){
        $honor->path=$path;
        $honor->filename=$fileName;
      }
      $honor->save();
      return redirect( 'honor/edit/'.$id )->with([ 'success' => true,
        'success.message'=> 'Honor  updated successfully!',
        'success.title' => 'Good Job!' ]);
    }

    private function getRules($id = 0){
     return [
      // 'url' => 'required|regex:/^(?:https?:\/\/)?(?:www[.])?(?:youtube[.]com\/watch[?]v=|youtu[.]be\/)([^&]{11})$/',
    ];
  }

  public function delete(Request $request)
  {
    if($request->ajax()){
      $id = $request->input('id');

      $honor = Honor::find($id);
      if($honor){
        $honor->delete();
        return response()->json(['status' => 'success']);
      }else{
        return response()->json(['status' => 'invalid_id']);
      }
    }else{
      return response()->json(['status' => 'not_ajax']);
    }
  }

}
