<?php
namespace CouponManager\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'coupons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'no_of_coupons', 'value', 'start_date', 'end_date'];

    public function couponItems()
    {
      return $this->hasMany(CouponItem::class);
    }

}
