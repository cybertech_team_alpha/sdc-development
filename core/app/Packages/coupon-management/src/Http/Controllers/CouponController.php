<?php

namespace CouponManager\Http\Controllers;

use App\Http\Controllers\Controller;
use CakeTypeManager\Models\CakeType;
use Illuminate\Support\Facades\DB;
use Response;
use Sentinel;
use Illuminate\Http\Request;
use Validator;
use \CouponManager\Models\Coupon;
use \CouponManager\Models\CouponItem;
use Cart;

class CouponController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest');
    }

    public function index() {
        return view('CouponManager::coupon.list');
    }

    public function add() {
        return view('CouponManager::coupon.add');
    }

    public function save(Request $request) {
        $name = $request->get('name');
        $no_of_coupons = $request->get('no_of_coupons');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $value = $request->get('value');


        $coupon = Coupon::Create([
          'name' => $name,
          'no_of_coupons' => $no_of_coupons,
          'value' => $value,
          'start_date' => $start_date,
          'end_date' => $end_date,
          // 'code' => strtoupper(substr(uniqid(),0,12))
        ]);

        for ($i=0; $i < $coupon->no_of_coupons; $i++) {
          $coupon->couponItems()->create(['code' => strtoupper(substr(uniqid(),0,12))]);
        }


        return redirect('coupon/list')->with(['success' => true,
          'success.message' => 'Coupon Created successfully!',
          'success.title' => 'Well Done!']);
    }

    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Coupon::get();
            $jsonList = array();
            $i = 1;

            foreach ($data as $key => $coupon) {
                $dd = array();

                array_push($dd, $coupon->id);
                array_push($dd, $coupon->name);
                array_push($dd, $coupon->code);
                array_push($dd, $coupon->no_of_coupons);
                array_push($dd, $coupon->value);
                array_push($dd, $coupon->start_date);
                array_push($dd, $coupon->end_date);

                array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('coupon/edit/' . $coupon->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit Cake Type"><i class="fa fa-pencil"></i></a></center>');



                array_push($dd, '<center><a href="#" class="coupon-delete"  data-id="' . $coupon->id . '" data-toggle="tooltip" data-placement="top" title="Delete Cake Type"><i class="fa fa-trash-o"></i></a></center>');


                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    public function delete(Request $request) {
        if ($request->ajax()) {
            $id = $request->input('id');
            $coupon = Coupon::where('id', '=', $id)->first();
            if ($coupon) {
                $coupon->delete();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    public function editView($id) {
        $coupon = Coupon::with('couponItems')->find($id);
        return view('CouponManager::coupon.add')->with(['coupon' => $coupon]);
    }

    public function update(Request $request, $id) {

        $coupon = Coupon::with('couponItems')->find($id);
        $coupon->name = $request['name'];
        $coupon->value = $request['value'];
        $coupon->start_date = $request['start_date'];
        $coupon->end_date = $request['end_date'];
        $coupon->save();

        if ($request->new_coupons) {
          for ($i=0; $i < $request->new_coupons; $i++) {
            $coupon->couponItems()->create(['code' => strtoupper(substr(uniqid(),0,12))]);
          }

          $coupon->update([ 'no_of_coupons' =>  $coupon->no_of_coupons + $request->new_coupons]);
        }


        return redirect('coupon/list')->with(['success' => true,
                    'success.message' => 'Coupon Type Edited successfully!',
                    'success.title' => 'Well Done!']);
    }

    public function couponItemDelete($id)
    {
      $couponItem = CouponItem::with('coupon')->findOrFail($id);

      if ($couponItem->status) {
        return back()->with(['error' => true,
          'error.message' => 'Coupon already used',
          'error.title' => 'Coupon allready used']);
      }

      $couponItem->delete();

      $couponItem->coupon->update(['no_of_coupons' => $couponItem->coupon->couponItems->count()]);

      return back()->with(['success' => true,
        'success.message' => 'Coupon Code been Deleted!',
        'success.title' => 'Well Done!']);
    }
    /*FRONT END*/

    public function verify_coupon(Request $request)
    {
       if ($request->ajax()) {
          $coupon_code=$request->get('coupon_code');
          $curDate=date('Y-m-d');
          $coupon_code=str_replace('-','',$coupon_code);
          $coupon=CouponItem::with(['coupon' => function($query) use ($curDate){
                    $query->where('start_date','<=',$curDate)->where('end_date','>=',$curDate);
                }])->where('status',0)->where('code',$coupon_code)->first();
          if (!empty($coupon->coupon)) {            
            $this->coupon_condition_add($coupon->code,$coupon->coupon->value);
            $total = Cart::getTotal();
            return Response::json(array('coupon' => $coupon,'cart_total'=>$total));          
          } else {
            return Response::json(array('coupon' => []));
          }
       }
       
       
       
    }
     public function remove_coupon(Request $request)
    {
       if ($request->ajax()) {
            $coupon_condition = Cart::getCondition('coupon');
            if ($coupon_condition) {
                 Cart::removeCartCondition('coupon');
                 $total = Cart::getTotal();
                 return Response::json(array('done'=>1,'cart_total'=>$total));     
            }else{
               return Response::json(array('done'=>0));     
            }
       }
       
       
       
    }
    public function coupon_condition_add($coupon_code,$coupon_amount)
    {
      $coupon_condition = new \Darryldecode\Cart\CartCondition(array(
          'name' => 'coupon',
          'type' => 'coupon',
          'target' => 'subtotal',
          'value' => '-'.$coupon_amount,
          'attributes' => array( // attributes field is optional
            'coupon_code' => $coupon_code
          )
      ));
      Cart::condition($coupon_condition);

    }
    public function getCartDetails()
    {
      # code...
    }

}
