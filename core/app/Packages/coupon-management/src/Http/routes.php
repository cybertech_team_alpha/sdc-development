<?php

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'coupon', 'namespace' => 'CouponManager\Http\Controllers'], function () {

        Route::get('list', [
            'as' => 'coupon.list', 'uses' => 'CouponController@index'
        ]);

        Route::get('json/list', [
            'as' => 'coupon.list', 'uses' => 'CouponController@jsonList'
        ]);
        Route::get('add', [
            'as' => 'coupon.add', 'uses' => 'CouponController@add'
        ]);

        Route::get('edit/{id}', [
            'as' => 'coupon.edit', 'uses' => 'CouponController@editView'
        ]);

        Route::get('coupon-item/delete/{id}', ['as' => 'coupon-item.delete' , 'uses' => 'CouponController@couponItemDelete']);


        Route::post('add', [
            'as' => 'coupon.add', 'uses' => 'CouponController@save'
        ]);

        Route::post('delete', [
            'as' => 'coupon.delete', 'uses' => 'CouponController@delete'
        ]);

        Route::post('edit/{id}', [
            'as' => 'coupon.edit', 'uses' => 'CouponController@update'
        ]);



    });
});
/*FRONT END*/
Route::group(['prefix' => 'coupon', 'namespace' => 'CouponManager\Http\Controllers'], function()
{
   Route::get('verify', [
        'as' => 'index', 'uses' => 'CouponController@verify_coupon'
      ]);
   Route::get('remove', [
    'as' => 'index', 'uses' => 'CouponController@remove_coupon'
  ]); 
    
  
});