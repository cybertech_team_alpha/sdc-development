<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2018 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'pricing', 'namespace' => 'PricingManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      

      // Route::get('add', [
      //   'as' => 'pricing.add', 'uses' => 'PricingController@addView'
      // ]);
      Route::get('edit/{id}', [
        'as' => 'pricing.edit', 'uses' => 'PricingController@editView'
      ]);   
       Route::get('list', [
        'as' => 'pricing.list', 'uses' => 'PricingController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'pricing.list', 'uses' => 'PricingController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'pricing.add', 'uses' => 'PricingController@add'
      ]);
      Route::post('edit/{id}', [
        'as' => 'pricing.edit', 'uses' => 'PricingController@edit'
      ]);

      Route::post('delete', [
        'as' => 'pricing.delete', 'uses' => 'PricingController@delete'
      ]);

     

    });
});