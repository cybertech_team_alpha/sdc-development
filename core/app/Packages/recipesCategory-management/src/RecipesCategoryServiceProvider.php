<?php

namespace RecipesCategoryManager;

use Illuminate\Support\ServiceProvider;

class RecipesCategoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'RecipesCategoryManager');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('RecipesCategoryManager', function($app){
            return new RecipesCategoryManager;
        });
    }
}
