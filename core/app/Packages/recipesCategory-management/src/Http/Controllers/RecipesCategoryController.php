<?php

namespace RecipesCategoryManager\Http\Controllers;

use App\Http\Controllers\Controller;
use CakeTypeManager\Models\CakeType;
use Illuminate\Support\Facades\DB;
use RecipesCategoryManager\Models\RecipesCategory;
use Response;
use Sentinel;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use SliderManager\Models\Slider;
use TestimonialsManager\Models\Testimonials;
use Validator;
use Image;
use SliderManager\Models\AnimationType;


class RecipesCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function index()
    {
        return view('RecipesCategoryManager::recipesCategory.list');
    }

    public function add()
    {
        return view('RecipesCategoryManager::recipesCategory.add');
    }

    public function save(Request $request)
    {
        $recipe=New RecipesCategory();
        $recipe->name=$request->txtType;
        $recipe->save();

        return redirect('recipesCategory/list')->with(['success' => true,
            'success.message' => ' Recipes Category Created successfully!',
            'success.title' => 'Well Done!']);

    }

    public function jsonList(Request $request)
    {
        if ($request->ajax()) {
            $data=RecipesCategory::where('status','=',1)->get();
            $jsonList = array();
            $i = 1;

            foreach ($data as $key => $recipe) {
                $dd = array();
                array_push($dd, $i);
                array_push($dd, $recipe->name);

                $permissions = Permission::whereIn('name', ['recipesCategory.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('recipesCategory/edit/' . $recipe->recipes_category_id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit Recipes Category"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['recipesCategory.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="caketype-delete" data-id="' . $recipe->recipes_category_id . '" data-toggle="tooltip" data-placement="top" title="Delete Recipes Category"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    public function delete(Request $request){
        if($request->ajax()){
            $id = $request->input('id');
            $recipe = RecipesCategory::where('recipes_category_id','=',$id)->first();
            if($recipe){
                $recipe->status=0;
                $recipe->save();
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    public function editView($id){
        $recipe = RecipesCategory::where('recipes_category_id','=',$id)->first();
        return view('RecipesCategoryManager::recipesCategory.edit')->with(['recipe' => $recipe]);
    }

    public function update(Request $request ,$id){
        $recipe =RecipesCategory::find($id)->first();
        $recipe->name=$request->txtType;
        $recipe->save();


        return redirect('recipesCategory/list')->with(['success' => true,
            'success.message' => 'Recipes Category Edited successfully!',
            'success.title' => 'Well Done!']);
    }
}
