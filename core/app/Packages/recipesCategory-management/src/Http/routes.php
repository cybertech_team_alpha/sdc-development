<?php

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'recipesCategory', 'namespace' => 'RecipesCategoryManager\Http\Controllers'], function () {

        Route::get('list', [
            'as' => 'recipesCategory.list', 'uses' => 'RecipesCategoryController@index'
        ]);

        Route::get('json/list', [
            'as' => 'recipesCategory.list', 'uses' => 'RecipesCategoryController@jsonList'
        ]);
        Route::get('add', [
            'as' => 'recipesCategory.add', 'uses' => 'RecipesCategoryController@add'
        ]);

        Route::get('edit/{id}', [
            'as' => 'recipesCategory.edit', 'uses' => 'RecipesCategoryController@editView'
        ]);


        Route::post('add', [
            'as' => 'recipesCategory.add', 'uses' => 'RecipesCategoryController@save'
        ]);

        Route::post('delete', [
            'as' => 'recipesCategory.delete', 'uses' => 'RecipesCategoryController@delete'
        ]);

        Route::post('edit/{id}', [
            'as' => 'recipesCategory.edit', 'uses' => 'RecipesCategoryController@update'
        ]);

    });
});