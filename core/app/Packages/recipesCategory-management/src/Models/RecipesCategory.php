<?php

namespace RecipesCategoryManager\Models;

use Illuminate\Database\Eloquent\Model;

class RecipesCategory extends Model
{
    protected $table = 'recipes_category';

    protected $primaryKey = 'recipes_category_id';

}
