<?php

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'publication', 'namespace' => 'PublicationManager\Http\Controllers'], function () {

        Route::get('list', [
            'as' => 'publication.list', 'uses' => 'PublicationController@SliderList'
        ]);

        Route::get('json/list', [
            'as' => 'publication.list', 'uses' => 'PublicationController@jsonList'
        ]);
        Route::get('add', [
            'as' => 'publication.add', 'uses' => 'PublicationController@add'
        ]);

        Route::get('edit/{id}', [
            'as' => 'publication.edit', 'uses' => 'PublicationController@editView'
        ]);

        Route::get('image/{id}', [
            'as' => 'publication.edit', 'uses' => 'PublicationController@getImage'
        ]);


        Route::post('add', [
            'as' => 'publication.add', 'uses' => 'PublicationController@save'
        ]);

        Route::post('delete', [
            'as' => 'publication.delete', 'uses' => 'PublicationController@delete'
        ]);

        Route::post('edit/{id}', [
            'as' => 'publication.edit', 'uses' => 'PublicationController@update'
        ]);

    });
});