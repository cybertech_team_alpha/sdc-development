<?php

namespace PublicationManager\Models;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model{
	protected $table = 'publication';

    protected $primaryKey = 'publication_id';

}
