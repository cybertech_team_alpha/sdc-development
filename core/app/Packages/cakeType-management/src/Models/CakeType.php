<?php

namespace CakeTypeManager\Models;

use Illuminate\Database\Eloquent\Model;

class CakeType extends Model{
	protected $table = 'cake_type';

    protected $primaryKey = 'cake_type_id';

}
