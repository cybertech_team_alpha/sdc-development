<?php

namespace CakeTypeManager\Http\Controllers;

use App\Http\Controllers\Controller;
use CakeTypeManager\Models\CakeType;
use Illuminate\Support\Facades\DB;
use Response;
use Sentinel;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use SliderManager\Models\Slider;
use TestimonialsManager\Models\Testimonials;
use Validator;
use Image;
use SliderManager\Models\AnimationType;


class CakeTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function index()
    {
        return view('CakeTypeManager::cakeType.list');
    }

    public function add()
    {
        return view('CakeTypeManager::cakeType.add');
    }

    public function save(Request $request)
    {
        $cake=New CakeType();
        $cake->type=$request->txtType;
        $cake->save();

        return redirect('caketype/list')->with(['success' => true,
            'success.message' => 'Cake Type Created successfully!',
            'success.title' => 'Well Done!']);

    }

    public function jsonList(Request $request)
    {
        if ($request->ajax()) {
            $data=CakeType::where('status','=',1)->get();
            $jsonList = array();
            $i = 1;

            foreach ($data as $key => $cake) {
                $dd = array();
                array_push($dd, $i);
                array_push($dd, $cake->type);

                $permissions = Permission::whereIn('name', ['caketype.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('caketype/edit/' . $cake->cake_type_id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit Cake Type"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['caketype.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="caketype-delete" data-id="' . $cake->cake_type_id . '" data-toggle="tooltip" data-placement="top" title="Delete Cake Type"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    public function delete(Request $request){
        if($request->ajax()){
            $id = $request->input('id');
            $cake = CakeType::where('cake_type_id','=',$id)->first();
            if($cake){
                $cake->status=0;
                $cake->save();
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    public function editView($id){
        $cake = CakeType::where('cake_type_id','=',$id)->first();
        return view('CakeTypeManager::cakeType.edit')->with(['cake' => $cake]);
    }

    public function update(Request $request ,$id){
        $cake =CakeType::find($id)->first();
        $cake->type=$request->txtType;
        $cake->save();


        return redirect('caketype/list')->with(['success' => true,
            'success.message' => 'Cake Type Edited successfully!',
            'success.title' => 'Well Done!']);
    }
}
