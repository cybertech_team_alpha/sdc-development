<?php

namespace CakeTypeManager;

use Illuminate\Support\ServiceProvider;

class CakeTypeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'CakeTypeManager');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('CakeTypeManager', function($app){
            return new CakeTypeManager;
        });
    }
}
