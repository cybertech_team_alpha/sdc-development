@extends('layouts.back.master') @section('current_title','Update Pages')
@section('css')
<link rel="stylesheet" href="{{asset('assets/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('pages/list')}}">Pages Management</a></li>

        <li class="active">
            <span>Pages Feature</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form method="POST" class="form-horizontal" id="form">
                    {!!Form::token()!!}



                    <div class="form-group"><label class="col-sm-2 control-label">Page Name</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="name" value="{{$data->page_name}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Allies Name</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="allies" value="{{$data->allies_name}}"></div>
                    </div>

                    <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                        <div class="col-sm-10"><textarea name="desc" class="form-control">{{$data->content}}</textarea></div>

                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">STATUS</label>
                        <input type="checkbox" class="form-check-input" value="checked" name="status"  @if(old('status',$data->status)==1) checked @endif>

                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-2">
                            <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                            <button class="btn btn-primary" type="submit">Save Changes</button>
                        </div>
                    </div>

                </form>
                </div>
            </div>
        </div>
        @stop
        @section('js')
        <script src="{{asset('assets/vendor/select2-3.5.2/select2.min.js')}}"></script>
        <script src="{{asset('assets/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
 <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script> <script type="text/javascript">
//<![CDATA[
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
  //]]>
  </script>
        @stop
