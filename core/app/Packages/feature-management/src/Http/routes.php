<?php
/**
 * MENU MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'feature', 'namespace' => 'FeatureManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'feature.add', 'uses' => 'FeatureController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'feature.edit', 'uses' => 'FeatureController@editView'
      ]);

      Route::get('list', [
        'as' => 'feature.list', 'uses' => 'FeatureController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'feature.list', 'uses' => 'FeatureController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'feature.add', 'uses' => 'FeatureController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'feature.edit', 'uses' => 'FeatureController@edit'
      ]);

      Route::post('status', [
        'as' => 'feature.status', 'uses' => 'FeatureController@status'
      ]);

      Route::post('delete', [
        'as' => 'feature.delete', 'uses' => 'FeatureController@delete'
      ]);
    });
});
