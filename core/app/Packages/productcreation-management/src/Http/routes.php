<?php
/**
 * ProductCreation MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'product/creation/category', 'namespace' => 'ProductCreationManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'product.creation.category.add', 'uses' => 'ProductCreationController@addView_category'
      ]);

      Route::get('edit/{id}', [
        'as' => 'product.creation.category.edit', 'uses' => 'ProductCreationController@editView_category'
      ]);

      Route::get('list', [
        'as' => 'product.creation.category.list', 'uses' => 'ProductCreationController@listView_category'
      ]);

      Route::get('json/list', [
        'as' => 'product.creation.category.list', 'uses' => 'ProductCreationController@jsonList_category'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'product.creation.category.add', 'uses' => 'ProductCreationController@add_category'
      ]);

      Route::post('edit/{id}', [
        'as' => 'product.creation.category.edit', 'uses' => 'ProductCreationController@edit_category'
      ]);

      Route::post('status', [
        'as' => 'product.creation.category.status', 'uses' => 'ProductCreationController@status_category'
      ]);

      Route::post('delete', [
        'as' => 'product.creation.category.delete', 'uses' => 'ProductCreationController@delete_category'
      ]);
    });
   Route::group(['prefix' => 'product/creation', 'namespace' => 'ProductCreationManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'product.creation.add', 'uses' => 'ProductCreationController@addView_product'
      ]);

      Route::get('edit/{id}', [
        'as' => 'product.creation.edit', 'uses' => 'ProductCreationController@editView_product'
      ]);

      Route::get('list', [
        'as' => 'product.creation.list', 'uses' => 'ProductCreationController@listView_product'
      ]);

      Route::get('json/list', [
        'as' => 'product.creation.list', 'uses' => 'ProductCreationController@jsonList_product'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'product.creation.add', 'uses' => 'ProductCreationController@add_product'
      ]);

      Route::post('edit/{id}', [
        'as' => 'product.creation.edit', 'uses' => 'ProductCreationController@edit_product'
      ]);

      Route::post('status', [
        'as' => 'product.creation.status', 'uses' => 'ProductCreationController@status_product'
      ]);

      Route::post('delete', [
        'as' => 'product.creation.delete', 'uses' => 'ProductCreationController@delete_product'
      ]);
      Route::delete('image/deleteFile', [
      'as' => 'product.creation.edit', 'uses' => 'ProductCreationController@jsonImageFileDelete_product',
    ]);
    });
});
