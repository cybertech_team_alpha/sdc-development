<?php
namespace ProductCreationManage\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ShoppingCartController ;
use ProductCreationManage\Http\Requests\ProductCreationRequest;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use ProductCreationManage\Models\ProductCreationCategory;
use ProductCreationManage\Models\ProductCreation;
use ProductCreationManage\Models\ProductCreationImage;
use ProductCreationManage\Models\ProductHasCategory;
use App\Models\MailTypeUsps;
use App\Models\ServiceTypeUsps;
use App\Models\SizeUsps;

// use DeviceManage\Models\Device;
use Sentinel;
use Response;
use File;
use Input;
use Usps;



class ProductCreationController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| ProductCreation Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	public function __construct()
	{


		//$this->middleware('guest');
	}

	/**
	 * Show the GALLERY CATEGORY add screen to the user.
	 *
	 * @return Response
	 */
	public function addView_category()
	{

		return view( 'ProductCreationManage::category.add');
	}

	/**
	 * Add new GALLERY CATEGORY data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add_category(ProductCreationRequest $request)
	{

        $count= ProductCreationCategory::where('name', '=',$request->get('name' ))->count();

        if ($count==0) {

            $productCreationCategory = ProductCreationCategory::Create(array(
                    'name' => $request->input('name'),
                    'created_by'=>Sentinel::getUser()->id,
                )
            );

            if ($productCreationCategory) {
                return redirect('product/creation/category/add')->with(['success' => true,
                    'success.message' => 'Product Cration Category Created successfully!',
                    'success.title' => 'Well Done!']);
            }
        }
        else{
            return redirect('product/creation/category/add')->with([ 'error' => true,
                'error.message'=> 'Product Creation Category Already Exsist!',
                'error.title' => 'Duplicate!']);
        }
	}

	/**
	 * View GALLERY CATEGORY List View
	 *
	 * @return Response
	 */
	public function listView_category()
	{
		return view( 'ProductCreationManage::category.list' );
	}

	/**
	 * GALLERY CATEGORY list
	 *
	 * @return Response
	 */
	public function jsonList_category(Request $request)
	{
		if($request->ajax()){
			$productCreationCategories= ProductCreationCategory::where('status','=',1)->get();
			$jsonList = array();
			$i=1;
			foreach ($productCreationCategories as $key => $productCreationCategory) {

				$dd = array();
				array_push($dd, $i);
                array_push($dd,$productCreationCategory->name);

				$permissions = Permission::whereIn('name',['product.creation.category.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('product/creation/category/edit/'.$productCreationCategory->id).'\'" data-toggle="tooltip" data-placement="top" title="Gallery Category Branch"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['product.creation.category.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="product-creation-category-delete" data-id="'.$productCreationCategory->id.'" data-toggle="tooltip" data-placement="top" title="Delete Series"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate user
	 * @param  Request $request series id with status to change
	 * @return json object with status of success or failure
	 */
	public function status_category(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$branch = Branch::find($id);
			if($branch){
				$branch->status = $status;
				$branch->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete a GALLERY CATEGORY
	 * @param  Request $request series id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete_category(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$productCreationCategory = ProductCreationCategory::find($id);
			if($productCreationCategory){
                $productCreationCategory->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the GALLERY CATEGORY edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView_category($id)
	{
		$productCreationCategory=ProductCreationCategory::find($id);
		if($productCreationCategory){
			return view( 'ProductCreationManage::category.edit' )->with(['productCreationCategory'=>$productCreationCategory]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new PRODUCT CREATION CATEGORY data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit_category(ProductCreationRequest $request, $id)
	{
		$count= ProductCreationCategory::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();

			if($count==0){
                $productCreationCategory =  ProductCreationCategory::find($id);
                $productCreationCategory->name = $request->get('name');
                $productCreationCategory->save();

				return redirect( 'product/creation/category/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Product Creation Category updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('product/creation/category/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Gallery Category Already Exsist!',
				'error.title' => 'Duplicate!']);
			}




	}

	/****************************************GALLERY ******/


	/**
	 * Show the GALLERY  add screen to the user.
	 *
	 * @return Response
	 */
	public function addView_product()
	{
		$productCategory=ProductCreationCategory::get();
		$usps_service_types=ServiceTypeUsps::get();
		$usps_mail_types=MailTypeUsps::get();
		$usps_sizes=SizeUsps::get();
		return view( 'ProductCreationManage::product.add')->with(['productCategory'=>$productCategory,
			'usps_service_types'=>$usps_service_types,'usps_mail_types'=>$usps_mail_types,'usps_sizes'=>$usps_sizes]);;
	}

	/**
	 * Add new GALLERY CATEGORY data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add_product(ProductCreationRequest $request,ShoppingCartController $shoppingCartController)
	{
		// ret$request->get('start_date');
		$date_start='';
		$date_end='';
		if ($request->get('on_sale')=='on') {
			if ($request->get('start_date')!='' && $request->get('end_date')!='') {
				$date_start=date_create($request->get('start_date'));
				$date_end=date_create($request->get('end_date'));
				date_format($date_start,"Y-m-d");
				date_format($date_end,"Y-m-d");
			}else{
				return [ 'error' => true,
		                'error.message'=> 'Start End date cannot be empty',

		                'error.title' => 'error!'];
			}

		}

		$cat= $request->get('product_category');
		if (!empty($cat)) {
			$count= ProductCreation::where('product_name', '=',$request->get('product_name' ))->count();
	        if (!file_exists(storage_path('uploads/images/ProductCreation'))) {
				 File::makeDirectory(storage_path('uploads/images/ProductCreation'));
			}
			$path='uploads/images/ProductCreation';

			$mail_type=MailTypeUsps::find($request->input('usps_mail_type'));
			$service_type=ServiceTypeUsps::find($request->input('usps_service_type'));
			$size=SizeUsps::find($request->input('usps_sizes'));

			 $rate=$shoppingCartController->get_usps_domestric_rate($service_type->name,$mail_type->name,'91601','91730',$request->input('usps_pounds'),$request->input('weight')*0.035274,$size->name);


			 $coverpath = 'uploads/images/ProductCreation';
			 $coverfileName = 'product-cover-' . date('YmdHis') . '.png';
			 $this->save_cover_image($request->cvr_img, 'core/storage/'.$coverpath.'/'.$coverfileName);


			if (is_numeric($rate) && $rate>0) {
				if ($count==0) {
		            $productCreation = ProductCreation::Create(array(
		                    'product_name' => $request->input('product_name'),
		                    'description' => $request->input('description'),
		                    'cover_path' => $coverpath,
		                    'cover_file' => $coverfileName,
		                    'categories' =>$cat,
		                    'price' => $request->input('price'),
		                    'width' => $request->input('width'),
		                    'length' => $request->input('length'),
		                    'height' => $request->input('height'),
		                    'weight' => $request->input('weight'),
		                    'on_sale' => $request->input('on_sale'),
		                    'sale_price' => $request->input('sale_price'),
		                    'start_date' => $date_start,
		                    'end_date' => $date_end,
		                    'qtyonhand' => $request->input('qtyonhand'),
		                    'popularity' => $request->input('popularity'),
		                    'usps_mail_type' => $request->input('usps_mail_type'),
		                    'usps_service_type' => $request->input('usps_service_type'),
		                    'usps_size' => $request->input('usps_sizes'),
		                    'pounds' => $request->input('usps_pounds'),
		                    'ounces' => $request->input('weight')*0.035274,
		                    'created_by'=>Sentinel::getUser()->id,
		                )
		            );

		            if ($productCreation) {
		            	foreach ($cat as $key => $value) {
		            		ProductHasCategory::Create([
		            		'category_id'=>$value,
		            		'product_id'=>$productCreation->id,
		            		]);
		            	}

		            	$project_files = $request->file('product_image');
						$i=0;
						if (!empty($project_files)) {
							foreach ($project_files as $key => $project_file) {
								if (File::exists($project_file)) {
										$file = $project_file;
										$extn =$file->getClientOriginalExtension();
										$destinationPath = storage_path($path);
										$project_fileName = 'product-creation-' .date('YmdHis') .'-'.$i. '.' . $extn;
										$file->move($destinationPath, $project_fileName);

										ProductCreationImage::create([
											'productCreation_id'=>$productCreation->id,
											'path'=>$path,
											'filename'=>$project_fileName
										]);
										$i++;
								 }
							}
						}


		                return ['success' => true,
		                    'success.message' => 'Product Creation  Created successfully!',
		                    'success.title' => 'Well Done!'];
		            }
		        }
		        else{
		            return [ 'error' => true,
		                'error.message'=> 'Gallery  Already Exsist!',
		                'error.message'=> 'Gallery  Already Exsist!',
		                'error.title' => 'Duplicate!'];
		        }
			}else{
				return [ 'error' => true,
		                'error.message'=> 'Invalied Values USPS',

		                'error.title' => 'error!'];
			}
		}else{
			return [ 'error' => true,
		                'error.message'=> 'Please Select category',

		                'error.title' => 'error!'];
		}



	}

	/**
	 * View GALLERY CATEGORY List View
	 *
	 * @return Response
	 */
	public function listView_product()
	{
		return view( 'ProductCreationManage::product.list' );
	}

	/**
	 * GALLERY CATEGORY list
	 *
	 * @return Response
	 */
	public function jsonList_product(Request $request)
	{
		if($request->ajax()){
			$productCreations= ProductCreation::where('status','=',1)->get();
			$jsonList = array();
			$i=1;
			foreach ($productCreations as $key => $productCreation) {

				$dd = array();
				array_push($dd, $i);
                array_push($dd,$productCreation->product_name);
                array_push($dd,$productCreation->price);
                array_push($dd,$productCreation->width);
                array_push($dd,$productCreation->length);
                array_push($dd,$productCreation->height);
                array_push($dd,$productCreation->weight);
                array_push($dd,$productCreation->qtyonhand);
                array_push($dd,$productCreation->popularity);

				$permissions = Permission::whereIn('name',['product.creation.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('product/creation/edit/'.$productCreation->id).'\'" data-toggle="tooltip" data-placement="top" title="Gallery "><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['product.creation.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="product-creation-delete" data-id="'.$productCreation->id.'" data-toggle="tooltip" data-placement="top" title="Delete Gallery"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Activate or Deactivate user
	 * @param  Request $request series id with status to change
	 * @return json object with status of success or failure
	 */
	public function status_product(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');
			$status = $request->input('status');

			$branch = Branch::find($id);
			if($branch){
				$branch->status = $status;
				$branch->save();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Delete a GALLERY CATEGORY
	 * @param  Request $request series id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete_product(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$productCreation = ProductCreation::find($id);
			if($productCreation){
                $productCreation->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the GALLERY CATEGORY edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView_product($id)
	{
		$productCreation=ProductCreation::find($id);
		$productCategory=ProductCreationCategory::get();
		$usps_service_types=ServiceTypeUsps::get();
		$usps_mail_types=MailTypeUsps::get();
		$usps_sizes=SizeUsps::get();

		if($productCreation){
			 // $cur_category=json_decode($productCreation->categories);
			 $cur_category=$productCreation->categories;
			$image_url_array=[];
			 $image_config_array=[];
			  foreach ($productCreation->getImages as $key => $value) {
					array_push($image_url_array,"<img style='height:160px' src='". url('') . '/core/storage/' .$value->path.'/'.$value->filename."'>");

			  }

			  foreach ($productCreation->getImages as $key => $value) {
					array_push($image_config_array,array('caption'=>$value->filename,'caption'=>$value->filename,'key'=>$value->id,'url'=>url('product/creation/image/deleteFile')));

			  }
			return view( 'ProductCreationManage::product.edit' )->with(['productCreation'=>$productCreation,'productCategory'=>$productCategory,'cur_category'=>$cur_category,'images'=>$image_url_array,'image_config'=>$image_config_array,'usps_service_types'=>$usps_service_types,'usps_mail_types'=>$usps_mail_types,'usps_sizes'=>$usps_sizes]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new GALLERY CATEGORY data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit_product(ProductCreationRequest $request, $id)
	{

		if ($request->get('on_sale')=='on') {
			if ($request->get('start_date')!='' && $request->get('end_date')!='') {

			}else{
				return redirect('product/creation/edit/'.$id)->with([ 'error' => true,
		                'error.message'=> 'Start End date cannot be empty',

		                'error.title' => 'error!'])->withInput(Input::all());
			}

		}
		if (!file_exists(storage_path('uploads/images/ProductCreation'))) {

			File::makeDirectory(storage_path('uploads/images/ProductCreation'));
		 }
		$count= ProductCreation::where('id', '!=', $id)->where('product_name', '=',$request->get('product_name' ))->count();

			if($count==0){
				$cat= $request->get('product_category');
                $productCreation =  ProductCreation::find($id);

								if ($request->cvr_img != 'notChanged') {
				          $coverpath = 'uploads/images/ProductCreation';
				          $coverfileName = 'product-cover-' . date('YmdHis') . '.png';
				          $this->save_cover_image($request->cvr_img, 'core/storage/'.$coverpath.'/'.$coverfileName);

				          $productCreation->cover_path = $coverpath;
				          $productCreation->cover_file = $coverfileName;
				        }

                $productCreation->product_name = $request->get('product_name');
                $productCreation->description = $request->get('description');
                $productCreation->categories = $cat;
                $productCreation->height = $request->get('height');
                $productCreation->width = $request->get('width');
                $productCreation->length = $request->get('length');
                $productCreation->weight = $request->get('weight');
                $productCreation->price = $request->get('price');
                $productCreation->qtyonhand = $request->get('qtyonhand');
                $productCreation->popularity = $request->get('popularity');
                $productCreation->start_date = $request->get('start_date');
                $productCreation->end_date = $request->get('end_date');
                $productCreation->sale_price = $request->get('sale_price');
                $productCreation->on_sale = $request->get('on_sale');

				$path='uploads/images/ProductCreation';

                 $productCreation->save();

                $curProductHasCategory=ProductHasCategory::where('product_id',$id)->delete();
                foreach ($cat as $key => $value) {
            		ProductHasCategory::Create([
            		'category_id'=>$value,
            		'product_id'=>$productCreation->id,
            		]);
            	}




				$project_files = $request->file('product_image');
				$i=0;
				if(!empty($project_files)){
					foreach ($project_files as $key => $project_file) {
						if (File::exists($project_file)) {
								$file = $project_file;
								$extn =$file->getClientOriginalExtension();
								$destinationPath = storage_path($path);
								$project_fileName = 'product-creation-' .date('YmdHis') .'-'.$i. '.' . $extn;
								$file->move($destinationPath, $project_fileName);

								ProductCreationImage::create([
									'productCreation_id'=>$productCreation->id,
									'path'=>$path,
									'filename'=>$project_fileName
								]);
								$i++;
						 }
					}
				}

				return redirect( 'product/creation/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Product Creation  updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('product/creation/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Product Creation  Already Exsist!',
				'error.title' => 'Duplicate!']);
			}




	}
	public function jsonImageFileDelete_product(Request $request)
	{
		$image_id=$request->get('key');
		$image=ProductCreationImage::find($image_id);
		$image->delete();
		return 1;
	}


	function save_cover_image($image, $path){// sase base64 encoded image,  path with image name

		list($type, $image) = explode(';', $image);
		list(, $image)      = explode(',', $image);
		$image = base64_decode($image);

		file_put_contents($path, $image);

	}

}
