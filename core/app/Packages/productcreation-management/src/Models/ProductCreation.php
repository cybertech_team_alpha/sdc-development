<?php
namespace ProductCreationManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

/**
 * ProductCreation Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class ProductCreation extends Model{
	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_productcreation';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $casts = [
        'categories' => 'json'
    ];
	protected $fillable = ['product_name','description','categories','price','width','length','height','weight','qtyonhand','popularity','path','filename','usps_mail_type','usps_service_type','usps_size','pounds','ounces','created_by','status','on_sale','sale_price','start_date','end_date', 'cover_path', 'cover_file'];

	public function getImages()
	{
		return $this->hasMany('ProductCreationManage\Models\ProductCreationImage', 'productCreation_id', 'id');
	}
	public function reviews()
	{
		return $this->hasMany('App\Models\Rating', 'product_id', 'id');
	}
	public function getUspsMailType()
	{
		return $this->belongsTo('App\Models\MailTypeUsps', 'usps_mail_type', 'id');
	}
	public function getUspsServiceType()
	{
		return $this->belongsTo('App\Models\ServiceTypeUsps', 'usps_service_type', 'id');
	}
	public function getUspsSize()
	{
		return $this->belongsTo('App\Models\SizeUsps', 'usps_size', 'id');
	}

	/**
	 * check product is in sale
	 *
	 * @return boolean
	 **/
	public function checkInSale()
	{
	    try{
            return $this->on_sale == 'on' & date('Y-m-d') <= $this->end_date & date('Y-m-d') >= $this->start_date;
        }catch (\Exception $ex){
            Log::error("Error in ProductCreation::checkInSale() error => " . $ex->getMessage(), $this->toArray());
        }
	    return false;
	}






}
