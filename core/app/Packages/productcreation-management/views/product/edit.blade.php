@extends('layouts.back.master') @section('current_title','Update Product Creation')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" />
<style media="screen">
.imageBox
{
position: relative;
width: 400px;
height: 400px;
border:1px solid #aaa;
background: #fff;
overflow: hidden;
background-repeat: no-repeat;
cursor:move;
}

.imageBox .thumbBox
{
position: absolute;
top: 50%;
left: 0;
right: 0;
top: 0;
bottom: 0;
width: 350px;
height: 350px;
margin-top: auto;
margin-bottom: auto;
margin-left: auto;
margin-right: auto;
box-sizing: border-box;
border: 1px solid rgb(102, 102, 102);
box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
background: none repeat scroll 0% 0% transparent;
}

.imageBox .spinner
{
position: absolute;
top: 0;
left: 0;
bottom: 0;
right: 0;
text-align: center;
line-height: 400px;
background: rgba(0,0,0,0.7);
}
</style>
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('product/creation/list')}}">Product Creation Management</a></li>

        <li class="active">
            <span>Product Creation Series</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                    {!!Form::token()!!}

                   <div class="form-group"><label class="col-sm-2 control-label">PRODUCT NAME</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="product_name" value="{{$productCreation->product_name}}"></div>
                    </div>
                    <div class="form-group">
                      <input type="hidden" id="cvr_img" name="cvr_img" value="notChanged" required form="form">
                        <label class="col-sm-2 control-label required">COVER IMAGE</label>
                        <div class="col-sm-10">
                          <div class="col-sm-10">
                            <div class="col-md-6">
                              <button type="button" class="btn bg-grey waves-effect" data-toggle="modal" data-target="#crop-modal" >Select Cover Image</button>
                            </div>
                            <div class="col-md-6">
                              <div class="cropped">
                                <img src="{{url('core/storage/'.$productCreation->cover_path.'/'.$productCreation->cover_file)}}" alt="" style="max-width : 100%;">
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                         <div class="col-sm-10"><textarea name="description" class="form-control">{{$productCreation->description}}</textarea></div>

                    </div>
                      <div class="form-group"><label class="col-sm-2 control-label">CATEGORY</label>
                         <div class="col-sm-10">
                         <select class="js-source-states" multiple="multiple" name="product_category[]" style="width: 100%">
                         <?php foreach ($productCategory as $key => $value): ?>
                          <?php if (count($cur_category)>0): ?>
                              <?php if (in_array($value->id, $cur_category)): ?>
                                <option selected="true" value="{{$value->id}}">{{$value->name}}</option>
                            <?php else: ?>
                                <option  value="{{$value->id}}">{{$value->name}}</option>
                            <?php endif ?>
                          <?php else: ?>
                            <option  value="{{$value->id}}">{{$value->name}}</option>

                          <?php endif ?>


                         <?php endforeach ?>


                        </select>
                    </div>

                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">PRICE</label>
                        <div class="col-sm-5"><input type="text" class="form-control" value="{{$productCreation->price}}" name="price"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">SIZE</label>
                        <div class="col-sm-2"><input type="text" value="{{$productCreation->width}}" placeholder="width" class="form-control" name="width"></div>
                        <div class="col-sm-2"><input type="text" placeholder="length" class="form-control" value="{{$productCreation->length}}" name="length"></div>
                        <div class="col-sm-2"><input type="text" value="{{$productCreation->height}}" placeholder="height" class="form-control" name="height"></div>
                    </div>

                    <div class="form-group"><label class="col-sm-2 control-label">WEIGHT</label>
                        <div class="col-sm-4"><input type="text" class="form-control" value="{{$productCreation->weight}}" name="weight" max="70"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">QTY ON HAND</label>
                        <div class="col-sm-4"><input type="text" class="form-control" value="{{$productCreation->qtyonhand}}" name="qtyonhand"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">POPULARITY</label>
                        <div class="col-sm-4"><input type="text" value="{{$productCreation->popularity}}" class="form-control" name="popularity"></div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label required">IMAGES</label>
                        <div class="col-sm-10">
                            <input id="product_image" name="product_image[]" type="file" multiple class="file-loading">
                        </div>
                    </div>
                      <div class="form-group"><label class="col-sm-2 control-label">USPS SERVICE TYPE</label>
                             <div class="col-sm-10">
                                 <select class="js-source-states"  name="usps_service_type" style="width: 100%">
                                 <?php foreach ($usps_service_types as $key => $value): ?>
                                     <option value="{{$value->id}}">{{$value->name}}</option>
                                 <?php endforeach ?>


                                </select>
                            </div>

                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">USPS MAIL TYPE</label>
                             <div class="col-sm-10">
                                 <select class="js-source-states"  name="usps_mail_type" style="width: 100%">
                                 <?php foreach ($usps_mail_types as $key => $value): ?>
                                     <option value="{{$value->id}}">{{$value->name}}</option>
                                 <?php endforeach ?>


                                </select>
                            </div>

                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">USPS SIZE</label>
                             <div class="col-sm-10">
                                 <select class="js-source-states"  name="usps_sizes" style="width: 100%">
                                 <?php foreach ($usps_sizes as $key => $value): ?>
                                     <option value="{{$value->id}}">{{$value->name}}</option>
                                 <?php endforeach ?>


                                </select>
                            </div>

                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">POUNDS</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="usps_pounds" value="{{$value->pounds}}"></div>
                        </div>


                    <div class="hr-line-dashed"></div>

                         <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-5">
                                <div class="checkbox checkbox-success">
                                    <?php if ($productCreation->on_sale): ?>
                                        <input id="checkbox3" type="checkbox" name="on_sale" checked="">
                                    <?php else: ?>
                                         <input id="checkbox3" type="checkbox" name="on_sale" >
                                    <?php endif ?>

                                    <label for="checkbox3">
                                        ON SALE
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">SALE PRICE</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="sale_price" value="{{$productCreation->sale_price}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">START DATE</label>
                            <div class="col-sm-2 input-group date" data-provide="datepicker">
                                <input type="text" class="form-control" name="start_date" date-format="dd-MM-yyyy" value="{{$productCreation->start_date != '0000-00-00' ? $productCreation->start_date : date('Y-m-d')}}">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>

                        </div>
                         <div class="form-group">
                            <label class="col-sm-2 control-label">END DATE</label>
                            <div class="col-sm-2 input-group date" data-provide="datepicker">
                                <input type="text" class="form-control" name="end_date" value="{{$productCreation->end_date != '0000-00-00'  ? $productCreation->end_date : date('Y-m-d')}}">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>

                        </div>

	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Save Changes</button>
	                    </div>
	                </div>

                </form>
        </div>
    </div>
</div>
<!-- crop modal -->
    <div class="modal fade" id="crop-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog " role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" >Crop Cover Image</h4>
          </div>
          <div class="modal-body">
            <div class="container">
              <div class="imageBox">
                <div class="thumbBox"></div>
                {{-- <div class="spinner" style="display: none">Loading...</div> --}}
              </div>
              <div class="action">
                <input type="file" id="cropFile" style="float:left; width: 250px">

                <input type="button" id="btnZoomIn" value="+" class="btn bg-grey waves-effect">
                <input type="button" id="btnZoomOut" value="-" class="btn bg-grey waves-effect">

              </div>

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn bg-success waves-effect" id="btnCrop">Crop</button>
            <button type="button" class="btn bg-amber waves-effect" data-dismiss="modal">CLOSE</button>
          </div>
        </div>
      </div>
    </div>
    <!-- crop modal -->
@stop
@section('js')
  <script src="{{url('assets/back/cropbox/cropbox.js')}}"></script>
<script type="text/javascript">


  var options =
  {
      thumbBox: '.thumbBox',
      spinner: '.spinner',
      imgSrc: 'avatar.png'
  }
  var cropper = $('.imageBox').cropbox(options);
  $('#cropFile').on('change', function(){
      var reader = new FileReader();
      reader.onload = function(e) {
          options.imgSrc = e.target.result;
          cropper = $('.imageBox').cropbox(options);
      }
      reader.readAsDataURL(this.files[0]);
      this.files = [];
  })
  $('#btnCrop').on('click', function(){
    var img = cropper.getDataURL()
    $('#cvr_img').val(img);
    document.querySelector('.cropped').innerHTML = '<img style="max-width:100%;" src="'+img+'">';
    $('#crop-modal').modal('hide');
  })
  $('#btnZoomIn').on('click', function(){
      cropper.zoomIn();
  })
  $('#btnZoomOut').on('click', function(){
      cropper.zoomOut();
  })

</script>
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".js-source-states").select2();

        $('.date').datepicker({
          format: 'yyyy-mm-dd'

        });
        $("#product_image").fileinput({
            uploadUrl: "", // server upload action
            uploadAsync: true,
            maxFileCount: 5,
            showUpload:false,
            allowedFileExtensions: ["jpg", "gif", "png"],
            initialPreview: <?php echo json_encode($images ); ?>,
            initialPreviewConfig: <?php echo json_encode($image_config) ?>,

        });

        $("#form").validate({
            rules: {
                product_name: {
                    required: true

                }, height: {
                    required: true,
                    number:true

                }, width: {
                    required: true,
                    number:true

                }, weight: {
                    required: true,
                    number:true,
                    max:70

                }, length: {
                    required: true,
                    number:true

                }, popularity: {
                    required: true,
                    number:true

                }, qtyonhand: {
                    required: true,
                    number:true

                }, price: {
                    required: true,
                    number:true

                }

            },
            submitHandler: function(form) {
                form.submit();
            }
        });
	});

    tinymce.init({
            selector: 'textarea',  // change this value according to your HTML
            plugins: [
                'advlist autolink lists link image charmap preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code',
                'insertdatetime media nonbreaking table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
            ],
            toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
            '| bullist numlist outdent indent | link | undo redo | forecolor backcolor emoticons ',
            fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
            menubar: false
        });



</script>
@stop
