@extends('layouts.back.master') @section('current_title','Update Tasting Schedule')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('tasting/schedule/list')}}">Tasting Management</a></li>
       
        <li class="active">
            <span>Tasting Schedule</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                    {!!Form::token()!!}
                    <div class="form-group"><label class="col-sm-2 control-label">DATE/TIME</label>
                        <div class="col-sm-3"><div class="input-group date" id="datetimepicker1">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            <input type="text" name="datetime" value="{{$tasting->event_date}}" class="form-control"/>
                        </div></div>
                    </div>  
                    {{-- <div class="form-group"><label class="col-sm-2 control-label">PEOPLE TYPE</label>
                        <div class="col-sm-10"><label class="control-label">{{$tasting->getPeopleType->name}}</label></div>
                    </div>   --}}
                     <div class="form-group"><label class="col-sm-2 control-label">NAME</label>
                        <div class="col-sm-10"><label class="control-label">{{$tasting->first_name.' '.$tasting->last_name}}</label></div>
                    </div>  
                     {{-- <div class="form-group"><label class="col-sm-2 control-label">PROVINCE</label>
                        <div class="col-sm-10"><label class="control-label">{{$tasting->event_date}}</label></div>
                    </div>         --}}
                     
                     {{-- <div class="form-group"><label class="col-sm-2 control-label">POST CODE</label>
                        <div class="col-sm-10"><label class="control-label">{{$tasting->post_code}}</label></div>
                    </div>         --}}
                     <div class="form-group"><label class="col-sm-2 control-label">VENUE</label>
                        <div class="col-sm-10"><label class="control-label">{{$tasting->venue}}</label></div>
                    </div>        
                    
                    
                    
                    <div class="hr-line-dashed"></div>
                	
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                            <?php if ($tasting->status==3): ?>
                                 <label class="label label-danger"> ALREADY REJECTED</label>
                            <?php else: ?>
                               <?php if ($tasting->status==2): ?>
                                   <label class="label label-success"> ALREADY ACCEPTED</label>
                               <?php else: ?>
                                   <button class="btn btn-primary" type="submit">Save & Accept</button>
                               <?php endif ?>
                            <?php endif ?>

	                        
	                    </div>
	                </div>
                	
                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/moment/moment.js')}}"></script>
<script src="{{asset('assets/back/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		
         $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss'
        });
        
        $("#form").validate({
            rules: {
                album_name: {
                    required: true
                  
                }
                
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
	});
     
	
	
</script>
@stop
