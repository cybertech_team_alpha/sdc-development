<?php
namespace TastingManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Tasting Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class Tasting extends Model{
	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tasting';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['people_type_id','event_date','details','first_name','last_name','province','post_code','venue','phone','email','status','accepted_date','ip_address','ip_location','created_by','status'];

	public function getPeopleType()
	{
		return $this->belongsTo('TastingManage\Models\TastingPeopleType', 'people_type_id', 'id');
	}

	


	

}
