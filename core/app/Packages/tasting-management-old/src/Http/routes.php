<?php
/**
 * TASTING MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'tasting/people/type', 'namespace' => 'TastingManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'tasting.people.type.add', 'uses' => 'TastingController@addView_people_type'
      ]);

      Route::get('edit/{id}', [
        'as' => 'tasting.people.type.edit', 'uses' => 'TastingController@editView_people_type'
      ]);

      Route::get('list', [
        'as' => 'tasting.people.type.list', 'uses' => 'TastingController@listView_people_type'
      ]);

      Route::get('json/list', [
        'as' => 'tasting.people.type.list', 'uses' => 'TastingController@jsonList_people_type'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'tasting.people.type.add', 'uses' => 'TastingController@add_people_type'
      ]);

      Route::post('edit/{id}', [
        'as' => 'tasting.people.type.edit', 'uses' => 'TastingController@edit_people_type'
      ]);

      Route::post('status', [
        'as' => 'tasting.people.type.status', 'uses' => 'TastingController@status_people_type'
      ]);

      Route::post('delete', [
        'as' => 'tasting.people.type.delete', 'uses' => 'TastingController@delete_people_type'
      ]);
    
    });
   Route::group(['prefix' => 'tasting/schedule', 'namespace' => 'TastingManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'tasting.schedule.add', 'uses' => 'TastingController@addView_tasting_schedule'
      ]);

      Route::get('edit/{id}', [
        'as' => 'tasting.schedule.edit', 'uses' => 'TastingController@editView_tasting_schedule'
      ]);

      Route::get('list', [
        'as' => 'tasting.schedule.list', 'uses' => 'TastingController@listView_tasting_schedule'
      ]);

      Route::get('json/list', [
        'as' => 'tasting.schedule.list', 'uses' => 'TastingController@jsonList_tasting_schedule'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'tasting.schedule.add', 'uses' => 'TastingController@add_tasting_schedule'
      ]);

      Route::post('edit/{id}', [
        'as' => 'tasting.schedule.edit', 'uses' => 'TastingController@edit_tasting_schedule'
      ]);

      Route::post('status', [
        'as' => 'tasting.schedule.status', 'uses' => 'TastingController@status_tasting_schedule'
      ]);

      Route::post('delete', [
        'as' => 'tasting.schedule.delete', 'uses' => 'TastingController@delete_tasting_schedule'
      ]);
      
    });
});
