<?php

Route::group(['middleware' => ['auth']], function () {
        Route::group(['prefix' => 'config', 'namespace' => 'ConfigManage\Http\Controllers'], function () {


        Route::get('add', [
            'as' => 'config.add', 'uses' => 'ConfigController@add'
        ]);

        Route::get('edit', [
            'as' => 'config.edit', 'uses' => 'ConfigController@editView'
        ]);



        Route::post('add', [
            'as' => 'config.add', 'uses' => 'ConfigController@save'
        ]);


        Route::post('edit', [
            'as' => 'config.edit', 'uses' => 'ConfigController@update'
        ]);

    });
});