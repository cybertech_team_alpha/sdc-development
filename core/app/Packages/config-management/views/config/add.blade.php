@extends('layouts.back.master') @section('current_title','Configuration')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all"/>
    <style>
        .rating-container .rating-stars:before {
            text-shadow: none;
        }
    </style>

@stop
@section('current_path')
    <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
            <li><a href="{{url('#')}}">Configuration</a></li>

            <li class="active">
                <span>Configure</span>
            </li>
        </ol>
    </div>
@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">
                    <form class="form-horizontal" id="form" method="post" files="true" enctype="multipart/form-data">
                        {!!Form::token()!!}


                        <div class="form-group"><label class="col-sm-2 control-label">COMPANY NAME</label>
                            <div class="col-sm-10">
                                @if (isset($config->company_name))
                                <input type="text" class="form-control" name="company_name" value="{{$config->company_name}}">
                                    @else
                                    <input type="text" class="form-control" name="company_name" placeholder="company name">
                                @endif
                            </div>
                        </div>


                        <div class="form-group"><label class="col-sm-2 control-label">ZIP CODE</label>
                            <div class="col-sm-10">
                                @if (isset($config->zip))
                                <input type="text" class="form-control" name="zip_code" value="{{$config->zip}}">
                                @else
                                    <input type="text" class="form-control" name="zip_code" placeholder="zip code">
                                @endif
                            </div>
                        </div>


                        <div class="form-group"><label class="col-sm-2 control-label">EMAIL ADDRESS</label>
                            <div class="col-sm-10">
                                @if (isset($config->email))
                                <input type="text" class="form-control" name="email" value="{{$config->email}}">
                                @else
                                    <input type="text" class="form-control" name="email" placeholder="email">
                                @endif
                            </div>
                        </div>


                        <div class="form-group"><label class="col-sm-2 control-label">PHONE NO</label>
                            <div class="col-sm-10">
                                @if (isset($config->phone))
                                <input type="text" class="form-control" name="phone" value="{{$config->phone}}" >
                                @else
                                    <input type="text" class="form-control" name="phone" placeholder="phone" >
                                @endif
                            </div>
                        </div>


                        <div class="form-group"><label class="col-sm-2 control-label">LOCATION</label>
                            <div class="col-sm-10">
                                @if (isset($config->location))
                                    <textarea name="location" class="form-control">{{$config->location}}</textarea>
                                @else
                                    <textarea name="location" class="form-control"></textarea>
                                @endif

                            </div>
                        </div>

                        {{-- <div class="form-group">
                            <label class="col-sm-2 control-label required">BACKGROUND IMAGE</label>
                            <div class="col-sm-10">
                                <input id="project-file" name="bg_img" type="file" class="file-loading" >
                            </div>
                        </div> --}}
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">FACEBOOK PAGE</label>
                            <div class="col-sm-10">
                                @if (isset($config->fb))
                                    <input type="text" class="form-control" name="fb_page" value="{{$config->fb}}">
                                @else
                                    <input type="text" class="form-control" name="fb_page" placeholder="facebook page">
                                @endif
                            </div>
                        </div>


                        <div class="form-group"><label class="col-sm-2 control-label">FACEBOOK CLIENT ID</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" name="facebook_client_id" value="{{env('FACEBOOK_CLIENT_ID')}}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">FACEBOOK CLIENT SECRET</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" name="facebook_client_secret" value="{{env('FACEBOOK_CLIENT_SECRET')}}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">FACEBOOK REDIRECT URL</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" name="facebook_redirect_url" value="{{env('FACEBOOK_REDIRECT')}}">
                            </div>
                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label">INSTAGRAM</label>
                            <div class="col-sm-10">
                                @if (isset($config->instagram))
                                    <input type="text" class="form-control" name="instagram" value="{{$config->instagram}}" >
                                @else
                                    <input type="text" class="form-control" name="instagram" placeholder="instagram" >
                                @endif
                            </div>
                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label">TWITTER</label>
                            <div class="col-sm-10">
                                @if (isset($config->twitter))
                                    <input type="text" class="form-control" name="twitter" value="{{$config->twitter}}" >
                                @else
                                    <input type="text" class="form-control" name="twitter" placeholder="twitter" >
                                @endif
                            </div>
                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label">YOUTUBE</label>
                            <div class="col-sm-10">
                                @if (isset($config->youtube))
                                    <input type="text" class="form-control" name="youtube" value="{{$config->youtube}}" >
                                @else
                                    <input type="text" class="form-control" name="youtube" placeholder="youtube" >
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">PINTEREST</label>
                            <div class="col-sm-10">
                                @if (isset($config->youtube))
                                    <input type="text" class="form-control" name="pinterest" value="{{$config->pinterest}}" >
                                @else
                                    <input type="text" class="form-control" name="pinterest" placeholder="Pinterest" >
                                @endif
                            </div>
                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label">TASTING DAYS</label>
                            <div class="col-sm-10">
                                @if (isset($config->tasting_days))
                                    <input type="text" class="form-control" name="t_d" value="{{$config->tasting_days}}">
                                @else
                                    <input type="text" class="form-control" name="t_d" placeholder="tasting days">
                                @endif
                            </div>
                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label">TASTING ERROR</label>
                            <div class="col-sm-10">
                                @if (isset($config->tasting_err))
                                    <input type="text" class="form-control" name="t_err" value="{{$config->tasting_err}}">
                                @else
                                    <input type="text" class="form-control" name="t_err" placeholder="tasting error messege">
                                @endif
                            </div>
                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label">QUOTE DAYS</label>
                            <div class="col-sm-10">
                                @if (isset($config->qoute_days))
                                    <input type="text" class="form-control" name="q_d" value="{{$config->qoute_days}}">
                                @else
                                    <input type="text" class="form-control" name="q_d" placeholder="quote days">
                                @endif
                            </div>
                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label">QUOTE ERROR</label>
                            <div class="col-sm-10">
                                @if (isset($config->qoute_err))
                                    <input type="text" class="form-control" name="q_err" value="{{$config->qoute_err}}">
                                @else
                                    <input type="text" class="form-control" name="q_err" placeholder="quote error messege">
                                @endif
                            </div>
                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label">WORKING TIME</label>
                            <div class="col-sm-10">
                                @if (isset($config->working_time))
                                    <textarea name="working_days" class="form-control">{{$config->working_time}}</textarea>
                                @else
                                    <textarea name="working_days" class="form-control"></textarea>
                                @endif
                            </div>
                        </div>

                        {{-- <div class="form-group"><label class="col-sm-2 control-label">TASTING MESSAGE</label>
                            <div class="col-sm-10">
                                @if (isset($config->tasting_success))
                                    <textarea name="tasting_success" class="form-control">{{$config->tasting_success}}</textarea>
                                @else
                                    <textarea name="tasting_success" class="form-control"></textarea>
                                @endif

                            </div>
                        </div> --}}


                        <div class="form-group"><label class="col-sm-2 control-label">GOOGLE CLIENT ID</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" name="google_client_id" value="{{env('GOOGLE_CLIENT_ID')}}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">GOOGLE CLIENT SECRET</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" name="google_client_secret" value="{{env('GOOGLE_CLIENT_SECRET')}}">
                            </div>
                        </div>
                         <div class="form-group"><label class="col-sm-2 control-label">GOOGLE REDIRECT URL</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" name="google_redirect_url" value="{{env('GOOGLE_REDIRECT')}}">
                            </div>
                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label text-uppercase">Promotion text</label>
                           <div class="col-sm-10">
                             @if (isset($config->promotion_text))
                                 <input type="text" class="form-control" name="promotion_text" value="{{$config->promotion_text}}">
                             @else
                                 <input type="text" class="form-control" name="promotion_text" placeholder="Promotion text">
                             @endif
                           </div>
                       </div>

                       <div class="form-group"><label class="col-sm-2 control-label text-uppercase">Promotion URL</label>
                          <div class="col-sm-10">
                            @if (isset($config->promotion_link))
                                <input type="text" class="form-control" name="promotion_link" value="{{$config->promotion_link}}">
                            @else
                                <input type="text" class="form-control" name="promotion_link" placeholder="Promotion URL">
                            @endif

                          </div>
                      </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <a class="btn btn-default" href="{{ url('config/add') }}">Cancel</a>
                                <button class="btn btn-primary" type="submit">Done</button>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
        @stop
        @section('js')
            <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
            <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
            <script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}"
                    type="text/javascript"></script>
            <script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
            <script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
            <script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>

            <script type="text/javascript">
                $(document).ready(function () {
                    $("#input-1").rating();
                    $(".js-source-states").select2({placeholder: "Select Animation type"});
                    $('.date').datepicker(
                        {
                            format: 'yyyy-mm-dd',
                        });

                    $("#project-file").fileinput({
                        allowedFileExtensions: ['jpg', 'png', 'gif'],
                        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
                        'showUpload': false,
                        overwriteInitial: true,
                        removeIcon: '<i class="glyphicon glyphicon-trash"></i>',


                    });


                    $("#form").validate({
                        rules: {
                            aType: {
                                required: true
                            },
                            sliderImg: {
                                required: true
                            },
                            aText: {
                                required: true
                            },
                            btnUrl: {
                                required: true
                            },
                            btnText: {
                                required: true
                            }
                        },
                        submitHandler: function (form) {
                            form.submit();
                        }
                    });
                });

                tinymce.init({
                    selector: 'textarea',  // change this value according to your HTML
                    plugins: [
                        'advlist autolink lists link image charmap preview hr anchor pagebreak',
                        'searchreplace wordcount visualblocks visualchars code',
                        'insertdatetime media nonbreaking table contextmenu directionality',
                        'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
                    ],
                    toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
                    '| bullist numlist outdent indent | link | undo redo | forecolor backcolor emoticons ',
                    fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
                    menubar: false
                });

            </script>
@stop
