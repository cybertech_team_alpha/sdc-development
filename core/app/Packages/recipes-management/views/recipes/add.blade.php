@extends('layouts.back.master') @section('current_title','Add new tutorial')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
<link rel="stylesheet"
href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css"
href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all"/>
<link rel="stylesheet" type="text/css"
href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all"/>
<style>
.rating-container .rating-stars:before {
  text-shadow: none;
}
.imageBox
{
  position: relative;
  width: 400px;
  height: 400px;
  border:1px solid #aaa;
  background: #fff;
  overflow: hidden;
  background-repeat: no-repeat;
  cursor:move;
}

.imageBox .thumbBox
{
  position: absolute;
  top: 50%;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  width: 350px;
  height: 350px;
  margin-top: auto;
  margin-bottom: auto;
  margin-left: auto;
  margin-right: auto;
  box-sizing: border-box;
  border: 1px solid rgb(102, 102, 102);
  box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
  background: none repeat scroll 0% 0% transparent;
}

.imageBox .spinner
{
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  text-align: center;
  line-height: 400px;
  background: rgba(0,0,0,0.7);
}
</style>

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
  <ol class="hbreadcrumb breadcrumb">
    <li><a href="{{url('recipe/list')}}">Recipie Management</a></li>

    <li class="active">
      <span>Add new tutorial</span>
    </li>
  </ol>
</div>
@stop
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="hpanel">
      <div class="panel-body">
        <form class="form-horizontal" id="myForm" method="post" files="true" enctype="multipart/form-data">
          {!!Form::token()!!}
          <div class="form-group"><label class="col-sm-2 control-label">TUTORIAL NAME <sup>*</sup></label>
            <div class="col-sm-10"><input required type="text" class="form-control" name="txtName" placeholder="Enter recipie Name" ></div>
          </div>

          <div class="form-group">
            <input type="hidden" id="cvr_img" name="cvr_img" value="" required form="myForm">
            <label class="col-sm-2 control-label required">COVER IMAGE <sup>*</sup></label>
            <div class="col-sm-10">
              <div class="col-sm-10">
                <div class="col-md-6">
                  <button type="button" class="btn bg-grey waves-effect" data-toggle="modal" data-target="#crop-modal" >Select Cover Image</button>
                </div>
                <div class="col-md-6">
                  <div class="cropped">

                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group"><label class="col-sm-2 control-label">FREE/PAID </label>
            <div class="col-sm-10">
              <input id="free" type="radio" name="free" value="0" required="">FREE<br>
              <input id="paid" type="radio" name="free" value="1">PAID
            </div>
          </div>

          <div class="form-group"><label class="col-sm-2 control-label">INTRO VIDEO<sup id="addRequired"></sup></label>
            <div class="col-sm-10"><input type="text" class="form-control" id="txtIntro" name="txtIntro"
              placeholder="Enter Intro Video URL"></div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">MAIN VIDEO<sup>*</sup></label>
              <div class="col-sm-10"><input required type="text" class="form-control" name="txtMain"
                placeholder="Enter Main Video URL"></div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">LESSON DESCRIPTION</label>
                <div class="col-sm-10"><textarea required rows="2" id="textarea1" class="form-control"
                 name="txtLesson"></textarea>
               </div>
             </div>

             <div class="form-group">
              <label class="col-sm-2 control-label">MATERIALS</label>
              <div class="col-sm-10"><textarea rows="2" id="textarea2" class="form-control"
               name="txtMaterials"></textarea>
             </div>
           </div>

           <div class="form-group">
            <label class="col-sm-2 control-label">INGREDIANTS</label>
            <div class="col-sm-10"><textarea rows="2" id="textarea3" class="form-control"
             name="txtIngrediants"></textarea>
           </div>
         </div>

         <div class="form-group">
          <label class="col-sm-2 control-label">RECIPE</label>
          <div class="col-sm-10"><textarea rows="2" id="textarea4" class="form-control"
           name="txtRecipes"></textarea>
         </div>
       </div>

       <div class="form-group">
        <label class="col-sm-2 control-label required">TEMPLATE PDF</label>
        <div class="col-sm-10">
          <input id="pdf" name="templatePdf" type="file" class="file-loading">
        </div>
      </div>


      <div class="form-group">
        <label class="col-sm-2 control-label required">PHOTO</label>
        <div class="col-sm-10">
          <input id="photo" name="file[]"  type="file" class="file-loading">
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label">NOTES</label>
        <div class="col-sm-10"><textarea rows="2" id="textarea5" class="form-control"
         name="txtNotes"></textarea>
       </div>
     </div>


     <div class="form-group"><label class="col-sm-2 control-label">CATEGORY</label>
      <div class="col-sm-10">
        <select class="js-source-states" name="categories[]" multiple="multiple" style="width: 100%">
          @foreach($cat as $item)
          <option value="{{$item->recipes_category_id}}">{{$item->name}}</option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="form-group"><label class="col-sm-2 control-label">SKILL LEVEL</label>
      <div class="col-sm-10">
        <select class="js-source-states form-control" name="skill_level">

          <option value="Beginner">Beginner</option>
          <option value="Intermediate">Intermediate</option>
          <option value="Advanced">Advanced</option>
          <option value="Kids">Kids</option>

        </select>
      </div>
    </div>
                        {{-- <div class="form-group"><label class="col-sm-2 control-label">CAKE TYPE</label>
                            <div class="col-sm-10">
                                <select class="js-source-states form-control" name="cake_type">
                                      @foreach($caketype as $type)
                                        <option value="{{$type->cake_type_id}}">{{ $type->type }}</option>
                                     @endforeach
                                </select>
                            </div>
                          </div> --}}



                          <div class="form-group"><label class="col-sm-2 control-label">PRICE <sup id="sup"></sup></label>
                            <div class="col-sm-10"><input id="price" type="text" class="form-control" name="txtPrice"
                              placeholder="Enter Price"></div>
                            </div>

                            
                            <div class="form-group"><label class="col-sm-2 control-label">PREMIUM ACCOUNT</label>
                              <div class="col-sm-10">
                                <input type="checkbox" name="premium" value="premium">
                              </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                              <div class="col-sm-8 col-sm-offset-2">
                                <button class="btn btn-default" type="button" onclick="location.reload();">Cancel
                                </button>
                                <button class="btn btn-primary" type="submit">Done</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- crop modal -->
                  <div class="modal fade" id="crop-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog " role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title" >Crop Cover Image</h4>
                        </div>
                        <div class="modal-body">
                          <div class="container">
                            <div class="imageBox">
                              <div class="thumbBox"></div>
                              {{-- <div class="spinner" style="display: none">Loading...</div> --}}
                            </div>
                            <div class="action">
                              <input type="file" id="cropFile" style="float:left; width: 250px">

                              <input type="button" id="btnZoomIn" value="+" class="btn bg-grey waves-effect">
                              <input type="button" id="btnZoomOut" value="-" class="btn bg-grey waves-effect">

                            </div>

                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn bg-success waves-effect" id="btnCrop">Crop</button>
                          <button type="button" class="btn bg-amber waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- crop modal -->
                  @stop
                  @section('js')
                  <script src="{{url('assets/back/cropbox/cropbox.js')}}"></script>
                  <script type="text/javascript">


                    var options =
                    {
                      thumbBox: '.thumbBox',
                      spinner: '.spinner',
                      imgSrc: 'avatar.png'
                    }
                    var cropper = $('.imageBox').cropbox(options);
                    $('#cropFile').on('change', function(){
                      var reader = new FileReader();
                      reader.onload = function(e) {
                        options.imgSrc = e.target.result;
                        cropper = $('.imageBox').cropbox(options);
                      }
                      reader.readAsDataURL(this.files[0]);
                      this.files = [];
                    })
                    $('#btnCrop').on('click', function(){
                      var img = cropper.getDataURL()
                      $('#cvr_img').val(img);
                      document.querySelector('.cropped').innerHTML = '<img style="max-width:100%;" src="'+img+'">';
                      $('#crop-modal').modal('hide');
                    })
                    $('#btnZoomIn').on('click', function(){
                      cropper.zoomIn();
                    })
                    $('#btnZoomOut').on('click', function(){
                      cropper.zoomOut();
                    })

                  </script>
                  <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
                  <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
                  <script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}"
                  type="text/javascript"></script>
                  <script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
                  <script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
                  {{-- <script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script> --}}

                  <script type="text/javascript">
                    $(document).ready(function () {
                      $(".js-source-states").select2();

                      if(document.getElementById("free").checked){
                        document.getElementById('txtIntro').setAttribute("disabled","");
                      }

                      if(document.getElementById("paid").checked){
                        document.getElementById('sup').innerHTML="*";
                        document.getElementById('price').setAttribute("required","");
                      }

                      $('#free').click(function () {
              // alert("clicked");
              document.getElementById('txtIntro').setAttribute("disabled","");
              document.getElementById('price').setAttribute("disabled","");
              document.getElementById('sup').innerHTML=""; 
              document.getElementById('price').removeAttribute("required");   
              document.getElementById('txtIntro').removeAttribute("required"); 
              document.getElementById('addRequired').innerHTML = "";          
            }); 

                      $('#paid').click(function () {
              // alert("clicked");
              document.getElementById('txtIntro').removeAttribute("disabled");
              document.getElementById('price').removeAttribute("disabled","");
              document.getElementById('sup').innerHTML="*";
              document.getElementById('price').setAttribute("required","");
              document.getElementById('txtIntro').setAttribute("required","");
              document.getElementById('addRequired').innerHTML = "*";
            }); 

                      $("#photo").fileinput({
                uploadUrl: "", // server upload action
                uploadAsync: true,
                maxFileCount: 1,
                showUpload: false,
                allowedFileExtensions: ["jpg", "gif", "png"]
              });

                      $("#pdf").fileinput({
                uploadUrl: "", // server upload action
                uploadAsync: true,
                maxFileCount: 1,
                showUpload: false,
                allowedFileExtensions: ["pdf"]
              });


                      $("#myForm").validate({
                        rules: {
                          txtName: {
                            required: true
                          },
                          cvr_img: {
                            required: true
                          },

                          txtPrice: {
                            number: true,
                            text:false
                          }
                        },
                        submitHandler: function (form) {
                          form.submit();
                        }
                      });
                    });

                  </script>
                  {{-- <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script> --}}
                  <script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
                  <script src="{{asset('assets/back/vendor/tinymce/js/tinymce/spellchecker/spellchecker.php')}}"></script>
                  <script type="text/javascript">

                    tinymce.init({
                      selector: 'textarea',  // change this value according to your HTML
                      plugins: [
                      'advlist autolink lists link image charmap preview hr anchor pagebreak',
                      'searchreplace wordcount visualblocks visualchars code',
                      'insertdatetime media nonbreaking table contextmenu directionality',
                      'emoticons template paste textcolor colorpicker textpattern imagetools codesample',
                      'spellchecker'
                      ],
                      toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
                      '| bullist numlist outdent indent  link spellchecker | undo redo | forecolor backcolor emoticons ',
                      spellchecker_rpc_url:'spellchecker.php',
                      fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
                      menubar: false
                    });

        //<![CDATA[
        // bkLib.onDomLoaded(function () {
        //     nicEditors.allTextAreas()
        // });
        //]]>

        // var myform = document.getElementById('myForm');
        // myform.addEventListener('submit', function () {
        //     var textarea1 = nicEditors.findEditor('textarea1');
        //     var textarea2 = nicEditors.findEditor('textarea2');
        //     var textarea3 = nicEditors.findEditor('textarea3');
        //     var textarea4 = nicEditors.findEditor('textarea4');
        //     var textarea5 = nicEditors.findEditor('textarea5');
        //     textarea1.saveContent();
        //     textarea2.saveContent();
        //     textarea3.saveContent();
        //     textarea4.saveContent();
        //     textarea5.saveContent();
        //     return true;
        // });
      </script>

      @stop
