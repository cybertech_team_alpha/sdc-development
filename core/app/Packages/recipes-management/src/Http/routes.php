<?php

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'recipe', 'namespace' => 'RecipesManager\Http\Controllers'], function () {

        Route::get('list', [
            'as' => 'recipe.list', 'uses' => 'RecipesController@index'
        ]);

        Route::get('json/list', [
            'as' => 'recipe.list', 'uses' => 'RecipesController@jsonList'
        ]);
        Route::get('add', [
            'as' => 'recipe.add', 'uses' => 'RecipesController@add'
        ]);

        Route::get('edit/{id}', [
            'as' => 'recipe.edit', 'uses' => 'RecipesController@editView'
        ]);


        Route::post('add', [
            'as' => 'recipe.add', 'uses' => 'RecipesController@save'
        ]);

        Route::post('delete', [
            'as' => 'recipe.delete', 'uses' => 'RecipesController@delete'
        ]);

        Route::post('edit/{id}', [
            'as' => 'recipe.edit', 'uses' => 'RecipesController@update'
        ]);

    });
});