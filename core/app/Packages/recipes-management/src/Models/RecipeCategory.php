<?php

namespace RecipesManager\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeCategory extends Model
{
    protected $table = 'recipe_categories';



    protected $fillable = ['recipe_id', 'recipes_category_id'];

    public function category()
    {
        return $this->belongsTo('RecipesCategoryManager\Models\RecipesCategory', 'recipes_category_id', 'recipes_category_id');
    }
}
