<?php

namespace RecipesManager;

use Illuminate\Support\ServiceProvider;

class RecipesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'RecipesManager');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('RecipesManager', function($app){
            return new RecipesManager;
        });
    }
}
