<?php
namespace App\Helpers;

class QueryArraySort
{
    public static function orderBy($galleries) { 
        $groupedGals = [];
        $galleries = $galleries->sortByDesc("catPriorValue");
        foreach ($galleries as $gallery) {
            if(array_key_exists($gallery->catPriorValue, $groupedGals)){
                $groupedGals[$gallery->catPriorValue][] = $gallery;
            }else{
                $groupedGals[$gallery->catPriorValue][] = $gallery;
            }
        }
        $gals = collect();
        foreach ($groupedGals as $groupedGal) {
            $groupedGal = collect($groupedGal);
             $gals = $gals->merge($groupedGal->sortBy("priority"));
        }
        return $gals;
    } 
}