<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
    a{
        text-decoration: none;
    }
    .btn{
        color: #fff!important;
        background-color: #630902;
        /*border: none;*/
        border-radius: 2px;
        display: inline-block;
        /*height: 24px;*/
        font-size: 14px;
        line-height: 16px;
        padding: 5px 8px;
        text-transform: uppercase;
        vertical-align: middle;
        text-align: center;
        width: auto;
        float: center;
        margin-top: 0px;
    }
</style>
</head>
<body>
    <h3><b>Dear User {{$order->getUser->first_name}},</b></h3>

    <div>
        <h4>  We're very happy to offer you our best products for your interesting. Below are the details for your purchasing confirmation,</h4>
        <h4>Invoice No : {{$order->inoice_no}}</h4>
        <table width="100%" align="center">
            <tr>
                <th>Date</th>
                <th>At</th>
                <th>Product name</th>
                <th>Qty</th>
                <th>Amount Paid</th>
            </tr>
            @foreach ($order->getItems->where('type', 1) as $item)
            <tr>
                <td align="center">{{$order->created_at->toDateString()}}</td>
                <td align="center">{{$order->created_at->toTimeString()}}</td>
                <td align="center">{{$item->name}}</td>
                <td align="center">{{$item->qty}}</td>
                <td align="center">{{$order->currency_code}} {{number_format($item->qty * $item->unit_price)}}</td>
            </tr>
            @endforeach
            
        </table>
        
		<h4>You may use below button to generate your payment receipt to print. </h4>
        <a href="{{route('paypal_recipt', $order->inoice_no)}}" class="btn">Generate a receipt</a><br>
		
        <h4>Please do not reply to this email. </h4>

        <h5>
            Thank You, 
            <br>
            Team Sweet Delights Cakery.
        </h5>
    </div>
    <hr>
    {{-- <h6>
        If you did not request a varify email, no further action is required.
        <br>
        Copyright © Sweet Delights Cakery 2015 - 2018. All right reserved.
    </h6> --}}
</body>
</html>