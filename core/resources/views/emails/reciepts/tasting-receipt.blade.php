<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
    a{
        text-decoration: none;
    }
    .btn{
        color: #fff!important;
        background-color: #630902;
        /*border: none;*/
        border-radius: 2px;
        display: inline-block;
        /*height: 24px;*/
        font-size: 14px;
        line-height: 16px;
        padding: 5px 8px;
        text-transform: uppercase;
        vertical-align: middle;
        text-align: center;
        width: auto;
        float: center;
        margin-top: 0px;
    }
</style>
</head>
<body>
    <h3><b>Dear User {{$order->first_name}},</b></h3>

    <div>
        <h4> Its very pleasure to see your tasting order and below is the confirmation details. Hope to see you within the discussed time as below.</h4>
        <h4>Invoice No : {{$order->id}}</h4>
        <table width="100%" align="center">
            <tr >
                <th>Date</th>
                <th>At</th>
                <th>No of Servings</th>
                <th align="center">No of People</th>
                <th>Amount Paid</th>
            </tr>
            <tr >
                <td align="center">{{$order->created_at->toDateString()}}</td>
                <td align="center">{{$order->created_at->toTimeString()}}</td>
                <td align="center">{{$order->servings}}</td>
                <td align="center">{{$order->getPeopleType->name}}</td>
                <td align="center">{{number_format($order->getPeopleType->price)}}</td>
            </tr>
        </table>
{{--         
        <h4>You may use below button to generate your payment receipt to print. </h4> --}}
        <h4></h4>
        @if (isset($confirmLink))
        <a href="{{url('tasting-confirm/'.$order->id)}}" class="btn">Generate a receipt</a><br>
        @endif
		
        <h4>Please do not reply to this email. </h4>

        <h5>
            Thank You, 
            <br>
            Team Sweet Delights Cakery.
        </h5>
    </div>
    <hr>
    {{-- <h6>
        If you did not request a varify email, no further action is required.
        <br>
        Copyright © Sweet Delights Cakery 2015 - 2018. All right reserved.
    </h6> --}}
</body>
</html>