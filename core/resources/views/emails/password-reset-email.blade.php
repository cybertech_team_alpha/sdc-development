<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
    a{
        text-decoration: none;
    }
    .btn{
        color: #fff!important;
        background-color: #630902;
        /*border: none;*/
        border-radius: 2px;
        display: inline-block;
        /*height: 24px;*/
        font-size: 14px;
        line-height: 16px;
        padding: 5px 8px;
        text-transform: uppercase;
        vertical-align: middle;
        text-align: center;
        width: auto;
        float: left;
        margin-top: 0px;
    }
</style>
</head>
<body>
    <h3><b>Dear User {{$user->first_name}},</b></h3>

    <div>
        <h4>You have requested a password reset for your Sweet Delights Cakery account, click below URL to change your password.</h4>
        
        <a href="{{ $url}}" class="btn">Reset Password</a><br>

        <h4>For further information please write to info@sweetdelightscakery.com or contact us on  (+248) 787-7766. </h4>
        <h4>Please do not reply to this email. </h4>
        
        <h5>
            Thank You, 
            <br>
            Sweet Delights Cakery.
        </h5>
    </div>
    <hr>
</body>
</html>