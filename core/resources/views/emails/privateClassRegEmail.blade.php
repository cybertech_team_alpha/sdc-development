<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
   
    <div>
        <h4>New private class registration from sweetdelightscakery.com</h4>
        <ul>
            <li>Class : {{$details->getPrivateClassCat->name}}</li>
            <li>Datetime : {{$details->getDateTime->getFullDate()}} - {{$details->getDateTime->getStart24Format()}} to {{$details->getDateTime->getEnd24Format()}}</li>
            <li>First Name : {{$details->first_name}}</li>
            <li>Last Name : {{$details->last_name}}</li>
            <li>Phone : {{$details->phone}}</li>
            <li>Email : {{$details->email}}</li>

            @if ($details->parent_is_reg == 1)
                <li>Parent Register : Yes</li>
                <li>Parent Full Name :  {{$details->parent_full_name}}</li>
            @else
                <li>Parent Register : No</li>
            @endif
        </ul>
    </div>
    <hr>
    {{-- <h6>
        If you did not request a varify email, no further action is required.
        <br>
        Copyright © Sweet Delights Cakery 2015 - 2018. All right reserved.
    </h6> --}}
</body>
</html>