@extends('layouts.front.master')  
<!-- @section('title','Register | www.princeofgalle.com') -->
@section('css')
@section('content')
<!-- MY ACCOUNT PAGE -->
<style>
  .mtop-m15{
      margin-top:-15px;
  }
  .modal{
    z-index:99999;
}
.modal-header .close {
    margin-top: -20px;
}
  .paddingl{
    padding-left: 40px!important;
  }
  .separator{
    border-top: 1px solid #CCCCCC;
    position: relative;
    margin: 10px 0 20px 0;
  }
  .separator-text{
    display: block;
    position: absolute;
    top: -10px;
    left: 50%;
    margin-left: -15px;
    padding: 0px 10px;
    background: #ededed;
    color: #8a8a8a;
  }

  .hyperlink {
      color: #8b5730;

  }
  .hyperlink:hover {
      color: #E5D4CD;
  }
  .well {
      background-image: linear-gradient(to bottom,#FCEEEB 0,#FCEEEB 100%);
  }
  .input-group-addon {
      color: #8b5730;
  } 
  .form-focus:focus{
      border-color: #8b5730;
      box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px #8b5730
  }
</style>
<section class="my_account parallax">
    <div class="container center">
        <div class="row">
            <div class="my_account_block clearfix">
                <!-- <div  class="login  col-lg-2"></div> -->
                <div class="login text-center col-lg-6 col-lg-offset-3">
                    <form class="well form-horizontal" id="login_form" method="POST" action="{{URL::to('user/login')}}">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <fieldset>
                            <legend>
                                <center>
                                    <h2 style="color: #8b5730;">Log In</h2>
                                </center>
                            </legend>
                            
                            <div class="form-group col-md-12">
                              <p class="pull-left col-md-offset-1">
                                Don't have an account? <a href="{{url('user/register')}}" class="hyperlink hyperlink-type"><b>Create a new account</b></a>
                              </p>
                            </div>

                            <div class="form-group col-md-12 text-center">
                                <div class="btn-facebook-login btn-social-login col-lg-5 col-md-5 col-md-offset-1 text-center">
                                   <!--  <a href="{{url('auth/facebook')}}">Login with <i class="fa fa-facebook-square large"></i></a> -->

                                  <a href="{{url('auth/facebook')}}" class="btn-block btn-social btn-facebook">
                                     <span class="fa fa-facebook"></span>&nbsp;&nbsp;Login with facebook
                                  </a>

                                </div>
                                <div class="btn-google-login btn-social-login col-lg-5 col-md-5 col-md-offset-1">
                                  <a href="{{url('auth/google')}}" class="btn-block btn-social btn-google">
                                      <span class="fa fa-google"></span>&nbsp;&nbsp;Login with Google
                                  </a>

                                    <!-- <a href="{{url('auth/google')}}">Login with <i class="fa fa-google-plus-square large"></i></a> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="separator">
                                      <span class="separator-text">OR</span>
                                      <!-- <p class="text-center"> OR </p> -->
                                    </div>
                                  </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                        <input name="user_name1" placeholder="Username or Email Address" class="form-control form-focus" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input name="password1" placeholder="Password" class="form-control form-focus"  type="password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12 mtop-m15">
                                <div class="col-md-5 col-md-offset-1">
                                    <div class="form-check pull-left">
                                        <input type="checkbox" class="form-check-input" id="remeberMe">
                                        <label name="login-remember" class="form-check-label" for="remeberMe" style="text-transform:none;">Remember me</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-check pull-right" style="padding: 3px 10px;">
                                        <a href="#" class="form-check-label" data-target="#pwdModal" data-toggle="modal">Forgot your password?</a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-1 col-md-offset-2"></div>
                              <div class="col-md-6">
                                <input type="submit" class="btn btn-block text-center" value="Log In" style="border-radius: 5px;">
                              </div>
                              <div class="col-md-1 col-md-offset-2"></div>
                            </div>
                            
                        </fieldset>
                    </form>
                </div>
                
            </div>
        </div>
    </div>

    <!--forgot password modal-->
    <div class="modal fade" id="pwdModal" tabindex="-1" role="dialog" aria-labelledby="pwdModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="pwdModalLabel">Forgot Password?</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="password-reset-form" method="post" action="{{url('/user/password-reset')}}">
        <div class="text-center">
                          
            <p>Please enter your email address. We'll email you a link to reset your password.</p>
            <div class="panel-body">
                <fieldset>
                  {{ csrf_field() }}
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-1 inputGroupContainer" style="margin-bottom: 10px;">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input name="email" placeholder="Email Address" class="form-control form-focus" type="text">
                            </div>
                            <br>
                            <input class="btn btn-lg btn-primary btn-block" value="Continue" type="submit" style="border-radius: 5px;">
                        </div>
                    </div>
                    
                </fieldset>
            </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
</section>

<!-- //MY ACCOUNT PAGE -->
<style type="text/css">
    #success_message{ display: none;}
</style>
@stop
@section('js')
<script type="text/javascript">
    $(document).ready(function() {
    $('#contact_form').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          // valid: 'glyphicon glyphicon-ok',
          // invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          fname: {
              validators: {
                      stringLength: {
                      min: 2,
                  },
                      notEmpty: {
                      message: 'Please enter your First Name'
                  }
                  ,
                  regexp: {
                      regexp: /^[a-zA-Z_]+$/,
                      message: 'The username can only consist of alphabetical & underscore'
                  }
              }
          },
           lname: {
              validators: {
                   stringLength: {
                      min: 2,
                  },
                  notEmpty: {
                      message: 'Please enter your Last Name'
                  }
                  ,
                  regexp: {
                      regexp: /^[a-zA-Z_]+$/,
                      message: 'The username can only consist of alphabetical & underscore'
                  }
              }
          },
           user_name: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  notEmpty: {
                      message: 'Please enter your Username'
                  }
              }
          },
           password: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  notEmpty: {
                      message: 'Please enter your Password'
                  }
              }
          },
          confirm_password: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  identical: {
                      field: 'password',
                      message: 'The password and its confirm are not the same'
                  },
                  notEmpty: {
                      message: 'Please confirm your Password'
                  }
              }
          },
          email: {
              validators: {
                  notEmpty: {
                      message: 'Please enter your Email Address'
                  },
                  emailAddress: {
                      message: 'Please enter a valid Email Address'
                  }
              }
          },
           confirm_email: {
              validators: {
                  notEmpty: {
                      message: 'Please confirm your email address'
                  },
                  identical: {
                      field: 'email',
                      message: 'The email and its confirm are not the same'
                  },
                  emailAddress: {
                      message: 'Please enter a valid Email Address'
                  }
              }
          },

          }
      })

      .on('success.form.bv', function(e) {
          $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
              $('#contact_form').data('bootstrapValidator').resetForm();

          // Prevent form submission
          e.preventDefault();

          // Get the form instance
          var $form = $(e.target);

          // Get the BootstrapValidator instance
          var bv = $form.data('bootstrapValidator');

          // Use Ajax to submit form data
          $.post($form.attr('action'), $form.serialize(), function(result) {
              console.log(result);
          }, 'json');
      });

$('#password-reset-form').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          // valid: 'glyphicon glyphicon-ok',
          // invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          
          email: {
              validators: {
                  notEmpty: {
                      message: 'Please enter your Email Address'
                  },
                  emailAddress: {
                      message: 'Please enter a valid Email Address'
                  }
              }
          }
          }
      })

      .on('success.form.bv', function(e) {
          $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
              $('#password-reset-form').data('bootstrapValidator').resetForm();

          // Prevent form submission
          e.preventDefault();

          // Get the form instance
          var $form = $(e.target);

          // Get the BootstrapValidator instance
          var bv = $form.data('bootstrapValidator');

          // Use Ajax to submit form data
          $.post($form.attr('action'), $form.serialize(), function(result) {
              console.log(result);
          }, 'json');
      });


      $('#login_form').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          // valid: 'glyphicon glyphicon-ok',
          // invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
           user_name1: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  notEmpty: {
                      message: 'Please enter your Username'
                  }
              }
          },
           password1: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  notEmpty: {
                      message: 'Please enter your Password'
                  }
              }
          },

          }
      })

      .on('success.form.bv', function(e) {
          $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
              $('#login_form').data('bootstrapValidator').resetForm();

          // Prevent form submission
          e.preventDefault();

          // Get the form instance
          var $form = $(e.target);

          // Get the BootstrapValidator instance
          var bv = $form.data('bootstrapValidator');

          // Use Ajax to submit form data
          $.post($form.attr('action'), $form.serialize(), function(result) {
              console.log(result);
          }, 'json');
      });

       $("#contact_reset").click(function(){
         $('#contact_form').bootstrapValidator("resetForm",true);
      });
        $("#login_reset").click(function(){
         $('#login_form').bootstrapValidator("resetForm",true);
      });

    });

</script>
@stop
