<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Sweet Delights Cakery</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="pinterest" content="nohover"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{asset('assets/front/images/favicon.ico')}}">

    <!-- CSS -->
    <!-- <link href="{{asset('assets/front/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" /> -->

<!--
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
     <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
     <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" rel="stylesheet" type="text/css" /> -->

     <link rel="stylesheet" href="{{asset('assets/front/bootstrap/bootstrap.min.css')}}">
     <link rel="stylesheet" href="{{asset('assets/front/bootstrap/bootstrap-theme.min.css')}}">
     <link rel="stylesheet" href="{{asset('assets/front/bootstrap/bootstrapValidator.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('assets/back/vendor/sweetalert/lib/sweet-alert.css')}}">


    <link href="{{asset('assets/front/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/front/css/fancySelect.css')}}" rel="stylesheet" media="screen, projection" />
    <link href="{{asset('assets/front/css/animate.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/front/css/responsive.css')}}" rel="stylesheet" type="text/css" media="responsive styles" />
    <link rel="stylesheet" href="{{asset('assets/back/vendor/blueimp-gallery/css/blueimp-gallery.min.css')}}">

    <link rel="stylesheet" href="{{asset('assets/back/vendor/toastr/build/toastr.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />

    <!-- FONTS -->
    <link href="{{asset('assets/front/font-awesome-4.7.0/css/font-awesome.css')}}" rel="stylesheet">

    <!-- slick -->
    <link href="{{asset('assets/front/css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('assets/front/css/slick-theme.css')}}" rel="stylesheet">

   <!-- jcarousel -->
    <link href="{{asset('assets/front/vendor/jcarousel/jcarousel.basic.css')}}" rel="stylesheet">

    <!-- Owl Carousel -->
    <link href="{{asset('assets/front/vendor/owl.carousel/assets/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('assets/front/vendor/owl.carousel/assets/owl.theme.default.css')}}" rel="stylesheet">

    <!-- Mosaic Plugin -->
    <!--<link href="{{ asset('assets/front/mosaic-plugin/css/jquery.mosaic.css') }}" rel="stylesheet">-->

    <style media="screen">
        .navbar-collapse {
            padding-left: 5%;
            padding-right: 5%;
        }
        .container.menu_header {
            padding-left: 5%;
            padding-right: 5%;

        }
        .address p{
            font-size: 11px !important;
            margin-bottom: 0px;
            margin-left: 8px;
        }
         .dropdown-menu li:hover .sub-menu {
            visibility: visible;
        }
        .dropdown:hover .dropdown-menu {
            display: block;
        }
        .fa-2x {
            font-size: 1.3em;
        }
        .caret{
            margin-left: 5px;
            margin-top: 0px;
        }
        .newsletter_btn{
            background: transparent;
            border: 2px solid #ccc;
            color: #ccc;
        }
        .promotionText{
            background: #2c2c2c;
            padding: 5px 15px;
            color: #4D4B4C;
        }
        .promotionText a{
             color: #4D4B4C;
        }
        .promotionText a:hover{
             <!--color: #ffc0cbd4 !important;-->
             color: #4D4B4C;
        }
        /*.fa {
            margin-right: 4px;
        }*/
        span.address p {
            margin-left: 6px;
        }

        li.link.active a.dropdown-toggle, li.active a.active {
            color: #E5D4CD !important;
        }

        .social_div a:hover { 
            border-color: black;
            width: 28px !important;
            height: 28px !important;
        }

        .social_div a:hover i {
            color: black;
            font-size: 15px !important;
            line-height: 24px !important;
        }

        .foot_mail.working-time span p{
            font-size: 12px !important;
        }
        .row.footer-top {
            margin: auto 8%;
        }
        
    </style>

@yield('css')
</head>
<body>
<?php $cartTotalQuantity = Cart::getTotalQuantity(); ?>
<!-- PRELOADER -->
<div id="preloader"><img src="{{asset('assets/front/images/preloader.gif')}}" alt="" /></div>
<!-- //PRELOADER -->
<div class="preloader_hide">
    <!-- PAGE -->
    <div id="page">
        <!-- HEADER -->
            <header>
            <div class="col-sm-12 promotionText" style="background-color: #E5D4CD;">
                <div class="container menu_header text-center">
                    {{ ConfigManage\Models\Config::find(1)->promotion_text }} <a style="margin-left: 3px;" href="{{ ConfigManage\Models\Config::find(1)->promotion_link }}"><strong>Click here</strong></a>
                </div>
            </div>
            <div class="row"></div>

            <!-- TOP INFO -->
            <div class="top_info">
                <!-- CONTAINER -->

                <div class="container menu_header clearfix">
                    <ul class="secondary_menu text-center">
                        <?php if ($user = Sentinel::check()): ?>
                        <?php $loged_user=Sentinel::getUser(); ?>
                        <li><a href="{{url('user-profile')}}" >Hi {{$loged_user->first_name}} {{$loged_user->last_name}}</a></li>
                        <li><a href="{{url('user/logout')}}" >LOGOUT</a></li>
                    <?php else: ?>
                        <li><a href="{{url('user/login')}}" >LOG IN</a></li>
                        <li><a href="{{url('user/register')}}" >REGISTER</a></li>
                    <?php endif ?>
                    </ul>
                    <!-- <div class="phone_top secondary_menu">have a question? <a href="tel:248-787-7766" >(248)-787-7766</a></div>
                    <div class="live_chat secondary_menu"><a href="javascript:void(0);" ><i class="fa fa-envelope-o"></i> info@sweetdelightscakery.com</a></div> -->
                    <div class="social-header">
                        <a href="{{ ConfigManage\Models\Config::find(1)->twitter }}" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>

                        <a href="{{ ConfigManage\Models\Config::find(1)->fb }}" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>

                        <a href="{{ ConfigManage\Models\Config::find(1)->youtube }}" target="_blank"><i class="fa fa-youtube-square fa-2x"></i></a>

                        <a href="{{ ConfigManage\Models\Config::find(1)->pinterest }}" target="_blank"><i class="fa fa-pinterest-square fa-2x"></i></a>

                        <a href="{{ ConfigManage\Models\Config::find(1)->instagram }}" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
                    </div>
                    <!-- <div class="social-header">
                            <a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
                            <a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a>
                            <a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
                            <a href="javascript:void(0);" ><i class="fa fa-pinterest"></i></a>
                            <a href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a>
                            <a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a>
                        </div> -->
                </div><!-- //CONTAINER -->
            </div><!-- TOP INFO -->
                <div class="menu_block">
                    <nav class="menubar navbar">
                      <div class="navbar-header">
                       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                         <span class="sr-only">Toggle navigation</span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                       </button>
                       <!-- <a class="navbar-brand" href="#">Brand</a> -->
                       <a class="logo logo_mob" href="{{url('/')}}"><img src="{{asset('assets/front/images/logo.png')}}" alt="Sweet Delight Logo" data-pin-no-hover="true"></a>
                     </div>
                    <div id="navmenu">
                      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="menu">
                            <li class="link {{ Request::is('/')?'active':''}}"><a href="{{url('/')}}" >HOME</a></li>
                            <li class="b-l link {{ Request::is('gallery/0')?'active':''}}"><a href="{{url('gallery/0')}}">GALLERY</a></li>
                            <li class="dropdown b-l link {{ Request::is('flavors')?'active':''}} {{ Request::is('pricing')?'active':''}} {{ Request::is('quotes')?'active':''}} {{ Request::is('tasting')?'active':''}}">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SERVICES<span class="caret margleft10"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="link {{ Request::is('flavors')?'active':''}}"><a href="{{url('flavors')}}">Flavors</a></li>
                                    <li class="link {{ Request::is('pricing')?'active':''}}"><a href="{{url('pricing')}}">Pricing</a></li>
                                    <li class="link {{ Request::is('quotes')?'active':''}}"><a href="{{url('quotes')}}">Request a Quote</a></li>
                                    <li class="link {{ Request::is('tasting')?'active':''}}"><a href="{{url('tasting')}}">Schedule a Tasting</a></li>
                                </ul>
                            </li>
                            <li class="dropdown b-l link {{ Request::is('PrivateClassesLand/0')?'active':''}} {{ Request::is('onlineTutorials')?'active':''}}">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">CLASSES<b class="caret margleft10"></b></a>
                                <ul class="dropdown-menu multi-level">
                                  <li class="{{ Request::is('PrivateClassesLand/0')?'active':''}}"><a href="{{url('PrivateClassesLand/0')}}">Private Classes</a></li>
                                  <li class="{{ Request::is('onlineTutorials')?'active':''}}"><a href="{{url('onlineTutorials')}}">Online Classes</a></li>
                                </ul>
                            </li>

                            <!-- <li class="text-center logo-style"><a class="logo logo_text" href="{{url('/')}}">Sweet Delight Cakery</a></li> -->
                            <li class="text-center logo-style">
                                <a class="logo logo_mob_2" href="{{url('/')}}"><img src="{{asset('assets/front/images/logo.png')}}" alt="Sweet Delight Logo"></a>
                                <a class="logo logo_mob_2_scroll" href="{{url('/')}}"><img style="width: 90px;" src="{{asset('assets/front/images/logo-scroll.png')}}" alt="Sweet Delight Logo"></a>
                            </li>


                            <li class="link {{ Request::is('blog')?'active':''}}"><a href="{{url('blog')}}">BLOG</a></li>
                            <li class="b-l link {{ Request::is('productload/all/all/1/min/hi')?'active':''}}"><a href="{{url('productload/all/all/1/min/hi')}}">SHOP</a></li>
                            <!-- <li class="dropdown b-l link">
                                <a href="{{url('productload?type=cake&category=all')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SHOP</a>
                                <ul class="dropdown-menu">
                                    <li class="link"><a href="{{url('productload?type=cake&category=all')}}">Products</a></li>
                                    <li class=" link"><a href="#">Payment</a></li>
                                </ul>
                            </li> -->
                            <li class="dropdown b-l link {{ Request::is('about')?'active':''}} {{ Request::is('news')?'active':''}} {{ Request::is('generalcontact')?'active':''}} {{ Request::is('faq')?'active':''}} {{ Request::is('tos')?'active':''}} {{ Request::is('privacy')?'active':''}}">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CONTACT<span class="caret margleft10"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="link {{ Request::is('about')?'active':''}}"><a href="{{url('about')}}">About Us</a></li>
                                    <li class="link {{ Request::is('news')?'active':''}}"><a href="{{url('news')}}">In The News</a></li>
                                    {{--<li class="link"><a href="{{url('quotes')}}">Contact Us</a></li>--}}
                                    <li class=" link {{ Request::is('generalcontact')?'active':''}}"><a href="{{url('generalcontact')}}">Contact Us</a></li>
                                    <li class=" link {{ Request::is('faq')?'active':''}}"><a href="{{url('faq')}}">FAQ</a></li>
                                    <li class=" link {{ Request::is('tos')?'active':''}}"><a href="{{url('tos')}}">Terms Of Service</a></li>
                                    <li class=" link {{ Request::is('privacy')?'active':''}}"><a href="{{url('privacy')}}">Privacy Policy</a></li>
                                </ul>
                            </li>
                            <li class="b-l link {{ Request::is('my-cart')?'active':''}}"><a class="shopping_bag_btn largeI" href="{{url('my-cart')}}" ><i class="fa fa-shopping-cart"></i><span class="vu_wc-count">{{$cartTotalQuantity}}</span></a></li>
                        </ul>
                      </div>
                    </div>

                </div>
            </div><!-- //CONTAINER -->
        </header><!-- //HEADER -->
        @yield('content')











        <!-- FOOTER -->
        <footer>

            <!-- CONTAINER -->
            <div class="container footer" data-animated='fadeInUp'>

                <!-- ROW -->
                <div class="row footer-top">

                    <div class="col-lg-3 col-md-2 col-sm-3 col-xs-12 padbot10" style="padding-right: 0;">
                        <h4 class="text-uppercase">Contact us</h4>
                        <div class="foot_address"><span><i class="fa fa-map-marker"></i>&nbsp;&nbsp;{{ ConfigManage\Models\Config::find(1)->company_name }}, </span >
                          <span class="address">{!! ConfigManage\Models\Config::find(1)->location !!}</span></div>
                        <div class="foot_phone"><i class="fa fa-phone" style="vertical-align: -moz-middle-with-baseline; padding-top: 10px;"></i>&nbsp;&nbsp;<a href="tel:{{ ConfigManage\Models\Config::find(1)->phone }}" >{{ ConfigManage\Models\Config::find(1)->phone }}</a></div>
                        <div class="foot_mail"><i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="mailto:{{ ConfigManage\Models\Config::find(1)->email }}" >{{ ConfigManage\Models\Config::find(1)->email }}</a></div>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 padbot10 about_mv">
                        <h4 class="text-uppercase">Information</h4>
                        <ul class="foot_menu">
                            <li><a href="about">About Us</a></li>

                            <li><a href="{{url('/terms')}}">Terms of Service</a></li>
                            <li><a href="{{url('/gallery/0')}}">Gallery <!-- <span class="new">new</span>--></a></li>
                            <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                            <!-- <li><a href="{{url('/blog')}}" >Blog</a></li> -->
                            <li><a href="{{url('/faq')}}">Faqs</a></li>
                            <li><a href="{{url('/quotes')}}">Contact Us</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padbot10" style="padding-right: 5%;">
                        <h4 class="text-uppercase">About Shop</h4>
                        <p class="text-justify">We specialize in creating exceptional wedding cakes to one of a kind special occasion cakes and all our sweet delights are made from scratch using only the freshest and finest ingredients. All sugar work is hand crafted with care and given special attention to detail. We offer a variety of cake decorating classes for every age, interest and skill level both privately and on our website.</p>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 padbot10 Newsletter" style="left: 50px;">
                        <h4 class="text-uppercase">Subscribe</h4>
                        <p class="text-justify">Subscribe to our newsletter to receive the latest news and updates.</p>
                        <form action="{{url('/sign-up-to-newsletter')}}" class="newsletter_form clearfix" method="post" id="newsletterFooterForm">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6" style="padding-right: 2px;">
                                    <input type="text" name="fname" value="Full Name" onFocus="if (this.value == 'Full Name') this.value = '';" onBlur="if (this.value == '') this.value = 'Full Name';" style="font-size: 13px !important; width: 100% !important;" required>   
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 2px;">
                                <input type="text" name="email" value="Email Address" onFocus="if (this.value == 'Email Address') this.value = '';" onBlur="if (this.value == '') this.value = 'Email Address';" style="font-size: 13px !important; width: 100% !important;" required>
                            </div></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                <button class="btn newsletter_btn" id="btn-sign-up" type="submit" value="Subscribe" style="float: right; width: 100% !important;">Subscribe</button>
                            </div></div>
                        </form>
                    </div>
                </div><!-- //ROW -->







                <div class="row footer-top" style="margin-top: 15px;">
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 padbot10">
                        <h4>Working Time</h4>
                        <div class="foot_mail working-time">
                            <span>
                                {!! ConfigManage\Models\Config::find(1)->working_time !!}
                            </span>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padbot10 text-center Newsletter" style="left: 80px;">
                        <h4>Follow Us</h4>
                        <div class="social">

                            <a href="{{ ConfigManage\Models\Config::find(1)->twitter }}" target="_blank"  data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>

                            <a href="{{ ConfigManage\Models\Config::find(1)->fb }}" target="_blank" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>

                            <a href="{{ ConfigManage\Models\Config::find(1)->youtube }}" target="_blank" data-placement="bottom" title="Youtube"><i class="fa fa-youtube-square"></i></a>

                            <a href="{{ ConfigManage\Models\Config::find(1)->pinterest }}" target="_blank" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>

                            <a href="{{ ConfigManage\Models\Config::find(1)->instagram }}" target="_blank" data-placement="bottom" title="Instagram"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>


                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 padbot10">
                        &nbsp;
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 padbot10 text-right" style="left: 50px;">
                        <div class="footer_logo">
                            <a href="javascript:void(0);" >
                                <img class="img-footer-logo" src="{{asset('assets/front/images/logo2.png')}}" alt="">
                            </a>
                        </div>

                    <!-- <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 padbot10 text-right" >
                        <div class="footer_logo"><a href="javascript:void(0);" ><img class="img-footer-logo" src="{{asset('assets/front/images/logo2.png')}}" alt=""></a></div>

                    </div> -->
                </div><!-- //ROW -->
            </div><!-- //CONTAINER -->

            <!-- COPYRIGHT -->
            <div class="copyright">

                <!-- CONTAINER -->
                <div class="container clearfix">
                    <!-- <div class="foot_logo"><a href="index.html" ><img src="{{asset('assets/front/images/foot_logo.png')}}" alt="" /></a></div> -->

                    <div class="copyright_inf" style="font-size: 12px;">
                        <span>{{ ConfigManage\Models\Config::find(1)->company_name }} © 2014 - <?php echo date("Y");?></span> |
                        <span>Another finest solution by <a href="https://www.facebook.com/cybertechInt.lk/" target="_blank" style="color: #ffa500;">Cybertech Int Ltd</a></span> |
                        <a class="back_top" href="javascript:void(0);" >Back to Top <i class="fa fa-angle-up"></i></a>
                    </div>
                </div><!-- //CONTAINER -->
            </div><!-- //COPYRIGHT -->
        </footer><!-- //FOOTER -->
    </div><!-- //PAGE -->
</div>

<!-- TOVAR MODAL CONTENT -->
<div id="modal-body" class="clearfix">
    <div id="tovar_content"></div>
    <div class="close_block"></div>
</div><!-- TOVAR MODAL CONTENT -->

    <!-- SCRIPTS -->
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!--[if IE]><html class="ie" lang="en"> <![endif]-->

    <!-- <script src="{{asset('assets/front/js/jquery.min.js')}}" type="text/javascript"></script> -->
    <!-- <script src="{{asset('assets/front/js/bootstrap.min.js')}}" type="text/javascript"></script> -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js" type="text/javascript"></script>



    <script src="{{asset('assets/front/js/jquery.sticky.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/front/js/parallax.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/front/js/jquery.flexslider-min.js')}}" type="text/javascript"></script>
    <!-- <script src="{{asset('assets/front/js/jquery.jcarousel.js')}}" type="text/javascript"></script> -->
    <script src="{{asset('assets/front/js/fancySelect.js')}}"></script>
    <script src="{{asset('assets/front/js/animate.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/front/js/myscript.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/front/js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/back/vendor/blueimp-gallery/js/jquery.blueimp-gallery.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/toastr/build/toastr.min.js')}}"></script>
    <script src="{{asset('assets/front/js/jquery.zoom.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/back/vendor/sweetalert/lib/sweet-alert.min.js')}}"></script>
<!--
     // slick -->

      <script src="{{asset('assets/front/js/slick.js')}}" type="text/javascript"></script>

      <script src="{{asset('assets/front/vendor/jcarousel/jquery.jcarousel.min.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/front/vendor/jcarousel/jcarousel.basic.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>

<!-- Owl Carousel-->
<script src="{{asset('assets/front/vendor/owl.carousel/owl.carousel.min.js')}}"></script>

<!-- Image Resizing Plugin-imgLiquid -->
<!--<script src="{{ asset('assets/front/imgLiquid/js/imgLiquid-min.js') }}"></script>-->

<!-- Image Resizing Plugin-fillContainer -->
<!--<script src="{{ asset('assets/front/fillContainer/js/jquery.fillcontainer.js') }}"></script>-->

<!-- Mosaic Plugin -->
<!--<script src="{{ asset('assets/front/mosaic-plugin/css/jquery.mosaic.js') }}"></script>-->

<script type="text/javascript">

    $(document).ready(function(){
                $('#newsletterFooterForm').validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    fname: {
                        required: true,
                        fname: true
                    }
                },
                sumbitHandler: function(form){
                    form.submit()
                }
            });
    });

</script>
<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>
    <!-- <script>
        if (top != self) top.location.replace(self.location.href);
    </script> -->
      @yield('js')

      <script type="text/javascript">
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

    @if(session('success'))
        Command: toastr["success"]("{{session('success.message')}} ", "{{session('success.title')}}")
      @elseif(session('error'))
        Command: toastr["error"]("{{session('error.message')}} ", "{{session('error.title')}}")
      @elseif(session('warning'))
        Command: toastr["warning"]("{{session('warning.message')}} ", "{{session('warning.title')}}")
      @elseif(session('info'))
        Command: toastr["info"]("{{session('info.message')}} ", "{{session('info.title')}}")
      @endif


    </script>  

    

</body>

</html>
