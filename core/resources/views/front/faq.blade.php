@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')
<style type="text/css">
      .widget_categories ul, .widget_categories ul li, .widget_popular_posts ul{
        padding-left: 0;
	  }
	  .sidepanel{
		padding: 0 23px 7px 0px;
	  }
	  .newsletter_wrapper {
    	padding: 14px 18px 7px 0px;
	 }
	 .widget_popular_post_img{
		 margin:0;
	 }
	 .newsletter_btn:hover {
	 	background-color: #5d391e !important;
	 }
</style>
@stop


@section('content')

    <style type="text/css">
      .widget_categories ul, .widget_categories ul li, .widget_popular_posts ul{
        padding-left: 0;
      }
    </style>

    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->


		<!-- PAGE HEADER -->
		<section class="page_header">

			<hr class="banner-top">
            <div class="banner-bg center">
                <h3>FAQ</h3>
                <p>FREQUENTLY ASKED QUESTIONS AND ANSWERS</p>
            </div>
            <hr class="banner-bottom">

			<!-- CONTAINER -->
			<div class="container">
			<!-- 	<hr class="banner-top">
            <div class="banner-bg center">
                <h3>FREQUENTLY ASKED QUESTIONS</h3>
                <p>Answer to some of the Frequently asked Questions!</p>
            </div>
            <hr class="banner-bottom"> -->
			</div><!-- //CONTAINER -->
		</section><!-- //PAGE HEADER -->


		<!-- FAQ PAGE -->
		<section class="faq_page">

			<!-- CONTAINER -->
			<div class="container">

				<!-- ROW -->
				<div class="row">

					<!-- FAQ BLOCK -->
					{{-- <div class="col-lg-9 col-md-9 col-sm-9 padbot30"> --}}
					<div class="col-sm-12 padbot30">
						<div class="title2">
							
							<h2 style="font-size: 18px;">Below are some of our most frequently asked questions. If you’re a new client or a new customer to our website, we encourage that you to read through the relevant questions.</h2>	
						</div>

						

						<!-- Accordion -->
						<div id="accordion">

						<h2 style="margin: 25px 0px 20px 0; font-weight: 500;">Getting Started</h2>

							<h4 class="accordion_title">How to return a defective product?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>

							<h4 class="accordion_title">What shipping methods do you use for International Orders?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>

							<h4 class="accordion_title">Where Can I Find My Item Purchase Code?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>

							<h4 class="accordion_title">Does This Theme Come With PSD Files?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>

							<h4 class="accordion_title">Does This Theme Come with Multi-Language Support?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>


							<h2 style="margin: 40px 0px 20px 0; font-weight: 500;">Membership Support</h2>


							<h4 class="accordion_title">How Do I Know About the Updates for This Theme?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>

							<h4 class="accordion_title">Does This Theme Come With PSD Files?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>

							<h4 class="accordion_title">Can I Preview the Documentation Before I Purchase the Theme?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>

							<h4 class="accordion_title">How to return a defective product?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>

							<h4 class="accordion_title">What shipping methods do you use for International Orders?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>

							<h4 class="accordion_title">When will my order ship?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>

							<h4 class="accordion_title">Can I Preview the Documentation Before I Purchase the Theme?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>

							<h4 class="accordion_title">Does This Theme Come With PSD Files?</h4>
							<div class="accordion_content">
								<p>You could say Barbara Casasola has had a charmed run. After a critically acclaimed London Fashion Week debut last September, the Porto Alegre, Brazil–born, London-based upstart was tapped to be the sole women’s wear presenter at Pitti Uomo, the biannual menswear trade show in Florence, an honor previously accorded to Peter Pilotto and Rodarte.</p>
								<p>Jean shorts mcsweeney’s salvia food truck viral. Squid keffiyeh blue bottle actually. Twee master cleanse VHS typewriter cray sartorial. 3 wolf moon irony umami meh, readymade mlkshk VHS squid gastropub mcsweeney’s raw denim locavore swag wolf. Single-origin coffee hoodie photo booth Austin blue bottle, kogi wolf.</p>
							</div>
						</div><!-- //Accordion -->





					</div><!-- //FAQ BLOCK -->


					<!-- SIDEBAR -->
					<div id="sidebar" class="col-lg-3 col-md-3 col-sm-3 padbot50">

						<!-- WIDGET SEARCH -->
						<!-- <div class="sidepanel widget_search">
							<form class="search_form" action="javascript:void(0);" method="get" name="search_form">
								<input type="text" name="Search..." value="Search..." onFocus="if (this.value == 'Search...') this.value = '';" onBlur="if (this.value == '') this.value = 'Search...';" />
							</form>
						</div> --><!-- //WIDGET SEARCH -->

						<!-- CATEGORIES -->
						{{-- <div class="sidepanel widget_categories">
							<h3>Sidebar Menu</h3>
							<ul>
								<li><a href="quotes" >Request</a></li>
							       <li><a href="{{url('/tasting')}}" >Schedule a Tasting</a></li>
								<li><a href="{{url('/onlineTutorials')}}" >Online Classes</a></li>
<!--								<li><a href="shopping-bag.html" >Shopping Bag</a></li>-->
<!--								<li><a href="articles.html" >Articles</a></li>-->
								<li><a href="{{url('PrivateClassesLand/0')}}"  >Private Classes</a></li>
<!--								<li><a href="update.html" >Site Update</a></li>-->
								<li><a href="{{url('productload/all/all/1/min/hi')}}" >Our Product</a></li>
								<li><a href="{{url('about')}}"  >About Us</a></li>
								<li><a href="{{url('gallery/0')}}"  >Gallery</a></li>
							</ul>
						</div> --}}
						<!-- //CATEGORIES -->


						<!-- NEWSLETTER FORM WIDGET -->
						{{-- <div class="sidepanel widget_newsletter">
							<div class="newsletter_wrapper">
								<h3>NEWSLETTER</h3>
								<form class="newsletter_form clearfix" action="{{url('/sign-up-to-newsletter')}}" id="newsletterForm" method="post">
									<input type="hidden" name="_token" value="{{csrf_token()}}">
									<input type="text" name="email" value="Join Our Mailing List" onFocus="if (this.value == 'Join Our Mailing List') this.value = '';" onBlur="if (this.value == '') this.value = 'Join Our Mailing List';" required />
									<input class="btn newsletter_btn" type="submit" value="Sign Up" style="background-color: #8a562f">
								</form>
							</div>
						</div> --}}
						<!-- //NEWSLETTER FORM WIDGET -->

						<!-- WIDGET POPULAR POSTS -->
						<!--<div class="sidepanel widget_popular_posts">
							<h3>POPULAR POSTS</h3>
							<ul>
								<li class="widget_popular_post_item clearfix">
									<a class="widget_popular_post_img" href="blog-post.html" ><img src="images/blog/popular1.jpg" alt="" /></a>
									<a class="widget_popular_post_title" href="blog-post.html" >New Fashion Vintage Double Breasted Trench Long</a>
									<span class="widget_popular_post_date">13 January 2014</span>
								</li>
								<li class="widget_popular_post_item clearfix">
									<a class="widget_popular_post_img" href="blog-post.html" ><img src="images/blog/popular2.jpg" alt="" /></a>
									<a class="widget_popular_post_title" href="blog-post.html" >In the Kitchen with…The New Potato</a>
									<span class="widget_popular_post_date">10 January 2014</span>
								</li>
								<li class="widget_popular_post_item clearfix">
									<a class="widget_popular_post_img" href="blog-post.html" ><img src="images/blog/popular3.jpg" alt="" /></a>
									<a class="widget_popular_post_title" href="blog-post.html" >2013 Hot Women thicken fleece Warm Coat Lady</a>
									<span class="widget_popular_post_date">5 January 2014</span>
								</li>
							</ul>
						</div>--><!-- //WIDGET POPULAR POSTS -->

						<!-- BANNERS WIDGET -->
						<!-- <div class="widget_banners">
							<a class="banner nobord margbot10" href="javascript:void(0);" ><img src="images/tovar/banner10.jpg" alt="" /></a>
							<a class="banner nobord margbot10" href="javascript:void(0);" ><img src="images/tovar/banner9.jpg" alt="" /></a>
						</div> --><!-- //BANNERS WIDGET -->
					</div><!-- //SIDEBAR -->
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</section><!-- //FAQ PAGE -->


@stop

@section('js')
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
		        $('#newsletterForm').validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    } 
                },
                sumbitHandler: function(form){
                    form.submit()
                }
            	});
	});

</script>
<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>

@stop
