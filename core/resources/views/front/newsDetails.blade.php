@extends('layouts.front.master') @section('title','About | www.theprinceofgalle.com')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/flipbook1.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/flipbook2.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/flipbook-ss-styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/flipbook-colorpicker.css')}}">
    <style type="text/css">
    
        #flipbook-1 {
            margin-top: 20px;
            margin-bottom: 0px;
            margin-left: 0px;
            margin-right: 0px;            
            width: 712px; 
            height: 510px; 
        }
        
        #flipbook-1 div.fb-page div.page-content {
            margin: 10px 0px; 
        }
        
        #flipbook-1 .turn-page {
            width: 356px;       
            height: 510px;
            background: #ECECEC;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            box-shadow: inset -1px 0px 1px 0px #BFBFBF;             
        }
        
        #flipbook-1 .last .turn-page,
        #flipbook-1 .even .turn-page {
            border-radius: 0px;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;    
            box-shadow: inset 1px 0px 1px 0px #BFBFBF;
        }
        
        #flipbook-1 .fpage .turn-page {
            border-radius: 0px;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
            box-shadow: inset 1px 0px 1px 0px  #BFBFBF;
        }
        
        #flipbook-1 .last .fpage .turn-page,
        #flipbook-1 .even .fpage .turn-page {
            border-radius: 0px;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            box-shadow: inset -1px 0px 1px 0px #BFBFBF;
        }
        
        #flipbook-container-1 div.page-content div.container img.bg-img {
            margin-top: 0px;
            margin-left: 0px;
        }
        
        #flipbook-container-1 div.page-content.first div.container img.bg-img {
            margin-top: 10px;
        }
    
        #flipbook-container-1 div.page-content.even div.container img.bg-img {
            left: 0px;
        }
        
        #flipbook-container-1 div.page-content.last div.container img.bg-img {
            left: 10px;
            margin-top: 10px;
        }

        img.bg-img{            
            background-position: center center !important;           
        }
    
        #flipbook-1 div.page-transition.last div.page-content,
        #flipbook-1 div.page-transition.even div.page-content,
        #flipbook-1 div.turn-page-wrapper.odd div.page-content {
            margin-left: 0px;
            margin-right: 10px; 
        }
    
        #flipbook-1 div.turn-page-wrapper.first div.page-content {
            margin-right: 10px;
            margin-left: 0px; 
        }
    
        #flipbook-1 div.page-transition.first div.page-content,
        #flipbook-1 div.page-transition.odd div.page-content,
        #flipbook-1 div.turn-page-wrapper.even div.page-content,
        #flipbook-1 div.turn-page-wrapper.last div.page-content {
            margin-left: 10px;
        }
        
        #flipbook-1 div.fb-page-edge-shadow-left,
        #flipbook-1 div.fb-page-edge-shadow-right,
        #flipbook-1 div.fb-inside-shadow-left,
        #flipbook-1 div.fb-inside-shadow-right {
            top: 10px;
            height: 490px;
        }
        
        #flipbook-1 div.fb-page-edge-shadow-left {
            left: 10px;
        }
        
        #flipbook-1 div.fb-page-edge-shadow-right {
            right: 10px;
        }
        
        /* Zoom */
                    
        #flipbook-container-1 div.zoomed-shadow {
            opacity: 0.2;
        }
        
        #flipbook-container-1 div.zoomed {
            border: 10px solid #ECECEC;
            border-radius: 10px;
            box-shadow: 0px 0px 0px 1px #D0D0D0;                
        }   
        
        /* Show All Pages */
        #flipbook-container-1 div.show-all div.show-all-thumb {
            margin-bottom: 12px;
            height: 180px;
            width: 125px;
            border: 1px solid #878787;
        }
        
        #flipbook-container-1 div.show-all div.show-all-thumb.odd {
            margin-right: 10px;
            border-left: none;
        }
        
        #flipbook-container-1 div.show-all div.show-all-thumb.odd img.bg-img {
            padding-left: 250px;
        }
        
        #flipbook-container-1 div.show-all div.show-all-thumb.odd.first img.bg-img {
            padding-left: 0px;
        }
        
        #flipbook-container-1 div.show-all div.show-all-thumb.even {
            border-right: none;
        }
        
        #flipbook-container-1 div.show-all div.show-all-thumb.last-thumb {
            margin-right: 0px;
        }
        
        #flipbook-container-1 div.show-all {
            background: #F6F6F6;
            border-radius: 10px;
            border: 1px solid #D6D6D6;
        }
        
        #flipbook-container-1 div.show-all div.content {
            top: 10px;
            left: 10px;
        }    
        
        /* Inner Pages Shadows */
                
        #flipbook-1 div.fb-page-edge-shadow-left,
        #flipbook-1 div.fb-page-edge-shadow-right {
            display: none;
        }

        footer{
            margin-top:10%;
        }  
        
        #item_des{
            margin-bottom: 4%;
        } 

        .backbtndiv{
            text-align:center;
        }

        .backbtn{
            background-color: #8a562f !important;
            color: white !important;
            margin-bottom: 20px;
            width: 10%;
            height: 50px;
            border-radius: 5px;
            border: none;
            padding-top:1.8%;
            padding-left:8px;
            padding-right:8px;
            font-size:14px;
            line-height:1.429px;
            font-weight:400;
        } 

        .backbtn:hover{
            background-color:#FCEEEB !important;
        }   

        .video_content{
            display: flex;
            align-items: center;
            justify-content: center;
        }
        
        /* Remove the stretch behavior of the image*/
        .sub-page-content{
            overflow: hidden; 
            width:346px;
            height:490px;
        }

        .sub-page-content img{            
            position: relative;
            vertical-align: middle;
            object-fit:cover;            
        }

        /*******************************************/

             

    </style> 


    <script type="c59587f4fab49b26d249e90f-text/javascript" src="{{asset('assets/front/js/swfobject2.js')}}"></script>
    
    <script type="c59587f4fab49b26d249e90f-text/javascript" src="{{asset('assets/front/js/jquery.min.js')}}"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.0.0.js" type="c59587f4fab49b26d249e90f-text/javascript"></script>
    <script src="{{asset('assets/front/js/jquery.sticky.js')}}" type="c59587f4fab49b26d249e90f-text/javascript"></script>
    <script src="{{asset('assets/front/js/parallax.js')}}" type="c59587f4fab49b26d249e90f-text/javascript"></script>
    <script src="{{asset('assets/front/js/jquery.flexslider-min.js')}}" type="c59587f4fab49b26d249e90f-text/javascript"></script>
    <script src="{{asset('assets/front/js/jquery.magnific-popup.min.js')}}" type="c59587f4fab49b26d249e90f-text/javascript"></script>
    <script type="c59587f4fab49b26d249e90f-text/javascript" src="{{asset('assets/front/js/jquery.easing.1.3.js')}}"></script>
    <script type="c59587f4fab49b26d249e90f-text/javascript" src="{{asset('assets/front/js/turn.js')}}"></script>
    <script type="c59587f4fab49b26d249e90f-text/javascript" src="{{asset('assets/front/js/flipbook.js')}}"></script>
    <script type="c59587f4fab49b26d249e90f-text/javascript" src="{{asset('assets/front/js/jquery.doubletap.js')}}"></script>
    <script type="c59587f4fab49b26d249e90f-text/javascript" src="{{asset('assets/front/js/jquery.color.js')}}"></script>
    <script type="c59587f4fab49b26d249e90f-text/javascript" src="{{asset('assets/front/js/page-js.js')}}"></script>
    <script type="c59587f4fab49b26d249e90f-text/javascript" src="{{asset('assets/front/js/ss-jquery.js')}}"></script>
    <script type="c59587f4fab49b26d249e90f-text/javascript" src="{{asset('assets/front/js/customSelect.js')}}"></script>
    <script type="c59587f4fab49b26d249e90f-text/javascript" src="{{asset('assets/front/js/easing-jquery.js')}}"></script>
    <script type="c59587f4fab49b26d249e90f-text/javascript" src="{{asset('assets/front/js/colorpicker.js')}}"></script>

@stop


@section('content')
    <!-- BREADCRUMBS -->
    <section class="breadcrumb parallax margbot30"></section>
    <!-- //BREADCRUMBS -->

    <!-- PAGE HEADER -->
    <section class="page_header" style="visibility: hidden;">
        <!-- CONTAINER -->
        <div class="container">
            <h3 class="pull-left"><b>{{ $news->title }}</b></h3>
        </div>
        <!-- //CONTAINER -->
    </section>
    <!-- //PAGE HEADER -->

    <section class="page_header">

        <!-- CONTAINER -->
        <div class="container">
            <center><h3><b>{{ $news->title }}</b></h3></center>
        </div>
        <!-- //CONTAINER -->
        
    </section>
    <!-- //PAGE HEADER -->

    <div id="flipbook-container-1" class="flipbook-container" style="margin-bottom: 50px;">

        <div id="flipbook-1" class="flipbook">

            <div class="fb-page">
                <div class="page-content">
                    <div class="container sub-page-content">
                        <img src="{{url('').'/core/storage/uploads/images/news'.'/'.$news->front_image}}" class="bg-img" />
                        <img src="{{url('').'/core/storage/uploads/images/news'.'/'.$news->front_image}}" class="bg-img zoom-large" />
                    </div>
                </div>
            </div>

            @foreach ($news->getImages as $key => $value)
                <div class="fb-page">
                    <div class="page-content">
                        <div class="container sub-page-content">
                            <img src="{{url('').'/core/storage/uploads/images/news'.'/'.$value->file_name}}" alt="" class="bg-img"/>
                            <img src="{{url('').'/core/storage/uploads/images/news'.'/'.$value->file_name}}" class="bg-img zoom-large" />
                        </div>
                    </div>
                </div>
            @endforeach

            @if (isset(explode('v=',$news->video_link)[1]))
                <div class="fb-page" style="width: 1000px;">
                    <div class="page-content video_content">
                        <div class="container vidoe_div">
                            <iframe height="100%" width="100%" class="iframe-news-video" src="https://www.youtube.com/embed/{{ explode('v=',$news->video_link)[1] }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            @endif

        </div> 

        <div id="fb-nav-1" class="fb-nav mobile stacked">
            <ul>
                <li class="toc left"><i class="fa fa-angle-double-left" aria-hidden="true" style="size: 50px;"></i></li>
                <li class="zoom center">Zoom</li>
                <li class="slideshow center">Slide Show</li>
                <li class="show-all right"></li>
            </ul>
        </div> 

    </div> 

    <script src="{{asset('assets/front/js/rocket-loader.min.js')}}" data-cf-settings="c59587f4fab49b26d249e90f-|49" defer=""></script>

    <!-- CONTAINER -->
    <div class="container" id="item_des">
        <center>{!! $news->description !!}</center>
    </div>
    <!-- //CONTAINER -->

    <div class="col-sm-12 col-md-12 col-lg-12 backbtndiv">
        <a href="{{url('news/')}}" class="btn btn-default backbtn">Back</a>
    </div>
@stop

@section('js')

    @if(session('success'))
    swal('Good Job','User Registered','success');
    @endif

    <!--$("#slideshow > div:gt(0)").hide();
    setInterval(function() {
    $('#slideshow > div:first')
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo('#slideshow');
    },  3000);-->   

@stop
