@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')


<style type="text/css">
    .tovar_img{
        height: 195px;
    }
    .tovar_view_shop {
        padding: 5px 8px;
        text-transform: uppercase;
        font-weight: 700;
        font-size: 12px;
        color: #fff;
        border: 2px solid transparent!important;
        background-color: transparent!important;
    }

    .tovar_view_shop:hover {
        padding: 5px 8px;
        text-transform: uppercase;
        font-weight: 700;
        font-size: 12px;
        border:none!important;
        color: #fff!important;
        border-color: transparent!important;

    }

    .add_lovelist_shop {
        display: inline-block;
        width: 40px;
        height: 40px;
        text-align: center;
        line-height: 37px;
        font-size: 24px;
        color: #fff;
        /*border: 2px solid #333;*/
        background-color: transparent!important;
    }

    .add_lovelist_shop:hover {
        display: inline-block;
        width: 40px;
        height: 40px;
        text-align: center;
        line-height: 37px;
        font-size: 28px;
        color: #ff0000!important;
        /*border: 2px solid #ff0000;*/
        background-color: transparent!important;
    }
    .keywords li{
            text-transform: uppercase;
            width: 145px;
            font-size: 12px;
    }
    .keywords li:hover{
            text-transform: uppercase;
            width: 145px;
            font-size: 12px;
     }

     .widget_categories li{
          margin-left: -40px;
          border-style: none;
          padding-left: 0px;
          padding-top: 2px;
        }
      .tovar_item_btns{
          margin-bottom: 0px!important;
          height: 195px;
          bottom: 0px!important;
        }
        .open-project-link{
          margin-top: 72px;
          background-color: #F8B4B9;
        }

 .keywords-shop {
    list-style:none;
    text-align: center;
    margin: 0 auto;
    padding: 0px;
    display:table;
    overflow: hidden;
    }

.keywords-shop li{
    vertical-align: bottom;
    float: left;
    padding: 6px 15px 6px 15px;
    /*width: 100px;*/
    margin-left: 3px;
    margin-right: 3px;
    background-color:none;
    font-weight: 600;
    font-size: 1.25em;
    border-radius: 5px;
    border: thin solid #8b5730;

}

.keywords-shop li:hover{
    vertical-align: bottom;
    float: left;

    margin-top: 0px;
    margin-left: 3px;
    margin-right: 3px;
    background-color: #8b5730;
    font-size: 1.25em;
    border-radius: 5px;
    cursor: pointer;
    border: no;
    -webkit-transition: all 0.4s ease-in-out;
}

.keywords-shop li a {
  color : #8b5730;
  -webkit-transition: all 0.3s ease-in-out;
}

.keywords-shop li:hover a {
  color : #fff;
  -webkit-transition: all 0.3s ease-in-out;
}

</style>

<style>
.btn.btn-block{
    margin:10px 0 10px 0;
}
.row{
    margin: 50px 0 50px 0;
}
.col-lg-4{
    margin:auto;
}
.card-body{
    padding:20px;
}
.pricing .card {
  border: none;
  border-radius: 1rem;
  transition: all 0.2s;
  box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
}


.pricing hr {
  margin: 1.5rem 0;
}

.pricing .card-title {
  margin: 15px 0;
  font-size: 2rem;
  letter-spacing: .1rem;
  font-weight: bold;
}

.pricing .card-price {
  font-size: 3rem;
  margin: 0;
}

.pricing .card-price .period {
  font-size: 0.8rem;
}

.pricing ul li {
  margin-bottom: 1rem;
}

.pricing .text-muted {
  opacity: 0.7;
}

.pricing .btn {
  font-size: 80%;
  border-radius: 5rem;
  letter-spacing: .1rem;
  font-weight: bold;
  padding: 1rem;
  opacity: 0.7;
  transition: all 0.2s;
}
.standard{
    background-color:#2dccd3!important;
    border:none;
    color:#fff;
}
.advantage{
    background-color:#00bf6f!important;
    border:none;
    color:#fff;
}
.premier{
    background-color:#05467e!important;
    border:none;
    color:#fff;
}
.btn-primary:hover{
    filter:saturate(50%);
    color:#fff;
}

.ribbon {
  position: absolute;
  right: 13px; top: -16px;
  z-index: 1;
  overflow: hidden;
  width: 75px; height: 75px;
  text-align: right;
}
.ribbon span {
  font-size: 10px;
  font-weight: bold;
  color: #FFF;
  text-transform: uppercase;
  text-align: center;
  line-height: 20px;
  transform: rotate(45deg);
  -webkit-transform: rotate(45deg);
  width: 100px;
  display: block;
  background: #79A70A;
  background: linear-gradient(#00BF6F 0%, #00bf6f 100%);
  box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
  position: absolute;
  top: 19px; right: -21px;
}
.ribbon span::before {
  content: "";
  position: absolute; left: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid #00bf6f;
  border-right: 3px solid transparent;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #00bf6f;
}
.ribbon span::after {
  content: "";
  position: absolute; right: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid transparent;
  border-right: 3px solid #00bf6f;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #00bf6f;
}
.modal-open .modal{
    overflow:none;
}
.modal-body{
    overflow-y: auto;
    max-height:600px!important;
}
.modal{
    z-index:99999;
}
.modal-header .close {
    margin-top: -20px;
}
.modal-title{
    color:#fff!important;
}
.modal-header.standard{
    background-color:#2dccd3!important;
}
.modal-header.advantage{
    background-color:#00bf6f!important;
}
.modal-header.premier{
    background-color:#05467e!important;
}
/* Hover Effects on Card */

@media (min-width: 992px) {
  .pricing .card:hover {
    margin-top: -.25rem;
    margin-bottom: .25rem;
    box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.3);
  }
  .pricing .card:hover .btn {
    opacity: 1;
  }
  .recommended{
    margin-top: -15px;
    margin-bottom: 15px;
    border:3px solid #00bf6f;
    border-radius: 10px;
}
}

@media (max-width: 1199px){
    .pricing .card {
        margin: 25px 0 25px 0;
    }
    .recommended{
        border:3px solid #00bf6f;
        border-radius: 10px;
    }
    .ribbon {
        right: 13px; top: -2px;
    }
}
@media (max-width: 992px){
    .pricing .card {
        margin: 10px 0 10px 0;
    }
    .recommended{
        border:3px solid #00bf6f;
        border-radius: 10px;
    }
    .ribbon {
        right: 13px; top: -2px;
    }
}

</style>

@stop

@section('content')


<!-- BREADCRUMBS -->
<section class="breadcrumb men parallax margbot30">

</section><!-- //BREADCRUMBS -->


<!-- SHOP BLOCK -->
<section class="shop">

        <hr class="banner-top">
        <div class="banner-bg center">
            <h3>Membership Plan</h3>
            <p>We are offering you to join our membership plan.</p>
        </div>
        <hr class="banner-bottom">

</section><!-- //SHOP -->

<section class="pricing py-5">
  <div class="container">
    <div class="row">
      <!-- Free Tier -->
      <div class="col-lg-4">
        <div class="card mb-5 mb-lg-0">
          <div class="card-body">
            <h5 class="card-title text-muted text-uppercase text-center">STANDARD</h5>
            <h6 class="card-price text-center">$0<span class="period">/month</span></h6>
            <a href="#" class="btn btn-block btn-primary text-uppercase standard">Sign Up</a>
            <hr>
            <ul class="fa-ul">
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Single User</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>5GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li class="text-muted"><span class="fa-li"><i class="fa fa-times"></i></span>Unlimited Private Projects</li>
              <li class="text-muted"><span class="fa-li"><i class="fa fa-times"></i></span>Dedicated Phone Support</li>
              <li class="text-muted"><span class="fa-li"><i class="fa fa-times"></i></span>Free Subdomain</li>
              <li class="text-muted"><span class="fa-li"><i class="fa fa-times"></i></span>Monthly Status Reports</li>
            </ul>
            <a href="#" class="btn btn-block text-uppercase text-center" data-toggle="modal" data-target="#standardModalMemership">View More</a>
          </div>
        </div>
      </div>
      <!-- Plus Tier -->
      <div class="col-lg-4">
        <div class="card mb-5 mb-lg-0">
          <div class="card-body recommended">
            <div class="ribbon"><span>BEST VALUE</span></div>
            <h5 class="card-title text-muted text-uppercase text-center">ADVANTAGE</h5>
            <h6 class="card-price text-center">$9<span class="period">/month</span></h6>
            <a href="#" class="btn btn-block btn-primary text-uppercase advantage">Sign Up</a>
            <hr>
            <ul class="fa-ul">
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>5 Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>50GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Free Subdomain</li>
              <li class="text-muted"><span class="fa-li"><i class="fa fa-times"></i></span>Monthly Status Reports</li>
            </ul>
            <a href="#" class="btn btn-block text-uppercase text-center" data-toggle="modal" data-target="#advantageModalMemership">View More</a>
          </div>
        </div>
      </div>
      <!-- Pro Tier -->
      <div class="col-lg-4">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title text-muted text-uppercase text-center">PREMIER</h5>
            <h6 class="card-price text-center">$49<span class="period">/month</span></h6>
            <a href="#" class="btn btn-block btn-primary text-uppercase premier">Sign Up</a>
            <hr>
            <ul class="fa-ul">
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
            </ul>
            <a href="#" class="btn btn-block text-uppercase text-center" data-toggle="modal" data-target="#premierModalMemership">View More</a>
          </div>
        </div>
      </div>
    </div>
  </div>


<!-- Modal Popups : Please add more video details here -->
 <!-- Standard Modal Section -->
<div class="modal fade" id="standardModalMemership" tabindex="-1" role="dialog" aria-labelledby="standardModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header standard">
        <h3 class="modal-title text-uppercase text-center" id="standardModalLongTitle">STANDARD</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <ul class="fa-ul">
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
            </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Advantage Modal Section -->
<div class="modal fade" id="advantageModalMemership" tabindex="-1" role="dialog" aria-labelledby="advantageModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header advantage">
        <h3 class="modal-title text-uppercase text-center" id="advantageModalLongTitle">Advantage</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <ul class="fa-ul">
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
            </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Premier Modal Section -->
<div class="modal fade" id="premierModalMemership" tabindex="-1" role="dialog" aria-labelledby="premierModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header premier">
        <h3 class="modal-title text-uppercase text-center" id="premierModalLongTitle">Premier</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <ul class="fa-ul">
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited Users</strong></li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>150GB Storage</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Public Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Community Access</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Unlimited Private Projects</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Dedicated Phone Support</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
              <li><span class="fa-li"><i class="fa fa-check"></i></span>Monthly Status Reports</li>
            </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</section>



@stop

@section('js')


@stop
