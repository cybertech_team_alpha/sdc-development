@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')

@stop


@section('content')
  
    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->
    
    
    <!-- CHECKOUT PAGE -->
    <section class="checkout_page">
      
      <!-- CONTAINER -->
      <div class="container">

        <!-- CHECKOUT BLOCK -->
        <div class="checkout_block">
          <ul class="checkout_nav checkout-padding-added">
            <a href="{{url('checkout1')}}"><li>1. Shipping Details</li></a>
            <a href="{{url('checkout2')}}"><li>2. Delivery Details</li></a>
            <a href="{{url('checkout3')}}"><li>3. Payment Details</li></a>
            <a href="{{url('checkout4')}}"><li class="last active_step">4. Confirm Order</li></a>
          </ul>
        </div><!-- //CHECKOUT BLOCK -->
          
        <!-- ROW -->
        <div class="row">
          <div class="col-lg-9 col-md-9 padbot60">
            <div class="checkout_confirm_orded clearfix">
              <div class="checkout_confirm_orded_bordright clearfix">
                <div class="billing_information">
                  <p class="checkout_title margbot10">Customer information</p>
                  
                  <div class="billing_information_content margbot40">
                    <span>{{$user->first_name}} {{$user->last_name}}</span>
                    <span>Contact No : {{$lastShipping->phone}}</span>
                    <span>{{$lastShipping->email}}</span>
                  </div>
                  
                  <p class="checkout_title margbot10">Shipping adress</p>
                  
                  <div class="billing_information_content margbot40">
                    <span>{{$lastShipping->first_name}} {{$lastShipping->last_name}}</span>
                    <span>{{$lastShipping->street_addresss_1}}</span>
                    <span>{{$lastShipping->street_addresss_2}}</span>
                    <span>{{$lastShipping->city}}, {{$lastShipping->postcode}}</span>
                    <span>{{$lastShipping->province}}</span>
                    <span>{{$lastShipping->getCountry->name}}</span>
                    <span>{{$lastShipping->phone}}</span>
                    
                  </div>
                </div>
                
                <div class="payment_delivery">
                  <p class="checkout_title margbot10">Payment and delivery</p>
                  
                  <p><span>Payment:<span> PayPal</p>
                  <p><span>Delivery:</span> {{$lastShipping->getShippingMethod->name}}</p>
                </div>
              </div>
              
              <div class="checkout_confirm_orded_products">
                <p class="checkout_title">Products</p>
                <ul class="cart-items">
                  <?php foreach ($shoppingCartArray_product as $key => $value): ?>
                    <li class="clearfix">
                    <img class="cart_item_product" src="{{$value['image']}}" alt="" />
                    <a href="{{url().'/product-detail/'.$value['id']}}" class="cart_item_title">{{ substr(strip_tags($value['name']),0,45)}}...
                    <span class="cart_item_price">{{number_format($value['price'],2)}} X {{number_format($value['quantity'])}} = {{number_format($value['price'] * $value['quantity'] ,2)}}</span></a>
                  </li>
                  <?php endforeach ?>
                  
                </ul>
                <p class="checkout_title">Tutorials</p>
                <ul class="cart-items">
                  <?php foreach ($shoppingCartArray_tutorial as $key => $value): ?>
                    <li class="clearfix">
                    <img class="cart_item_product" src="{{$value['image']}}" alt="" />
                    <?php 
                      $geturl = "";
                      if($value['details']['free_paid'] == 0){
                          $geturl="free";
                      }else{
                          $geturl="paid";
                      }
                    ?>
                    <a href="{{url('viewTutorials/'.$geturl."/".$value['id'])}}" class="cart_item_title">{{ substr(strip_tags($value['name']),0,45)}}...</a>
                    <span class="cart_item_price">{{number_format($value['price'],2)}}</span>
                  </li>
                  <?php endforeach ?>
                  
                </ul>
              </div>
            </div>
          </div>
          
          <div class="col-lg-3 col-md-3 padbot60">
            
            <!-- BAG TOTALS -->
            <div class="sidepanel widget_bag_totals your_order_block">
              <h3 class="text-center total-new">Order Summary</h3>
              <table class="bag_total">
                <tr class="cart-subtotal clearfix">
                  <th>Sub total</th>
                  <td>US$ {{number_format($subTotal,2)}}</td>
                </tr>
                <tr class="shipping clearfix">
                  <th>SHIPPING</th>
                  <td>US$ {{number_format($shipping_charges,2)}}</td>
                </tr>
                <?php if (!empty($coupon_condition)): ?>
                <tr class="shipping clearfix">
                  <th>COUPON</th>
                  <td>US$ {{number_format(abs($coupon_condition->getValue()),2)}}</td>
                </tr>
                <?php endif ?>
               
                <tr class="total clearfix total-new">
                  <th>Total</th>
                  <td>US$ {{number_format($total,2)}}</td>
                </tr>
              </table>
              <form method="POST" >
                  {!!Form::token()!!}
                    <a class="btn inactive" href="{{url('checkout3')}}" >Step Back</a>
                   <?php if ($cartTotalQuantity>0): ?>
                    <button class="btn checkout-btn" type="submit">Confirm and Pay</button>
                   <?php endif ?>
              </form>
              
             
             
            </div><!-- //REGISTRATION FORM -->
          </div><!-- //SIDEBAR -->
        </div><!-- //ROW -->
      </div><!-- //CONTAINER -->
    </section><!-- //CHECKOUT PAGE -->

    
@stop

@section('js')


 
@stop
