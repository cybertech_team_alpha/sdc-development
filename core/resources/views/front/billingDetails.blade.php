@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')


<style type="text/css">
  .skip {
    padding: 8px 6px 0px 0px;
  }

  a.skip-a {
    font-size: 14px;
    color: #0000ff;
  }

  a.skip-a:hover {
    text-decoration: underline;
  }

</style>


@stop


@section('content')
  
    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->
        
        
        <!-- PAGE HEADER -->
        <section class="page_header">
            
            <!-- CONTAINER -->
            <div class="container">
                <hr class="banner-top">
            <div class="banner-bg center">
                <h3>Billing Details</h3>
                <p>Add your Billing Details to complete the registration!</p>
            </div>
            <hr class="banner-bottom">
            </div><!-- //CONTAINER -->
        </section><!-- //PAGE HEADER -->
    
    
    <!-- CHECKOUT PAGE -->
    <section class="checkout_page">
      
      <!-- CONTAINER -->
      <div class="container">

       <form class="form-horizontal"  method="post" action="{{ url('/billingdetail2') }}"  id="contact_form">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <fieldset>
             
              <div class="form-group"> 
                <label class="col-md-4 control-label">Country</label>
                  <div class="col-md-4 selectContainer">
                  <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                  <select name="country" class="form-control selectpicker">
                    <option value="">Select your Country</option>
                    <?php foreach ($country as $key => $value): ?>                  
                      <option value="{{$value->id}}" {{$user->getBillingDetails && $value->id == $user->getBillingDetails->country_id  ? 'selected' : ''}}>{{$value->name}}</option>
                    <?php endforeach ?>
                 
                    
                  </select>
                </div>
              </div>
              </div>
              <!-- Text input-->
              <div class="form-group">
                  <label class="col-md-4 control-label">City</label>  
                  <div class="col-md-4 inputGroupContainer">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                      <input  name="city" placeholder="City" class="form-control" value="{{$user->getBillingDetails ? $user->getBillingDetails->city : ''}}" type="text">
                      </div>
                  </div>
              </div>
              <!-- Text input-->
              <div class="form-group">
                  <label class="col-md-4 control-label" > Province </label> 
                  <div class="col-md-4 inputGroupContainer">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                          <input name="province" placeholder="Province" class="form-control"  type="text" value="{{$user->getBillingDetails ? $user->getBillingDetails->province : ''}}">
                      </div>
                  </div>
              </div>
              <!-- Text input-->
              <div class="form-group">
                  <label class="col-md-4 control-label" >Zip/Postal Code</label> 
                  <div class="col-md-4 inputGroupContainer">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                          <input name="postcode" placeholder="Postcode" class="form-control" value="{{$user->getBillingDetails ? $user->getBillingDetails->postcode : ''}}"  type="text">
                      </div>
                  </div>
              </div>
               <!-- Text input-->
              <div class="form-group">
                  <label class="col-md-4 control-label" >Street Adress 1</label> 
                  <div class="col-md-4 inputGroupContainer">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                          <input name="address1" placeholder="Street Adress 1" value="{{$user->getBillingDetails ? $user->getBillingDetails->street_addresss_1 : ''}}" class="form-control"  type="text">
                      </div>
                  </div>
              </div>
               <!-- Text input-->
              <div class="form-group">
                  <label class="col-md-4 control-label" >Street Adress 2</label> 
                  <div class="col-md-4 inputGroupContainer">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                          <input name="address2" placeholder="Street Adress 2" class="form-control" value="{{$user->getBillingDetails ? $user->getBillingDetails->street_addresss_2 : ''}}"  type="text">
                      </div>
                  </div>
              </div>
               <!-- Text input-->
              <div class="form-group">
                  <label class="col-md-4 control-label" >First Name</label> 
                  <div class="col-md-4 inputGroupContainer">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                          <input name="first_name" placeholder="First Name" class="form-control"  type="text" value="{{$user->getBillingDetails ? $user->getBillingDetails->first_name : $user->first_name}}">
                      </div>
                  </div>
              </div>
               <!-- Text input-->
              <div class="form-group">
                  <label class="col-md-4 control-label" >Last name</label> 
                  <div class="col-md-4 inputGroupContainer">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                          <input name="last_name" placeholder="Last name" class="form-control"  type="text" value="{{$user->getBillingDetails ? $user->getBillingDetails->last_name : $user->last_name}}">
                      </div>
                  </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">Contact No.</label>  
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                   <input name="phone" placeholder="+248-787-7766" class="form-control" type="text" value="{{$user->getBillingDetails ? $user->getBillingDetails->phone : $user->mobile}}">
                  </div>
                </div>
              </div>
              <!-- Text input-->
              <div class="form-group">
                  <label class="col-md-4 control-label">E-Mail</label>  
                  <div class="col-md-4 inputGroupContainer">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                          <input name="email" placeholder="E-Mail Address" class="form-control"  type="text" value="{{$user->getBillingDetails ? $user->getBillingDetails->email : $user->email}}">
                      </div>
                  </div>
              </div>
              
              <!-- Select Basic -->
              <!-- Success message -->
              <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>
              <!-- Button -->
              <div class="form-group">
                  <label class="col-md-4 control-label"></label>
                  <div class="col-md-4" style="vertical-align: middle;"><br>
                      <button  type="submit" class="btn btn-block col-md-6 sumbit" id="sumbit" >Continue</button>
                      <div class="skip pull-right">
                        <a class="skip-a" href="{{url('/')}}">Skip for now</a>
                      </div>
                  </div>
              </div>
          </fieldset>
      </form>
    </div><!-- //CONTAINER -->
  </section><!-- //CHECKOUT PAGE -->
  <style type="text/css">
    #success_message{ display: none;}
</style>

    
@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
    $('#contact_form').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          // valid: 'glyphicon glyphicon-ok',
          // invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          first_name: {
              validators: {
                      stringLength: {
                      min: 2,
                  },
                      notEmpty: {
                      message: 'Please enter your First Name'
                  }
                  ,
                  regexp: {
                      regexp: /^[a-zA-Z_]+$/,
                      message: 'The username can only consist of alphabetical & underscore'
                  }
              }
          },
           last_name: {
              validators: {
                   stringLength: {
                      min: 2,
                  },
                  notEmpty: {
                      message: 'Please enter your Last Name'
                  }
                  ,
                  regexp: {
                      regexp: /^[a-zA-Z_0-9]+$/,
                      message: 'The username can only consist of alphabetical & underscore'
                  }
              }
          },
          country: {
              validators: {
                  notEmpty: {
                      message: 'Please enter your country'
                  }
                  
              }
          }
          ,city: {
              validators: {
                   stringLength: {
                      min: 2,
                  },
                  notEmpty: {
                      message: 'Please enter your City'
                  }
                  
              }
          },province: {
              validators: {
                   stringLength: {
                      min: 2,
                  },
                  notEmpty: {
                      message: 'Please enter your Province'
                  }
                  ,
                  regexp: {
                      regexp: /^[a-zA-Z_\s]+$/,
                      message: 'The Province can only consist of alphanumerics'
                  }
              }
          },postcode: {
              validators: {
                   stringLength: {
                      min: 1,
                  },
                  notEmpty: {
                      message: 'Please enter your ZIP / Postal code'
                  },
                  regexp: {
                      regexp: /^[a-zA-Z0-9\s#_-]+$/,
                      message: 'The ZIP cannot have any special characters except for #, - and _'
                  }
              }
          },address1: {
              validators: {
                   stringLength: {
                      min: 1,
                  },
                  notEmpty: {
                      message: 'Please enter your address1'
                  }
                
              }
          },address2: {
              validators: {
                   stringLength: {
                      min: 1,
                  }
              }
          },
          email: {
              validators: {
                  notEmpty: {
                      message: 'Please enter your Email Address'
                  },
                  emailAddress: {
                      message: 'Please enter a valid Email Address'
                  }
              }
          },
          phone: {
              validators: {
                  notEmpty: {
                      message: 'Please enter a valid Contact Number'
                  },
                  stringLength: {
                        min: 10, 
                        max: 13,
                  }
              }
          }
        
          }
      })
      .on('success.form.bv', function(e) {
          $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
              $('#contact_form').data('bootstrapValidator').resetForm();
    
          // Prevent form submission
          e.preventDefault();
    
          // Get the form instance
          var $form = $(e.target);
    
          // Get the BootstrapValidator instance
          var bv = $form.data('bootstrapValidator');
    
          // Use Ajax to submit form data
          $.post($form.attr('action'), $form.serialize(), function(result) {
              console.log(result);
          }, 'json');
      });
       $("#contact_reset").click(function(){
         $('#contact_form').bootstrapValidator("resetForm",true);    
      });
    });
</script>

 
@stop
