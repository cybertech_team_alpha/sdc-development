@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/lightbox/css/lightgallery.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/lightbox/css/lightslider.css')}}"/>
<!-- LightBox -->
<style type="text/css">
.orgprice{
    color:#313131;
    position: relative;
    display: inline-block;
    font-size: 17px;
    font-weight: 600;
    margin-top: 20px;
}
.saleprice{
 /* color:#FF1C03;  */
 color:#4D4B4C;
 font-weight:500;
 font-size: 15px;
 /* text-shadow: 0px 0px 8px rgba(10, 10, 10, 0.5); */
 margin-left: 15px;
}
.oldprice{
    /* color:#313131; */
    color:#B4ABAA;
    position: relative;
    display: inline-block;
    text-decoration: line-through;
}

.oldprice::before, .oldprice::after {
    content: '';
    width: 100%;
    position: absolute;
    right: 0;
    top: 50%;
}
.oldprice::before {
    border-bottom: 2px solid #714949;
    -webkit-transform: skewY(-10deg);
    transform: skewY(-10deg);
}
.oldprice::after {
    border-bottom: 2px solid #714949;
    -webkit-transform: skewY(10deg);
    transform: skewY(10deg);
}
.corner-text-wrapper {
    -webkit-transform: rotate(45deg);
    -moz-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    -o-transform: rotate(45deg);
    transform: rotate(45deg);
    clip: rect(0px, 141.421px, 70.7107px, 0px);
    height: 141.421px;
    position: absolute;
    right: -8.7107px;
    top: -22.7107px;
    width: 141.421px;
    z-index: 1;
}
.corner-text {
    color: white;
    -webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -ms-transform: rotate(-45deg);
    -o-transform: rotate(-45deg);
    transform: rotate(-45deg);
    left: 20px;
    top: 20px;
    background: #8b5730;
    background: linear-gradient(#8b5730 0%, #cc7432 100%);
    box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
    display: block;
    height: 100px;
    position: absolute;
    width: 100px;
    z-index: 2;
}
.corner-text span {
    position: inherit;
    top: 5px;
    font-weight: 600;
    font-size: 14px;
    left: 40px;
    display: block;
    text-align: right;
}
.image {
    display: block;
    width: 100%;
    height: auto;
}
.overlay {
    position: relative;
    top: 10px;
    bottom: 0;
    left: 0;
    right: 2px;
    height: 300px;
    /*width:425px;  */
    margin-top: -300px;
    opacity: 0;
    transition: .3s ease;
    background-color: transparent;
    z-index: 4;
}
.overlay:hover {
    opacity: 1;
    cursor: pointer;
}
.icon {
    border-radius: 60px;
    box-shadow: 0px 0px 2px #888;
    padding: 5px;
    background: #fff;
    color: #000;
    font-size: 30px;
    position: absolute;
    top: 9%;
    left: 10%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    text-align: center;
}
.icon_sub {
    border-radius: 75%;
    box-shadow: 0px 0px 2px #888;
    padding: 6px 8px;
    background: #fff;
    color: #000;
    font-size: 22px;
    position: relative;
    top: 20px;
    left: 100px;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    text-align: center;
}
@media (min-width: 100px) and (max-width: 780px){
    .icon {
        top:50%;
        left: 50%;
    }
    .icon_sub {
        top: 125px;
        left: 0px;
    }
}
.fa-user:hover {
    color: #eee;
}
.lg-backdrop.in {
    opacity: 0.85;
    z-index: 9999;
}
.lg-outer{
    z-index: 99999;
    padding: 10px;
}
.hover_zm_icon{
}
.fixed-size.lg-outer .lg-inner {
    background-color: #FFF;
}
.fixed-size.lg-outer .lg-sub-html {
    position: absolute;
    text-align: left;
}
.fixed-size.lg-outer .lg-toolbar {
    background-color: transparent;
    height: 0;
}
.fixed-size.lg-outer .lg-toolbar .lg-icon {
    color: #939092;
}
.fixed-size.lg-outer .lg-img-wrap {
    padding: 12px;
}
</style>
<style>
/* these styles are for the demo, but are not required for the plugin */
.zoom {
    display:inline-block;
    position: relative;
}
/* magnifying glass icon */
.zoom:after {
    content:'';
    display:block;
    width:33px;
    height:33px;
    position:absolute;
    top:0;
    right:0;
    /*background:url(icon.png);*/
}
.zoom img {
    display: block;
}
.zoom img::selection { background-color: transparent; }
    /***
    Bootstrap Line Tabs by @keenthemes
    A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
    Licensed under MIT
    ***/
    /* Tabs panel */
    .tabbable-panel {
        border:1px solid #eee;
        padding: 10px;
    }
    /* Default mode */
    .tabbable-line > .nav-tabs {
        border: none;
        margin: 0px;
    }
    .tabbable-line > .nav-tabs > li {
        margin-right: 2px;
    }
    .tabbable-line > .nav-tabs > li > a {
        border: 0;
        margin-right: 0;
        color: #737373;
    }
    .tabbable-line > .nav-tabs > li > a > i {
        color: #a6a6a6;
    }
    .tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
        border-bottom: 4px solid #fbcdcf;
    }
    .tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
        border: 0;
        background: none !important;
        color: #333333;
    }
    .tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
        color: #a6a6a6;
    }
    .tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
        margin-top: 0px;
    }
    .tabbable-line > .nav-tabs > li.active {
        border-bottom: 4px solid #f3565d;
        position: relative;
    }
    .tabbable-line > .nav-tabs > li.active > a {
        border: 0;
        color: #333333;
    }
    .tabbable-line > .nav-tabs > li.active > a > i {
        color: #404040;
    }
    .tabbable-line > .tab-content {
        margin-top: -3px;
        background-color: #fff;
        border: 0;
        border-top: 1px solid #eee;
        padding: 15px 0;
    }
    .portlet .tabbable-line > .tab-content {
        padding-bottom: 0;
    }
    /* Below tabs mode */
    .tabbable-line.tabs-below > .nav-tabs > li {
        border-top: 4px solid transparent;
    }
    .tabbable-line.tabs-below > .nav-tabs > li > a {
        margin-top: 0;
    }
    .tabbable-line.tabs-below > .nav-tabs > li:hover {
        border-bottom: 0;
        border-top: 4px solid #fbcdcf;
    }
    .tabbable-line.tabs-below > .nav-tabs > li.active {
        margin-bottom: -2px;
        border-bottom: 0;
        border-top: 4px solid #f3565d;
    }
    .tabbable-line.tabs-below > .tab-content {
        margin-top: -10px;
        border-top: 0;
        border-bottom: 1px solid #eee;
        padding-bottom: 15px;
    }
    button.add-to-cart {
        background-color:#8b5730;
        color: #E5D4CD;
        border: 1px solid transparent;
        border-radius: 4px;
        font-size: 12px;
        font-weight: 600;
        padding: 6px 10px;
        margin: 3px 0 0 0;
    }
    button.add-to-cart i {
        font-size: 15px;
    }
    button.add-to-cart:hover {
        background-color:#8b5730;
        color: #E5D4CD;
    }
    button.view-cart {
        background-color: transparent;
        color: #8b5730;
        border: 1px solid #8b5730;
        border-radius: 4px;
        font-size: 12px;
        font-weight: 600;
        padding: 6px 10px;
        margin: 3px 0 0 0;
    }
    button.view-cart:hover {
        background-color: #fff;
        color: #5d391e;
        border: 1px solid #8b5730;
    }
    button.view-cart i {
        font-size: 15px;
    }
    img.product-details-sub-img{
        opacity: 0.5;
        margin-bottom: 15px;
        width: 100px;
    }
    img.product-details-sub-img:hover{
        opacity: 1.0;
    }
    .tovar_items_small {
        padding: 35px 0 20px 0;
    }
    .view-rate-div {
        margin-top: 30px;
        font-size: 15px;
        font-weight: 500;
    }
    .martop0 {
        margin-top: 0px;
    }
    .product_view_price {
        font-size: 19px;
        font-weight: 700;
        margin-top: 25px;
    }
    div.item-text-name-new {
        font-size: 15px;
        min-height: 50px;
        font-weight: 500;
    }
    div.item-text-price-new {
        font-size: 14px;
        min-height: 30px;
        font-weight: 600;
    }
    p.item-category-new {
        font-size: 15px;
        min-width: 100%;
    }
    p.item-category-new span {
        padding-right: 0px;
    }
    .img-details-main{
        width: 100%;
    }
    #imageGallery li:hover{
      cursor: pointer;
  }
  #imageGallery li{
      text-align: center;
  }

  /*review section*/
  .review-box-title{
    font-size: 15px;
    color: #428BCA;

}
.review-box-review{
    font-style: italic;
    margin-top: 5px;
    margin-bottom: 5px;
}
.review-box{
    margin-bottom: 10px;
    padding-bottom: 10px;
    border-bottom: 1px solid #ddd;
}
</style>
<style>
input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input[type=number]:focus::-webkit-inner-spin-button, input[type=number]:focus::-webkit-outer-spin-button, input[type=number]:hover::-webkit-inner-spin-button, input[type=number]:hover::-webkit-outer-spin-button {
    -webkit-appearance: inner-spin-button;
    margin: 0 2px 0 0;
}
input[type=number] {
    -moz-appearance: textfield;
}
input[type=number]:focus, input[type=number]:hover {
    -moz-appearance: number-input;
}
</style>
@stop
@section('content')
<!-- BREADCRUMBS -->
<section class="breadcrumb parallax margbot30"></section>
<!-- //BREADCRUMBS -->
<!-- TOVAR DETAILS -->
<section class="tovar_details padbot70">
    <!-- CONTAINER -->
    <div class="container" style="margin-top: 150px;">
        <!-- ROW -->
        <div class="row">
            <div class="col-md-5">
                <!-- If function needed -->
                @if ($current_product->on_sale=='on' & date('Y-m-d')<=$current_product->end_date & date('Y-m-d')>=$current_product->start_date )
                @include('includes.product-sale-tag')
                @endif


                <div class="item">
                    <div class="clearfix" style="max-width:474px;">

                      <ul id="imageGallery">
                          <li data-thumb="{{url(). '/core/storage/'. $current_product->cover_path . '/' . $current_product->cover_file}}" data-src="{{url(). '/core/storage/'. $current_product->cover_path . '/' . $current_product->cover_file}}">
                              <img alt="{{$current_product->product_name}}" src="{{url(). '/core/storage/'. $current_product->cover_path . '/' . $current_product->cover_file}}" />
                          </li>
                        @if (!empty($current_product->getImages))
                        @foreach ($current_product->getImages as $key => $value)
                        <li data-thumb="{{url(). '/core/storage/'. $value->path . '/' . $value->filename}}" data-src="{{url(). '/core/storage/'. $value->path . '/' . $value->filename}}">
                          <img src="{{url(). '/core/storage/'. $value->path . '/' . $value->filename}}" />
                      </li>
                      @endforeach
                      @endif
                  </ul>


            </div>
        </div>

    </div>
    <div class="col-md-5" style=" padding: 0px 10px 40px 10px;">
        <div class="tovar_view_title">{{$current_product->product_name}}</div>
        <!-- <div class="product_view_price">US ${{number_format($current_product->price,2)}}</div> -->
        <?php if ($current_product->on_sale=='on' & date('Y-m-d')<=$current_product->end_date & date('Y-m-d')>=$current_product->start_date ): ?>
        <span class="oldprice">US ${{number_format($current_product->price,2) }}</span>
        <span class="saleprice">US ${{number_format($current_product->sale_price,2) }}</span>
        <?php else: ?>
        <span class="orgprice">US ${{number_format($current_product->price,2) }}</span>
        <?php endif ?>
        <p class="text-justify"></p>
        <?php if (!empty(Cart::get('P-'.$current_product->id))): ?>
                <!--<div class="row">
                    <div class="col-md-3">
                        <input value="1" id="qty" type="number" min="1" class="numericOnly" />
                    </div>
                </div>-->
                <?php endif ?>
                <br>
                <input type="hidden" name="product_id" value="{{$current_product->id}}" id="product_id">
                <?php if (!empty(Cart::get('P-'.$current_product->id))): ?>
                <button onclick="removeFromCart({{$current_product->id}},{{1}})" class="btn add-to-cart">ADDED TO CART <i class="fa fa-check"></i></button>
                <a href="{{url('my-cart')}}"><button class="btn view-cart">View Cart</button></a>
                <?php else: ?>
                <button onclick="addtocart({{$current_product->id}})"  class="btn add-to-cart">Add to cart <i class="fa fa-cart-plus"></i></button>
                <a href="{{url('productload/all/all/1/min/hi')}}"><button class="btn view-cart">Other products</button></a>
                <?php endif ?>
                <div id="addCartErrors_{{$current_product->id}}" style="color: #f00 !important;"></div>
                <div class="view-rate-div">
                    <label class="pull-left rating-box-label">Included Categories:</label>
                    <div class="rating-box clearfix">
                        <?php
                        $cat='';
                        foreach ($categories as $key => $value) {
                           $cat.= '<a style="color : #8b5730;" href="'.url('productload/'.$value->category_id.'/all/1/min/hi').'">'.$value->getProductCategory->name.'</a> ,';
                       }

                       ?>
                       {!! rtrim($cat,',') !!}
                   </div>
               </div>
               <div class="view-rate-div">
                <input type="number" style="max-width : 100px;" name="pro_qty" min="1" value="1"  id="pro_qty">
                <div id="qtyCheckErrors" style="color: #f00 !important;"></div>
            </div>
            <div class="view-rate-div">
                <label class="pull-left rating-box-label">Avarage Rate:</label>
                <input  id="input-1" class="rating" name="rate" data-min="0" data-max="5" data-step="0.5" data-size="xs" data-show-clear="false" value="{{number_format($current_product->popularity/2,1)}}" readonly="">
            </div>
            <br style="max-height: 1px;" />

        </div>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="tabbable-panel">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs text-center">
                        <li class="active"><a href="#tab_default_1" data-toggle="tab">Description </a></li>
                        <li><a href="#tab_default_2" data-toggle="tab">Reviews</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_default_1">
                            <?php echo nl2br($current_product->description); ?>
                        </div>
                        <div class="tab-pane" id="tab_default_2">
                            <div class="review-cont">
                                @foreach($current_product->reviews as $review)
                                <div class="review-box">
                                    <div class="review-box-title">{{$review->name}}</div>
                                    <input  id="input-1" class="rating" name="rate" data-min="0" data-max="5" data-step="0.5" data-size="xs" data-show-clear="false" value="{{number_format($review->star_count/2,1)}}" readonly="">
                                    <div class="review-box-review">{{$review->review}}</div>
                                    {{-- Review: --}}
                                    <div class="review-box-date">{{date('Y-m-d',strtotime($review->created_at))}}</div>
                                </div> 
                                @endforeach
                               {{--  <div class="review-box">
                                    <div class="review-box-title">Rumesh Dananjaya</div>
                                    <input  id="input-1" class="rating" name="rate" data-min="0" data-max="5" data-step="0.5" data-size="xs" data-show-clear="false" value="{{number_format($current_product->popularity/2,1)}}" readonly="">
                                    <div class="review-box-review">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</div>
                                    Review: --}}
                                    {{-- <div class="review-box-date">2018-12-08</div>
                                </div>  --}}

                            </div>

                            <?php if ($user = Sentinel::check()): ?>





                            <form id="memberReview" action="{{url('product-detail/'.$current_product->id)}}" method="POST">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

<br>
<br>
                                <h2>Write a Review</h2>



                                <div class="clearfix">
                                    <div class="row">
                                        <div class="col-md-4"><input placeholder="Name*" type="text" name="name" id="name" value="{{$user->first_name}} {{$user->last_name}}"></div>
                                        <div class="col-md-4"><input placeholder="E-mail*" type="text" name="email" id="email" value="{{$user->email}}"></div>
                                        <div class="col-md-4"><input placeholder="Contact No*" type="text" name="contact_no" id="contact_no" value="{{$user->mobile}}"></div>
                                    </div>
                                    <div class="row"><div class="col-md-12">
                                        <textarea id="review-textarea" name="review" placeholder="Write a review*" style="margin-bottom: 0px;"></textarea>
                                    </div></div>
                                    <div class="row" style="margin-top: 20px;"><div class="col-md-12">
                                        <label class="pull-left rating-box-label" for="rate">Your Rate:</label>
                                        <input id="input-1" class="rating" name="rate" data-min="0" data-max="5" data-step="0.5" data-size="sm" data-show-clear="false">
                                    </div></div>
                                    <!-- <button  class="btn add-to-cart">Submit a review</button> -->
                                    <button type="submit" class="btn add-to-cart big pull-right" >Submit a review</button>
                                </div>
                            </form>

                            <?php else: ?>

                            <form id="userReview" action="{{url('product-detail/'.$current_product->id)}}" method="POST">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <h2>Write a Review</h2>
                                <div class="clearfix">
                                    <div class="row">
                                        <div class="col-md-4"><input placeholder="Name*" type="text" name="name" id="name"></div>
                                        <div class="col-md-4"><input placeholder="E-mail*" type="text" name="email" id="email"></div>
                                        <div class="col-md-4"><input placeholder="Contact No*" type="text" name="contact_no" id="contact_no"></div>
                                    </div>
                                    <div class="row"><div class="col-md-12">
                                        <textarea id="review-textarea" name="review" placeholder="Write a review*" style="margin-bottom: 0px;"></textarea>
                                    </div></div>
                                    <div class="row" style="margin-top: 20px;"><div class="col-md-12">
                                        <label class="pull-left rating-box-label" for="rate">Your Rate:</label>
                                        <input id="input-1" class="rating" name="rate" data-min="0" data-max="5" data-step="0.5" data-size="sm" data-show-clear="false">
                                    </div></div>
                                    <!-- <button  class="btn add-to-cart">Submit a review</button> -->
                                    <button type="submit" class="btn add-to-cart big pull-right" >Submit a review</button>
                                </div>
                            </form>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <br>
            <h2>Related Products</h2>
            <br>
            @foreach ($related_product as $key => $relatedPro)
                @if ($relatedPro->getProduct)
                    <div class="tovar_wrapper col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ss-12 padbot40">
                        <div class="tovar_item clearfix">
                            <a href="{{url().'/product-detail/'.$relatedPro->getProduct->id}}" >
                                @if ($relatedPro->getProduct->on_sale=='on' & date('Y-m-d')<=$relatedPro->getProduct->end_date & date('Y-m-d')>=$relatedPro->getProduct->start_date )
                                @include('includes.product-sale-tag')
                                @endif
                                <div class="tovar_img">
                                    <div class="tovar_img_wrapper">
                                        <img  src="{{url(). '/core/storage/'. $relatedPro->getProduct->cover_path . '/' . $relatedPro->getProduct->cover_file}}" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-12 text-center">
                                    <div class="row item-text-name-new" style="min-height : 20px; font-weight: 600;">
                                        {{substr($relatedPro->getProduct->product_name,0,55)}}
                                    </div>
                                    <div class="row item-text-price-new">
                                       <?php if ($relatedPro->getProduct->on_sale=='on' & date('Y-m-d')<=$relatedPro->getProduct->end_date & date('Y-m-d')>=$relatedPro->getProduct->start_date ): ?>
                                       <span class="oldprice">US ${{number_format($relatedPro->getProduct->price,2) }}</span>
                                       <span class="saleprice">US ${{number_format($relatedPro->getProduct->sale_price,2) }}</span>
                                       <?php else: ?>
                                       <span>US ${{number_format($relatedPro->getProduct->price,2) }}</span>
                                       <?php endif ?>
                                   </div>
                               </div>
                           </a>
                           <center>
                            <?php if (!empty(Cart::get('P-'.$relatedPro->getProduct->id))): ?>
                            <button class="btn add-to-cart">ADDED TO CART <i class="fa fa-check"></i></button>
                            <a href="{{url('my-cart')}}"><button class="btn view-cart">View Cart</button></a>
                            <?php else: ?>
                            <button onclick="addtocart({{$relatedPro->getProduct->id}})"  class="btn add-to-cart">Add to cart <i class="fa fa-cart-plus"></i></button>
                            <a href="{{url().'/product-detail/'.$relatedPro->getProduct->id}}"><button class="btn view-cart">View Item</button></a>
                            <?php endif ?>
                            <div id="addCartErrors_{{$relatedPro->getProduct->id}}" style="color: #f00 !important;"></div>
                        </center>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>
<!-- //ROW -->
</div>
<!-- //CONTAINER -->
</section>
<!-- //TOVAR DETAILS -->
@stop
@section('js')
<script src="{{asset('assets/back/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<!-- LightBox -->
<script src="{{asset('assets/front/lightbox/js/lightgallery-all.min.js')}}"></script>
<script src="{{asset('assets/front/lightbox/js/lightslider.js')}}"></script>
{{-- <script src="{{asset('assets/front/lightbox/js/lightgallery-all.min.js')}}"></script> --}}

<script type="text/javascript">
    $(document).ready(function() {
      $('#imageGallery').lightSlider({
          gallery:true,
          item:1,
          loop:true,
          thumbItem:9,
          slideMargin:0,
          enableDrag: false,
          thumbItem:5,
          currentPagerPosition:'left',
          onSliderLoad: function(el) {
              el.lightGallery({
                  selector: '#imageGallery .lslide'
              });
          }
      });
  });

</script>

<script type="text/javascript">
    $("#pro_qty").on('change', function(){
        $('#qtyCheckErrors').html('')
        $('#addCartErrors_{{$current_product->id}}').html('')
        $.ajax({
            method: "POST",
            url: '{{url('check/pro_qty')}}',
            data:{
                '_token' : '{{csrf_token()}}',
                'product_id' : {{$current_product->id}} ,
                'qty' : $(this).val()
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown){
            if(jqXHR.responseJSON.errors){
                mes = '<ul style="padding-left : 0;">';
                $.each(jqXHR.responseJSON.errors, function(index, value){
                    mes += '<li>'+value+'</li>';
                });
                mes += '</ul>';

                $('#qtyCheckErrors').html(mes)
            }
        });
    });

    $("#input-1").rating({min:0, max:5, step:0.5,stars:5, size:'lg'});
    function addtocart(product_id){
       $('#addCartErrors_'+product_id).html('')
       $('#qtyCheckErrors').html('')
       var qty = $("#pro_qty").val() || 1;
       $.ajax({
        method: "GET",
        url: '{{url('cart/add')}}',
        data:{ 'product_id' : product_id, 'qty': qty, 'product_type':1 }
    })
       .done(function(msg) {
        location.reload();
    })
       .fail(function (jqXHR, textStatus, errorThrown){
        if(jqXHR.responseJSON.errors){
            mes = '<ul>';
            $.each(jqXHR.responseJSON.errors, function(index, value){
                mes += '<li>'+value+'</li>';
            });
            mes += '</ul>';

            $('#addCartErrors_'+product_id).html(mes)
        }
    });
   }

    //zoom function
    $(document).ready(function(){
        $('.zoom').zoom();

        $(".numericOnly").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
             (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
             (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
             (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
             (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
             return;
         }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
        $("#content-slider").lightSlider({
            loop:true,
            keyPress:true
        });



            // $('#image-gallery').lightSlider({
            //     gallery:true,
            //     item:1,
            //     width: '700px',
            //     height: '700px',
            //     thumbItem:5,
            //     slideMargin:0,
            //     speed:1500,
            //     enableDrag: false,
            //     currentPagerPosition:'left',
            //     // auto:true,
            //     loop:true,
            //     onSliderLoad: function() {
            //         $('#image-gallery').removeClass('cS-hidden');
            //     }
            //
            // });
             // $('#image-gallery').lightGallery({
             //    width: '700px',
             //    height: '700px',
             //    mode: 'lg-fade',
             //    addClass: 'fixed-size',
             //    counter: false,
             //    download: false,
             //    startClass: '',
             //    enableSwipe: false,
             //    enableDrag: false,
             //    speed: 1500,
             //    closable:false
             //    });

             $('#userReview').validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    name : {
                        required: true
                    },
                    contact_no : {
                        required: true
                    },
                    review : {
                        required : true
                    },
                    rate : {
                        required : true
                    }
                },
                sumbitHandler: function(form){
                    form.submit();
                }
            });

         });

    //show selected image
    function viewImg(img){
     var imgPath=$(img).attr('src');
     $('#product_img').attr('src',imgPath);
     $('.zoom').find('img').attr('src',imgPath);
 }
 function removeFromCart(id, type) {
    $.ajax({method: "GET",
        url:'{{url('cart/remove')}}/'+id+'/'+type})
    .done(function(msg) {
        window.location.href='{{url('my-cart')}}';
    });
}

</script>



<!-- <script type="text/javascript">
    $('.fixed-size_ID').lightGallery({
    width: '700px',
    height: '700px',
    mode: 'lg-fade',
    addClass: 'fixed-size',
    counter: false,
    download: false,
    startClass: '',
    enableSwipe: false,
    enableDrag: false,
    speed: 500
    });
</script> -->
<!-- <script type="text/javascript">
    $('.fixed-size_ID').lightGallery();
</script> -->


@stop
