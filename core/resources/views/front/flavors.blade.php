@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')

@section('css')

<style type="text/css">
/* .fix_silder4 {
  position: relative;
  left: 550px;
  -webkit-animation: slideIn 2s forwards;
  -moz-animation: slideIn 2s forwards;
  animation: slideIn 2s forwards;
}
@-webkit-keyframes slideIn {
  0% {
    transform: translateX(-900px);
  }
  100% {
    transform: translateX(0);
  }
}
@-moz-keyframes slideIn {
  0% {
    transform: translateX(-900px);
  }
  100% {
    transform: translateX(0);
  }
}
@keyframes slideIn {
  0% {
    transform: translateX(-900px);
  }
  100% {
    transform: translateX(0);
  }
} */
    .flavors_details{
    margin-bottom: 100px;
    margin-top: 100px;
    padding-bottom: 50px;
    }
    .fix_silder4 h1 {
      font-size: 30px;
      font-family: 'Montserrat', sans-serif;
    }
    .accordion_title {
    font-weight: 600;
    text-indent: 2px;
    line-height: 30px;
    text-align: center;
    font-size: 20px;
    color: #8b5730;
    }
    .accordion_title.with-margin{
      margin-top: 60px;
    }
    .col-md-3{
    margin-top: 10px;
    color: #3f3f3f;
    font-weight: 700;
    font-size: 15px;
    padding: 5px;
    text-transform: uppercase;
    }
    .alert-warning{
    background-color: #fceeeb!important;
    background-image: linear-gradient(to bottom,#E5D4CD 0,#f7d0c8 100%) !important;
    /* border-color: #8b5730!important; */
    color: #666!important;
    margin-bottom: 60px;
    }
    .alert:after{
    content:"";
    position:absolute;
    left:15px;
    top:5px;
    bottom:5px;
    height:30px;
    width:1020px;
    border: 1px ridge #8b5730;
}
</style>

@stop

@section('content')
<section class="breadcrumb men parallax margbot30">
</section>
<!-- //BREADCRUMBS -->
<!-- PAGE HEADER -->
<hr class="banner-top">
        <div class="banner-bg center">
           <h3>Flavors</h3>
            <p>MADE USING THE FRESHEST AND FINEST INGREDIENTS</p>
        </div>
        <hr class="banner-bottom">
<section class="page_header">
    <!-- CONTAINER -->
    <div class="container">

    </div>
    <!-- //CONTAINER -->
</section>
<!-- //PAGE HEADER -->
<!-- FAQ PAGE -->
<section class="faq_page">
    <!-- CONTAINER -->
    <div class="" style="width: 100%;">


        @foreach ($flavors as $el)
        <section class="fix_silder_img4" style="background-image:url({{url('core/storage/uploads/images/flavors/'.$el->img)}});">
            <div class="fix_silder4" id="slide">
                <h1>{{$el->flavor_name}}</h1>
                <h4><?php echo $el->description; ?></h4>
            </div>
        </section>
        <section class="flavors_details">
          <div class="container">
              @if (isset($el->basicTypes) && $el->basicTypes->count() > 0)
                  <h4 class="accordion_title text-center">BASIC FLAVORS</h4>
                    <div class="row">
                        @foreach ($el->basicTypes as $item)
                            <div class="col-md-3">{{$item->name}}</div>
                        @endforeach
                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
              @endif

              @if (isset($el->premiumTypes) && $el->premiumTypes->count() > 0)
                  <h4 class="accordion_title text-center">PREMIUM FLAVORS</h4>
                    <div class="row">
                        @foreach ($el->premiumTypes as $item)
                            <div class="col-md-3">{{$item->name}}</div>
                        @endforeach
                    </div>
              @endif
           
          </div>
        </section>
            
        @endforeach

        {{--  <section class="fix_silder_img4">
            <div class="fix_silder4 text-center">
                <h1>CAKE FLAVORS</h1>
                <h4>A small description about the cake flavors that we offer</h4>
            </div>
        </section>
        <section class="flavors_details">
          <div class="">
            <h4 class="accordion_title text-center">BASIC FLAVORS</h4>
            <div class="row">
                <div class="col-md-3">Vanilla Bean</div>
                <div class="col-md-3">Chocolate</div>
                <div class="col-md-3">Confetti Vanilla</div>
                <div class="col-md-3">Lemon</div>
                <div class="col-md-3">Red Velvet</div>
            </div>
            <h4 class="accordion_title with-margin text-center">PREMIUM FLAVORS</h4>
            <div class="row">
                <div class="col-md-3">Almond</div>
                <div class="col-md-3">Pumpkin Spice</div>
                <div class="col-md-3">Pineapple Supreme</div>
                <div class="col-md-3">Lemon Berry</div>
                <div class="col-md-3">Carrot</div>
                <div class="col-md-3">Strawberry</div>
                <div class="col-md-3">Coconut</div>
            </div>
          </div>
        </section>  --}}
        {{--  <section class="fix_silder_img4">
            <div class="fix_silder4 text-center">
                <h1>BUTTERCREAM FLAVORS</h1>
                <h4>A small description about the BUTTERCREAM FLAVORS that we offer</h4>
            </div>
        </section>
        <section class="flavors_details">
          <div class="container">
            <h4 class="accordion_title text-center">BASIC FLAVORS</h4>
            <div class="row">
                <div class="col-md-3">Vanilla</div>
                <div class="col-md-3">Chocolate</div>
                <div class="col-md-3">Chocolate Mocha</div>
                <div class="col-md-3">Almond</div>
                <div class="col-md-3">Cream Cheese</div>
                <div class="col-md-3">Lemon</div>
            </div>
            <h4 class="accordion_title with-margin text-center">PREMIUM FLAVORS</h4>
            <div class="row">
                <div class="col-md-3">Red Raspberry</div>
                <div class="col-md-3">Salted Caramel</div>
                <div class="col-md-3">Wild Strawberry</div>
                <div class="col-md-3">Maple Pecan</div>
                <div class="col-md-3">Chocolate Ganache</div>
                <div class="col-md-3">Apricot</div>
                <div class="col-md-3">Triple Berry</div>
                <div class="col-md-3">Coconut Rum</div>
                <div class="col-md-3">Passion Fruit</div>
                <div class="col-md-3">Black Cherry</div>
                <div class="col-md-3">White Chocolate</div>
                <div class="col-md-3">Pineapple</div>
            </div>
          </div>
        </section>  --}}
        {{--  <section class="fix_silder_img4" >
            <div class="fix_silder4">
                <h1>SPECIALTY FILLINGS</h1>
                <h4>A small description about the SPECIALTY FILLINGS that we offer</h4>
            </div>
        </section>
        <section class="flavors_details text-center">
          <div class="container">
            <div class="row">
                <div class="col-md-3">Raspberry *</div>
                <div class="col-md-3">Black Cherry *</div>
                <div class="col-md-3">Lemon Curd</div>
                <div class="col-md-3">Strawberry *</div>
                <div class="col-md-3">Pineapple *</div>
                <div class="col-md-3">Chocolate Ganache</div>
            </div>
          </div>
        </section>  --}}
        </div>
        <div class="container">
          <!-- ROW -->
          <div class="row">
              <div class="alert alert-warning" style="margin-top: 15px;">
                  <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
                  <span style="float: left;margin-top:-2px;padding-right: 10px;font-size: 25px;"><i class="fa fa-info-circle"></i></span>
                 <?php echo $msg->description;?>
                  {{--  Please note that our products may contain and/or come into contact with nuts, dairy, and/or soy products.  --}}
              </div>
          </div>
          <!-- //ROW -->
      </div>
    <!-- //CONTAINER -->
</section>
<!-- //FAQ PAGE -->
@stop

@section('js')

@stop
