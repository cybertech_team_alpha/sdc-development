@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')
  <style media="screen" type="text/css">
    .sidemenu {        
        padding: 5px 5px 15px 5px;
        border: 2px solid #e9e9e9;        
        margin-bottom: 5px;        
    }
    .sidemenu-title{
        margin-left: 5%;
        margin-top: 5%;
        margin-bottom :inherit;
        padding: 10px;
        max-width: 92%;
        background-color: #fceeeb;
        text-align: center;
        font-weight: 600;
        font-size: 0.9em;
        border: 2px solid #8b5730;        
    }
    .blogs{
        position: relative;
        margin: 2px 10px 2px 10px;
        padding: 5px;
        border: thin solid #e9e9e9;        
    }
    .tags{
        position: relative;
        margin: 2px 10px 2px 0px;
        padding: 5px;        
    }
    .blogs:hover{
        box-shadow: 0px 2px 2px grey;
    }
    .blog-title{
        color:black;
        font-weight: 600;
        font-size: 0.9em;
        overflow:hidden;
    }
    .tagsurl:hover{
        cursor: pointer;
        text-decoration: underline;
    }
    .tagsurl{
        margin-left:5%;
    }
    .query{
       border:black solid 2px !important; 
    }

    input[type="text"].query::placeholder{ /* Chrome/Opera/Safari */
        color:#E5D4CD !important;
    }

    input[type="text"].query::-webkit-input-placeholder{ /* Chrome/Opera/Safari */
        color:#E5D4CD !important;
    }

    input[type="text"].query::-moz-placeholder{ /* Firefox 19+ */
        color:#E5D4CD !important;
    }

    input[type="text"].query:-ms-input-placeholder{ /* IE 10+ */
        color:#E5D4CD !important;
    }

    input[type="text"].query:-moz-placeholder{ /* Firefox 18- */
        color:#E5D4CD !important;
    } 

    /*.blog_post_content p::first-letter{
      -webkit-initial-letter: 8;
        initial-letter: 8;        
        font-weight: bold;
        margin-right: .20em;
        font-size:350%;
        float: left;
        position: relative;                
    }*/
    .additional_image{
      margin:4px;
    }

    .owl-prev, .owl-next {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        display: block !important;
        border:0px solid black;        
    }
    .owl-theme .owl-nav [class*="owl-"]{
      background:0;
      margin-top:5%;
    }

    .owl-theme .owl-nav [class*="owl-"]:hover{
      background:0;
    }
    .owl-prev { left: -20px; }
    .owl-next { right: -20px; }
    .owl-prev i, .owl-next i {transform : scale(4,6); color: #ccc;}

    .pagination_area{
      border-bottom: 3px solid #827f7f;
      padding-bottom:5%;
      padding-top:2%;
      margin-bottom:5%;
    }

    .pagination_area li a.previous_a{
      text-align:left;
    }
    .pagination_area li a.next_a{
      text-align:right;
    }
    .fa.fa-facebook{
        color: #FFFFF;

    }
    .fa.fa-youtube-square{
        color: #FFFFF;

    }
    .fa.fa-pinterest{
        color: #FFFFF;

    }
    .fa.fa-twitter{
        color: #FFFFF;

    }
    .instagram{
     overflow: hidden;
     text-align: center;
    }
    .instagram > span{
    position: relative;
    display: inline-block;
    font-weight: 540;
    font-size: 14px;
    }
    .instagram > span:before, .instagram > span:after{
    content: '';
    position: absolute;
    top: 50%;
    border-bottom: 2px solid;
    width: 591px; /* half of limiter*/
    margin: 0 20px;
    }
    .instagram > span:before{
    right: 100%;
    }
    .instagram > span:after{
    left: 100%;
    }
    .sidemenuInstagramImg{
         /*height:300px;*/
         padding: 2px 2px 2px 2px;
         border: 0px solid white;
         /*border-radius: 5px;*/
         margin-bottom: 3px;
         /*box-shadow: 0px -1px 5px grey;*/
     }
     .sidemenuInstagramImg .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
    }
    .sidemenuInstagramImg img {
        display: block;
        position: relative;
        -webkit-transition: all 0.4s ease-in;
        transition: all 0.4s ease-in;
    }
    .sidemenuInstagramImg:hover img {
        filter: url('data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feColorMatrix type="matrix" color-interpolation-filters="sRGB" values="0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0 0 0 1 0" /><feGaussianBlur stdDeviation="3" /></filter></svg>#filter');
        filter: grayscale(1) blur(3px);
        -webkit-filter: grayscale(1) blur(3px);
        -webkit-transform: scale(1.2);
        -ms-transform: scale(1.2);
        transform: scale(1.2);
    }
    .sidemenuInstagramImg i {
        text-transform: uppercase;
        text-align: center;
        position: relative;
        font-size: 17px;
        padding: 10px;
        background: transparent;
    }
    .sidemenuInstagramImg i {
        -webkit-transform: scale(0.7);
        -ms-transform: scale(0.7);
        transform: scale(0.7);
        -webkit-transition: all 0.4s ease-in;
        transition: all 0.4s ease-in;
        opacity: 0;
        filter: alpha(opacity=0);
        color: #fff;
        text-transform: uppercase;
    }
    .sidemenuInstagramImg:hover i {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: scale(1);
        -ms-transform: scale(1);
         transform: scale(1);
    }

    .pagination_area li a{
      font-weight:bold;
    }

    .pagination_area .disabled {
      pointer-events:none; //This makes it not clickable         
    }
    .no-drop{ 
      cursor: no-drop;       
    }

    #comments{
      margin-top:15%;
    }

    #commentBtn{
      
      #FCEEEB
      background-color:#8b5730 !important;
      border:#8b5730 solid 2px !important;
    }

    .post .post_title{
      background-color: #FCEEEB;
      padding-top:18px; 
      /* padding-bottom: 20px; */
      padding-left:18px;
      /* height: 80px; */
      margin-right: 200px;
      /* top: 50%; */
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
    } 
    
    .insta-preview{
        margin-top: 10% !important;
        margin-bottom: 2%;
    }

    .social_div a{
        width: 16%;
        height: 32px !important;
    }

    .social_div a i{
        line-height: 30px !important;
        font-size: 18px !important;
    }
    .h2 > span{
    position: relative;
    display: inline-block;
    }
    .h2 > span:after{
    content: '';
    position: absolute;
    top: 50%;
    border-bottom: 2px solid;
    width: 322px; /* half of limiter*/
    margin: 0 20px;
    color: lightgray;
    }
    .h2 > span:before{
    content: '';
    position: absolute;
    top: 50%;
    border-bottom: 2px solid;
    width: 280px; /* half of limiter*/
    margin: 0 20px;
    color: lightgray;

    }
    .h2 > span:before{
    right: 85%;
    }
    .h2 > span:after{
    left: 120%;
    }
  </style>

  <script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
@stop


@section('content')

    <!-- BREADCRUMBS -->
    <section class="breadcrumb parallax margbot30"></section>
    <!-- //BREADCRUMBS -->

    <!-- PAGE HEADER -->
    <section class="page_header">
        <hr class="banner-top"/>
        <div class="banner-bg center">
            <h3>Blog</h3>
            <p>GET INFORMED ABOUT OUR LATEST NEWS AND EVENTS</p>
        </div>

      <!-- CONTAINER -->
      <div class="container">
        <h3 class="pull-left">&nbsp;</h3>
      </div>
      <!-- //CONTAINER -->
    </section>
    <!-- //PAGE HEADER -->


     <!-- BLOG BLOCK -->
    <section class="blog">
      <!-- CONTAINER -->
      <div class="container">
        <!-- ROW -->
        <div class="row">

          <!-- BLOG LIST -->
          <div class="col-lg-9 col-md-9 col-sm-9 padbot30 blog_block">

            <article class="post blog_post clearfix margbot20" data-appear-top-offset='-100' data-animated='fadeInUp'>
              
              
              <div class="post_large_image col-md-12 col-md-12" style="padding-bottom:10px;">
                
                  <div class="post_title" href="blog-post.html" style="font-family: 'Passion_sans';"><div class="recent_post_date">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('d')}}<span>{{substr(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('F'),0,3)}}</span></div><div style="margin-left: 80px;">{{$blog->name}}</div></div>
                  <a href="https://www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"></a>
                <center>
                  <img id="top_img" class="blog-img-border" src="{{asset('core/storage/uploads/images/blog/'.$blog->cover_image)}}" alt="" />                  
                </center>
              </div>

              <div class="blog_post_content">                
                <div class="blog_description col-md-12 col-md-12 text-justify" id="blog_description">
                    @if(count($blog->getImages) == 0)
                    <div style="float:left;width:200px; height:200px; object-fit: cover; overflow: hidden; margin-right: 15px; background-color: #FCEEEB;"><img id="top_img" class="blog-img-border" src="{{asset('core/storage/uploads/images/blog/'.$blog->front_image)}}" alt="" style="max-width: 200px; max-height: 200px;" /></div>
                    {!!$blog->description!!}
                @else
                    <div style="float:left;width:200px; height:auto; object-fit: cover; overflow: hidden; margin-right: 15px;"><img class="blog-img-border" src="{{asset($blog->getImages->first()->path.'/'.$blog->getImages->first()->filename)}}" alt="" /></div>
                    {!!$blog->description!!}
                    @endif
                    {{-- <img id="top_img" class="blog-img-border" src="{{asset('core/storage/uploads/images/blog/'.$blog->front_image)}}" alt="" />  --}}
                  
                    {{-- {!! $blog->description !!} --}}
                  
                </div>
                <div class="col-md-12 col-sm-12 wow" data-wow-duration="1s">
                  <div id="additional_images" class="owl-carousel owl-theme">
                    @foreach($blog->getImages as $index=>$image)
                      <div class="additional_image">                      
                          <div> <img src='{{ url('').'/core/storage/uploads/images/blog/'.$image->filename }}' alt="additional_image-{{ $index }}" class="img-responsive"></div>
                      </div>
                    @endforeach
                  </div> 
                </div>
                @if(count($blog->getImages) > 0)
                <div class="main col-md-12 col-md-12" style="display: flex; justify-content: center;"><img  src="{{asset($blog->getImages->last()->path.'/'.$blog->getImages->last()->filename)}}" style="height:auto; width: auto; min-width: 100;" alt="" /></div>
                @endif
              </div>
            </article> 

            {{-- <iframe src="" width="100%" height=""></iframe> --}}
            @if (isset(explode('v=',$blog->video_url)[1]))
             <div class="vedio col-md-12 col-md-12"><iframe width="100%" height="490" src="https://www.youtube.com/embed/{{ explode('v=',$blog->video_url)[1] }}" frameborder="0" allowfullscreen></iframe></div>
            @endif

            <!-- <div class="shared_tags_block clearfix" data-appear-top-offset='-100' data-animated='fadeInUp'>
              <div class="pull-left post_tags"></div>

              <div class="pull-right tovar_shared clearfix">
                <p>Share item with friends</p>
                <ul>
                  <li>
                    <a class="facebook" target="_blank" href="https://www.facebook.com/sharer.php?u={{ urlencode(url('blog/'.$blog->id)) }}&t={{ urlencode($blog->name) }}" ><i class="fa fa-facebook"></i></a>
                  </li>
                  <li>
                    <a class="twitter" target="_blank" href="http://twitter.com/intent/tweet?status={{ urlencode($blog->name) }}+{{ urlencode(url('blog/'.$blog->id)) }}" ><i class="fa fa-twitter"></i></a>
                  </li>
                  <li>
                    <a class="linkedin" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url={{ urlencode(url('blog/'.$blog->id)) }}&title={{ urlencode($blog->name) }}&source={{ urlencode(url('/')) }}" ><i class="fa fa-linkedin"></i></a>
                  </li>
                  <li>
                    <a class="google-plus" target="_blank" href="https://plus.google.com/share?url={{ urlencode(url('blog/'.$blog->id)) }}" ><i class="fa fa-google-plus"></i></a>
                  </li>
                  <li>
                    <a class="tumblr" target="_blank" href="http://www.tumblr.com/share?v=3&u={{ urlencode(url('blog/'.$blog->id)) }}&t={{ urlencode($blog->name) }}" ><i class="fa fa-tumblr"></i></a>
                  </li>
                </ul>
              </div>              
            </div> -->

            <div class="col-lg-12 col-md-12 col-sm-12 pagination_area" data-appear-top-offset='-100' data-animated='fadeInUp'>
                <ul>
                  <?php
                    $blog_id=$blog->id;
                    $blog_list_previous=BlogManage\Models\Blog::where('id','<',$blog->id)->get();
                    $blog_list_next=BlogManage\Models\Blog::where('id','>',$blog->id)->get();
                    $disable_previous='';
                    $disable_next='';
                    $cursor="";
                    if($blog_list_previous->count()==0){
                      $disable_previous='disabled';
                      $cursor="no-drop";
                    }
                    if($blog_list_next->count()==0){
                      $disable_next='disabled';
                      $cursor="no-drop";
                    }

                    $previous_blog=BlogManage\Models\Blog::where('id','<',$blog->id)->where('status',1)->latest()->first();
                    $next_blog=BlogManage\Models\Blog::where('id','>',$blog->id)->where('status',1)->oldest()->first();

                    $previous_blog_id=0;
                    $next_blog_id=0;

                    if($previous_blog!=null){
                      $previous_blog_id=$previous_blog->id;
                    }
                    
                    if($next_blog!=null){
                      $next_blog_id=$next_blog->id;
                    }                                        
                    
                  ?>
                  <li class="<?php echo $disable_previous; echo " "; echo $cursor;?>">
                    <a href="{{ url('blog/'.$previous_blog_id) }}" class="col-lg-6 col-md-6 previous_a"><<  PREVIOUS POST</a>
                  </li>
                  <li class="<?php echo $disable_next; echo " "; echo $cursor; ?>">
                    <a href="{{ url('blog/'.$next_blog_id) }}" class="col-lg-6 col-md-6 next_a">NEXT POST  >></a>
                  </li>
                </ul>
            </div>
                

            <!-- COMMENTS -->
            <div id="comments" data-appear-top-offset='-100' data-animated='fadeInUp' >
              <h2 class="h2" style="display: inline-block; position: relative; margin-left:280px;"><span>Comments</span> ({{ $blog->comments->count()}})</h2>
              <ol style="list-style:none">
                @foreach ($blog->comments as $element)
                  <li>
                    {{-- <div class="pull-left avatar"><a href="javascript:void(0);"><img src="{{asset('assets/front/images/avatar1.jpg')}}" alt="" /></a></div> --}}
                    <div class="comment_right">
                      <div class="comment_info">
                        <a class="comment_author" href="javascript:void(0);" >{{ $element->name }}</a>
                        {{-- <a class="comment_reply" href="javascript:void(0);" ><i class="fa fa-share"></i> Reply</a> --}}
                        <div class="clear"></div>
                        <div class="comment_date">{{ $element->created_at->format('d M Y') }}</div>
                      </div>
                      {{$element->content  }}
                    </div>
                    <hr>

                    <div class="clear"></div>

                  </li>
                @endforeach
              </ol>
            </div>
            <!-- //COMMENTS -->

            <!-- LEAVE A COMMENT -->
            <div id="comment_form" data-appear-top-offset='-100' data-animated='fadeInUp'>
              <h2>Leave a Comment</h2>
              <div class="alert alert-danger" id="commentErrors" hidden>
                <ul></ul>
              </div>
              <div class="comment_form_wrapper">
                <form action="javascript:void(0);" method="post" id="commentForm">
                  {!! csrf_field(); !!}
                  <input type="hidden" name="blog_id" value="{{ $blog->id }}">
                  <input type="text" name="name" placeholder="Full Name *"/ class="col-lg-6 col-md-6">
                  <input class="email col-lg-6 col-md-6" type="text" name="email" placeholder="Email Address*" /></br>
                  <textarea name="content" placeholder="Your comment *"></textarea>
                  <div class="clear"></div>
                  <span class="comment_note">Your email address will not be published. Required fields are marked *</span>
                  <input style="background-color:lightgray;border-color: white;color: white" type="submit" value="Post Comment" id="commentBtn"/>
                  <div class="clear"></div>
                  <div class="g-recaptcha" data-sitekey="6LeRREcUAAAAAOn6ULfVo70a2-W_g9Lqxn7TlcsH"></div>
                </form>
              </div>
            </div>
            <!-- //LEAVE A COMMENT -->

            {{-- @foreach($latest_blogs as $blog)
              <article class="post margbot40 clearfix" data-appear-top-offset='-100' data-animated='fadeInUp'>
                <a class="post_image pull-left" href="{{url('blog/'.$blog->id)}}" >
                  <div class="recent_post_date">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('d')}}<span>{{substr(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('F'),0,3)}}</span></div>
                  @if(count($blog->getImages) == 0)
                      <img class="blog-img-border" src="{{asset('assets/front/images/blog/2.jpg')}}" alt="" />
                  @else
                      <img class="blog-img-border" src="{{asset($blog->getImages->first()->path.'/'.$blog->getImages->first()->filename)}}" alt="" />
                  @endif
                </a>
                <a class="post_title" href="{{url('blog/'.$blog->id)}}" >{{$blog->name}}</a>
                <div class="post_content">{!!substr(strip_tags($blog->description),0,800)!!}</div>
              </article>
            @endforeach --}}

          </div>
          <!-- //BLOG LIST -->
          
          <section class="col-lg-3 col-md-3 col-sm-3">
            <div class="container-fluid">
                    <div class="row">
                        <!-- WIDGET SEARCH -->
                        <div class="widget_search">
                             <input type="text" style="border: 2px solid #4d4b4c; color: #FCEEEB;" class="query" name="search" value="{{app('request')->input('qu')}}" id="qu" placeholder="SEARCH"/>
                        </div>
                        <!-- //WIDGET SEARCH -->
                    </div>

                    <div class="row" style="box-shadow: 0 0 5px black;" >
                    <div class="col-sm-12  sidemenu recent_blogs">
                        <div class="sidemenu-title" style="border-width: thin; border: 2px solid #8b5730;"><div class="side" style="border-width: thin; border: 1px solid #8b5730;">RECENT POSTS</div></div>
                        @foreach($recent_blogs as $r)
                            <a href="{{url('blog/'.$r->id)}}">
                                <div class="col-sm-11 blogs" style="box-shadow: black;  margin-bottom: 10px;border-bottom: 2px solid #DCDCDC; border-top: 2px solid white; border-left: 2px solid white;border-right: 2px solid white;">
                                    <div class="col-sm-5">
                                        <img src="{{url('core/storage/uploads/images/blog/'.$r->front_image)}}" alt="" style="height: 40px;width: 40px;">
                                    </div>
                                    <div class="col-sm-7 blog-title" style="padding-left: 0;">
                                        {{$r->name}}<br>
                                        <div style="color: #e0e0e0">{{ date('F d, y',strtotime($r->start_date))}}</div>
                                    </div>
                                    
                                </div>
                            </a>

                        @endforeach
                    </div>
                </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 sidemenu categories" style="box-shadow: 0 0 5px black;">
                            <div class="sidemenu-title"  style="border-width: thin; border: 2px solid #8b5730;"><div class="side" style="border-width: thin; border: 1px solid #8b5730;">CATEGORIES</div></div>
                            @foreach($category_list as $key => $value)
                                <a href="{{ url('productload/'.$value->id.'/all/1/min/hi') }}">
                                    <div class="col-lg-11 col-md-11 col-sm-11 blogs">
                                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0;">
                                            {{$value->name}}<br>                                            
                                        </div>
                                    </div>                                        
                                </a>                                   
                            @endforeach
                        </div>
                    </div>

                    <div class="row" style="box-shadow: 0 0 5px black;" >
                    <div class=" col-sm-12 sidemenu featured_posts">
                        <div class="sidemenu-title" style="border-width: thin; border: 2px solid #8b5730;">
                            <div class="side" style="border-width: thin; border: 1px solid #8b5730;">
                            FEATURED POSTS
                            </div>
                        </div>
                        @foreach($featured_blogs as $r)
                            <a href="{{url('blog/'.$r->id)}}">
                                <div class="col-sm-11 blogs">
                                    <div class="col-sm-5" >
                                        <img src="{{url('core/storage/uploads/images/blog/'.$r->front_image)}}" alt="" style="height: 40px;width: 40px;">
                                    </div>
                                    <div class="col-sm-7 blog-title" style="padding-left: 0;">
                                        {{$r->name}}<br>
                                        <div style="color: #e0e0e0">{{$r->start_date}}</div>
                                    </div>
                                </div>
                            </a>

                        @endforeach
                    </div>
                </div>

                    <div class="row" style="box-shadow: 0 0 5px black;" >
                    <div class="instagram">
                        <span>
                        {{-- <div class="sidemenu-title"  style="border-width: thin; border: 2px solid #8b5730;"> --}}
                            {{-- <div class="side" style="border-width: thin; border: 1px solid #8b5730;"> --}}
                            INSTAGRAM
                            </span>
                            {{-- </div> --}}
                        {{-- </div> --}}
                        
                    </div >
                    <div id="instagram" class="instagramImg">

                    </div>
                </div>
                    <div class="row" style="box-shadow: 0 0 5px black;" >
                    <div class="sidemenu">
                        <div class="sidemenu-title" style="border-width: thin; border: 2px solid #8b5730;">
                            <div class="side" style="border-width: thin; border: 1px solid #8b5730;">
                            TAGS
                            </div>
                        </div>
                        <div class="tags">
                            @foreach($tags as $tag)
                                <a class="tagsurl" href="{{url('blog/searchByTag/'.$tag)}}">{{$tag}}</a>
                            @endforeach
                        </div>
                    </div>
                </div><br>

                    <div class="row">
                        <div class="">

                            <div class="social-icon">

                                <div class="social social_div center"> 

                                    <a href="{{ ConfigManage\Models\Config::find(1)->twitter }}" style="margin: 0 5px 10px 0 !important;" target="_blank"  data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>

                                    <a href="{{ ConfigManage\Models\Config::find(1)->fb }}" style="margin: 0 5px 10px 0 !important;" target="_blank" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>

                                    <a href="{{ ConfigManage\Models\Config::find(1)->youtube }}" style="margin: 0 5px 10px 0 !important;" target="_blank" data-placement="bottom" title="Youtube"><i class="fa fa-youtube-square"></i></a>

                                    <a href="{{ ConfigManage\Models\Config::find(1)->pinterest }}" style="margin: 0 5px 10px 0 !important;" target="_blank" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>

                                    <a href="{{ ConfigManage\Models\Config::find(1)->instagram }}" style="margin: 0 5px 10px 0 !important;" target="_blank" data-placement="bottom" title="Instagram"><i class="fa fa-instagram" style="color: #FFFFF;"></i></a>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
    <!-- //ROW -->
</div>
<!-- //CONTAINER -->
</section><!-- //BLOG BLOCK -->

@stop

@section('js')
<script type="text/javascript">

$(window).load(function(){  
  $main_img_top_width=$("#top_img").width();
  $main_img_top_height=$("#top_img").height();

  $("#blog_description p img").each(function() {
    var $this_p=$(this); 
    if($this_p.width()>$this_p.height()){

    }else{
      $new_width=$main_img_top_width/2;
      $new_height=$main_img_top_height
      $this_p.width($new_width);
      $this_p.height($new_height);     
    }
  });  
});

toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

  $('#commentBtn').click(function(event) {
    data = $('#commentForm').serializeArray()

    $.ajax({
      url: '{{ url('comment') }}',
      type: 'POST',
      dataType: 'json',
      data: data
    })
    .done(function() {
      location.reload()
      Command: toastr["success"]("Comment Published Successfully", "Good Job")
    })
    .fail(function(data) {
      $('#commentErrors ul').html('')
      $.each(data.responseJSON, function(index, val) {
          $('#commentErrors ul').append('<li>'+val+'</li>')
      });
      $('#commentErrors').show()
    })
  });

  $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  });

  $(document).ready(function () {
    var blog_block = $('.blog_block');
    var resultObj =  '';
    $('.query').on('keyup',function(){
      if(this.value.length >= 2){
        $.ajax({
            type: 'GET',
            url: 'blog/search',
            data: { qu: this.value },
            resultObj: this.resultObj,
            dataType:'JSON',
            success: function (json) {
                console.log("success");
                let dataObj = json;
                //console.log(dataObj);
                blog_block.html('');

                if (typeof dataObj.forEach == 'function') {
                    dataObj.forEach(function(element) {
                        var hitTemplate =
                                '<article class="post margbot40 clearfix" data-appear-top-offset="-100" data-animated="fadeInUp"><a class="post_image pull-left" href="blog/'+element["id"]+'" ><div class="recent_post_date">'+element['date']+'<span>'+element["month"]+'</span></div>'+'<img  src="core/storage/uploads/images/blog/'+element["front_image"]+'" alt="" /></a><a class="post_title" href="blog/'+element["id"]+'" >'+element["name"]+'</a><div class="post_content text-justify">'+element["description"]+'...</div><ul class="post_meta valign-bot pull-left"><li><a href="blog/'+element["id"]+'"><button class="btn button-new " > Read more <i class="fa fa-arrow-circle-right" style="font-size: 13px;" aria-hidden="true"></i></button></a></li>             </ul></article><hr>';

                        // let output = $.mustache.render(hitTemplate, element);
                        $('.blog_block').append(hitTemplate);
                        $('.searching_ico').hide();
                    });
                }

                if(dataObj.length == 0){
                    $('.blog_block').html('<div class="alert alert-warning" role="alert">No Results!!!</div>');
                }
            }
        });
      }else{
        $.ajax({
            type: 'GET',
            url: 'blog/stdSearch',
            data: { qu: this.value },
            resultObj: this.resultObj,
            dataType:'JSON',
            success: function (json) {
                console.log("success");
                let dataObj = json;
                //console.log(dataObj);
                blog_block.html('');

                if (typeof dataObj.forEach == 'function') {
                    dataObj.forEach(function(element) {

                        var hitTemplate =
                                '<article class="post margbot40 clearfix" data-appear-top-offset="-100" data-animated="fadeInUp"><a class="post_image pull-left" href="blog/'+element["id"]+'" ><div class="recent_post_date">'+element['date']+'<span>'+element["month"]+'</span></div>'+'<img  src="core/storage/uploads/images/blog/'+element["front_image"]+'" alt="" /></a><a class="post_title" href="blog/'+element["id"]+'" >'+element["name"]+'</a><div class="post_content text-justify">'+element["description"]+'...</div><ul class="post_meta valign-bot pull-left"><li><a href="blog/'+element["id"]+'"><button class="btn button-new " > Read more <i class="fa fa-arrow-circle-right" style="font-size: 13px;" aria-hidden="true"></i></button></a></li>             </ul></article><hr>';

                        // let output = $.mustache.render(hitTemplate, element);
                        $('.blog_block').append(hitTemplate);
                        $('.searching_ico').hide();
                    });
                }
            }
        });
      }         
    });
  });

  $(document).ready(function (){
      var token = '1351653747.1677ed0.e686f3deeb4c49a5b8e1bcf90176d7c2',
          userid = 1351653747,
          num_photos = 9; // how much photos do you want to get

      $.ajax({
          url: 'https://api.instagram.com/v1/users/' + userid + '/media/recent',
          dataType: 'jsonp',
          type: 'GET',
          data: {access_token: token, count: num_photos},
          success: function(data){
              console.log(data);
              for( n in data.data ){

                  $('#instagram').append('<div class="col-sm-4 insta-preview"><img src="'+data.data[n].images.standard_resolution.url+'" style="height: 50px;width: auto;"></div>');

              }
          },
          error: function(data){
              console.log(data);
          }
      });
  });

  $(function () {
    $("#additional_images").owlCarousel({
        items: 3,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayHoverPause: true,
        nav: true,
        dots: false,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],        
        responsive:{
            //breakpoint from 0 up
            0:{
                items: 1
            },
            //breakpoint from 480 up
            480:{
               items: 3 
            }
        }
    });
});
</script>
@stop
