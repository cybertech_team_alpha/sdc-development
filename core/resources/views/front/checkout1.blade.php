@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')

<style type="text/css">
  .dropdown-a{
    margin-bottom: 10px;text-transform: uppercase;color: #666;
    width: 100%;
    height: 40px !important;
    margin: 0 6px 10px 0;
    padding: 10px;
    text-transform: none;
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    line-height: 20px;
    font-size: 11px !important;
    color: #666;
    font-style: normal;
    border-radius: 0;
    background: #fff;
    border: 2px solid #e9e9e9;
    box-shadow: none;
    transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
  }
  .submit{
    margin: 75px 0 0 0!important;
  }
  .input-group .form-control:last-child, .input-group-addon:last-child, .input-group-btn:last-child > .btn, .input-group-btn:last-child > .btn-group > .btn, .input-group-btn:last-child > .dropdown-toggle, .input-group-btn:first-child > .btn:not(:first-child), .input-group-btn:first-child > .btn-group:not(:first-child) > .btn{
    border-top-right-radius: 4px !important;
    border-bottom-right-radius: 4px !important;
  }
</style>

@stop


@section('content')

    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->


    <!-- CHECKOUT PAGE -->
    <section class="checkout_page">

      <!-- CONTAINER -->
      <div class="container">

        <!-- CHECKOUT BLOCK -->
        <div class="checkout_block">
          <ul class="checkout_nav checkout-padding-added">
            <a href="{{url('checkout1')}}"><li class="active_step">1. Shipping Details</li></a>
            <a href="{{url('checkout2')}}"><li>2. Delivery Details</li></a>
            <a href="{{url('checkout3')}}"><li>3. Payment Details</li></a>
            <a href="{{url('checkout4')}}"><li class="last">4. Confirm Order</li></a>
          </ul>


        </div><!-- //CHECKOUT BLOCK -->
      </div><!-- //CONTAINER -->
    </section><!-- //CHECKOUT PAGE -->

     <section class="contacts_block">

      <!-- CONTAINER -->
      <div class="container">

        <!-- <div class="row center">
          <h3>Add new Billing Address</h3>
          <h4>Or</h4>
          <h3>Select from below address(es)</h3>
        </div>

        <div class="clearfix"></div> -->

        <!-- ROW -->
        <div class="row padbot30">
          <div class="col-lg-4 col-md-4 col-sm-6 padbot30">
            <!-- CONTACT FORM -->
            <div class="contact_form">

              <h3><b>ADD BILLING DETAILS</b></h3>


              <div id="fields">
                <form class="well form-horizontal" method="POST" id="address_form" >
                  {!!Form::token()!!}
                  <input type="hidden" name="submit_type" value="new_address">
                  <div class="form-group">
                      <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                              <input  name="first_name" placeholder="First Name" class="form-control" type="text">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                              <input  name="last_name" placeholder="Last Name" class="form-control" type="text">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                              <input  name="address1" placeholder="Address 1" class="form-control" type="text">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                              <input  name="address2" placeholder="Address 2" class="form-control" type="text">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                              <input  name="city" placeholder="City" class="form-control" type="text">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                              <input  name="province" placeholder="Province" class="form-control" type="text">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                              <input  name="postcode" placeholder="Zip Code" class="form-control" type="text">
                          </div>
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
                                <select class="form-control" name="country" >
                                <?php foreach ($country as $key => $value): ?>
                                  <option value="{{$value->id}}">{{$value->name}}</option>
                                <?php endforeach ?>

                               </select>
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                              <input name="email" placeholder="E-Mail Address" class="form-control"  type="text">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                          <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-phone-alt"></i></span>
                              <input  name="phone" placeholder="Contact no" class="form-control" type="text">
                          </div>
                      </div>
                  </div>




                  <input class="btn active submit checkout_block_btn" type="submit" value="Submit" style="position: relative;"/>
                </form>
              </div>
            </div><!-- //CONTACT FORM -->
          </div>

          <?php foreach ($billing_details as $key => $billing_detail): ?>
             <div class="col-lg-3 col-md-3 col-sm-6 padbot30 ">
                <ul class="contact_info_block">
                  <li>
                    <h3><i class="fa fa-user"></i><b>{{$billing_detail->first_name}} {{$billing_detail->last_name}}</b></h3>
                    <span>{{$billing_detail->street_addresss_1}}</span><br>
                    <span>{{$billing_detail->street_addresss_2}}</span><br>
                    <span>{{$billing_detail->city}}</span></br>
                    <span>{{$billing_detail->province}}</span></br>
                    <span>{{$billing_detail->getCountry->name}}</span></br>

                  </li>
                  <li>
                    <h3><i class="fa fa-phone"></i><b>Phones</b></h3>
                    <p class="phone">{{$billing_detail->phone}}</p>

                  </li>
                  <li>
                    <h3><i class="fa fa-envelope"></i><b>E-mail</b></h3>
                    <a href="mailto:{{$billing_detail->email}}">{{$billing_detail->email}}</a>
                  </li>
                  <form method="POST" >
                    <input type="hidden" name="submit_type" value="exsist_address">
                    <input type="hidden" name="billing_id" value="{{$billing_detail->id}}">
                    {!!Form::token()!!}
                    <input class="btn active" type="submit" value="Use this" style="position: relative;"/>
                  </form>

                </ul>


              </div>

           <?php endforeach ?>


        </div><!-- //ROW -->
      </div><!-- //CONTAINER -->
    </section><!-- //CONTACTS BLOCK -->


@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
    $('#address_form').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          // valid: 'glyphicon glyphicon-ok',
          // invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          first_name: {
              validators: {
                      stringLength: {
                      min: 2,
                  },
                      notEmpty: {
                      message: 'Please enter your First Name'
                  }
                  ,
                  regexp: {
                      regexp: /^[a-zA-Z_]+$/,
                      message: 'The username can only consist of alphabetical & underscore'
                  }
              }
          },
           last_name: {
              validators: {
                   stringLength: {
                      min: 2,
                  },
                  notEmpty: {
                      message: 'Please enter your Last Name'
                  }
                  ,
                  regexp: {
                      regexp: /^[a-zA-Z_]+$/,
                      message: 'The username can only consist of alphabetical & underscore'
                  }
              }
          },
          address1: {
              validators: {
                   stringLength: {
                      min: 2,
                  },
                  notEmpty: {
                      message: 'Please enter your Address 1'
                  }


              }
          },
          city: {
              validators: {
                   stringLength: {
                      min: 2,
                  },
                  notEmpty: {
                      message: 'Please enter your City'
                  }


              }
          },
          province: {
              validators: {
                   stringLength: {
                      min: 2,
                  },
                  notEmpty: {
                      message: 'Please enter your Province'
                  }


              }
          },
          postcode: {
              validators: {
                   stringLength: {
                      min: 2,
                  },
                  notEmpty: {
                      message: 'Please enter your Zip Code'
                  }


              }
          },
           user_name: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  notEmpty: {
                      message: 'Please enter your Username'
                  }
              }
          },
           password: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  notEmpty: {
                      message: 'Please enter your Password'
                  }
              }
          },
          confirm_password: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  identical: {
                      field: 'password',
                      message: 'The password and its confirm are not the same'
                  },
                  notEmpty: {
                      message: 'Please confirm your Password'
                  }
              }
          },
          email: {
              validators: {
                  notEmpty: {
                      message: 'Please enter your Email Address'
                  },
                  emailAddress: {
                      message: 'Please enter a valid Email Address'
                  }
              }
          },
           confirm_email: {
              validators: {
                  notEmpty: {
                      message: 'Please enter your Email Address'
                  },
                  identical: {
                      field: 'email',
                      message: 'The email and its confirm are not the same'
                  },
                  emailAddress: {
                      message: 'Please enter a valid Email Address'
                  }
              }
          },
          phone: {
              validators: {
                  notEmpty: {
                      message: 'Please enter a valid Contact Number'
                  }

              }
          },

          }
      })

      .on('success.form.bv', function(e) {
          $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
              $('#contact_form').data('bootstrapValidator').resetForm();

          // Prevent form submission
          e.preventDefault();

          // Get the form instance
          var $form = $(e.target);

          // Get the BootstrapValidator instance
          var bv = $form.data('bootstrapValidator');

          // Use Ajax to submit form data
          $.post($form.attr('action'), $form.serialize(), function(result) {
              console.log(result);
          }, 'json');
      });





       $("#contact_reset").click(function(){
         $('#contact_form').bootstrapValidator("resetForm",true);
      });
        $("#login_reset").click(function(){
         $('#login_form').bootstrapValidator("resetForm",true);
      });

    });

</script>


@stop
