@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')

@stop


@section('content')


   <!-- BREADCRUMBS -->
    <section class="breadcrumb parallax margbot30"></section>
    <!-- //BREADCRUMBS -->


    <!-- PAGE HEADER -->
    <section class="page_header">

      <!-- CONTAINER -->
      <div class="container border0 margbot0">
        <h3 class="pull-left"><b>Request a Quote Page </b></h3>


      </div><!-- //CONTAINER -->
    </section><!-- //PAGE HEADER -->


    <!-- CHECKOUT PAGE -->
    <section class="checkout_page">

      <!-- CONTAINER -->
      <div class="container">
            <div class="alert alert-danger alert-dismissable" id="alart">

              <strong>ERROR alert!</strong> Thank you for contacting Sweet Delights Cakery.  Unfortunately we're unavailable for that day and will not be able to take your order.
            </div>
        <!-- CHECKOUT BLOCK -->
        <div class="checkout_block">


          <form class="checkout_form clearfix" action="javascript:void(0);" method="get">
            <div class="checkout_form_input country">
              <label>Cake Type <span class="color_red">*</span></label>
              <select class="basic" id="caketype">
                <option value="1">Wedding Cake</option>
                <option value="2">Birthday Cake</option>
                <option value="3">Other Cake</option>
              </select>
            </div>

            <div class="checkout_form_input " >
              <label>Event Date/Time1<span class="color_red">*</span></label>
              <input type="text" class="form-control" id="date" name="date" value="" placeholder="" />
            </div>

            <div class="checkout_form_input territory">
              <label>Province / Territory <span class="color_red">*</span></label>
              <input type="text" name="name" value="" placeholder="" />
            </div>

            <div class="checkout_form_input last postcode">
              <label>Postcode <span class="color_red">*</span></label>
              <input type="text" name="name" value="" placeholder="" />
            </div>

            <div class="checkout_form_input2 adress">
              <label>Details</label>
              <textarea rows="2" id ="textarea1"></textarea>

            </div>

            <div class="checkout_form_input2 last adress">
              <label>Venue<span class="color_red">*</span></label>
              <input type="text" name="name" value="" placeholder="" />
            </div>

            <hr class="clear">

            <div class="checkout_form_input first_name">
              <label>First Name <span class="color_red">*</span></label>
              <input type="text" name="name" value="" placeholder="" />
            </div>

            <div class="checkout_form_input last_name">
              <label>Last name <span class="color_red">*</span></label>
              <input type="text" name="name" value="" placeholder="" />
            </div>

            <div class="checkout_form_input phone">
              <label>Phone </label>
              <input type="text" name="name" value="" placeholder="" />
            </div>

            <div class="checkout_form_input last E-mail">
              <label>e-mail <span class="color_red">*</span></label>
              <input type="text" name="name" value="" placeholder="" />
            </div>

            <div class="clear"></div>

            <div class="checkout_form_note">All fields marked with (<span class="color_red">*</span>) are required</div>

            <a class="btn active pull-right" href="" >Continue</a>
          </form>
        </div><!-- //CHECKOUT BLOCK -->
      </div><!-- //CONTAINER -->
    </section><!-- //CHECKOUT PAGE -->

@stop

@section('js')

<script type="text/javascript">
      $(document).ready(function(){
    var date_input=$('input[name="date"]'); //our date input has the name "date"
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    date_input.datetimepicker({
      format: 'yyyy-mm-dd hh:ii',
      // container: container,
      todayHighlight: true,
      autoclose: true,
        weekStart: 1,

    })

    $("#alart").hide();

  })
  bkLib.onDomLoaded(function() {

        new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('textarea1');

    });
  $("#date").change(function(){
        var caketype = $("#caketype").val();
        var eventdate = $("#date").val();
         console.log("Date: ", caketype);
          console.log("Date: ", eventdate);
         if ( (caketype=="null") || (eventdate=="null") ) {
     console.log("Date is Null");
            }
        else
        {
          if( caketype == "1")
          {
          var today = new Date();
       var targetDate= new Date();
        targetDate.setDate(today.getDate()+ 30);
        targetDate.setHours(0);
        targetDate.setMinutes(0);
        targetDate.setSeconds(0);
        if (Date.parse(targetDate ) >Date.parse(eventdate)) {
    alert('Within Date limits');
    $("#alart").show();
  } else {
    alert('not Within Date limits');
  }
        }}
    });
</script>

@stop
