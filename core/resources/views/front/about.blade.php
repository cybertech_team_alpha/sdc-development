@extends('layouts.front.master') @section('title','About | www.theprinceofgalle.com')
@section('css')

@stop


@section('content')

    <section class="breadcrumb men parallax margbot30">

    </section><!-- //BREADCRUMBS -->

    <style type="text/css">
        .div_text_section {
            margin-top: 100px;
            margin-bottom: 100px;
        }
        .text_section {
            font-size: 13pt;
            line-height: 18pt;
            text-align: start;
            color: #242424;
        }
    </style>

    <!-- PAGE HEADER -->
    <section class="page_header">

      <hr class="banner-top"/>
            <div class="banner-bg center">
                <h3>About Us</h3>
                <p>WHO WE ARE</p>
            </div>
            <hr class="banner-bottom">
      <!-- CONTAINER -->
      <div class="container">

           <!--  <hr class="banner-top">
            <div class="banner-bg center">
                <h3>Blog Page</h3>
                <p>View our portfolio of Blogs!</p>
            </div>
            <hr class="banner-bottom"> -->

      </div><!-- //CONTAINER -->
    </section><!-- //PAGE HEADER -->
        
        <!-- ABOUT US INFO -->
        <section class="about_us_info" >
            
            <!-- CONTAINER -->
            {{-- <div class="container"> --}}
                
                <!-- ROW -->
                <div class="row">
                        <div style="width:1000px; margin:0 auto; text-align: center; padding-bottom: 30px;padding-bottom: 30px">
                    <p style="font-size:15px; margin: 0 0 70px;">At Sweet Delights Cakery we are passionate about creating gorgeous individually tailored cakes for weddings and any special occasion using the freshest and finest ingredients! 
                        <br/>Combining delicious flavors with attention to detail, we aim to create the perfect cake for your special day. Serving Oakland County and beyond.</p>
                        </div>
                    <div class="col-md-12" style="background-image: url(http://localhost/sdc-development/assets/front/images/about_Us.jpg); background-repeat: no-repeat; background-size: 98%; background-position: center; min-height: 450px; ">
                        <div class="col-lg-8 col-md-8 div_text_section">
                            <p class="text_section" style="visibility: hidden">At Sweet Delights Cakery we are passionate about creating gorgeous individually tailored cakes for weddings and any special occasion using the freshest and finest ingredients!
                            <br/>
                            Combining delicious flavors with attention to detail, we aim to create the perfect cake for your special day. Serving Oakland County and beyond.</p>
                        </div>
                    </div>
                    {{-- <div class="container-fluid">
                    <img src="http://localhost/sdc-development/assets/front/images/about_Us.jpg" alt="" width="" height="" style="margin-top: 25px;background-repeat: no-repeat; background-size: 100% 15px;position: absolute; background-position: right; left:100px; position: relative">
                    </div> --}}
                    <div class="container">
	                   	<!-- <img src="http://localhost/sdc-development/assets/front/images/USA-logo.jpg" alt="" width="400px" height="800px" style="margin-left: auto; margin-right: auto; display: block;"> -->
	                    <div class="text-center col-md-12" style="display:inline-flex; margin: 65px 0;">
	                    	<img src="http://localhost/sdc-development/assets/front/images/USA-logo.jpg" alt="" width="400px" height="200px" style="margin-left: auto;  display: block;">
	                    	<div style="min-width: 5%;"></div>
	                    	<img src="http://localhost/sdc-development/assets/front/images/USA-logo.jpg" alt="" width="400px" height="200px" style="margin-right: auto; display: block;">
	                    </div>
	                           
		                <!-- ROW -->
		                <div class="row" style="margin-right: 0px; margin-left: 0px; margin-top: 25px;">
		                    <div class="team_wrapper padbot60" data-appear-top-offset='-100' data-animated='fadeInUp'>
		                        <iframe width="100%" height="650" src="https://www.youtube.com/embed/AtaWIzXDrzA" frameborder="0" allowfullscreen></iframe>
		                    </div>
		                </div><!-- //ROW -->
	                </div>
        </section><!-- //ABOUT US INFO -->


@stop

@section('js')

@stop