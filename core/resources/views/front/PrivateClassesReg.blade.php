@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')

<link href="{{asset('assets/front/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css"/>

<style type="text/css">
    .height500{
        position: relative;
        height: 600px;
        z-index: -99999;
    }
    .backg{
        position: relative;
        background-color: #F2F0F1;
        height: 600px;
        z-index: -00000;
        margin-left: -15px;
        margin-right: -15px;
    }
    .backg h1{
        margin-top: 25px;
        text-transform: uppercase;
        text-align: center;
        font-size: 30px;
        font-weight: 600;
    }
    .top-border{
        background: url("{{asset('assets/front/images/top-border-image.png')}}");
        background-repeat: repeat-x;
        height: 24px;
        margin-top: -5px;
        position: absolute;
        z-index: 1000;
        width: 100%;
    }
    .backgroundimg{
         background-repeat: no-repeat;
         /*background-size: 600px 600px;*/
         background-size:cover;
         background-position:center center;
         /*width: 600px;*/
         height: 600px;
    }
    .detail{
        margin-top: 25px;
    }
    .detail i{
        font-size: 20px;
        margin-bottom: 5px;
    }
    .detail p{
        font-size: 15px;
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .detail span{
        font-size: 17px;
        margin-top: 0px;
        margin-bottom: 0px;
        font-weight: 600;
    }
    hr.line1{
        border-top: 2px solid #8c8b8b;
        margin: 0 0 15px 0!important;
    }
    .height600{
        background-color: #f4f2f2;
        position: relative;
        z-index: -999999;
    }
    input.checkbox-private[type="checkbox"]+label{
      padding-top: 0;
    }
    input.checkbox-private[type="checkbox"]+label:before{
      top: 1px;
    }
    .btn.active.private{
      position: relative;
      margin-top: 15px;
    }
    .modal-header .close{
    	margin-top: -20px;
    }
    .modal-header {
    min-height: 16.43px;
    padding: 25px;
    border-bottom: 1px solid #e5e5e5;
	}
	.modal-footer button[type="button"] {
    border: thin solid gray;
    background-color: #8b5730;
    border-radius: 5px;
}
h4.modal-title{
	color: #8a562f !important;
	font-size: 18px;
	padding-left: 0;
}
input.register {
    background-color:#8b5730;
    color: #ffffff;
    border: 1px solid transparent;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    padding: 6px 10px;
    margin: 3px 0 0 0;
    }
  input.register i {
    font-size: 15px;
    }
  input.register:hover {
    background-color:#5d391e;
    color: #eeeeee;
    }
  .form-focus:focus{
      border-color: #8b5730;
      box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px #8b5730
  }
</style>

@stop

@section('content')


    <section class="breadcrumb parallax margbot30"></section>
    <!-- //BREADCRUMBS -->


    <!-- PAGE HEADER -->
    <section class="page_header">

        <!-- CONTAINER -->
        <div class="container">
            <hr class="banner-top">

            <hr class="banner-bottom" style="margin-bottom: 80px !important;">


            <div class="row height600" style="">
                <!-- <div class="top-border"></div> -->
                <!-- Update Background Image -->

                <div class="col-sm-12 col-md-6 col-lg-6 backgroundimg" style="background-image: url({{asset('core/storage/'.$cat->path.'/'.$cat->filename )}});">
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 height600 backg" style="margin-left: -10px;">
                    <!-- Update Class Name -->

                    <h1>{{ $cat->name }}</h1>
                  <div class="row col-sm-12">
                    <div class="col-sm-5 col-sm-offset-1 detail">
                          <i class="fa fa-calendar"></i> <span>Date(s)</span>
                          <?php foreach ($date_time as $values) {?>
                             <p>{{ $values->getFullDate()}}</p>
                          <?php } ?>
                       
                          
                    </div>
                    <div class="col-sm-5 detail">
                        <i class="fa fa-clock-o"></i> <span>Time</span>
                        
                         <?php foreach ($date_time as $values) {?>
                             <p>{{ $values->getStart24Format() }}- {{ $values->getEnd24Format() }}</p>
                          <?php } ?>
                       
                    </div>
                  </div>
                   <div class="row col-sm-12">
                    <div class="col-sm-5 col-sm-offset-1 detail">
                        
                    </div>
                    <div class="col-sm-5 detail">
                        <i class="fa fa-money"></i> <span>Fee</span>
                        <p>{{ $cat->price }}</p>

                    </div>
                  </div>
                </div>

            </div>

            <!-- <h3 class="pull-left"><b>Private Classes Registration</b></h3> -->
        </div><!-- //CONTAINER -->
    </section><!-- //PAGE HEADER -->


    <section class="checkout_page">
        <div class="container">
            <div class="row">
              <div class="col-sm-5 col-md-6">
                  <h1>Class Description</h1>
                  <hr class="line1"/>
                    <p>{{ $cat->description}}</p>
              </div>

              <div class="col-sm-5 col-md-6">
                    <h1>Register</h1>
                    <hr class="line1"/>

                    <!-- CHECKOUT BLOCK -->
                    <div class="checkout_block">


                        <form class="checkout_form clearfix" id="form"  method="post" action="{{url('/PrivateClassesReg')}}" style="border: none; padding-top: 0;">
                            {!!Form::token()!!}
                        <input type="hidden" name="class_id" value="{{$cat->id}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="checkout_form_input2">
                                        <h2 style="margin-top: 0;">
                                            <input name="parent" type="checkbox" id="categorymanufacturer1" class="a1 checkbox-private">
                                            <label for="categorymanufacturer1">Parent is Registering</label>
                                        </h2>
                                    </div>

                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div id="idparent">
                                        <div class="checkout_form_input_p first_name">
                                            <label>Parent Full Name <span class="color_red">*</span></label>
                                            @if($user)
                                                <input type="text" name="parentName" value="{{$user->first_name.' '.$user->last_name}}" class="width537" placeholder="" style="margin-bottom: 10px!important;"/>
                                            @else
                                                <input type="text" name="parentName" value="" class="width537" placeholder="" style="margin-bottom: 10px!important;"/>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($user)
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkout_form_input first_name" style="width: 100%;">
                                        <label id="firstnamelb">First Name <span class="color_red">*</span></label>
                                        <input type="text" name="fn" value="{{$user->first_name}}" placeholder="" readonly/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="checkout_form_input last_name" style="width: 100%;">
                                        <label id="lastnamelb">Last name <span class="color_red">*</span></label>
                                        <input type="text" name="ln" value="{{$user->last_name}}" placeholder="" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="checkout_form_input last E-mail" style="width: 100%;">
                                        <label>E-mail <span class="color_red">*</span></label>
                                        <input type="text" name="email" value="{{$user->email}}" placeholder="" readonly/>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkout_form_input first_name" style="width: 100%;">
                                        <label id="firstnamelb">First Name <span class="color_red">*</span></label>
                                        <input type="text" name="fn" value="" placeholder=""/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="checkout_form_input last_name" style="width: 100%;">
                                        <label id="lastnamelb">Last name <span class="color_red">*</span></label>
                                        <input type="text" name="ln" value="" placeholder=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="checkout_form_input last E-mail" style="width: 100%;">
                                        <label>E-mail <span class="color_red">*</span></label>
                                        <input type="text" name="email" value="" placeholder=""/>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="checkout_form_input phone" style="width: 100%;">
                                        <label>Phone Number<span class="color_red">*</span></label>
                                        <input class="form-control form-focus" type="text" name="phone" value="" placeholder=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="checkout_form_input" style="width: 100%;">
                                        <label>Date and Time<span class="color_red">*</span></label>
                                        <select name="datetime_id" class="form-control">
                                            <option value="">Please select from Drop down</option>
                                            @foreach($dt as $j)
                                                <option value="{{$j->id}}">{{$j->getFullDate()}} - {{$j->getStart24Format()}} to {{$j->getEnd24Format()}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 pull-left" style="margin-top: 15px;">
                                    <div class="g-recaptcha checkout_form_recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>

                                    {{--<div><input type="checkbox" name="agree" id="agree" value="agree">agree</div>--}}
                                </div>
                            </div>
                            <div class="checkout_form_policy pull-left">
                                <h2>
                                    <input name="categorymanufacturer" type="checkbox" id="categorymanufacturer" class="a1 policy_checkbox" value="accepted">
                                    <label for="categorymanufacturer">I accept the <a class="cancellation-url" data-toggle="modal" data-target="#cancellationModel">terms for the classes </a></label>
                                </h2>
                                <div class="row">
                                    <div class="col-md-12 pull-left">
                                        <h3 style="margin-top: 10px;">
                                        <label for="mandatory" class="pull-left">(<span class="color_red">*</span>) Mandatory Fields</label>
                                    </h3>
                                    </div>
                                </div>
                                <input type="hidden" name="class" value="{{$class}}">
                                <input type="submit" id="continue" class="btn register private pull-left" value="Continue" disabled="disabled" />
                            </div>


                        </form>
                    </div><!-- //CHECKOUT BLOCK -->
              </div>
            </div>
        </div>

    </section>

    <div class="modal fade" id="cancellationModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel"><strong>Cancellation Policy</strong></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Please read these Terms of Service ("Terms", "Terms of Service") carefully before using the http://www.mywebsite.com (change this) website and the My Mobile App (change this) mobile application (the "Service") operated by My Company (change this) ("us", "we", or "our").</p>

        <p>Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</p>

        <p>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>

        <h3>Termination</h3>

        <p>We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>

        <p>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn active" style="position: relative;" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>



@stop

@section('js')
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#idparent").hide();

            var date_input = $('input[name="date"]'); //our date input has the name "date"
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
            if (date_input.length) {
                date_input.datetimepicker({
                    format: 'yyyy-mm-dd hh:ii',
                    container: container,
                    todayHighlight: true,
                    autoclose: true,
                    weekStart: 1,

                });
            }
            

            $("#alart").hide();
            $("#idparent").hide();

            
            $('#form').validate({
                rules: {
                    parentName: {
                        required: '#categorymanufacturer1:checked'
                    },
                    fn: {
                        required: function (element) {
                            if ($('#categorymanufacturer1').is(':checked')) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                    },
                    ln: {
                        required: function (element) {
                            if ($('#categorymanufacturer1').is(':checked')) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true,
                        maxlength: 10,
                        minlength: 10,
                        digits: true
                    },
                    datetime_id : {
                        required: true
                    }
                    
                },
                messages: {
                    phone: "Please enter a valid phone number."
                },
                sumbitHandler: function(form){
                    form.submit();
                }
            });

        });
            @if($user)
            var first_name = '{{$user->first_name}}';
            var last_name = '{{$user->last_name}}';
            @else
            var first_name = '';
            var last_name = '';
            @endif
        $("#categorymanufacturer1").on("change", function () {

            if ($(this).hasClass("a1")) {
                if ($('#categorymanufacturer1').is(':checked')) {

                    $("#idparent").show();
                    $("#firstnamelb").html("Child First Name");
                    $("#lastnamelb").html("Child Last Name");
                    if (first_name == '') {
                        $('input[name=fn]').prop('readonly', false);
                        $('input[name=ln]').prop('readonly', false);
                    } else {
                        $('input[name=fn]').prop('readonly', false);
                        $('input[name=fn]').prop('value', '');
                        $('input[name=ln]').prop('readonly', false);
                        $('input[name=ln]').prop('value', '');
                        $('input[name=parentName]').prop('readonly', true);
                    }
                    $('input[name=fn]').attr('placeholder', '');
                    $('input[name=ln]').attr('placeholder', '');

                }
                else {

                    $("#idparent").hide();
                    $("#firstnamelb").html("First Name");
                    $("#lastnamelb").html("Last Name");
                    if (first_name == '') {
                        $('input[name=fn]').prop('readonly', false);
                        $('input[name=ln]').prop('readonly', false);
                    } else {
                        $('input[name=fn]').val(first_name);
                        $('input[name=ln]').val(last_name);
                        $('input[name=fn]').prop('readonly', true);
                        $('input[name=ln]').prop('readonly', true);
                    }
                }
            }

        });

        $("#categorymanufacturer").on("change", function () {

            if ($('#categorymanufacturer').is(':checked')) {
                $('#continue').removeAttr("disabled");
            } else {
                $('#continue').attr("disabled", "disabled");
            }
            
        });



    </script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

@stop
