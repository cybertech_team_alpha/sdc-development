@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')
<style type="text/css">
    .saleprice{
       /* color:#FF1C03;  */
       color:#4D4B4C; 
       font-weight:500;
       font-size: 15px;
       /* text-shadow: 0px 0px 8px rgba(10, 10, 10, 0.5); */
       margin-left: 15px;
    }
    .oldprice{
        /* color:#313131; */
        color:#B4ABAA;
        position: relative;
        display: inline-block;
        text-decoration: line-through;
    }

    .oldprice::before, .oldprice::after {
        content: '';
        width: 100%;
        position: absolute;
        right: 0;
        top: 50%;
    }
    /* .oldprice::before {
        border-bottom: 2px solid #714949;
        -webkit-transform: skewY(-10deg);
        transform: skewY(-10deg);
    }
    .oldprice::after {
        border-bottom: 2px solid #714949;
        -webkit-transform: skewY(10deg);
        transform: skewY(10deg);
    } */
     .corner-text-wrapper {
      -webkit-transform: rotate(45deg);  
         -moz-transform: rotate(45deg);  
          -ms-transform: rotate(45deg);  
           -o-transform: rotate(45deg);  
              transform: rotate(45deg);
        clip: rect(0px, 141.421px, 70.7107px, 0px);
        height: 141.421px;
        position: absolute;
        right: -16px;
        top: -22px;
        width: 155.421px;
        z-index: 1;

    }
    .corner-text {
      color: white;
     -webkit-transform: rotate(-45deg);  
         -moz-transform: rotate(-45deg); 
          -ms-transform: rotate(-45deg); 
           -o-transform: rotate(-45deg); 
              transform: rotate(-45deg);
        right: 0;
        top: 18px;
        background: #8b5730;
        //background: #7a3535;
        background: linear-gradient(#8b5730 0%, #cc7432 100%);
        //background: linear-gradient(#7a3535 0%, #c8978c 100%);
        box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
        display: block;
        height: 179px;
        position: absolute;
        width: 120px;
        z-index: 9999;
    }
    .corner-text span {
        position: inherit;
        top: -1px;
        font-weight: 600;
        font-size: 14px;
        left: 53px;
        display: block;
        text-align: right;
        margin: 10px
    }

    div.fancy-select div.trigger {
    position: relative;
    cursor: pointer;
    overflow: hidden;
    text-overflow: ellipsis;
    padding: 13px 30px 13px 8px;
    background: none;
    color: #666;
    width: 220px!important;
    height: 50px;
    border: 2px solid #ccc;
    transition: all 240ms ease-out;
    -webkit-transition: all 240ms ease-out;
    }
    #price-range input {
    margin-bottom: 13px!important;
    width: 100%!important;
    /*padding: 10px!important;   */
    font-size: 14px!important;
    font-family: 'Roboto', sans-serif!important;
    text-transform: uppercase!important;
    font-weight: 600!important;
    font-style: normal!important;
    line-height: 20px!important;
    background-color: transparent;
    color: #8b5730;
    border: 1px solid #8b5730;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    padding: 6px 10px;
    margin: 3px 0 0 0;

    }
    #price-range input:hover{
   background-color:#5d391e;
    color: #eeeeee;
    }
    /* Base for label styling */
    [type="checkbox"]:not(:checked),
    [type="checkbox"]:checked {
    position: absolute;
    left: -9999px;
    }
    [type="checkbox"]:not(:checked) + label,
    [type="checkbox"]:checked + label {
    position: relative;
    padding-left: 40px;
    cursor: pointer;
    margin-bottom: 4px;
    display: inline-block;
    font-size: 13px;
    }
    /* checkbox aspect */
    [type="checkbox"]:not(:checked) + label:before,
    [type="checkbox"]:checked + label:before {
    content: '';
    position: absolute;
    left: 9px;
    top: 5px;
    width: 16px;
    height: 16px;
    border: 1px solid #601B1B;
    background: #ffffff;
    border-radius: 0px;
    box-shadow: inset 0 1px 3px rgba(0,0,0,.1);
    }
    /* checked mark aspect */
    input[type="checkbox"]:checked + label::before {
    content: '✔'!important;
    position: absolute;
    top: 5px;
    left: 9px;
    font-weight: 900;
    font-size: 25px;
    line-height: 0.9;
    color: #09AD7E!important;
    transition: all .2s;
    }
    /*-----*/
    /* Dropdown */
    select {
    /* styling */
    background-color: white;
    border: 2px solid #dddddd;
    border-radius: 4px;
    display: inline-block;
    padding: 8px 10px 8px 10px;
    width: 100%;
    /* reset */
    margin: 0;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    -webkit-appearance: none;
    -moz-appearance: none;
    background-image:
    linear-gradient(45deg, transparent 50%, gray 50%),
    linear-gradient(135deg, gray 50%, transparent 50%),
    linear-gradient(to right, #ccc, #ccc);
    background-position:
    calc(100% - 16px) calc(1em + 5px),
    calc(100% - 11px) calc(1em + 5px),
    calc(100% - 2.7em) 0.5em;
    background-size:
    5px 5px,
    5px 5px,
    1px 2em;
    background-repeat: no-repeat;
    }
    .shop-sorting {
    font-size: 14px;
    }
    button.add-to-cart {
    background-color:#8b5730;
    color: #E5D4CD;
    border: 1px solid transparent;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    /*padding: 6px 10px;*/
    margin: 3px 6px 0 0;
    }
    button.add-to-cart i {
    font-size: 15px;
    }
    button.add-to-cart:hover {
    background-color:#E5D4CD;
    color: #8b5730;
    }
    button.view-cart {
    background-color: transparent;
    color: #8b5730;
    border: 1px solid #8b5730;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    padding: 6px 10px;
    margin: 3px 0 0 0;
    }
    button.view-cart:hover {
    background-color: #eeeeee;
    color: #5d391e;
    border: 1px solid #8b5730;
    }
    button.view-cart i {
    font-size: 15px;
    }
    div.item-text-name-new {
    font-size: 16px;
    min-height: 30px;
    font-weight: 600;
    }
    div.item-text-price-new {
    font-size: 15px;
    min-height: 30px;
    font-weight: 600;
    color:#4D4B4C;
    }
    p.item-category-new {
    font-size: 15px;
    min-width: 100%;
    }
    p.item-category-new span {
    padding-right: 0px;
    }

    .keywords-shop {
    list-style:none;
    text-align: center;
    margin: 0 auto;
    padding: 0px;
    display:table;
    overflow: hidden;
}

.keywords-shop li{
    vertical-align: bottom;
    float: left;
    padding: 5px 7px 5px 7px;
    width: auto;
    margin-left: 3px;
    margin-right: 3px;
    background-color:none;
    font-weight: 600;
    font-size: 1.15em;
    border-radius: 5px;
    border: thin solid #8b5730;

}

.keywords-shop li:hover{
    vertical-align: bottom;
    float: left;
    padding: 5px 7px 5px 7px;
    width: auto;
    margin-top: 0px;
    margin-left: 3px;
    margin-right: 3px;
    background-color: #8b5730;
    font-size: 1.15em;
    border-radius: 5px;
    cursor: pointer;
    border: no;
    -webkit-transition: all 0.4s ease-in-out;
}

.keywords-shop li a {
  color : #8b5730;
  -webkit-transition: all 0.3s ease-in-out;
}

.keywords-shop li:hover a {
  color : #fff;
  -webkit-transition: all 0.3s ease-in-out;
}

.keywords-hover-new-shop li:hover{
    vertical-align: bottom;
    float: left;
    padding: 6px 0px 6px 0px;
    width: 100px;
    margin-top: 0px;
    margin-left: 3px;
    margin-right: 3px;
    background-color: #8b5730;
    color: #ffffff;
    font-size: 1.25em;
    border-radius: 5px;
    cursor: pointer;
    border: none;
    -webkit-transition: all 0.4s ease-in-out;
}

    /* input.newsletter_btn{
        margin-bottom: 13px!important;
        width: 100%!important;
        padding: 6px!important;
        font-size: 14px!important;
        font-family: 'Roboto', sans-serif!important;
        text-transform: uppercase!important;
        font-weight: 600!important;
        font-style: normal!important;
        line-height: 20px!important;
        background-color: transparent !important;
        color: #8b5730 !important;
        border: 1px solid #8b5730;
        border-radius: 4px;
        font-size: 12px;
        font-weight: 600;
        padding: 6px 10px;
        margin: 3px 0 0 0;
    }

    input.newsletter_btn:hover {
        background-color:#5d391e !important;
        color: #eeeeee !important;
    } */




</style>
@stop

@section('content')

<section class="breadcrumb men parallax margbot30">
</section>
<!-- //BREADCRUMBS -->
<!-- PAGE HEADER -->
<section class="page_header">
    <!-- CONTAINER -->

        <hr class="banner-top">
            <div class="banner-bg center">
                <h3>Shop</h3>
                <p>BROWSE THROUGH OUR PRODUCTS</p>
            </div>
        <hr class="banner-bottom">

    <div class="container">
        <!-- <hr class="banner-top">
        <div class="banner-bg center">
            <h3>OUR PRODUCTS</h3>
            <p>Our portfolio of products you can purchase!</p>
        </div>
        <hr class="banner-bottom"> -->
    </div>
    <!-- //CONTAINER -->
</section>
<!-- //PAGE HEADER -->
<!-- SHOP BLOCK -->
<section class="shop">
    <!-- CONTAINER -->
    <div class="container">
        <!-- ROW -->
        <div class="row">
            <!-- SIDEBAR -->
            <div id="sidebar" class="col-lg-3 col-md-3 col-sm-3">
                <!-- CATEGORIES -->
                <div class="sidepanel widget_categories">
                    <h3>Search Products:</h3>
                    <input type="hidden" value="{{$category}}" name="category_id" id="category_id">
                    <div class="widget_search">
                        <?php if ($search=='all'): ?>
                        <input type="text" id="search_id" name="search"   />
                        <?php else: ?>
                        <input type="text" id="search_id" name="search" value="{{$search}}"   />
                        <?php endif ?>
                    </div>
                    <!-- //WIDGET SEARCH -->
                </div>
                <!-- //CATEGORIES -->
                <!-- SORTING -->
                <div class="sidepanel widget_pricefilter country">
                    <h3>Sort by:</h3>
                    <select id="sort" name="sort" class="shop-sorting" onchange="search_data()">
                        <?php foreach ($sortType as $key => $value): ?>
                        <?php if ($value->id==$sort): ?>
                        <option value="{{$value->id}}" selected="true">{{$value->name}}</option>
                        <?php else: ?>
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        <?php endif ?>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="sidepanel widget_pricefilter" style="margin-top: 10px;">
                    <h3>Filter by Price:</h3>
                    <div class="checkout_form_input" style="padding-right: 5px;">
                        <label>From</label>
                        <?php if (is_numeric($mprice)): ?>
                        <input type="text" name="mprice" id="mprice" value="{{$mprice}}" placeholder=""/>
                        <?php else: ?>
                        <input type="text" name="mprice" id="mprice" value="" placeholder=""/>
                        <?php endif ?>
                    </div>
                    <div class="checkout_form_input" style="padding-left: 5px;">
                        <label>To</label>
                        <?php if (is_numeric($hprice)): ?>
                        <input type="text" name="hprice" id="hprice" value="{{$hprice}}" placeholder=""/>
                        <?php else: ?>
                        <input type="text" name="hprice" id="hprice" value="" placeholder=""/>
                        <?php endif ?>
                    </div>
                    <div class="clearfix" id="price-range">
                        <div class="padding-range">
                            <div id="slider-range"></div>
                        </div>
                        <input class="btn submit-search" type="button" name="" value="Search Products" onclick="search_data()">
                    </div>
                </div>
                <!-- SHOP BY BRANDS -->
                <div class="sidepanel widget_brands"> 
                    <h3>Product Categories:</h3>
                    <?php foreach ($category_list as $key => $value): ?>
                        <?php if ($value->id == $category): ?>
                            <a href="{{url('/productload')}}/{{$value->id}}/{{$search}}/{{$sort}}/{{$mprice}}/{{$hprice}}">
                        <p class="item-category-new" style="color: blue">{{$value->name}}<span class="pull-right">({{$value->count}})</span></p></a>
                        <?php else: ?>
                            <a href="{{url('/productload')}}/{{$value->id}}/{{$search}}/{{$sort}}/{{$mprice}}/{{$hprice}}">
                        <p class="item-category-new">{{$value->name}}<span class="pull-right">({{$value->count}})</span></p>
                    </a>
                        <?php endif ?>

                    <?php endforeach ?>

                </div>
                <!-- //SHOP BY BRANDS -->
                <!-- NEWSLETTER FORM WIDGET -->
                <div style="display: none;" class="sidepanel widget_newsletter">
                    <div class="newsletter_wrapper">
                        <h3>Subscribe Now!</h3>
                        <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
                            <input type="text" name="newsletter" id="offer" value="Enter E-mail & Get latest offers" onFocus="if (this.value == 'Enter E-mail & Get 10% off') this.value = '';" onBlur="if (this.value == '') this.value = 'Enter E-mail & Get letest offers';" />
                            <input class="btn newsletter_btn" type="button" id="sendOffer" value="Submit to get offers">
                        </form>
                    </div>
                </div>
                <!-- //NEWSLETTER FORM WIDGET -->
            </div>
            <!-- //SIDEBAR -->
            <!-- SHOP PRODUCTS -->
            <div class="col-lg-9 col-sm-9 col-sm-9 padbot20">
                <!-- SHOP BANNER -->
                <!-- <div class="banner_block margbot15">
                    <a class="banner nobord" href="#" ><img src="{{asset('assets/front/images/tovar/banner21.jpg')}}" alt="" /></a>
                    </div> -->
                <!-- //SHOP BANNER -->
                <!-- SORTING TOVAR PANEL -->
                <div class="sorting_options clearfix">
                    <!-- COUNT TOVAR ITEMS -->
                    <div class="count_tovar_items">
                        <p>Shop</p>
                        <span>{{$product->total()}} Items found</span>
                    </div>
                    <!-- //COUNT TOVAR ITEMS -->
                    <!-- PRODUC SIZE -->
                    {{-- <div id="toggle-sizes">
                        <a class="view_box active" href="javascript:void(0);"><i class="fa fa-th-large"></i></a>
                        <!-- <a class="view_full" href="javascript:void(0);"><i class="fa fa-th-list"></i></a> -->
                    </div> --}}
                    <!-- //PRODUC SIZE -->
                </div>
                <!-- //SORTING TOVAR PANEL -->
                <section class="search-section" style="clear: both; margin-bottom: 10px !important;">
                    <div class="container" style="padding-left: 0; padding-right: 0;">
                        <div class="btn-group" style="margin-top: 10px;">
                            <ul class="keywords-shop">
                                <?php if ($category=='all'): ?>
                                <li>
                                    <a class="filter-button active" href="{{url('/productload')}}/all/{{$search}}/{{$sort}}/{{$mprice}}/{{$hprice}}">Filter - All</a>
                                </li>
                                <?php else: ?>
                                <li>
                                    <a class="filter-button" href="{{url('/productload')}}/all/{{$search}}/{{$sort}}/{{$mprice}}/{{$hprice}}">Filter - All</a>
                                </li>
                                <?php endif ?>
                                <?php foreach ($categories as $key => $categorie): ?>
                                <?php if ($categorie->id==$category): ?>
                                <li>
                                    <a href="{{url('/productload')}}/{{$categorie->id}}/{{$search}}/{{$sort}}/{{$mprice}}/{{$hprice}}" class="filter-button active">{{$categorie->name}}</a>
                                </li>
                                <?php else: ?>
                                <li>
                                    <a href="{{url('/productload')}}/{{$categorie->id}}/{{$search}}/{{$sort}}/{{$mprice}}/{{$hprice}}" class="filter-button">{{$categorie->name}}</a>
                                    <?php endif ?>
                                    <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </section>
                <!-- ROW -->
                <div class="row shop_block">
                    <?php foreach ($product as $key => $value): ?>
                    <a  href="{{url().'/product-detail/'.$value->id}}" >
                        <!-- TOVAR1 -->
                        <div class="tovar_wrapper col-lg-4 col-md-4 col-sm-6 col-xs-6 col-ss-12 padbot40">

                            <div class="tovar_item clearfix">
                                <!-- If Fuction needed -->
                               @if ($value->on_sale=='on' & date('Y-m-d')<=$value->end_date & date('Y-m-d')>=$value->start_date )
                                    @include('includes.product-sale-tag')
                               @endif
                                    
                                <div class="tovar_img">                                    
                                    <div class="tovar_img_wrapper">                                        
                                        <!-- <?php if (isset($value->getImages[0])): ?>
                                            {{-- <img class="img" src="{{url(). '/core/storage/'. $value->getImages[0]->path . '/' . $value->getImages[0]->filename}}" alt="" /> --}}
                                        
                                            {{-- <img class="img_h" src="{{url(). '/core/storage/'. $value->getImages[0]->path . '/' . $value->getImages[1]->filename}}" alt="" /> --}}
                                        <?php endif ?> -->

                                            <img class="img" src="{{url(). '/core/storage/'. $value->cover_path . '/' . $value->cover_file}}" alt="" />
                                        
                                        <?php if (isset($value->getImages[0])): ?>
                                        
                                            <img class="img_h" src="{{url(). '/core/storage/'. $value->getImages[0]->path . '/' . $value->getImages[0]->filename}}" alt="" />
                                        <?php else: ?>
                                            <img class="img_h" src="{{url(). '/core/storage/'. $value->cover_path . '/' . $value->cover_file}}" alt="" />
                                        <?php endif ?>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center">
                                    <div class="row item-text-name-new">
                                        {{ substr(strip_tags($value['product_name']),0,50)}}
                                    </div>
                                    <div class="row item-text-price-new">
                                        <!-- <span>US ${{number_format($value->price,2) }}</span> -->
                                        
                                        <!-- IF function needed -->
                                        <?php if ($value->on_sale=='on' & date('Y-m-d')<=$value->end_date & date('Y-m-d')>=$value->start_date ): ?>
                                            <span class="oldprice">US ${{number_format($value->price,2) }}</span>
                                            <span class="saleprice">US ${{number_format($value->sale_price,2) }}</span>
                                        <?php else: ?>
                                            <span>US ${{number_format($value->price,2) }}</span>
                                        <?php endif ?>
                                        

                                    </div>
                    </a>
                    <div class="row">
                        
                        <?php if (!empty(Cart::get('P-'.$value->id))): ?>
                           <button onclick="removeFromCart({{$value['id']}},{{1}})" class="btn add-to-cart">ADDED TO CART <i class="fa fa-check"></i></button>
                           <!-- <a href="{{url('my-cart')}}"><button class="btn view-cart">View Cart</button></a> -->
                        <?php else: ?>
                           <button onclick="addtocart({{$value->id}})" class="btn add-to-cart">ADD TO CART <i class="fa fa-cart-plus"></i></button>
                           <!-- <a href="{{url().'/product-detail/'.$value->id}}"><button class="btn view-cart">View Item</button></a> -->
                           <a href="{{url().'/product-detail/'.$value->id}}"><button class="btn view-cart">View more</button></a>
                        <?php endif ?>
                    <div id="addCartErrors_{{$value->id}}" style="color: #f00 !important;"></div>
                    </div>
                    </a>
                    </div>
                    <div class="tovar_content">
                    <?php
                        echo nl2br($value->description);
                        ?>
                    </div>
                    </div>
                    </div><!-- //TOVAR1 -->
                    <?php endforeach ?>
                </div>
                <!-- //ROW -->
                <hr>
                {!! $product->appends(['category'=>$category])->render() !!}
                <!-- <div class="clearfix"> -->
                <!-- PAGINATION -->
                <!--  <ul class="pagination">
                    <li><a href="javascript:void(0);" >1</a></li>
                    <li><a href="javascript:void(0);" >2</a></li>
                    <li class="active"><a href="javascript:void(0);" >3</a></li>
                    <li><a href="javascript:void(0);" >4</a></li>
                    <li><a href="javascript:void(0);" >5</a></li>
                    <li><a href="javascript:void(0);" >6</a></li>
                    <li><a href="javascript:void(0);" >...</a></li>
                    </ul> --><!-- //PAGINATION -->
                <!-- <a class="show_all_tovar" href="javascript:void(0);" >show all</a> -->
            </div>
        </div>
        <!-- //SHOP PRODUCTS -->
    </div>
    <!-- //ROW -->
    </div><!-- //CONTAINER -->
</section>
<!-- //SHOP -->

@stop
@section('js')
<script type="text/javascript">
    $( "#search_id" ).on( "keydown", function(event) {
     if(event.which == 13)
        search_data();
    });

    function getURLParameters(url){

       var result = {};
       var searchIndex = url.indexOf("?");
       if (searchIndex == -1 ) return result;
       var sPageURL = url.substring(searchIndex +1);
       var sURLVariables = sPageURL.split('&');
       for (var i = 0; i < sURLVariables.length; i++)
       {
           var sParameterName = sURLVariables[i].split('=');
           result[sParameterName[0]] = sParameterName[1];
       }
       return result;
    }
    function filter_price() {
     var min_price=$('#amount-from').val();
     var max_price=$('#amount-to').val();
     var parameters = getURLParameters(window.location.href);
     console.log(parameters);
     var newUrl = location.href.replace("prilo=1", "page=2");
     // window.location.href=newUrl;
    }

    function addtocart(product_id){
        $('#addCartErrors').html('')
    var product_id= product_id;
    var qty= 1;

     $.ajax({
       method: "GET",
       url: '{{url('cart/add')}}',
       data:{ 'product_id' : product_id,'qty': qty,'product_type':1 },
     })
        .done(function( msg ) {
        // window.location.href='{{url('my-cart')}}';
        location.reload();
        })
        .fail(function (jqXHR, textStatus, errorThrown){
            if(jqXHR.responseJSON.errors){
                mes = '<ul>';
                $.each(jqXHR.responseJSON.errors, function(index, value){
                    mes += '<li>'+value+'</li>';
                });
                mes += '</ul>';
                
                $('#addCartErrors_'+product_id).html(mes)
            }
        });
    }
    function search_data() {
     $category=$('#category_id').val();
     $search=$('#search_id').val();
     $sort=$('#sort').val();
     $mprice=$('#mprice').val();
     $hprice=$('#hprice').val();

     if ($search=='') {
       $search='all';
     }
     if ($mprice=='') {
       $mprice='min';
     }
     if ($hprice=='') {
       $hprice='hi';
     }


     $url="{{url('productload')}}"+"/"+$category+"/"+$search+"/"+$sort+"/"+$mprice+"/"+$hprice;
     console.log($url);
     window.location.href =$url;
    }
</script>

<script>
$('document').ready(function(){
$('#sendOffer').click(function(){
    var offer=document.getElementById("offer").value;
    $.ajax({
        
        method:'POST',
        url:'{{url('sendoffer')}}',
       
        data:{'email':offer}
    })
      .done(function(data){
                alert(data);
            })
})
    
})
      function removeFromCart(id, type) {
    $.ajax({method: "GET",
            url:'{{url('cart/remove')}}/'+id+'/'+type})
            .done(function(msg) {
            window.location.href='{{url('productload/all/all/1/min/hi')}}';
            });
    }

</script>
@stop
