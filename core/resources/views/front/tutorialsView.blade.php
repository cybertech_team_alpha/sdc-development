@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')


@section('css')
<script src="{{asset('assets/front/js/afterglow.min.js')}}"></script>
<script src="{{asset('assets/front/js/tutorials/modernizr.custom.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/tutorials/demo.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/tutorials/component.css')}}" />


<style type="text/css">
    .btn.view-cart{
        margin-left:auto!important;
        margin-right: auto!important;
        display: -webkit-box;
    }
    .btn_width_50 {
        width: 48%;
    }
    .carousel-control.left {
        height: 0;
        margin-top: 150px;
        color: black;
        margin-left: -5px!important;

    }
    .carousel-control.right {
        height: 0;
        margin-top: 150px;
        color: black;

    }
    .carousel{
        padding-left: 5%;
    }
    .col-md-6 {
        width: 48%!important;
    }
    .glyphicon-chevron-left::before {
        content: "<"!important;
    }
    .glyphicon-chevron-right::before {
        content: ">"!important;
    }

    .carousel-control {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        width: 5%!important;
        font-size: 20px;
        color: #fff;
        text-align: center;
        text-shadow: 0 1px 2px rgba(0,0,0,.6);
        filter: alpha(opacity=50);
        opacity: .5;
    }

    .thumbnail {
        display: block;
        padding: 15px!important;
        margin-bottom: 20px;
        text-align: justify!important;
        line-height: 1.42857143;
        background-color: #E5E5E5!important;
        border: 0px solid #ddd!important;
        border-radius: 4px;
        -webkit-transition: border .2s ease-in-out;
        -o-transition: border .2s ease-in-out;
        transition: border .2s ease-in-out;
        height:300px!important;
    }
    .thumbnail h3{
        font-weight: 800!important;
    }

    .carousel li {
        margin-right: 5px;
    }
    .carousel-indicators li {
        display: inline-block;
        width: 10px;
        height: 10px;
        margin: 1px;
        margin-right: 1px;
        text-indent: -999px;
        cursor: pointer;
        background-color: #FDFBF9!important;
        border: 1px solid #626262!important;
        border-radius: 10px;
    }

    #review_count .row{
        margin-top: 10px;
        height: 200px;
        background-color: #5A5A5A;
        padding: 30px 0px 30px 0px !important;
    }
    #review_count .row div:nth-child(odd){
        border-left: 2px solid #ffffff;
        border-right: 2px solid #ffffff;
        height: 100%;
    }
    #review_count .row div:first-child{
        border-left: 0px solid #ffffff;

    }
    #review_count .row div:last-child{
        border-right: 0px solid #ffffff;

    }
    #review_count .row div span{
        display: block;
        text-align: center;
    }
    #review_count .row div span#rCount0, #review_count .row div span#rCount2{
        margin-top: 50px;
        font-family: 'Roboto', sans-serif;
        color: #FECF52;
        font-size: 60px;
    }
    #review_count .row div span#rCount1, #review_count .row div span#rCount3{
        margin-top: 20px;
        font-family: 'Roboto', sans-serif;
        color: #FCF9F0;
        font-size: 30px;
    }
    .widthImg{
        height: 300px!important;
        width:95%!important;
    }
    .tutorial_details{
        background: #FCEEEB;
        height: 432px;
    }
    .tutorial_details span{
        font-size: 16px;
    }
    .tutorial_details h2{
        margin-top: 20px;
        font-size: 26px;
       /* font-weight: 800;*/
        color: #8B5730;

    }
    .tutorial_details h3{
        margin-top: 10px;
        font-size: 30px;
        /* font-weight: 800; */
        /*color: #299490;*/
        margin-bottom: 0px;
    }
    .tutorial_details h3>span{
        font-size: 16px;
        text-transform: lowercase;
        font-weight: 0;
    }

    .tovar_img{
        height: 195px!important;

    }
    .tovar_view_shop {
        padding: 5px 8px;
        text-transform: uppercase;
        font-weight: 700;
        font-size: 12px;
        color: #fff;
        border: 2px solid transparent!important;
        background-color: transparent!important;
    }

    .tovar_view_shop:hover {
        padding: 5px 8px;
        text-transform: uppercase;
        font-weight: 700;
        font-size: 12px;
        color: #ff0000!important;
        border: 2px solid #ff0000!important;
        background-color: transparent!important;
    }

    .add_lovelist_shop {
        display: inline-block;
        width: 40px;
        height: 40px;
        text-align: center;
        line-height: 37px;
        font-size: 24px;
        color: #fff;
        /*border: 2px solid #333;*/
        background-color: transparent!important;
    }

    .add_lovelist_shop:hover {
        display: inline-block;
        width: 40px;
        height: 40px;
        text-align: center;
        line-height: 37px;
        font-size: 28px;
        color: #ff0000!important;
        /*border: 2px solid #ff0000;*/
        background-color: transparent!important;
    }

    button.add-to-cart, a.add-to-cart {
    background-color:#8b5730;
    color: #E5D4CD;
    border: 1px solid transparent;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    padding: 6px 10px;
    margin: 3px 0 0 0;
    }
    button.add-to-cart i {
    font-size: 15px;
    }
    button.add-to-cart:hover {
    background-color:#E5D4CD;
    color: #8b5730;
    }
    button.view-cart {
    background-color: transparent;
    color: #8b5730;
    border: 1px solid #8b5730;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    padding: 6px 10px;
    margin: 3px 0 0 0;
    }
    button.view-cart:hover {
    background-color: #eeeeee;
    color: #5d391e;
    border: 1px solid #8b5730;
    }
    button.view-cart i {
    font-size: 15px;
    }
    
 div.item-text-name-new {
    font-size: 16px;
    min-height: 30px;
    font-weight: 600;
    word-wrap: break-word;
    word-break: break-all;
}
div.item-text-price-new {
    font-size: 15px;
    min-height: 30px;
    font-weight: 600;
}

</style>

@stop


@section('content')


<!-- BREADCRUMBS -->
<section class="breadcrumb men parallax margbot30">

</section><!-- //BREADCRUMBS -->

<!-- SHOP BLOCK -->
<section class="shop">

    <hr class="banner-top"/>
    <div class="banner-bg center">
        <h3>Online Classes</h3>
        <p>View our portfolio of online classes!</p>
    </div>
    <hr class="banner-bottom"/>

    <!-- CONTAINER -->
    <div class="container">
        <div class="col-md-8 col-sm-8 col-xs-12" style="padding-left: 0; margin-bottom: 35px;">
                @if ($id=="paid" && filter_var($getData['intro'], FILTER_VALIDATE_URL))
                    <iframe width="780" height="432" src="<?php echo $getData['intro'] ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                @elseif(isset($getData['main']) && filter_var($getData['main'], FILTER_VALIDATE_URL))
                    <iframe width="780" height="432" src="<?php echo $getData['main'] ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                @endif


        </div>

        <div class="col-md-4 col-sm-4 col-xs-12 tutorial_details">
            <h2><?php echo $getData['name'] ?></h2><br>

            @if (isset($getData['premium']) && $getData['premium'] == 1)
                <span><?php echo $getData['tutorial_text'] ?></span>
            @endif
            <br> <br>
            <!-- Membership price demo only, please apply sitable equation for this -->
            <?php if($id=="paid"){?>

                

            <!-- <h3>$<?php echo round($getData['price'] / 29.5, 2) ?><span>/month</span></h3>
            <br>
            <button type="button" class="btn btn-danger">Membership option <i class="fa fa-arrow-right"></i></button>
            <br><br>
            <span>Or just save to cart/buy this thutorial</span>
            <br><br>-->
            <h3 style="margin-top: 100px;margin-left:10px;">$<?php echo $getData['price'] ?></h3>
            <br> 
            <?php if ($getData['pay_button'] == true) { ?>

                <?php if (!empty(Cart::get('T-'.$getData['recipe_id']))): ?>
                   <button onclick="removeFromCart({{$getData['recipe_id']}},{{2}})" class="btn add-to-cart btn_width_50">ADDED TO CART <i class="fa fa-check"></i></button>
                   <a href="{{url('my-cart')}}"><button class="btn view-cart pull-right btn_width_50">View Cart</button></a>
                <?php else: ?>
                   <button onclick="addtocart({{$getData['recipe_id']}})" class="btn add-to-cart btn_width_50">ADD TO CART <i class="fa fa-cart-plus"></i></button>
                   <a href="{{url('onlineTutorials')}}"><button class="btn view-cart pull-right btn_width_50" style="margin-left:75px;">More tutorials</button></a>
                <?php endif ?>

              <!-- <button type="button" class="btn add-to-cart" id="addcard" onclick="addtocart(<?php echo $getData['recipe_id']?>)">Add to Cart <i class="fa fa-cart-plus"></i></button>
              <a href="{{url('onlineTutorials')}}"><button class="btn view-cart">View more items</button></a> -->

            <?php } ?>
            <?php if ($getData['pay_button'] == false) { ?>
                <button type="button" class="btn add-to-cart btn_width_50" id="favorite" >Favorite</button>
            <?php } ?>
            <?php if ($getData['login'] != true) { ?>
                <!-- <button class="btn add-to-cart" href="{{url('user/login')}}"><?= $getData['mor_button'] ?></button> -->
            <?php } }else{?>
                    <!-- <button type="button" class="btn add-to-cart" id="favorite" >Add to favorite</button> -->
                    <a href="{{url('onlineTutorials')}}"><button class="btn view-cart btn_width_50" style="margin-left:100px;">View more items</button></a>
                    
            <?php } ?>

        </div>


        <?php if ($getData['login'] == true && $getData['show_details'] == true) { ?>
            <div class="col-lg-12 col-md-12 col-sm-12 padbot30" style="padding-right: 0;">
                <div id="accordion">


                <?php if(($getData['lesson'])!="<br>"){ ?>
                    <div id="accordion">
                        <h4 class="accordion_title">LESSON DESCRIPTION</h4>
                        <div class="accordion_content">
                            <div class="container">
                                <div class="col-md-10">
                                    <p><?php echo $getData['lesson'] ?></p>

                                </div>
                            </div>
                        </div>
                    </div>

                <?php } ?>


                <?php if(!empty($slider[0]->filename)){ 
                
                ?>
                <div id="accordion">
                        <h4 class="accordion_title">PHOTO GALLERY</h4>
                        <div class="accordion_content">
                            <div class="container">
                                <div class="col-md-10">

                                    <div id="myCarousel" class="carousel slide">
                                        <ol class="carousel-indicators">
                                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                            <li data-target="#myCarousel" data-slide-to="1"></li>
                                            <li data-target="#myCarousel" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Carousel items -->
                                        <div class="carousel-inner">
                                            <?php foreach($slider as $item){  $i=0;   ?>
                                            <?php if($i <= 0){ ?>
                                            <div class="item active" id="1">
                                                <div class="row">
                                                    <div class="col-md-12"><a href="#"><img src="{{asset("core/storage/".$item->path.'/'.$item->filename ) }}" alt="Image" class="img-responsive"></a>
                                                    </div>

                                                </div>
                                                <!--/row-->
                                            </div>
                                            <?php $i++; }else{ ?>
                                             <div class="item" id="1">
                                                <div class="row">
                                                    <div class="col-md-12"><a href="#"><img src="{{asset("core/storage/".$item->path.'/'.$item->filename) }}" alt="Image" class="img-responsive"></a>
                                                    </div>

                                                </div>
                                                <!--/row-->
                                            </div>
                                            <?php } ?>
                                            <?php } ?>
                                            <!--/item-->

                                        </div>
                                        <!--/carousel-inner--> <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>

                                        <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
                                    </div>
                                    <!--/myCarousel-->

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(($getData['material'])!="<br>"){ ?>
                    <div id="accordion">
                        <h4 class="accordion_title">MATERIALS</h4>
                        <div class="accordion_content">
                            <p><?php echo $getData['material'] ?></p>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(($getData['ingrediant'])!="<br>"){ ?>
                    <div id="accordion">
                        <h4 class="accordion_title">INGREDIANTS</h4>
                        <div class="accordion_content">
                            <p><?php echo $getData['ingrediant'] ?></p>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(($getData['recipes'])!="<br>"){ ?>
                    <div id="accordion">
                        <h4 class="accordion_title">RECIPE</h4>
                        <div class="accordion_content">
                            <p><?php echo $getData['recipes'] ?></p>

                        </div>
                    </div>
                    <?php } ?>
                    <?php if(!empty($getData['pdf_file'])){ ?>
                    <div id="accordion">
                        <h4 class="accordion_title">TEMPLATE (PDF)</h4>
                        <div class="accordion_content">
                        <!-- <p><?php echo $getData['note'] ?></p> -->
                            <div id="tutorial_PDF" class="carousel slide" style="padding-left: 0;">
                                <!-- Indicators -->
                                <!-- <ol class="carousel-indicators">
                                  <li data-target="#tutorial_PDF" data-slide-to="0" class="active"></li>
                                  <li data-target="#tutorial_PDF" data-slide-to="1"></li>
                                  <li data-target="#tutorial_PDF" data-slide-to="2"></li>
                                </ol> -->
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <img src="{{asset('assets/front/images/pdf-bg.png')}}" class="img-responsive widthImg">
                                        <div class="container">
                                          	<div class="carousel-caption">
                                          		<h2 style="margin-bottom: 25px;">Get your pdf file from here</h2>
                                                <a class="btn btn-primary" href="{{asset("core/storage/".$getData['pdf_path']."/".$getData['pdf_file'])}}" target="_blank">Click to Download</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- Controls -->

                                <a class="left carousel-control" href="#tutorial_PDF" data-slide="prev">
                                    <span class="icon-prev"></span>
                                </a>
                                <a class="right carousel-control" href="#tutorial_PDF" data-slide="next">
                                    <span class="icon-next"></span>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
                <?php } ?>
                    <!-- <section id="review_count">

                          <div class="row">
                            <div class="col-md-4"><span id="rCount0">1400</span><span id="rCount1">views</span></div>
                            <div class="col-md-4"><img src="" alt="logo" /></div>
                            <div class="col-md-4"><span id="rCount2">99.9%</span><span id="rCount3">service rating</span></div>
                          </div>
                    </section> -->




            </div>


        <?php } ?>
            
    </div><!-- //CONTAINER -->
</section>

                <section id="related_tutorials" style="margin-top: 100px;">
                    <div id="grid-gallery" class="grid-gallery">
                    	<h1 class="text-center">Recommended tutorials</h1>
                        <section class="grid-wrap">
                            <ul class="grid">
                                <li class="grid-sizer"></li><!-- for Masonry column width -->
                                <?php foreach ($getsamedata as $data) { ?>
                                    <?php
                                    $geturl = "";
                                    $getPremium = "";
                                    if ($data->free_paid == 0) {
                                        $geturl = "free";
                                    } else {
                                        $geturl = "paid";
                                    }
                                    ?>
                                    <li>
                                        <figure>
                                            <img style="width: 200px;" class='img' src='{{asset("core/storage/".$data->cover_path."/".$data->cover_file)}}' alt=''/>
                                            <figcaption style="width: 200px;">
                                                <div class="col-md-12 text-center">
                                                    <a  href="{{url('viewTutorials'.'/'.$geturl.'/'.$data->recipe_id)}}" >  
                                                            <div class="row item-text-name-new">
                                                                {{ substr(strip_tags($data->name),0,50)}}
                                                            </div>
                                                            <div class="row item-text-price-new">
                                                
                                                                    <span>{{ $data->free_paid == 0 ? "FREE" : "US $".number_format($data->price) }}</span>
                                                                

                                                            </div>
                                                    </a>
                                                    </div>
                                            </figcaption>
                                        </figure>
                                    </li>
                                <?php } ?>
                            </ul>
                        </section><!-- // grid-wrap -->
                        <section class="slideshow">
                            <ul>
                                <li>
                                    <figure>
                                        <figcaption>
                                            <h3>Letterpress asymmetrical</h3>
                                            <p>Kale chips lomo biodiesel stumptown Godard Tumblr, mustache sriracha tattooed cray aute slow-carb placeat delectus. Letterpress asymmetrical fanny pack art party est pour-over skateboard anim quis, ullamco craft beer.</p>
                                        </figcaption>
                                        <img src="img/large/1.png" alt="img01"/>
                                    </figure>
                                </li>

                            </ul>
                        </section><!-- // slideshow -->
                    </div><!-- // grid-gallery -->

                    <script src="{{asset('assets/front/js/tutorials/imagesloaded.pkgd.min.js')}}"></script>
                    <script src="{{asset('assets/front/js/tutorials/masonry.pkgd.min.js')}}"></script>
                    <script src="{{asset('assets/front/js/tutorials/classie.js')}}"></script>
                    <script src="{{asset('assets/front/js/tutorials/cbpGridGallery.js')}}"></script>
                    <script>
    new CBPGridGallery(document.getElementById('grid-gallery'));
                    </script>

                </section>

@stop

@section('js')

<script src="{{asset('assets/front/js/video.js')}}"></script>
<script>
    function addtocart(product_id){
        var product_id= product_id;
        var qty= 1;

        $.ajax({
            method: "GET",
            url: '{{url('cart/add')}}',
            data:{ 'product_id' : product_id,'qty': qty,'product_type':2 }
        })
            .done(function( msg ) {
                console.log(msg);
                window.location.href='{{url('my-cart')}}';

            });
    }

</script>


<script type="text/javascript">
    $(document).ready(function() {
        $('#myCarousel').carousel({
            interval: false
        })
    });

    function removeFromCart(id, type) {
    $.ajax({method: "GET",
            url:'{{url('cart/remove')}}/'+id+'/'+type})
            .done(function(msg) {
            window.location.href='{{url('viewTutorials/paid/'.$getData['recipe_id'])}}';
            });
    }

</script>

@stop
