@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')
@section('css')

<style type="text/css">

    .keywords-blog {
        list-style:none;
        text-align: center;
        margin: 0 auto;
        padding: 0px;
        display:table;
        overflow: hidden;
    }
    .fa.fa-facebook{
        color: #FFFFF;

    }
    .fa.fa-youtube-square{
        color: #FFFFF;

    }
    .fa.fa-pinterest{
        color: #FFFFF;

    }
    .fa.fa-twitter{
        color: #FFFFF;

    }
    .instagram{
     overflow: hidden;
     text-align: center;
    }
    .instagram > span{
    position: relative;
    display: inline-block;
    font-weight: 540;
    font-size: 14px;
    }
    .instagram > span:before, .instagram > span:after{
    content: '';
    position: absolute;
    top: 50%;
    border-bottom: 2px solid;
    width: 591px; /* half of limiter*/
    margin: 0 20px;
    }
    .instagram > span:before{
    right: 100%;
    }
    .instagram > span:after{
    left: 100%;
    }

    .keywords-blog li{
        vertical-align: bottom;
        float: left;
        padding: 6px 0px 6px 0px;
        width: 100px;
        margin-left: 3px;
        margin-right: 3px;
        background-color:none;
        font-weight: 600;
        font-size: 1.25em;
        border-radius: 5px;
        border: thin solid #8b5730;
    }

    .keywords-blog li:hover{
        vertical-align: bottom;
        float: left;
        padding: 6px 0px 6px 0px;
        width: 100px;
        margin-top: 0px;
        margin-left: 3px;
        margin-right: 3px;
        background-color: #8b5730;
        font-size: 1.25em;
        border-radius: 5px;
        cursor: pointer;
        border: no;
        -webkit-transition: all 0.4s ease-in-out;
    }

    .keywords-blog li a {
        color : #8b5730;
        -webkit-transition: all 0.3s ease-in-out;
    }

    .keywords-blog li:hover a {
        color : #fff;
        -webkit-transition: all 0.3s ease-in-out;
    }

    .keywords-hover-new-shop li:hover{
        vertical-align: bottom;
        float: left;
        padding: 6px 0px 6px 0px;
        width: 100px;
        margin-top: 0px;
        margin-left: 3px;
        margin-right: 3px;
        background-color: #8b5730;
        color: #ffffff;
        font-size: 1.25em;
        border-radius: 5px;
        cursor: pointer;
        border: no;
        -webkit-transition: all 0.4s ease-in-out;
    }     

    .sidemenu {
        /*height:300px;*/
        padding: 5px 5px 15px 5px;
        border: 2px solid #e9e9e9;
        /*border-radius: 5px;*/
        margin-bottom: 5px;
        /*box-shadow: 0px -1px 5px grey;*/
    }

    .sidemenu-social{
        /*height:300px;*/
        padding: 5px 5px 15px 5px;
        /*border: 2px solid #e9e9e9;*/
        /*border-radius: 5px;*/
        margin-bottom: 5px;
        /*box-shadow: 0px -1px 5px grey;*/
    }

    .sidemenu-title{
        margin-left: 5%;
        margin-top: 5%;
        margin-bottom :inherit;
        padding: 10px;
        max-width: 92%;
        background-color: #fceeeb;
        text-align: center;
        font-weight: 600;
        font-size: 0.9em;
        border: 2px solid #8b5730;
        /*border-radius: 5px;*/
    }

    .blogs{
        position: relative;
        margin: 2px 10px 2px 10px;
        padding: 5px;
        border: thin solid #e9e9e9;
        /*box-shadow: 0px 2px 2px grey;*/
    }

    .tags{
        position: relative;
        margin: 2px 10px 2px 0px;
        padding: 5px;
        /*box-shadow: 0px 2px 2px grey;*/
    }

    .blogs:hover{
        /*box-shadow: 0px 2px 2px grey;*/
    }

    .blog-title{
        color:black;
        font-weight: 600;
        font-size: 0.9em;
        overflow:hidden;
    }

    .tagsurl:hover{
        cursor: pointer;
        text-decoration: underline;
    }

    .tagsurl{
        margin-left:5%;
    }

    .fa-tag {
        /*padding : 5px 5px;*/
        /*text-shadow: 0px 0px 1px #E5D4CD, 0px 0px 1px #E5D4CD,0px 0px 1px #E5D4CD,0px 0px 1px #E5D4CD,0px 0px 1px #E5D4CD;*/
        text-shadow: 0px 0px 1px #8b5730, 0px 0px 1px #8b5730,0px 0px 1px #8b5730,0px 0px 1px #8b5730,0px 0px 1px #8b5730;
        color: white;
        font-size: 2.1em !important;
    }

    .fa-tag:hover{
        color: #8b5730;
        cursor: pointer;
    }

    /*.pagination>.active>span:hover{
        color: #ccc !important;
        background-color: #333 !important;
        border: 2px solid #ccc !important;
        /*background-color: #ccc;*/
        /*color: #4d4b4c;*/
    /*}*/

    .pagination li a,.pagination>.active>a, .pagination>.active>span, .pagination>.active>a:hover, .pagination>.active>a:focus, .pagination>.active>span:focus{
        /*border-color: #8b5730!important;*/
        /*background-color: #E5D4CD !important;*/
        /*color: #8b5730 !important;*/
        background-color: #ccc;
        color: #4d4b4c;
    }

    .pagination li a{
        font-size:12px !important;
    }

    .pagination li.active a{
        background-color: #ccc;
        color: #4d4b4c;
    }

    .pagination li a:hover{
        color: #fff !important;
        border-color: #333;
        background-color: #333;
    }

    .post_meta_a_tag{
        bottom: 0;
        vertical-align: bottom;
        position: absolute;
    }

    .post_meta_a_tag:hover .button-new{
        padding: 5px 10px !important;
        border: 1px solid #633e23 !important;              
        min-height:0 !important;
    }

    .query{
       border:black solid 2px !important; 
    }
    .sidemenuInstagramImg{
         /*height:300px;*/
         padding: 2px 2px 2px 2px;
         border: 0px solid white;
         /*border-radius: 5px;*/
         margin-bottom: 3px;
         /*box-shadow: 0px -1px 5px grey;*/
     }
     .sidemenuInstagramImg .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
    }
    .sidemenuInstagramImg img {
        display: block;
        position: relative;
        -webkit-transition: all 0.4s ease-in;
        transition: all 0.4s ease-in;
    }
    .sidemenuInstagramImg:hover img {
        filter: url('data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feColorMatrix type="matrix" color-interpolation-filters="sRGB" values="0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0 0 0 1 0" /><feGaussianBlur stdDeviation="3" /></filter></svg>#filter');
        filter: grayscale(1) blur(3px);
        -webkit-filter: grayscale(1) blur(3px);
        -webkit-transform: scale(1.2);
        -ms-transform: scale(1.2);
        transform: scale(1.2);
    }
    .sidemenuInstagramImg i {
        text-transform: uppercase;
        text-align: center;
        position: relative;
        font-size: 17px;
        padding: 10px;
        background: transparent;
    }
    .sidemenuInstagramImg i {
        -webkit-transform: scale(0.7);
        -ms-transform: scale(0.7);
        transform: scale(0.7);
        -webkit-transition: all 0.4s ease-in;
        transition: all 0.4s ease-in;
        opacity: 0;
        filter: alpha(opacity=0);
        color: #fff;
        text-transform: uppercase;
    }
    .sidemenuInstagramImg:hover i {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: scale(1);
        -ms-transform: scale(1);
         transform: scale(1);
    }


    input[type="text"].query::placeholder{ /* Chrome/Opera/Safari */
        color:#E5D4CD !important;
    }

    input[type="text"].query::-webkit-input-placeholder{ /* Chrome/Opera/Safari */
        color:#E5D4CD !important;
    }

    input[type="text"].query::-moz-placeholder{ /* Firefox 19+ */
        color:#E5D4CD !important;
    }

    input[type="text"].query:-ms-input-placeholder{ /* IE 10+ */
        color:#E5D4CD !important;
    }

    input[type="text"].query:-moz-placeholder{ /* Firefox 18- */
        color:#E5D4CD !important;
    }  

    .insta-preview{
        margin-top: 10% !important;
        margin-bottom: 2%;
    }

    .social_div a{
        width: 30px;
        height: 30px !important;
    }

    .social_div a i{
        line-height: 30px !important;
        font-size: 18px !important;
    }
    
    /* Media Queries */
    @media only screen and (max-width:320px) and (min-width: 320px){

        .social_div a {
            width: 16%;
            height: 42px !important;
        }

        .social_div a i {
            line-height: 38px !important;
            font-size: 20px !important;
        }

        .social_div a:hover{
            width: 35px !important;
            height: 35px !important;
        }

        .social_div a:hover i{
            font-size: 18px !important;
            line-height: 32px !important;
        }

        .banner-bg{
            padding-top: 67px;
            padding-bottom: 0px;
        }

        .banner-bg h3{
            font-size: 100% !important;
            margin-top: -16% !important;            
        }

        .banner-bg h3::after{
            width: 50px !important;
            left: 58% !important;
            top: -23px !important;
            height: 2px !important;
        }

        .banner-bg p{
            font-size: 50% !important;
            margin-top: -48px;
        }

        .banner-bottom{
            margin-bottom: -10px !important;
        }

        .post .post_title{
            padding-left:5% !important;
            text-align:center !important;
        }

        .post .post_image{
            margin-left: 25% !important;
        }

        .post .post_content{
            font-size:14px;
            padding-left:6%;
            padding-right:6%;          
        }

        .valign-bot{
            right:30px !important;
            bottom:unset !important;
        }

        .blog_block hr{
            margin-top: 50px !important;
        }

        .pagination{          
            padding-left: 2px;
        }

        .blogs{
            text-align: center;
        }

        .col-sm-4.insta-preview{
            text-align:center;
        }      

        .social{
            text-align:center;
        }
    }

    @media only screen and (max-width: 360px) and (min-width: 321px){
        .social_div a{
            height: 47px !important;
        }

        .social_div a i{
            line-height: 42px !important;
            font-size: 25px !important;
        }

        .social_div a:hover{
            width: 40px !important;
            height: 40px !important;
        }

        .social_div a:hover i{
            font-size: 20px !important;
            line-height: 38px !important;
        }
        
        .banner-bg{
            padding-top: 75px;
            padding-bottom: 0px;
        }

        .banner-bg h3{
            font-size: 100% !important;
            margin-top: -15% !important;            
        }

        .banner-bg h3::after{
            width: 50px !important;
            left: 58% !important;
            top: -23px !important;
            height: 2px !important;
        }

        .banner-bg p{
            font-size: 50% !important;
            margin-top: -48px;
        }

        .banner-bottom{
            margin-bottom: -10px !important;
        }

        .keywords-blog{
            margin-left:6%;
        }

        .post .post_title{
            padding-left:1% !important;
            text-align:center !important;
        }

        .post .post_image{
            margin-left: 28% !important;
        }

        .post .post_content{
          font-size:14px;
          padding-left:6%;
          padding-right:6%;          
        }

        .valign-bot{
            right:30px !important;
            bottom:unset !important;
        }

        .blog_block hr{
            margin-top: 50px !important;
        }

        .pagination{          
          padding-left:4px;
        }

        .blogs{
            text-align: center;
        }

        .col-sm-4.insta-preview{
          text-align:center;
        }

        .social{
          text-align:center;
        }
    }

    @media only screen and (max-width: 412px) and (min-width: 361px){

        .social_div a{
            width: 16% !important;
            height: 54px !important;
        }

        .banner-bg{
            padding-top: 78px;
            padding-bottom: 0px;
        }

        .social_div a i{
            line-height: 48px !important;
            font-size: 28px !important;
        }

        .social_div a:hover{
            width: 42px !important;
            height: 42px !important;
        }

        .social_div a:hover i{
            font-size: 20px !important;
            line-height: 36px !important;
        }

        .banner-bg h3{
            font-size: 120% !important;
            margin-top: -16% !important;            
        }

        .banner-bg h3::after{
            width: 60px !important;
            left: 55% !important;
            top: -23px !important;
            height: 2px !important;
        }

        .banner-bg p{
            font-size: 50% !important;
            margin-top: -48px;
        }

        .banner-bottom{
            margin-bottom: -10px !important;
        }

        .keywords-blog{
            margin-left:11%;
        }

        .post .post_image{
            margin-left: 30% !important;
        }

        .post .post_title{            
            text-align:center !important;
        }

        .post .post_content{
          font-size:14px;
          padding-left:6%;
          padding-right:6%;          
        }

        .valign-bot{
            right:30px !important;
            bottom:unset !important;
        }

        .blog_block hr{
            margin-top: 50px !important;
        }

        .pagination{          
            padding-left: 4px;
        }

        .blogs{
            text-align: center;
        }

        .col-sm-4.insta-preview{
          text-align:center;
        }

        .social{
          text-align:center;
        }
    }

    @media only screen and (max-width:414px) and (min-width:413px){

        .social.social_div{
            text-align: center;
        }

        .social_div a{
            height: 54px !important;
        }

        .social_div a i{
            line-height: 48px !important;
            font-size: 28px !important;
        }

        .social_div a:hover{
            width: 42px !important;
            height: 42px !important;
        }

        .social_div a:hover i{
            font-size: 20px !important;
            line-height: 38px !important;
        }

        .banner-bg{
            padding-top: 3px;
            padding-bottom: 0px;
        }

        .banner-bg h3::after{
            width:70px !important;
            margin-left: -40px;
            margin-top:10px !important;
        }

        .banner-bg h3{
            margin-top: 20px !important;
            font-size: 120% !important;
        }

        .banner-bg p{
            font-size:90% !important;
            margin-top: -20px !important;
        }

        .banner-bottom{
            margin-top: -20px !important;
            margin-bottom: 0px !important;
        }

        .search-section{
            margin-left: 50px;
        }

        .post .post_image{
            margin-left: 33%;
        }

        .post .post_title{
            text-align:center;
        }

        .post .post_content{
            padding-left: 4%;
            padding-right: 4%;
        }

        .valign-bot{
            position:relative;
            margin-left: 240px !important;
        }

        .sidemenu{
            text-align:center;
        }

        .pagination{          
            padding-left: 2px;
        }
    }

    @media only screen and (max-width: 480px) and (min-width: 477px){

        .social_div a{
            height: 60px !important;
        }

        .social_div a i{
            line-height: 55px !important;
            font-size: 32px !important;
        }

        .social_div a:hover{
            width: 47px !important;
            height: 47px !important;
        }

        .social_div a:hover i{
            font-size: 25px !important;
            line-height: 42px !important;
        }

        .banner-bg{
            padding-top: 100px;
            padding-bottom: 0px;
        }

        .banner-bg h3{
            font-size: 120% !important;
            margin-top: -14% !important;            
        }

        .banner-bg h3::after{
            width: 60px !important;
            left: 55% !important;
            top: -23px !important;
            height: 2px !important;
        }

        .banner-bg p{
            font-size: 50% !important;
            margin-top: -48px;
        }

        .banner-bottom{
            margin-top:-50px !important;
        }

        .keywords-blog{
            padding-left:10%;
        }

        .keywords-blog li{
            margin-bottom: 5px;
            width: 120px;
        }

        .post .post_image{
            margin-left: 30% !important;
        }

        .post .post_title{            
            text-align:center !important;
        }

        .post .post_content{
          font-size:14px;
          padding-left:6%;
          padding-right:6%;          
        }

        .valign-bot{
            right:30px !important;
            bottom:unset !important;
        }

        .blog_block hr{
            margin-top: 50px !important;
        }

        /*.pagination{          
            padding-left: 70px;
        }*/

        .blogs{
            text-align: center;
        }

        .col-sm-4.insta-preview{
          text-align:center;
        }

        .social{
          text-align:center;
        }
    }

    @media only screen and (max-width: 600px) and (min-width: 481px){
        .social_div a{
            width: 14%;
            height: 62px !important;
        }

        .social_div a i{
            line-height: 60px !important;
            font-size: 32px !important;
        }

        .social_div a:hover{
            width: 50px !important;
            height: 50px !important;
        }

        .social_div a:hover i{
            font-size: 25px !important;
            line-height: 44px !important;
        }

        .banner-bg{
            padding-bottom:0px;
            padding-top:32px;
            padding-bottom: 13px;
        }

        .banner-bg h3{
            font-size: 180% !important;
        }

        .banner-bg h3::after{
            width: 70px !important;
            margin-top: 12px !important;
            margin-left: -35px !important;
        }

        .banner-bg p{
            font-size: 80% !important;
            margin-top: -20px;
        }

        .banner-bottom{
            margin-top:0px !important;
            margin-bottom:1px !important;;
        }

        .keywords-blog li{
            width: 180px;
            margin-left: 13px;
            margin-bottom: 5px;
        }

        .post .post_title{
            text-align: center;
        }

        .post .post_content{
            padding-left:3%;
            padding-right:3%;
        }

        .valign-bot{
            right:30px !important;
            bottom:unset !important;            
        }        

        .blog_block hr{
            margin-top: 50px !important;
        }

        /*.pagination{          
            padding-left: 100px;
        }*/

        .blogs{
            text-align: center;
        }

        .col-sm-4.insta-preview{
          text-align:center;
        }

        .social{
          text-align:center;
        }
    } 

    @media only screen and (max-width: 601px) and (min-width: 601px){
        .social_div a{
            height: 71px !important;
        }

        .social_div a i{
            line-height: 64px !important;
            font-size: 36px !important;
        }

        .social_div a:hover{
            width: 50px !important;
            height: 50px !important;
        }

        .social_div a:hover i{
            font-size: 25px !important;
            line-height: 46px !important;
        }

        .banner-bg{
            padding-bottom:0px;
            padding-top:4px;
            padding-bottom: 13px;
        }

        .banner-bg h3{
            font-size: 180% !important;
        }

        .banner-bottom{
            margin-top: -8px !important;
            margin-bottom:0 !important;
        }

        .keywords-blog li{
            width:180px !important;
            margin-bottom:5px !important;
        }

        .keywords-blog{
            padding-left: 20px;
        }

        .post .post_title{
            padding-left: 3%;
        }

        .post .post_content{
            padding-left: 3%;
            padding-right: 3%;
        }

        .valign-bot{
            position: relative;
            margin-left: 70%;
        }

        .blogs{
            text-align: center;
        }

        .col-sm-4.insta-preview{
          text-align:center;
        }

        .social{
          text-align:center;
        }

    }   

    @media only screen and (max-width: 768px) and (min-width: 766px){
        .insta-preview{
            width: 100% !important;
        }

        .insta-preview{
            margin-top: 25% !important;
            margin-bottom: 6%;
        }

        .social_div a{
            width: 34%;
            height: 32px !important;
        }

        .social_div a:hover{
            width: 28px !important;
            height: 28px !important;
        }

        .banner-bg{
            padding-bottom: 17px;
        }

        .banner-bg h3{
            margin-top: -45px !important;
            font-size: 300% !important;
        }

        .banner-bg p{
            margin-top: -20px !important;            
        }

        .banner-bottom{
            margin-top:0px !important;
            margin-bottom:1px !important;;
        }

        .search-section .container{
            padding-left:0;
            padding-right:0;
        }

        .blog-image{
            padding:0;
            margin-left: 25%;            
        }

        .blog-title-recent-posts{
            margin-left: 20%;
        }

        .valign-bot{
            position:relative !important;
            left:65% !important;
            margin-bottom: -40px;
        }

        .insta-preview {
            margin-top: 20% !important;
        }
        
    }

    @media only screen and (max-width:766px) and (min-width: 721px){
        .social.social_div{
            text-align: center;
        }

        .social_div a{
            width: 14%;
            height: 89px !important;
        }

        .social_div a i{
            line-height: 85px !important;
            font-size: 35px !important;
        }

        .social_div a:hover{
            width: 70px !important;
            height: 70px !important;
        }

        .social_div a:hover i{
            font-size: 30px !important;
            line-height: 62px !important;
        }
    }

    @media only screen and (max-width: 720px) and (min-width:707px){
        .social.social_div{
            text-align:center;
        }

        .social_div a{
            width: 12%;
            height: 75px !important;
        }

        .social_div a i{
            line-height: 70px !important;
            font-size: 35px !important;
        }

        .social_div a:hover{
            width: 60px !important;
            height: 60px !important;
        }

        .social_div a:hover i{
            font-size: 28px !important;
            line-height: 53px !important;
        }

        .banner-bg{
            padding-bottom: 0px;
        }

        .banner-bg h3{
            margin-top: -45px !important;
            font-size: 300% !important;
        }

        .post .post_title{
            padding-left: 2%;
        }

        .post .post_content{
            padding-left:3%;
            padding-right:3%;
        }

        .valign-bot{
            position:relative;
            right: -550px !important;
        }

        .sidemenu{
            text-align:center;
        }

        .pagination{
            padding-left:4px;
        }
    }

    @media only screen and (max-width: 800px) and (min-width:797px){
        .banner-bg{
            padding-bottom: 0px !important;
        }

        .keywords-blog{
            margin-left: -5% !important;
        }

        .valign-bot{
            margin-bottom: -40px;
        }

        .insta-preview {
            margin-top: 18% !important;
        }

        .social_div a{
            width: 25%;
            height: 26px !important;
        }

        .social_div a i{
            line-height: 23px !important;
            font-size: 14px !important;
        }

        .social_div a:hover{
            width: 22px !important;
            height: 22px !important;
        }

        .social_div a:hover i{
            font-size: 10px !important;
            line-height: 0px !important;
        }
    }

    @media only screen and (max-width: 992px) and (min-width: 769px){
        .social_div a{
            width:14%;
            height:21px !important;
        }

        .social_div a i{
            line-height: 0px !important;
            font-size: 11px !important;
        }

        .social_div a:hover{
            width: 19px !important;
            height: 19px !important;
        }

        .social_div a:hover i{
            font-size: 11px !important;
            line-height: 0px !important;
        }
        
        .banner-bg{
            padding-bottom:39px;
        }

        .banner-bg h3{
            margin-top: -4%;
        }

        .banner-bottom{
            margin-top:0px !important;
            margin-bottom:1px !important;;
        } 

        .valign-bot{
            position:relative !important;
            left: -35px !important;
            margin-bottom: -40px;            
        } 
        
        .insta-preview {
            margin-top: 18% !important;
        }
    }
    
</style>

@stop


@section('content')

    <section class="breadcrumb men parallax margbot30"></section><!-- //BREADCRUMBS -->

    <!-- PAGE HEADER -->
    <section class="page_header">
        <hr class="banner-top"/>
        <div class="banner-bg center">
            <h3>Blog</h3>
            <p>GET INFORMED ABOUT OUR LATEST NEWS AND EVENTS</p>
        </div>
        <hr class="banner-bottom">

        <!-- CONTAINER -->
        <div class="container">
            <!--<hr class="banner-top">
                <div class="banner-bg center">
                    <h3>Blog Page</h3>
                    <p>View our portfolio of Blogs!</p>
                </div>
                <hr class="banner-bottom">-->
        </div>
      <!-- //CONTAINER -->
    </section>
    <!-- //PAGE HEADER -->

    <!-- <section class="search-section" style="margin-bottom: 2%;">
        <div class="container">
            <div class="row" style="text-align: center">
                <div class="col-md-3">
                    <div class="widget_search">

                        <input type="text" class="query" name="search" value="{{app('request')->input('qu')}}" id="qu" placeholder="Search"/>

                    </div>
                </div>

                <div class="col-lg-12 col-md-12">
                    <ul class="keywords-blog">
                        <li><a href="{{ url('blog') }}">Filter - All</a></li>
                        @foreach ($keywords as $element)
                            <li><a href="{{ url('blog?search='.$element) }}">{{$element}}</a></li>
                        @endforeach
                    </ul> -->
                </div>
            </div>
        </div>
    </section>

    <!-- BLOG BLOCK -->
    <section class="blog">
        <!-- CONTAINER -->
        <div class="container">
            <!-- ROW -->
            <div class="row">
                <!-- BLOG LIST -->
                {{--<div class="col-lg-12 col-md-12 col-sm-12 padbot30 blog_block">--}}
                <div class="col-lg-9 col-md-9 col-sm-9 padbot30 blog_block">
                    @foreach($blogs as $blog)
                        <article class="post margbot40 clearfix" data-appear-top-offset='-100' data-animated='fadeInUp'>
                            <a class="post_image pull-left" href="{{url('blog/'.$blog->id)}}" >
                                <div class="recent_post_date">
                                    {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('d')}}
                                    <span>
                                        {{substr(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->format('F'),0,3)}}
                                    </span>
                                </div>
                                <img  src="{{asset('core/storage/uploads/images/blog'.'/'.$blog->front_image)}}" alt="" />
                            </a>
                            <a class="post_title" href="{{url('blog/'.$blog->id)}}" >{{$blog->name}}</a>
                            <div class="post_content text-justify">{{ substr(strip_tags($blog->description),0,1000)}}...
                            <div style="position: absolute; display: block; left: 43%; right: auto; bottom: 0;">
                                <a href="{{ url('blog/'.$blog->id) }}" class="post_meta_a_tag">
                                    <button class="btn button-new " > Read more <i class="fa fa-arrow-circle-right" style="font-size: 13px; color: #e9e9e9; " aria-hidden="true"></i></button>
                                </a>
                            </div>
                            </div>
                                
                        </article>
                        <hr>
                    @endforeach

                    <!--<div class="col-lg-12 col-md-12 col-sm-12 pagination" style="text-align: center">{!! $blogs->render() !!}</div>-->
                    <div class="col-lg-12 col-md-12 col-sm-12 pagination" style="text-align: center">@include('front.pagination', ['paginator' => $blogs])</div>

                </div>
                <!-- //BLOG LIST -->

                <section class="col-lg-3 col-md-3 col-sm-3">
                    <div class="container-fluid">
                            <div class="row">
                                <!-- WIDGET SEARCH -->
                                <div class="widget_search">
                                     <input type="text" style="border: 2px solid #4d4b4c; color: #FCEEEB;" class="query" name="search" value="{{app('request')->input('qu')}}" id="qu" placeholder="SEARCH"/>
                                </div>
                                <!-- //WIDGET SEARCH -->
                            </div>

                            <div class="row" style="box-shadow: 0 0 5px black;" >
                            <div class="col-sm-12  sidemenu recent_blogs">
                                <div class="sidemenu-title" style="border-width: thin; border: 2px solid #8b5730;"><div class="side" style="border-width: thin; border: 1px solid #8b5730;">RECENT POSTS</div></div>
                                @foreach($recent_blogs as $r)
                                    <a href="{{url('blog/'.$r->id)}}">
                                        <div class="col-sm-11 blogs" style="box-shadow: black;  margin-bottom: 10px;border-bottom: 2px solid #DCDCDC; border-top: 2px solid white; border-left: 2px solid white;border-right: 2px solid white;">
                                            <div class="col-sm-5">
                                                <img src="{{url('core/storage/uploads/images/blog/'.$r->front_image)}}" alt="" style="height: 40px;width: 40px;">
                                            </div>
                                            <div class="col-sm-7 blog-title" style="padding-left: 0;">
                                                {{$r->name}}<br>
                                                <div style="color: #e0e0e0">{{ date('F d, y',strtotime($r->start_date))}}</div>
                                            </div>
                                            
                                        </div>
                                    </a>

                                @endforeach
                            </div>
                        </div>

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 sidemenu categories" style="box-shadow: 0 0 5px black;">
                                    <div class="sidemenu-title"  style="border-width: thin; border: 2px solid #8b5730;"><div class="side" style="border-width: thin; border: 1px solid #8b5730;">CATEGORIES</div></div>
                                    @foreach($category_list as $key => $value)
                                        <a href="{{ url('productload/'.$value->id.'/all/1/min/hi') }}">
                                            <div class="col-lg-11 col-md-11 col-sm-11 blogs">
                                                <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0;">
                                                    {{$value->name}}<br>                                            
                                                </div>
                                            </div>                                        
                                        </a>                                   
                                    @endforeach
                                </div>
                            </div>

                            <div class="row" style="box-shadow: 0 0 5px black;" >
                            <div class=" col-sm-12 sidemenu featured_posts">
                                <div class="sidemenu-title" style="border-width: thin; border: 2px solid #8b5730;">
                                    <div class="side" style="border-width: thin; border: 1px solid #8b5730;">
                                    FEATURED POSTS
                                    </div>
                                </div>
                                @foreach($featured_blogs as $r)
                                    <a href="{{url('blog/'.$r->id)}}">
                                        <div class="col-sm-11 blogs">
                                            <div class="col-sm-5" >
                                                <img src="{{url('core/storage/uploads/images/blog/'.$r->front_image)}}" alt="" style="height: 40px;width: 40px;">
                                            </div>
                                            <div class="col-sm-7 blog-title" style="padding-left: 0;">
                                                {{$r->name}}<br>
                                                <div style="color: #e0e0e0">{{$r->start_date}}</div>
                                            </div>
                                        </div>
                                    </a>

                                @endforeach
                            </div>
                        </div>

                            <div class="row" style="box-shadow: 0 0 5px black;" >
                            <div class="instagram">
                                <span>
                                {{-- <div class="sidemenu-title"  style="border-width: thin; border: 2px solid #8b5730;"> --}}
                                    {{-- <div class="side" style="border-width: thin; border: 1px solid #8b5730;"> --}}
                                    INSTAGRAM
                                    </span>
                                    {{-- </div> --}}
                                {{-- </div> --}}
                                
                            </div >
                            <div id="instagram" class="instagramImg">

                            </div>
                        </div>
                            <div class="row" style="box-shadow: 0 0 5px black;" >
                            <div class="sidemenu">
                                <div class="sidemenu-title" style="border-width: thin; border: 2px solid #8b5730;">
                                    <div class="side" style="border-width: thin; border: 1px solid #8b5730;">
                                    TAGS
                                    </div>
                                </div>
                                <div class="tags">
                                    @foreach($tags as $tag)
                                        <a class="tagsurl" href="{{url('blog/searchByTag/'.$tag)}}">{{$tag}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div><br>

                            <div class="row">
                                <div class="">

                                    <div class="social-icon">

                                        <div class="social social_div center"> 

                                            <a href="{{ ConfigManage\Models\Config::find(1)->twitter }}" style="margin: 0 5px 10px 0 !important;" target="_blank"  data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>

                                            <a href="{{ ConfigManage\Models\Config::find(1)->fb }}" style="margin: 0 5px 10px 0 !important;" target="_blank" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>

                                            <a href="{{ ConfigManage\Models\Config::find(1)->youtube }}" style="margin: 0 5px 10px 0 !important;" target="_blank" data-placement="bottom" title="Youtube"><i class="fa fa-youtube-square"></i></a>

                                            <a href="{{ ConfigManage\Models\Config::find(1)->pinterest }}" style="margin: 0 5px 10px 0 !important;" target="_blank" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>

                                            <a href="{{ ConfigManage\Models\Config::find(1)->instagram }}" style="margin: 0 5px 10px 0 !important;" target="_blank" data-placement="bottom" title="Instagram"><i class="fa fa-instagram" style="color: #FFFFF;"></i></a>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
            <!-- //ROW -->
        </div>
        <!-- //CONTAINER -->
    </section>
    <!-- //BLOG BLOCK -->    
@stop

@section('js')

<script type="text/javascript">
    $(document).ready(function () {
        var blog_block = $('.blog_block');
        var resultObj =  '';
        $('.query').on('keyup',function(){
            if(this.value.length >= 2){
                $.ajax({
                    type: 'GET',
                    url: 'blog/search',
                    data: { qu: this.value },
                    resultObj: this.resultObj,
                    dataType:'JSON',
                    success: function (json) {
                        console.log("success");
                        let dataObj = json;
                        //console.log(dataObj);
                        blog_block.html('');

                        if (typeof dataObj.forEach == 'function') {
                            dataObj.forEach(function(element) {
                                var hitTemplate =
                                        '<article class="post margbot40 clearfix" data-appear-top-offset="-100" data-animated="fadeInUp"><a class="post_image pull-left" href="blog/'+element["id"]+'" ><div class="recent_post_date">'+element['date']+'<span>'+element["month"]+'</span></div>'+'<img  src="core/storage/uploads/images/blog/'+element["front_image"]+'" alt="" /></a><a class="post_title" href="blog/'+element["id"]+'" >'+element["name"]+'</a><div class="post_content text-justify">'+element["description"]+'...</div><ul class="post_meta valign-bot pull-left"><li><a href="blog/'+element["id"]+'"><button class="btn button-new " > Read more <i class="fa fa-arrow-circle-right" style="font-size: 13px;" aria-hidden="true"></i></button></a></li>             </ul></article><hr>';

                                // let output = $.mustache.render(hitTemplate, element);
                                $('.blog_block').append(hitTemplate);
                                $('.searching_ico').hide();
                            });
                        }

                        if(dataObj.length == 0){
                            $('.blog_block').html('<div class="alert alert-warning" role="alert">No Results!!!</div>');
                        }
                    }
                });
            }else{
                $.ajax({
                    type: 'GET',
                    url: 'blog/stdSearch',
                    data: { qu: this.value },
                    resultObj: this.resultObj,
                    dataType:'JSON',
                    success: function (json) {
                        console.log("success");
                        let dataObj = json;
                        //console.log(dataObj);
                        blog_block.html('');

                        if (typeof dataObj.forEach == 'function') {
                            dataObj.forEach(function(element) {

                                var hitTemplate =
                                        '<article class="post margbot40 clearfix" data-appear-top-offset="-100" data-animated="fadeInUp"><a class="post_image pull-left" href="blog/'+element["id"]+'" ><div class="recent_post_date">'+element['date']+'<span>'+element["month"]+'</span></div>'+'<img  src="core/storage/uploads/images/blog/'+element["front_image"]+'" alt="" /></a><a class="post_title" href="blog/'+element["id"]+'" >'+element["name"]+'</a><div class="post_content text-justify">'+element["description"]+'...</div><ul class="post_meta valign-bot pull-left"><li><a href="blog/'+element["id"]+'"><button class="btn button-new " > Read more <i class="fa fa-arrow-circle-right" style="font-size: 13px;" aria-hidden="true"></i></button></a></li>             </ul></article><hr>';

                                // let output = $.mustache.render(hitTemplate, element);
                                $('.blog_block').append(hitTemplate);
                                $('.searching_ico').hide();
                            });
                        }
                    }
                });
            }         
        });
    });

    $(document).ready(function (){
        var token = '1351653747.1677ed0.e686f3deeb4c49a5b8e1bcf90176d7c2',
            userid = 1351653747,
            num_photos = 9; // how much photos do you want to get

        $.ajax({
            url: 'https://api.instagram.com/v1/users/' + userid + '/media/recent',
            dataType: 'jsonp',
            type: 'GET',
            data: {access_token: token, count: num_photos},
            success: function(data){
                console.log(data);
                for( n in data.data ){

                    $('#instagram').append('<div class="col-sm-4  sidemenuInstagramImg" align="middle"><img src="'+data.data[n].images.standard_resolution.url+'"  style="height: 60px;width: 60px;"><div class="overlay"><i class="fa fa-instagram" style="font-size:36px;"></i></div></div>');

                }
            },
            error: function(data){
                console.log(data);
            }
        });
    });
</script>    

@stop
