@extends('layouts.front.master') @section('title','Gallery | www.princeofgalle.com')


@section('css')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/front/css/froala_editor.css')}}">
<link rel="stylesheet" href="{{asset('assets/front/css/froala_style.css')}}">
<link rel="stylesheet" href="{{asset('assets/front/css/plugins/code_view.css')}}">
<link rel="stylesheet" href="{{asset('assets/front/css/plugins/image_manager.css')}}">
<link rel="stylesheet" href="{{asset('assets/front/css/plugins/image.css')}}">
<link rel="stylesheet" href="{{asset('assets/front/css/plugins/table.css')}}">
<link rel="stylesheet" href="{{asset('assets/front/css/plugins/video.css')}}">
<link rel="stylesheet" href="{{asset('assets/back/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">


<style type="text/css">
label{
	text-transform: none!important;
	font-size: 13px!important;
}
span.color_red{
	font-family: 'Source Sans Pro', sans-serif!important;
	font-size: 18px!important;
	font-weight: normal;
}
.intl-tel-input{
	width: 100%;
	margin-top: 2px;
}

.bootstrap-datetimepicker-widget table td.active, .bootstrap-datetimepicker-widget table td.active:hover{
		background-color: #8A562F !important;
}

.bootstrap-datetimepicker-widget table td.today:before{
 border-bottom-color: #8A562F !important;
}
</style>

<style type="text/css">
.checkout_form{
	border: none;
}
.checkout_form .country .fancy-select .trigger{
	width: 550px;
}
textarea, input[type="text"], input[type="password"], input[type="date"], input[type="email"], input[type="number"]{
	width: 96%;
}
	/* .checkout_form_policy input[type="submit"]{
		border: thin solid gray;
		background-color: #8b5730;
		border-radius: 5px;
		} */
		input[type="submit"], input[type="reset"]{
    /*background-color: #fff!important;
    border: 3px solid #434343!important;
    color: #434343!important;
    padding-bottom: 7px;
    padding-top: 7px;*/
    border: 1px solid transparent;
    border-radius: 4px;
    font-size: 12px;
    font-weight: 600;
    padding: 6px 10px;
    margin: 3px 0 0 0;
    font-family: 'Roboto', sans-serif;
    text-transform: none;
    font-style: normal;
    line-height: 20px;
    transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;

}
p.scheduletop{
	color: #696969!important;
}
p.scheduletop a{
	text-decoration: none;
}
h3.scheduletop{
	color: #8a562f!important;
}
textarea#textarea1{
	color: #000000;
}
label.error{
	color: #E5D4CD !important;
}

.CP-list{
	margin-bottom: 30px;
}

.CP-list li{
	list-style: disc;
	font-size: 15px;
	margin-bottom: 6px;
}

</style>
<style>
div#editor {
	width: 100%;
	margin: auto;
	text-align: left;
}
.fr-toolbar{
	border-top: none;
}
.modal-header .close{
	margin-top: -20px;
}
.modal-header {
	min-height: 16.43px;
	padding: 25px;
	border-bottom: 1px solid #e5e5e5;
}
.modal-footer button[type="button"] {
	border: thin solid gray;
	background-color: #8b5730;
	border-radius: 5px;
}
h4.modal-title{
	color: #8a562f !important;
	font-size: 18px;
	padding-left: 0;
}
input.add-to-cart {
	background-color:#8b5730;
	color: #ffffff;
	border: 1px solid transparent;
    /* border-radius: 4px;
    font-size: 12px; */
    font-weight: 600;
    padding: 6px 10px;
    margin: 3px 0 0 0;
}
input.add-to-cart i {
	font-size: 15px;
}
input.add-to-cart:hover {
	background-color:#5d391e;
	color: #eeeeee;
}
.dial-code{
	display: none;
}
#successModal .modal-dialog {
	width: 40%;
	margin-top: 5%;
}
.hyperlink {
	color: #8A562F;
}
.hyperlink:hover {
	color: #E5D4CD;
}
</style>

@stop


@section('content')
<section class="breadcrumb men parallax margbot30">

</section><!-- //BREADCRUMBS -->


<!-- PAGE HEADER -->
<section class="page_header">

	<!-- CONTAINER -->
	<hr class="banner-top">
	<div class="banner-bg center">
		<h3>Schedule a Tasting</h3>
		<p>MEET WITH US TO DESIGN YOUR DREAM WEDDING CAKE!</p>
	</div>
	<hr class="banner-bottom">
</section><!-- //PAGE HEADER -->




<section class="checkout_page">

	<!-- CONTAINER -->
	<div class="container">
		<div class="alert alert-danger alert-dismissable" id="alart-warning" style="display: none">

			{{ $config->tasting_err }}
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul class="">
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<!-- CHECKOUT BLOCK -->
		<div class="checkout_block">

			<div class="row">
				<div class="col-md-12">
					{!! $config->tasting_success !!}
					<h3 class="scheduletop"><strong>CONGRATULATIONS ON YOUR SPECIAL DAY!</strong></h3>

					<p class="scheduletop">We will be honored to work with you to design your dream wedding cake and make your wedding day extra special!  Please fill out the form below and we will contact you to confirm your appointment.</p>

					<p class="scheduletop" style="color: #000 !important;"><strong>*If your wedding date is less than six months away, please <a href="{{url('generalcontact')}}" class="hyperlink">contact us</a> first to confirm that we’re available for that day.</strong></p>
				</div>
			</div>

			<form id="main-form" class="checkout_form clearfix" action="{{URL::to('tasting')}}" method="POST" style="border: none;">

				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


						<!-- <div class="checkout_form_input country">
							<label>Tasting People <span class="color_red">*</span></label>
							<select class="basic" id="people_count" name="people_count">
							<?php foreach ($tastingPeopleType as $key => $value): ?>
								<option value="{{$value->id}}">{{$value->name}}</option>
							<?php endforeach ?>
							</select>
						</div> -->

						<!-- <div class="checkout_form_input">
							<label>Event Date<span class="color_red">*</span></label>
							<input type="text" class="form-control date" id="datetime" onchange="checkRange()"  name="datetime" value="{{date('Y-m-d')}}" placeholder="" /> -->
							<!-- <input type="text" class="form-control date" id="dateP" name="date" value="{{date('Y-m-d')}}"
								placeholder="" onchange="dateChanege()"/> -->
								<!-- </div> -->

						<!-- <div class="checkout_form_input territory">
							<label>Province / Territory <span class="color_red">*</span></label>
							<input type="text" name="province" value="" placeholder="" />
						</div>

						<div class="checkout_form_input last postcode">
							<label>Postcode <span class="color_red">*</span></label>
							<input type="text" name="postcode"  value="" placeholder="" />
						</div> -->

						<div class="row">
							<div class="col-md-3">
								<div class="checkout_form_input first_name" style="width: 100%;">
									<label>First Name<span class="color_red">*</span></label>
									<input type="text" name="first_name"  value="{{ Sentinel::getUser() ? Sentinel::getUser()->first_name : ""}}" placeholder="" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="checkout_form_input last_name" style="width: 100%;">
									<label>Last Name<span class="color_red">*</span></label>
									<input type="text" name="last_name"  value="{{ Sentinel::getUser() ? Sentinel::getUser()->last_name : ""}}" placeholder="" />
								</div>
							</div>
							<div class="col-md-3">
								<div class="checkout_form_input phone" style="width: 100%;">
									<label>Phone </label>
									<!-- <input type="text" name="phone_no" value="{{ Sentinel::getUser() ? Sentinel::getUser()->mobile : ""}}" placeholder="" /> -->
									<input onchange="formatNumber()" type="text" name="phone_no" value="{{ Sentinel::getUser() ? Sentinel::getUser()->mobile : ""}}" id="mobile-number" placeholder="(201) 555-5555">
								</div>
								    <script> $(":input").inputmask(); $("#mobile-number").inputmask({"mask": "(999) 999-9999"});</script>
							</div>
							<div class="col-md-3">
								<div class="checkout_form_input last E-mail" style="width: 100%;">
									<label>Email<span class="color_red">*</span></label>
									<input type="text" name="email"  value="{{ Sentinel::getUser() ? Sentinel::getUser()->email : ""}}" placeholder="" />
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3">
								<div class="checkout_form_input2 servings" style="width: 100%;">
									<label>Number of Servings<span class="color_red">*</span></label>
									<input type="number" required="true" name="servings" placeholder=""/>
								</div>
							</div>
							<div class="col-md-3">
								<div class="checkout_form_input" style="width: 100%;">
									<label>Event Date<span class="color_red">*</span></label>
									<input type="text" class="date"  onchange="checkRange()"  name="datetime" value="{{date('Y-m-d')}}" placeholder="" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="checkout_form_input2 last adress" style="width: 102%;">
									<label>Venue<span class="color_red">*</span></label>
									<input type="text" required="true" name="venu" value="" placeholder="" />
								</div>
							</div>
						</div>

						

						<div class="row">
							<div class="col-md-6">
								<div class="checkout_form_input_details" style="padding-right: 10px;">
									<label>Details</label>
									<textarea rows="4" id="textarea1" name="details"></textarea>



								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<hr class="clear" style="border: none;">
								<div class="g-recaptcha checkout_form_recaptcha" data-sitekey="{{ env('CAPTCHA_KEY') }}"></div>


								{{--<div><input type="checkbox" name="agree" id="agree" value="agree">agree</div>--}}

								<hr class="clear" style="border: none;">
								<div class="row" style="margin-top: 30px;">
									<div class="col-md-5">
										<div class="form-check">
											<input type="checkbox" class="form-check-input" id="cancellationPloicy" name="cancellationPloicy">
											<label class="form-check-label" for="cancellationPloicy">I have read and agree to the <a class="cancellation-url hyperlink"  data-toggle="modal" data-target="#cancellationModel">cancellation policy*</a></label>
											<label id="cancel-policy-error" style="color: #f00 !important;" for="venu">This field is required.</label>
										</div>
									</div>
								</div>
								<div class="row" style="margin-top: 30px;">
									<div class="col-md-7">
										<div class="checkout_form_policy" style="float: left;">
											<input type="submit" class="btn register private pull-left" value="Submit" style="position: relative"/>
										</div>
									</div>
								</div>
							</div>
						</div>


						<hr class="clear" style="border: none;">
						
						<!-- Modal -->
						<div class="modal fade" id="cancellationModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="exampleModalLabel"><strong>Tasting Reschedule/Cancellation/Refund Policy</strong></h4>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
									{{--
									<p>Please read these Terms of Service ("Terms", "Terms of Service") carefully before using the http://www.mywebsite.com (change this) website and the My Mobile App (change this) mobile application (the "Service") operated by My Company (change this) ("us", "we", or "our").</p>
  
							<p>Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</p>

							<p>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>

							<h3>Termination</h3>

							<p>We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>

							<p>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>  
							--}}

							<ul class="CP-list">
								<li>A $40.00 credit card payment is required to secure the appointment.</li>
								{{-- <li>We bake our cakes fresh and our baking process starts within 48 hours of your appointment.</li> --}}
								<li>Due to high volume of inquiries, if you wish to cancel/reschedule a tasting, you must call us a week prior to your scheduled appointment day at 248‐787‐7766. This gives us the opportunity to offer that time slot to another customer. </li>
								<li> If we are not notified a week  prior to your appointment, your $40.00 tasting fee will not be refunded.</li>
								<li>If you’re unable to come at your scheduled time, we will allow you to reschedule it for another time the same day (if there’s availability) or another day. You’re allowed to reschedule the appointment up to ONE time. Failure to come on the rescheduled day/time will result in the forfeiture of your tasting fee and NO Refunds or Credits will be given.</li>
							</ul>

<!--							<p>
								If you would like to pay for any additional servings you have the option to do so during the payment process.
							</p>-->

						</div>
						<div class="modal-footer">
							<button type="button" class="btn active" style="position: relative;" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>

						<!-- Modal -->
						<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="exampleModalLabel"><strong>Congratulations on your Engagement and your upcoming Wedding!!</strong></h4>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
									<p>Thank you for contacting Sweet Delights Cakery to schedule your cake tasting and consultation! We will email you within 24 - 48 hours to schedule a day and time for you to meet with us. If you don't hear back from us within 48 hours please email us at info@sweetdelightscakery.com.</p>
  
							<p>Thank you!</p>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn active" style="position: relative;background-color:#8b5730" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			
	                    <!-- <div class="checkout_form_policy pull-right">
	                    	<h2>
	                            <input name="parent"  required type="checkbox" id="categorymanufacturer" class="a1 policy_checkbox">
	                            <label for="categorymanufacturer">User should accept to our <a href="#" class="cancellation-url">Cancellation policy</a></label>
	                        </h2>
	                        <h2>
	                            <label for="mandatory" class="pull-right">All fields marked with (<span class="color_red">*</span>) are required</label>
	                        </h2>
	                        <input type="submit" class="btn active pull-right" value="Continue"/>
	                    </div> -->

	                </form>
	                <div id="mydata" data-successModal="{{$modal}}"></div>
	            </div><!-- //CHECKOUT BLOCK -->
	        </div><!-- //CONTAINER -->
	    </section><!-- //CHECKOUT PAGE -->
	    @stop

	    @section('js')
	    <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>

	    <script src="{{asset('assets/back/vendor//summernote/dist/summernote.min.js')}}"></script>
	    <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>


	    <script src="{{asset('assets/back/vendor/moment/moment.js')}}"></script>

	    <script src="{{asset('assets/back/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>

	    <script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
	    <script type="text/javascript">
	    	tinymce.init({
      selector: 'textarea',  // change this value according to your HTML
      plugins: [
      'advlist autolink lists link image charmap preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code',
      'insertdatetime media nonbreaking table contextmenu directionality',
      'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
      ],
      toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
      '| bullist numlist outdent indent | link | undo redo | forecolor backcolor emoticons ',
      fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
      menubar: false
  });  
</script>


<script type="text/javascript">
	jQuery.validator.addMethod("dateCheck", function(value, element) {

		let allowedDate = moment( "{{ \Carbon\Carbon::now()->addDays($config->qoute_days ? $config->qoute_days : 1)->format('Y-m-d H:i A') }}")
		let date = moment(value);

		if (date < allowedDate ) {
			return false;
		}

		return true;

	}, "Date is not Available");

	$(document).ready(function() {
		$('.date').datetimepicker({
      //format: 'YYYY-MM-DD hh:mm:ss ',
      format: 'YYYY-MM-DD',
      widgetPositioning:{
      	horizontal: 'right',
      	vertical: 'top'
      } 
  })
		
		if (document.getElementById('mydata').getAttribute('data-successModal') == '1') {
			$('#successModal').modal('show');
		} else {
			
		}
		
	});

	function checkRange() {
		var selected_date=$('#datetime').val();
		var d1=new Date(selected_date);
		var now = new Date();
		var dif=d1-now;
		var days=dif / 1000 / 60 / 60 / 24;
		if(days>=0 && days>= {{ $config->tasting_days ? $config->tasting_days : 1  }}){
			$('#alart-warning').hide();
		}else{
			$('#alart-warning').show();
		}
	}

	$("#main-form").validate({
		rules: {
			people_count: {
				required: true
			},
			datetime: {
				required: true,
				date: true,
				dateCheck : true
			},
			province: {
				required: true,
			},
			postcode: {
				required: true
			},
			venue: {
				required: true
			},
			details: {
				required: true
			},
			first_name: {
				required: true
			},
			last_name: {
				required: true
			},
			email: {
				required: true
			},


		},
		submitHandler: function (form) {
			if( validatePolicy()){
				if (datevalidated) {
          // form.submit();
      }
  }
  return false;
}
});

	$('#cancel-policy-error').hide();

	function validatePolicy(){

		if($('#cancellationPloicy').is(':checked')){
			$('#cancel-policy-error').hide();
			return true;
		}
		$('#cancel-policy-error').show();
		return false;
	}
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>


<!-- Text Editor -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
<script type="text/javascript" src="{{asset('assets/front/js/froala_editor.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/align.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/code_beautifier.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/code_view.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/draggable.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/image.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/image_manager.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/link.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/lists.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/paragraph_format.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/paragraph_style.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/video.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/url.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/plugins/entities.min.js')}}"></script>

<script>
	$(function(){
		$('#edit').froalaEditor({
			toolbarButtons: ['bold', 'italic', 'underline', 'fontSize', '|', 'undo', 'redo']
		})
	});
</script>

<script>
	$(function(){
		$('#edit').froalaEditor({
			theme: 'royal'
		})
	});
</script>


<link rel="stylesheet" href="https://www.jquery-az.com/jquery/css/intlTelInput/intlTelInput.css">

<script src="https://www.jquery-az.com/jquery/js/intlTelInput/intlTelInput.js"></script>
<script>
	$("#mobile-number").intlTelInput({
		initialCountry: "us",
		nationalMode: true,
		// geoIpLookup: function(callback) {
		// 	$.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
		// 		var countryCode = (resp && resp.country) ? resp.country : "";
		// 		callback(countryCode);
		// 	});
		// },
		onlyCountries: ["us", "ca"],
          utilsScript: "../../build/js/utils.js" // just for formatting/placeholders etc
      });

	    function formatNumber(){
        var number =  $('#mobile-number').val();
        var classf = $(".selected-flag > div").attr("class");
        var flag = classf.slice(-2);
  
        console.log("check number: ", number);
        console.log("check classf: ", classf);
        console.log("check flag: ", flag);
        
        var formattedNumber = intlTelInputUtils.formatNumber(number, flag, intlTelInputUtils.numberFormat.INTERNATIONAL);
        $('#mobile-number').val(formattedNumber);

}
  </script>


  @stop
