<div class="modal fade" id="commonModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="commonModalTitle"></h4>
      </div>
      <div class="modal-body" id="commonModalBody"></div>
      <div class="modal-footer">
        <button type="button" class="btn button-new" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>