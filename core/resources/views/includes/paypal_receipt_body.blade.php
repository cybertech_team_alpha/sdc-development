
<div class="container">
    <div class="row">
        <div class="main">
            <div class="col-md-12">
               <div class="row">
                    <div class="col-md-3">
                        <img class="img-responsive" alt="Company Logo" src="{{asset('assets/front/images/logo.png')}}" />
                    </div>
                    <div class="col-md-9 text-right">
                      </div>
                    <div class="col-md-9 text-right">
                        <h5 style="color: #F81D2D; margin: 10px 0 0 0;"><strong>SWEET DELIGHTS CAKERY</strong></h5>
                        <p style="font-size: 14px; line-height: 18px;">
                            1755 Poppleton Dr<br/>
                            West Bloomfield Township<br/>
                            MI 48324<br/>
                            USA<br/>
                            (+897) 256 2564 25<br/>
                            info@sweetdelightscakery.com</p>
                    </div>

                    <div class="col-md-8 text-left">
                        <h5 style="color: #F81D2D; margin: 10px 0 0 0; text-transform: uppercase;"><strong>{{$purchase->first_name}} {{$purchase->last_name}}</strong>
                        </h5>
                        <p style="font-size: 14px; line-height: 18px;">{{$purchase->street_addresss_1}}<br/>
                            {{$purchase->province}} {{$purchase->postcode}}<br/>
                            {{$purchase->phone}}<br/>
                            {{$purchase->email}}
                        </p>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-sm-4">
                        <div class="panel panel-info ">
                          <div class="panel-heading"><strong><!-- Shipping Method --></strong></div>
                          <div style="font-size: 14px;" class="panel-body"><strong>Invoice: </strong>{{$purchase->inoice_no}}</div>
                        </div>
                    </div>
                 </div>
                <!-- <div class="row">
                    <div class="col-md-12 text-left">
                        <h3>Method</h3>
                        <h5>00000846382</h5>
                    </div>
                </div> -->
                <br />
                <div>
                    <table class="table">
                        <thead>
                            <tr style="vertical-align: middle;">
                                <th><h5 style="color: #ffffff; margin: 0 0 0 0;">Description</h5></th>
                                <th><h5 style="color: #ffffff; margin: 0 0 0 0;">Qty.</h5></th>
                                <th><h5 style="color: #ffffff; margin: 0 0 0 0;">Amount</h5></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($purchase->getItems as $key => $value): ?>
                            <tr>
                                <td style="font-size: 13px;" class="col-md-8">{{$value->name}}</td>
                                <td style="font-size: 13px;" class="col-md-2"> {{$value->qty}} </td>
                                <td style="font-size: 13px;" class="col-md-3">US <i class="fa fa-usd" aria-hidden="true"></i> {{number_format($value->unit_price,2)}}</td>
                            </tr>
                            <?php endforeach ?>
                            
                            
                            <tr>
                                
                                <td class="text-right" colspan="2">
                                <p style="font-size: 14px;">Sub Total: </p>
                                <p style="font-size: 14px;">Shipping Fees: </p>
                                <p style="font-size: 14px;">Coupon Value: </p>
							    <!-- <p>
                                    <strong>Payable Amount: </strong>
                                </p>
							    <p>
                                    <strong>Balance Due: </strong>
                                </p> -->
							    </td>
                                <td>
                                <p style="font-size: 14px;">US <i class="fa fa-usd" aria-hidden="true"></i> {{number_format(($purchase->amount+$purchase->coupon_value)-$purchase->shipping_amount,2)}}</p>

                                <p style="font-size: 14px;">US <i class="fa fa-usd" aria-hidden="true"></i> {{$purchase->shipping_amount}}</p>
                                
                                <p style="font-size: 14px;">US <i class="fa fa-usd" aria-hidden="true"></i>-{{$purchase->coupon_value}}</p>
							    <!-- <p>
                                    <strong><i class="fa fa-usd" aria-hidden="true"></i> 159,00 </strong>
                                </p>
							    <p>
                                    <strong><i class="fa fa-usd" aria-hidden="true"></i> 5010,00 </strong>
                                </p> -->
							    </td>
                            </tr>
                            <tr style="color: #F81D2D;">
                                <td></td>
                                <td class="text-right"><h5 style="font-size: 17px;"><strong>Total:</strong></h5></td>
                                <td class="text-left"><h5 style="font-size: 17px;"><strong>US <i class="fa fa-usd" aria-hidden="true"></i> {{$purchase->amount}} </strong></h5></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <div class="col-md-12">
                        <p><b>Date :</b> <span >{{$purchase->created_at}}</span></p>
                        <br />
                        <p><img class="img-responsive" alt="Company Signature" src="{{asset('assets/front/images/signature.png')}}" /><br/><b>Signature</b></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
function formatDate(date) {
      var hours = date.getHours();
      var minutes = date.getMinutes();
      // var ampm = hours >= 12 ? 'PM' : 'AM';
      // hours = hours % 12;
      // hours = hours ? hours : 12; // the hour '0' should be '12'
      // minutes = minutes < 10 ? '0'+minutes : minutes;
      // var strTime = hours + ':' + minutes + ' ' + ampm;
      return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear();
}

var d = new Date();
var e = formatDate(d);
document.getElementById("date").innerHTML = e;


</script>