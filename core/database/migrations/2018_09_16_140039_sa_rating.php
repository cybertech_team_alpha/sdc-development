<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SaRating extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('sa_rating', function ($table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('name', 200);
            $table->string('email', 200);
            $table->string('contact_no', 200);
            $table->string('review', 200);
            $table->integer('star_count');
            $table->string('created_by', 200);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
            
            
            $table->engine = 'InnoDB';
            $table->unique('id');
	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
