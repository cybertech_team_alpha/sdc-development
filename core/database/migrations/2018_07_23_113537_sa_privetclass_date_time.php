<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SaPrivetclassDateTime extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sa_privetclass_date_time', function ($table) {
            $table->increments('id');
            $table->integer('privateclass_cat_id');
            $table->string('date', 200);
            $table->string('start', 200);
            $table->string('end', 200);
            $table->timestamps();
            
            $table->engine = 'InnoDB';
            $table->unique('id');
	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('sa_privetclass_date_time');
    }

}
