<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Recipe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('recipe', function($table)
      {
        $table->increments('recipe_id');
        $table->string('name');
        $table->text('cover_path')->nullable();
        $table->text('cover_file')->nullable();
        $table->text('intro')->nullable();
        $table->text('main')->nullable();
        $table->text('lesson')->nullable();
        $table->text('material')->nullable();
        $table->text('ingrediant')->nullable();
        $table->text('recipes')->nullable();
        $table->text('pdf_path')->nullable();
        $table->text('pdf_file')->nullable();
        $table->text('note')->nullable();
        $table->integer('category')->nullable();
        $table->string('skill_level')->nullable();
        $table->string('cake_type')->nullable();
        $table->float('price')->nullable();
        $table->integer('free_paid')->nullable();
        $table->integer('premium')->nullable();
        $table->integer('status')->nullable();
        $table->timestamp('created_at')->nullable();
        $table->timestamp('updated_at')->nullable();

        // We'll need to ensure that MySQL uses the InnoDB engine to
        // support the indexes, other engines aren't affected.
        $table->engine = 'InnoDB';
        $table->unique('name');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recipe');
    }
}
