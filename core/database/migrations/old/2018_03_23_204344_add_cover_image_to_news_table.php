<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoverImageToNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sa_news', function (Blueprint $table) {
              $table->text('cover_image');
              $table->text('front_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sa_news', function (Blueprint $table) {
          $table->dropColumn('cover_image');
          $table->dropColumn('front_image');
        });
    }
}
