<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPromotionTextLinkToConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('config', function($table) {
           $table->text('promotion_text');
           $table->text('promotion_link')->nullable();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('config', function($table) {
          $table->dropColumn('promotion_text');
          $table->dropColumn('promotion_link');
      });
    }
}
