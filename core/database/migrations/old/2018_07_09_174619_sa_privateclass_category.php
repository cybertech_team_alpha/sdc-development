<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SaPrivateclassCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sa_privateclass_category', function($table)
      {

                $table->increments('recipe_id');
                $table->string('name');
                $table->text('description')->nullable();
                $table->timestamp('datetime')->nullable();
                $table->integer('noofseat')->nullable();
                $table->double('price')->nullable();
                $table->text('path')->nullable();
                $table->text('filename')->nullable();
                $table->string('skill_level')->nullable();
                $table->integer('created_by')->nullable();
                $table->created_at('datetime')->nullable();
                $table->updated_at('datetime')->nullable();
                $table->deleted_at('datetime')->nullable();
                $table->integer('status')->nullable();
                $table->engine = 'InnoDB';
                $table->unique('name');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sa_privateclass_category');
    }
}
