<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CR05AddDropColumnsSlidesTable extends Migration
{
     public function up()
    {
        Schema::table('slides', function (Blueprint $table) 
        {
            $table->dropColumn('btn_txt');
            $table->dropColumn('btn_txt_color');
            $table->dropColumn('btn_clr');
            $table->string('btn_image');
            $table->integer('btn_animation_type');
            $table->string('message_type');
            $table->string('message_first_line_img')->nullable();
            $table->string('message_second_line_img')->nullable();
            $table->string('message_third_line_img')->nullable();
            $table->string('message_fourth_line_img')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('slides', function (Blueprint $table) {
          $table->string('btn_txt');
            $table->string('btn_txt_color');
            $table->string('btn_clr');
            $table->dropColumn('btn_image');
            $table->dropColumn('btn_animation_type');
             $table->dropColumn('message_type');
            $table->dropColumn('message_first_line_img');
            $table->dropColumn('message_second_line_img');
            $table->dropColumn('message_third_line_img');
            $table->dropColumn('message_fourth_line_img');
        });
    }
}
